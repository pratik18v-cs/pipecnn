# README #

This is a README file with instructions to compile and run the PipeCNN code on our DE-10 Standard FPGA board. For the rest of the document, follow these keywords:

**Host**: srv365-13

**Board**: DE-10 Standard

Before you start, download the test vector and golden reference files from PipeCNN's own [ModelZoo](https://drive.google.com/open?id=0B3srpZY5rHcASEhSSmh3Tm5LclU). Put all the folders in the google drive, in the ./data folder. Make sure this data folder is also present the same directory as the object files while running them on the board.

### Host ###

* Log-In to dtyu account on the host machine.
* Enter into admin session

		>>> sudo -s

* Check if OpenCL environment is initialized by:


		>>> aocl version

		aocl 16.1.0.196 (Intel(R) FPGA Runtime Environment for OpenCL(TM), Version 16.1.0 Build 196, Copyright (C) 2016 Intel Corporation)

		>>> aoc --list-boards
		Board list:
		  de10_standard_sharedonly_vga


* Compile project


		>>> cd /home/pratik/Documents/PipeCNN-ocv/

		#It is already compiled, to compile afresh first delete the already compiled files as follows:
		>>> cd project/device/RTL
		>>> make clean
		>>> cd ../../
		>>> make clean

		#Compile the project as new
		>>> cd device/RTL
		>>> make
		>>> cd ../../
		>>> make host #To compile host code
		>>> make fpga #To compile kernel

		#Host code compilation may fail if the default cross compiler is not version 4.9. To check and change, run the following commands:
		>>> arm-linux-gnueabihf-g++ --version

		arm-linux-gnueabihf-g++ (Ubuntu/Linaro 4.9.3-13ubuntu2) 4.9.3

		>>> arm-linux-gnueabihf-gcc --version

		arm-linux-gnueabihf-gcc (Ubuntu/Linaro 4.9.3-13ubuntu2) 4.9.3

		#If not the same version, change as follows
		>>> update-alternatives --config arm-linux-gnueabihf-g++

		Selection	Path					          Prority		Status
		------------------------------------------------------------------------------------
		0		/usr/bin/arm-linux-gnueabihf-g++-5	  20			auto mode
		1		/usr/bin/arm-linux-gnueabihf-g++-4.9  10			manual mode
		2		/usr/bin/arm-linux-gnueabihf-g++-5	  20			manual mode

		Press <enter> to keep the current choice[*], or type selection number:
		#Here, type the selection number corresponding to version 4.9 and press enter. Repeat this command for arm-linux-gnueabihf-gcc. Confirm version change.


### Board ###
The board OS image is saved in the Downloads directory of the host machine (filename: c5soc_opencl_lxde_all_in_one_180317.img). You need to follow the instructions [here](https://github.com/thinkoco/c5soc_opencl#run-opencl-application) (especially step 2) to run an OpenCL application on this image.

* Mount usb-drive (sda1) to folder */media/usb-drive*:
		
		>>> mount /dev/sda1 /media/usb-drive/
		

* Initialize OpenCL environment


		>>> source ./init_opencl_16.1.sh
		>>> aocl version #Should be the same as the host machine

		aocl 16.1.0.196 (Intel(R) FPGA Runtime Environment for OpenCL(TM), Version 16.1.0 Build 196,   Copyright (C) 2016 Intel Corporation)


* Run the project


		>>> cd pipecnn
		>>> aocl program /dev/acl0 conv.aocx

		aocl program: Running reprogram from /root/aocl-rte-16.1.0-1.arm32/board/c5soc/arm32/bin
		Reprogramming was successful!

		>>> ./run.exe conv.aocx


