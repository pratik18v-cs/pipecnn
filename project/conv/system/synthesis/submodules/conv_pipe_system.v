// (C) 1992-2016 Intel Corporation.                            
// Intel, the Intel logo, Intel, MegaCore, NIOS II, Quartus and TalkBack words    
// and logos are trademarks of Intel Corporation or its subsidiaries in the U.S.  
// and/or other countries. Other marks and brands may be claimed as the property  
// of others. See Trademarks on intel.com for full list of Intel trademarks or    
// the Trademarks & Brands Names Database (if Intel) or See www.Intel.com/legal (if Altera) 
// Your use of Intel Corporation's design tools, logic functions and other        
// software and tools, and its AMPP partner logic functions, and any output       
// files any of the foregoing (including device programming or simulation         
// files), and any associated documentation or information are expressly subject  
// to the terms and conditions of the Altera Program License Subscription         
// Agreement, Intel MegaCore Function License Agreement, or other applicable      
// license agreement, including, without limitation, that your use is for the     
// sole purpose of programming logic devices manufactured by Intel and sold by    
// Intel or its authorized distributors.  Please refer to the applicable          
// agreement for further details.                                                 
    


/////////////////////////////////////////////////////////////////
// MODULE conv_pipe_system
/////////////////////////////////////////////////////////////////
module conv_pipe_system
(
   input logic clock,
   input logic clock2x,
   input logic resetn,
   // AVS avs_coreConv_cra
   input logic avs_coreConv_cra_enable,
   input logic avs_coreConv_cra_read,
   input logic avs_coreConv_cra_write,
   input logic [3:0] avs_coreConv_cra_address,
   input logic [63:0] avs_coreConv_cra_writedata,
   input logic [7:0] avs_coreConv_cra_byteenable,
   output logic [63:0] avs_coreConv_cra_readdata,
   output logic avs_coreConv_cra_readdatavalid,
   // AVS avs_lrn_cra
   input logic avs_lrn_cra_enable,
   input logic avs_lrn_cra_read,
   input logic avs_lrn_cra_write,
   input logic [3:0] avs_lrn_cra_address,
   input logic [63:0] avs_lrn_cra_writedata,
   input logic [7:0] avs_lrn_cra_byteenable,
   output logic [63:0] avs_lrn_cra_readdata,
   output logic avs_lrn_cra_readdatavalid,
   // AVS avs_maxPool_cra
   input logic avs_maxPool_cra_enable,
   input logic avs_maxPool_cra_read,
   input logic avs_maxPool_cra_write,
   input logic [3:0] avs_maxPool_cra_address,
   input logic [63:0] avs_maxPool_cra_writedata,
   input logic [7:0] avs_maxPool_cra_byteenable,
   output logic [63:0] avs_maxPool_cra_readdata,
   output logic avs_maxPool_cra_readdatavalid,
   // AVS avs_memRead_cra
   input logic avs_memRead_cra_enable,
   input logic avs_memRead_cra_read,
   input logic avs_memRead_cra_write,
   input logic [4:0] avs_memRead_cra_address,
   input logic [63:0] avs_memRead_cra_writedata,
   input logic [7:0] avs_memRead_cra_byteenable,
   output logic [63:0] avs_memRead_cra_readdata,
   output logic avs_memRead_cra_readdatavalid,
   // AVS avs_memWrite_cra
   input logic avs_memWrite_cra_enable,
   input logic avs_memWrite_cra_read,
   input logic avs_memWrite_cra_write,
   input logic [3:0] avs_memWrite_cra_address,
   input logic [63:0] avs_memWrite_cra_writedata,
   input logic [7:0] avs_memWrite_cra_byteenable,
   output logic [63:0] avs_memWrite_cra_readdata,
   output logic avs_memWrite_cra_readdatavalid,
   output logic kernel_irq,
   // AVM avm_memgmem0_port_0_0_rw
   output logic avm_memgmem0_port_0_0_rw_enable,
   output logic avm_memgmem0_port_0_0_rw_read,
   output logic avm_memgmem0_port_0_0_rw_write,
   output logic [4:0] avm_memgmem0_port_0_0_rw_burstcount,
   output logic [29:0] avm_memgmem0_port_0_0_rw_address,
   output logic [255:0] avm_memgmem0_port_0_0_rw_writedata,
   output logic [31:0] avm_memgmem0_port_0_0_rw_byteenable,
   input logic avm_memgmem0_port_0_0_rw_waitrequest,
   input logic [255:0] avm_memgmem0_port_0_0_rw_readdata,
   input logic avm_memgmem0_port_0_0_rw_readdatavalid,
   input logic avm_memgmem0_port_0_0_rw_writeack
);
   logic coreConv_start;
   logic [0:0] coreConv_start_chain;
   logic [0:0] coreConv_start_kernel_copy;
   logic [0:0] coreConv_start_task_fd;
   logic [0:0] coreConv_start_finish_element;
   logic coreConv_finish;
   logic [0:0] coreConv_finish_kernel_copy;
   logic [0:0] coreConv_finish_chain;
   logic [31:0] coreConv_global_size [2:0];
   logic [31:0] coreConv_num_groups [2:0];
   logic [31:0] coreConv_local_size [2:0];
   logic [31:0] coreConv_global_offset [2:0];
   logic [31:0] coreConv_work_dim;
   logic [31:0] coreConv_wg_size;
   logic [0:0] coreConv_wg_disp_stall_in;
   logic [0:0] coreConv_wg_disp_valid_out;
   logic coreConv_wg_disp_start_out;
   logic [31:0] coreConv_wg_disp_group_id_out [2:0];
   logic [31:0] coreConv_wg_disp_global_id_base_out [2:0];
   logic coreConv_wg_disp_dispatched_all_groups;
   logic [31:0] coreConv_global_id [1][2:0];
   logic [31:0] coreConv_local_id [1][2:0];
   logic [31:0] coreConv_group_id [1][2:0];
   logic [0:0] coreConv_pending_write;
   logic [0:0] coreConv_lsu_active;
   logic [0:0] coreConv_valid_in;
   logic [0:0] coreConv_valid_out;
   logic [0:0] coreConv_stall_in;
   logic [0:0] coreConv_stall_out;
   logic coreConv_cra_pending_write;
   logic coreConv_cra_lsu_active;
   logic coreConv_cra_valid_in;
   logic [119:0] coreConv_kernel_arguments;
   logic lrn_start;
   logic [0:0] lrn_start_chain;
   logic [0:0] lrn_start_kernel_copy;
   logic [0:0] lrn_start_task_fd;
   logic [0:0] lrn_start_finish_element;
   logic lrn_finish;
   logic [0:0] lrn_finish_kernel_copy;
   logic [0:0] lrn_finish_chain;
   logic [31:0] lrn_global_size [2:0];
   logic [31:0] lrn_num_groups [2:0];
   logic [31:0] lrn_local_size [2:0];
   logic [31:0] lrn_global_offset [2:0];
   logic [31:0] lrn_work_dim;
   logic [31:0] lrn_wg_size;
   logic [0:0] lrn_wg_disp_stall_in;
   logic [0:0] lrn_wg_disp_valid_out;
   logic lrn_wg_disp_start_out;
   logic [31:0] lrn_wg_disp_group_id_out [2:0];
   logic [31:0] lrn_wg_disp_global_id_base_out [2:0];
   logic lrn_wg_disp_dispatched_all_groups;
   logic [31:0] lrn_global_id [1][2:0];
   logic [31:0] lrn_local_id [1][2:0];
   logic [31:0] lrn_group_id [1][2:0];
   logic [0:0] lrn_pending_write;
   logic [0:0] lrn_lsu_active;
   logic [0:0] lrn_valid_in;
   logic [0:0] lrn_valid_out;
   logic [0:0] lrn_stall_in;
   logic [0:0] lrn_stall_out;
   logic lrn_cra_pending_write;
   logic lrn_cra_lsu_active;
   logic lrn_cra_valid_in;
   logic [151:0] lrn_kernel_arguments;
   logic maxPool_start;
   logic [0:0] maxPool_start_chain;
   logic [0:0] maxPool_start_kernel_copy;
   logic [0:0] maxPool_start_task_fd;
   logic [0:0] maxPool_start_finish_element;
   logic maxPool_finish;
   logic [0:0] maxPool_finish_kernel_copy;
   logic [0:0] maxPool_finish_chain;
   logic [31:0] maxPool_global_size [2:0];
   logic [31:0] maxPool_num_groups [2:0];
   logic [31:0] maxPool_local_size [2:0];
   logic [31:0] maxPool_global_offset [2:0];
   logic [31:0] maxPool_work_dim;
   logic [31:0] maxPool_wg_size;
   logic [0:0] maxPool_wg_disp_stall_in;
   logic [0:0] maxPool_wg_disp_valid_out;
   logic maxPool_wg_disp_start_out;
   logic [31:0] maxPool_wg_disp_group_id_out [2:0];
   logic [31:0] maxPool_wg_disp_global_id_base_out [2:0];
   logic maxPool_wg_disp_dispatched_all_groups;
   logic [31:0] maxPool_global_id [1][2:0];
   logic [31:0] maxPool_local_id [1][2:0];
   logic [31:0] maxPool_group_id [1][2:0];
   logic [0:0] maxPool_pending_write;
   logic [0:0] maxPool_lsu_active;
   logic [0:0] maxPool_valid_in;
   logic [0:0] maxPool_valid_out;
   logic [0:0] maxPool_stall_in;
   logic [0:0] maxPool_stall_out;
   logic maxPool_cra_pending_write;
   logic maxPool_cra_lsu_active;
   logic maxPool_cra_valid_in;
   logic [55:0] maxPool_kernel_arguments;
   logic memRead_start;
   logic [0:0] memRead_start_chain;
   logic [0:0] memRead_start_kernel_copy;
   logic [0:0] memRead_start_task_fd;
   logic [0:0] memRead_start_finish_element;
   logic memRead_finish;
   logic [0:0] memRead_finish_kernel_copy;
   logic [0:0] memRead_finish_chain;
   logic [31:0] memRead_global_size [2:0];
   logic [31:0] memRead_num_groups [2:0];
   logic [31:0] memRead_local_size [2:0];
   logic [31:0] memRead_global_offset [2:0];
   logic [31:0] memRead_work_dim;
   logic [31:0] memRead_wg_size;
   logic [0:0] memRead_wg_disp_stall_in;
   logic [0:0] memRead_wg_disp_valid_out;
   logic memRead_wg_disp_start_out;
   logic [31:0] memRead_wg_disp_group_id_out [2:0];
   logic [31:0] memRead_wg_disp_global_id_base_out [2:0];
   logic memRead_wg_disp_dispatched_all_groups;
   logic [31:0] memRead_global_id [1][2:0];
   logic [31:0] memRead_local_id [1][2:0];
   logic [31:0] memRead_group_id [1][2:0];
   logic [0:0] memRead_pending_write;
   logic [0:0] memRead_lsu_active;
   logic [0:0] memRead_valid_in;
   logic [0:0] memRead_valid_out;
   logic [0:0] memRead_stall_in;
   logic [0:0] memRead_stall_out;
   logic memRead_cra_pending_write;
   logic memRead_cra_lsu_active;
   logic memRead_cra_valid_in;
   logic [447:0] memRead_kernel_arguments;
   logic memWrite_start;
   logic [0:0] memWrite_start_chain;
   logic [0:0] memWrite_start_kernel_copy;
   logic [0:0] memWrite_start_task_fd;
   logic [0:0] memWrite_start_finish_element;
   logic memWrite_finish;
   logic [0:0] memWrite_finish_kernel_copy;
   logic [0:0] memWrite_finish_chain;
   logic [31:0] memWrite_global_size [2:0];
   logic [31:0] memWrite_num_groups [2:0];
   logic [31:0] memWrite_local_size [2:0];
   logic [31:0] memWrite_global_offset [2:0];
   logic [31:0] memWrite_work_dim;
   logic [31:0] memWrite_wg_size;
   logic [0:0] memWrite_wg_disp_stall_in;
   logic [0:0] memWrite_wg_disp_valid_out;
   logic memWrite_wg_disp_start_out;
   logic [31:0] memWrite_wg_disp_group_id_out [2:0];
   logic [31:0] memWrite_wg_disp_global_id_base_out [2:0];
   logic memWrite_wg_disp_dispatched_all_groups;
   logic [31:0] memWrite_global_id [1][2:0];
   logic [31:0] memWrite_local_id [1][2:0];
   logic [31:0] memWrite_group_id [1][2:0];
   logic [0:0] memWrite_pending_write;
   logic [0:0] memWrite_lsu_active;
   logic [0:0] memWrite_valid_in;
   logic [0:0] memWrite_valid_out;
   logic [0:0] memWrite_stall_in;
   logic [0:0] memWrite_stall_out;
   logic memWrite_cra_pending_write;
   logic memWrite_cra_lsu_active;
   logic memWrite_cra_valid_in;
   logic [175:0] memWrite_kernel_arguments;
   logic [4:0] kernel_irqs;
   logic avm_kernel_rd_enable [5];
   logic avm_kernel_rd_read [5];
   logic avm_kernel_rd_write [5];
   logic [4:0] avm_kernel_rd_burstcount [5];
   logic [29:0] avm_kernel_rd_address [5];
   logic [255:0] avm_kernel_rd_writedata [5];
   logic [31:0] avm_kernel_rd_byteenable [5];
   logic avm_kernel_rd_waitrequest [5];
   logic [255:0] avm_kernel_rd_readdata [5];
   logic avm_kernel_rd_readdatavalid [5];
   logic avm_kernel_rd_writeack [5];
   logic avm_kernel_wr_enable [2];
   logic avm_kernel_wr_read [2];
   logic avm_kernel_wr_write [2];
   logic [4:0] avm_kernel_wr_burstcount [2];
   logic [29:0] avm_kernel_wr_address [2];
   logic [255:0] avm_kernel_wr_writedata [2];
   logic [31:0] avm_kernel_wr_byteenable [2];
   logic avm_kernel_wr_waitrequest [2];
   logic [255:0] avm_kernel_wr_readdata [2];
   logic avm_kernel_wr_readdatavalid [2];
   logic avm_kernel_wr_writeack [2];
   logic ic_avm_enable [1];
   logic ic_avm_read [1];
   logic ic_avm_write [1];
   logic [4:0] ic_avm_burstcount [1];
   logic [29:0] ic_avm_address [1];
   logic [255:0] ic_avm_writedata [1];
   logic [31:0] ic_avm_byteenable [1];
   logic ic_avm_waitrequest [1];
   logic [255:0] ic_avm_readdata [1];
   logic ic_avm_readdatavalid [1];
   logic ic_avm_writeack [1];
   logic [31:0] avs_channel_id_bias_ch_fifosize;
   logic avm_channel_id_bias_ch_write_valid;
   logic avm_channel_id_bias_ch_write_ready;
   logic [127:0] avm_channel_id_bias_ch_write_data;
   logic avm_channel_id_bias_ch_read_valid;
   logic avm_channel_id_bias_ch_read_ready;
   logic [127:0] avm_channel_id_bias_ch_read_data;
   logic avs_channel_id_bias_ch_write_valid;
   logic avs_channel_id_bias_ch_write_ready;
   logic [127:0] avs_channel_id_bias_ch_write_data;
   logic avs_channel_id_bias_ch_read_valid;
   logic avs_channel_id_bias_ch_read_ready;
   logic [127:0] avs_channel_id_bias_ch_read_data;
   logic avs_channel_id_bias_ch_write_almostfull;
   logic avm_channel_id_bias_ch_write_almostfull;
   logic [31:0] avs_channel_id_data_ch_fifosize;
   logic avm_channel_id_data_ch_write_valid;
   logic avm_channel_id_data_ch_write_ready;
   logic [511:0] avm_channel_id_data_ch_write_data;
   logic avm_channel_id_data_ch_read_valid;
   logic avm_channel_id_data_ch_read_ready;
   logic [511:0] avm_channel_id_data_ch_read_data;
   logic avs_channel_id_data_ch_write_valid;
   logic avs_channel_id_data_ch_write_ready;
   logic [511:0] avs_channel_id_data_ch_write_data;
   logic avs_channel_id_data_ch_read_valid;
   logic avs_channel_id_data_ch_read_ready;
   logic [511:0] avs_channel_id_data_ch_read_data;
   logic avs_channel_id_data_ch_write_almostfull;
   logic avm_channel_id_data_ch_write_almostfull;
   logic [31:0] avs_channel_id_weight_ch_fifosize;
   logic avm_channel_id_weight_ch_write_valid;
   logic avm_channel_id_weight_ch_write_ready;
   logic [511:0] avm_channel_id_weight_ch_write_data;
   logic avm_channel_id_weight_ch_read_valid;
   logic avm_channel_id_weight_ch_read_ready;
   logic [511:0] avm_channel_id_weight_ch_read_data;
   logic avs_channel_id_weight_ch_write_valid;
   logic avs_channel_id_weight_ch_write_ready;
   logic [511:0] avs_channel_id_weight_ch_write_data;
   logic avs_channel_id_weight_ch_read_valid;
   logic avs_channel_id_weight_ch_read_ready;
   logic [511:0] avs_channel_id_weight_ch_read_data;
   logic avs_channel_id_weight_ch_write_almostfull;
   logic avm_channel_id_weight_ch_write_almostfull;
   logic [31:0] avs_channel_id_bypass_ch_fifosize;
   logic avm_channel_id_bypass_ch_write_valid;
   logic avm_channel_id_bypass_ch_write_ready;
   logic [127:0] avm_channel_id_bypass_ch_write_data;
   logic avm_channel_id_bypass_ch_read_valid;
   logic avm_channel_id_bypass_ch_read_ready;
   logic [127:0] avm_channel_id_bypass_ch_read_data;
   logic avs_channel_id_bypass_ch_write_valid;
   logic avs_channel_id_bypass_ch_write_ready;
   logic [127:0] avs_channel_id_bypass_ch_write_data;
   logic avs_channel_id_bypass_ch_read_valid;
   logic avs_channel_id_bypass_ch_read_ready;
   logic [127:0] avs_channel_id_bypass_ch_read_data;
   logic avs_channel_id_bypass_ch_write_almostfull;
   logic avm_channel_id_bypass_ch_write_almostfull;
   logic [31:0] avs_channel_id_conv_ch_fifosize;
   logic avm_channel_id_conv_ch_write_valid;
   logic avm_channel_id_conv_ch_write_ready;
   logic [127:0] avm_channel_id_conv_ch_write_data;
   logic avm_channel_id_conv_ch_read_valid;
   logic avm_channel_id_conv_ch_read_ready;
   logic [127:0] avm_channel_id_conv_ch_read_data;
   logic avs_channel_id_conv_ch_write_valid;
   logic avs_channel_id_conv_ch_write_ready;
   logic [127:0] avs_channel_id_conv_ch_write_data;
   logic avs_channel_id_conv_ch_read_valid;
   logic avs_channel_id_conv_ch_read_ready;
   logic [127:0] avs_channel_id_conv_ch_read_data;
   logic avs_channel_id_conv_ch_write_almostfull;
   logic avm_channel_id_conv_ch_write_almostfull;
   logic [31:0] avs_channel_id_pool_ch_fifosize;
   logic avm_channel_id_pool_ch_write_valid;
   logic avm_channel_id_pool_ch_write_ready;
   logic [127:0] avm_channel_id_pool_ch_write_data;
   logic avm_channel_id_pool_ch_read_valid;
   logic avm_channel_id_pool_ch_read_ready;
   logic [127:0] avm_channel_id_pool_ch_read_data;
   logic avs_channel_id_pool_ch_write_valid;
   logic avs_channel_id_pool_ch_write_ready;
   logic [127:0] avs_channel_id_pool_ch_write_data;
   logic avs_channel_id_pool_ch_read_valid;
   logic avs_channel_id_pool_ch_read_ready;
   logic [127:0] avs_channel_id_pool_ch_read_data;
   logic avs_channel_id_pool_ch_write_almostfull;
   logic avm_channel_id_pool_ch_write_almostfull;

   assign coreConv_start_chain[0] = coreConv_start;
   assign coreConv_finish_chain[0] = 1'b1;
   assign coreConv_cra_pending_write = |coreConv_pending_write;
   assign coreConv_cra_lsu_active = |coreConv_lsu_active;
   assign coreConv_cra_valid_in = |coreConv_valid_in;
   assign coreConv_stall_in = 0;
   // INST coreConv_cra_slave_inst of coreConv_function_cra_slave
   coreConv_function_cra_slave coreConv_cra_slave_inst
   (
      .clock(clock),
      .resetn(resetn),
      .start(coreConv_start),
      .finish(coreConv_finish),
      .has_a_lsu_active(coreConv_cra_lsu_active),
      .has_a_write_pending(coreConv_cra_pending_write),
      .valid_in(coreConv_cra_valid_in),
      .kernel_arguments(coreConv_kernel_arguments),
      .cra_irq(kernel_irqs[0]),
      // AVS avs_cra
      .avs_cra_enable(avs_coreConv_cra_enable),
      .avs_cra_read(avs_coreConv_cra_read),
      .avs_cra_write(avs_coreConv_cra_write),
      .avs_cra_address(avs_coreConv_cra_address),
      .avs_cra_writedata(avs_coreConv_cra_writedata),
      .avs_cra_byteenable(avs_coreConv_cra_byteenable),
      .avs_cra_readdata(avs_coreConv_cra_readdata),
      .avs_cra_readdatavalid(avs_coreConv_cra_readdatavalid)
   );

   // INST coreConv_inst_0 of coreConv_top_wrapper_0
   coreConv_top_wrapper_0 coreConv_inst_0
   (
      .start(coreConv_start_kernel_copy[0]),
      .kernel_arguments(coreConv_kernel_arguments),
      .kernel_valid_in(coreConv_valid_in[0]),
      .kernel_valid_out(coreConv_valid_out[0]),
      .has_a_write_pending(coreConv_pending_write[0]),
      .has_a_lsu_active(coreConv_lsu_active[0]),
      .clock(clock),
      .resetn(resetn),
      .clock2x(clock2x),
      // AVST avm_channel_id_bias_ch_read
      .avm_channel_id_bias_ch_read_valid(avm_channel_id_bias_ch_read_valid),
      .avm_channel_id_bias_ch_read_ready(avm_channel_id_bias_ch_read_ready),
      .avm_channel_id_bias_ch_read_data(avm_channel_id_bias_ch_read_data),
      // AVST avm_channel_id_data_ch_read
      .avm_channel_id_data_ch_read_valid(avm_channel_id_data_ch_read_valid),
      .avm_channel_id_data_ch_read_ready(avm_channel_id_data_ch_read_ready),
      .avm_channel_id_data_ch_read_data(avm_channel_id_data_ch_read_data),
      // AVST avm_channel_id_weight_ch_read
      .avm_channel_id_weight_ch_read_valid(avm_channel_id_weight_ch_read_valid),
      .avm_channel_id_weight_ch_read_ready(avm_channel_id_weight_ch_read_ready),
      .avm_channel_id_weight_ch_read_data(avm_channel_id_weight_ch_read_data),
      // AVST avm_channel_id_bypass_ch_write
      .avm_channel_id_bypass_ch_write_valid(avm_channel_id_bypass_ch_write_valid),
      .avm_channel_id_bypass_ch_write_ready(avm_channel_id_bypass_ch_write_ready),
      .avm_channel_id_bypass_ch_write_data(avm_channel_id_bypass_ch_write_data),
      .avm_channel_id_bypass_ch_write_almostfull(avm_channel_id_bypass_ch_write_almostfull),
      // AVST avm_channel_id_conv_ch_write
      .avm_channel_id_conv_ch_write_valid(avm_channel_id_conv_ch_write_valid),
      .avm_channel_id_conv_ch_write_ready(avm_channel_id_conv_ch_write_ready),
      .avm_channel_id_conv_ch_write_data(avm_channel_id_conv_ch_write_data),
      .avm_channel_id_conv_ch_write_almostfull(avm_channel_id_conv_ch_write_almostfull)
   );

   // INST coreConv_start_elem_inst_0 of acl_start_signal_chain_element
   acl_start_signal_chain_element coreConv_start_elem_inst_0
   (
      .clock(clock),
      .resetn(resetn),
      .start_in(coreConv_start_chain[0]),
      .start_kernel(coreConv_start_kernel_copy[0]),
      .start_finish_detector(coreConv_start_task_fd[0]),
      .start_finish_chain_element(coreConv_start_finish_element[0]),
      .start_chain()
   );

   // INST coreConv_task_finish_detector_inst_0 of acl_task_copy_finish_detector
   acl_task_copy_finish_detector coreConv_task_finish_detector_inst_0
   (
      .clock(clock),
      .resetn(resetn),
      .start(coreConv_start_task_fd[0]),
      .kernel_copy_valid_out(coreConv_valid_out[0]),
      .kernel_copy_has_pending_write(coreConv_pending_write[0]),
      .kernel_copy_finished(coreConv_finish_kernel_copy[0])
   );

   // INST coreConv_finish_elem_inst_0 of acl_finish_signal_chain_element
   acl_finish_signal_chain_element coreConv_finish_elem_inst_0
   (
      .clock(clock),
      .resetn(resetn),
      .start(coreConv_start_finish_element[0]),
      .kernel_copy_finished(coreConv_finish_kernel_copy[0]),
      .finish_in(coreConv_finish_chain[0]),
      .finish_out(coreConv_finish)
   );

   assign lrn_start_chain[0] = lrn_start;
   assign lrn_finish_chain[0] = 1'b1;
   assign lrn_cra_pending_write = |lrn_pending_write;
   assign lrn_cra_lsu_active = |lrn_lsu_active;
   assign lrn_cra_valid_in = |lrn_valid_in;
   assign lrn_stall_in = 0;
   // INST lrn_workgroup_dispatcher of acl_work_group_dispatcher
   acl_work_group_dispatcher
   #(
      .WIDTH(32),
      .NUM_COPIES(1),
      .RUN_FOREVER(0)
   )
   lrn_workgroup_dispatcher
   (
      .clock(clock),
      .resetn(resetn),
      .start(lrn_start),
      .num_groups(lrn_num_groups),
      .local_size(lrn_local_size),
      .stall_in(lrn_wg_disp_stall_in),
      .valid_out(lrn_wg_disp_valid_out),
      .group_id_out(lrn_wg_disp_group_id_out),
      .global_id_base_out(lrn_wg_disp_global_id_base_out),
      .start_out(lrn_wg_disp_start_out),
      .dispatched_all_groups(lrn_wg_disp_dispatched_all_groups)
   );

   // INST lrn_finish_detector of acl_kernel_finish_detector
   acl_kernel_finish_detector
   #(
      .NUM_COPIES(1),
      .WG_SIZE_W(32),
      .GLOBAL_ID_W(32),
      .TESSELLATION_SIZE(0)
   )
   lrn_finish_detector
   (
      .clock(clock),
      .resetn(resetn),
      .start(lrn_start),
      .wg_size(lrn_wg_size),
      .wg_dispatch_valid_out(lrn_wg_disp_valid_out),
      .wg_dispatch_stall_in(lrn_wg_disp_stall_in),
      .dispatched_all_groups(lrn_wg_disp_dispatched_all_groups),
      .kernel_copy_valid_out(lrn_valid_out),
      .kernel_copy_stall_in(lrn_stall_in),
      .pending_writes(lrn_cra_pending_write),
      .finish(lrn_finish)
   );

   // INST lrn_cra_slave_inst of lrn_function_cra_slave
   lrn_function_cra_slave lrn_cra_slave_inst
   (
      .clock(clock),
      .resetn(resetn),
      .start(lrn_start),
      .finish(lrn_finish),
      .global_offset_0(lrn_global_offset[0]),
      .global_offset_1(lrn_global_offset[1]),
      .global_offset_2(lrn_global_offset[2]),
      .work_dim(lrn_work_dim),
      .has_a_lsu_active(lrn_cra_lsu_active),
      .has_a_write_pending(lrn_cra_pending_write),
      .valid_in(lrn_cra_valid_in),
      .global_size_0(lrn_global_size[0]),
      .global_size_1(lrn_global_size[1]),
      .global_size_2(lrn_global_size[2]),
      .num_groups_0(lrn_num_groups[0]),
      .num_groups_1(lrn_num_groups[1]),
      .num_groups_2(lrn_num_groups[2]),
      .local_size_0(lrn_local_size[0]),
      .local_size_1(lrn_local_size[1]),
      .local_size_2(lrn_local_size[2]),
      .workgroup_size(lrn_wg_size),
      .kernel_arguments(lrn_kernel_arguments),
      .cra_irq(kernel_irqs[1]),
      // AVS avs_cra
      .avs_cra_enable(avs_lrn_cra_enable),
      .avs_cra_read(avs_lrn_cra_read),
      .avs_cra_write(avs_lrn_cra_write),
      .avs_cra_address(avs_lrn_cra_address),
      .avs_cra_writedata(avs_lrn_cra_writedata),
      .avs_cra_byteenable(avs_lrn_cra_byteenable),
      .avs_cra_readdata(avs_lrn_cra_readdata),
      .avs_cra_readdatavalid(avs_lrn_cra_readdatavalid)
   );

   // INST lrn_id_iter_inst_0 of acl_id_iterator
   acl_id_iterator
   #(
      .WIDTH(32),
      .LOCAL_WIDTH_X(6),
      .LOCAL_WIDTH_Y(6),
      .LOCAL_WIDTH_Z(6),
      .ENABLE_TESSELLATION(0)
   )
   lrn_id_iter_inst_0
   (
      .clock(clock),
      .resetn(resetn),
      .start(lrn_wg_disp_start_out),
      .valid_in(lrn_wg_disp_valid_out[0]),
      .stall_out(lrn_wg_disp_stall_in[0]),
      .stall_in(lrn_stall_out[0]),
      .valid_out(lrn_valid_in[0]),
      .group_id_in(lrn_wg_disp_group_id_out),
      .global_id_base_in(lrn_wg_disp_global_id_base_out),
      .local_size(lrn_local_size),
      .global_size(lrn_global_size),
      .local_id(lrn_local_id[0]),
      .global_id(lrn_global_id[0]),
      .group_id(lrn_group_id[0])
   );

   // INST lrn_inst_0 of lrn_top_wrapper_0
   lrn_top_wrapper_0 lrn_inst_0
   (
      .start(lrn_start_kernel_copy[0]),
      .kernel_arguments(lrn_kernel_arguments),
      .work_dim(lrn_work_dim),
      .global_offset(lrn_global_offset),
      .kernel_valid_out(lrn_valid_out[0]),
      .has_a_write_pending(lrn_pending_write[0]),
      .has_a_lsu_active(lrn_lsu_active[0]),
      .global_id(lrn_global_id[0]),
      .local_id(lrn_local_id[0]),
      .group_id(lrn_group_id[0]),
      .global_size(lrn_global_size),
      .local_size(lrn_local_size),
      .num_groups(lrn_num_groups),
      .workgroup_size(lrn_wg_size),
      .kernel_stall_out(lrn_stall_out[0]),
      .kernel_valid_in(lrn_valid_in[0]),
      .clock(clock),
      .resetn(resetn),
      .clock2x(clock2x),
      // AVM avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0
      .avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_enable(avm_kernel_rd_enable[0]),
      .avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_read(avm_kernel_rd_read[0]),
      .avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_write(avm_kernel_rd_write[0]),
      .avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_burstcount(avm_kernel_rd_burstcount[0]),
      .avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_address(avm_kernel_rd_address[0]),
      .avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_writedata(avm_kernel_rd_writedata[0]),
      .avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_byteenable(avm_kernel_rd_byteenable[0]),
      .avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_waitrequest(avm_kernel_rd_waitrequest[0]),
      .avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_readdata(avm_kernel_rd_readdata[0]),
      .avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_readdatavalid(avm_kernel_rd_readdatavalid[0]),
      .avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_writeack(avm_kernel_rd_writeack[0]),
      // AVM avm_local_bb3_st_c0_exe111_inst0
      .avm_local_bb3_st_c0_exe111_inst0_enable(avm_kernel_wr_enable[0]),
      .avm_local_bb3_st_c0_exe111_inst0_read(avm_kernel_wr_read[0]),
      .avm_local_bb3_st_c0_exe111_inst0_write(avm_kernel_wr_write[0]),
      .avm_local_bb3_st_c0_exe111_inst0_burstcount(avm_kernel_wr_burstcount[0]),
      .avm_local_bb3_st_c0_exe111_inst0_address(avm_kernel_wr_address[0]),
      .avm_local_bb3_st_c0_exe111_inst0_writedata(avm_kernel_wr_writedata[0]),
      .avm_local_bb3_st_c0_exe111_inst0_byteenable(avm_kernel_wr_byteenable[0]),
      .avm_local_bb3_st_c0_exe111_inst0_waitrequest(avm_kernel_wr_waitrequest[0]),
      .avm_local_bb3_st_c0_exe111_inst0_readdata(avm_kernel_wr_readdata[0]),
      .avm_local_bb3_st_c0_exe111_inst0_readdatavalid(avm_kernel_wr_readdatavalid[0]),
      .avm_local_bb3_st_c0_exe111_inst0_writeack(avm_kernel_wr_writeack[0])
   );

   // INST lrn_start_elem_inst_0 of acl_start_signal_chain_element
   acl_start_signal_chain_element lrn_start_elem_inst_0
   (
      .clock(clock),
      .resetn(resetn),
      .start_in(lrn_start_chain[0]),
      .start_kernel(lrn_start_kernel_copy[0]),
      .start_finish_detector(lrn_start_task_fd[0]),
      .start_finish_chain_element(lrn_start_finish_element[0]),
      .start_chain()
   );

   assign maxPool_start_chain[0] = maxPool_start;
   assign maxPool_finish_chain[0] = 1'b1;
   assign maxPool_cra_pending_write = |maxPool_pending_write;
   assign maxPool_cra_lsu_active = |maxPool_lsu_active;
   assign maxPool_cra_valid_in = |maxPool_valid_in;
   assign maxPool_stall_in = 0;
   // INST maxPool_workgroup_dispatcher of acl_work_group_dispatcher
   acl_work_group_dispatcher
   #(
      .WIDTH(32),
      .NUM_COPIES(1),
      .RUN_FOREVER(0)
   )
   maxPool_workgroup_dispatcher
   (
      .clock(clock),
      .resetn(resetn),
      .start(maxPool_start),
      .num_groups(maxPool_num_groups),
      .local_size(maxPool_local_size),
      .stall_in(maxPool_wg_disp_stall_in),
      .valid_out(maxPool_wg_disp_valid_out),
      .group_id_out(maxPool_wg_disp_group_id_out),
      .global_id_base_out(maxPool_wg_disp_global_id_base_out),
      .start_out(maxPool_wg_disp_start_out),
      .dispatched_all_groups(maxPool_wg_disp_dispatched_all_groups)
   );

   // INST maxPool_finish_detector of acl_kernel_finish_detector
   acl_kernel_finish_detector
   #(
      .NUM_COPIES(1),
      .WG_SIZE_W(32),
      .GLOBAL_ID_W(32),
      .TESSELLATION_SIZE(0)
   )
   maxPool_finish_detector
   (
      .clock(clock),
      .resetn(resetn),
      .start(maxPool_start),
      .wg_size(maxPool_wg_size),
      .wg_dispatch_valid_out(maxPool_wg_disp_valid_out),
      .wg_dispatch_stall_in(maxPool_wg_disp_stall_in),
      .dispatched_all_groups(maxPool_wg_disp_dispatched_all_groups),
      .kernel_copy_valid_out(maxPool_valid_out),
      .kernel_copy_stall_in(maxPool_stall_in),
      .pending_writes(maxPool_cra_pending_write),
      .finish(maxPool_finish)
   );

   // INST maxPool_cra_slave_inst of maxPool_function_cra_slave
   maxPool_function_cra_slave maxPool_cra_slave_inst
   (
      .clock(clock),
      .resetn(resetn),
      .start(maxPool_start),
      .finish(maxPool_finish),
      .global_offset_0(maxPool_global_offset[0]),
      .global_offset_1(maxPool_global_offset[1]),
      .global_offset_2(maxPool_global_offset[2]),
      .work_dim(maxPool_work_dim),
      .has_a_lsu_active(maxPool_cra_lsu_active),
      .has_a_write_pending(maxPool_cra_pending_write),
      .valid_in(maxPool_cra_valid_in),
      .global_size_0(maxPool_global_size[0]),
      .global_size_1(maxPool_global_size[1]),
      .global_size_2(maxPool_global_size[2]),
      .num_groups_0(maxPool_num_groups[0]),
      .num_groups_1(maxPool_num_groups[1]),
      .num_groups_2(maxPool_num_groups[2]),
      .local_size_0(maxPool_local_size[0]),
      .local_size_1(maxPool_local_size[1]),
      .local_size_2(maxPool_local_size[2]),
      .workgroup_size(maxPool_wg_size),
      .kernel_arguments(maxPool_kernel_arguments),
      .cra_irq(kernel_irqs[2]),
      // AVS avs_cra
      .avs_cra_enable(avs_maxPool_cra_enable),
      .avs_cra_read(avs_maxPool_cra_read),
      .avs_cra_write(avs_maxPool_cra_write),
      .avs_cra_address(avs_maxPool_cra_address),
      .avs_cra_writedata(avs_maxPool_cra_writedata),
      .avs_cra_byteenable(avs_maxPool_cra_byteenable),
      .avs_cra_readdata(avs_maxPool_cra_readdata),
      .avs_cra_readdatavalid(avs_maxPool_cra_readdatavalid)
   );

   // INST maxPool_id_iter_inst_0 of acl_id_iterator
   acl_id_iterator
   #(
      .WIDTH(32),
      .LOCAL_WIDTH_X(1),
      .LOCAL_WIDTH_Y(1),
      .LOCAL_WIDTH_Z(1),
      .ENABLE_TESSELLATION(0)
   )
   maxPool_id_iter_inst_0
   (
      .clock(clock),
      .resetn(resetn),
      .start(maxPool_wg_disp_start_out),
      .valid_in(maxPool_wg_disp_valid_out[0]),
      .stall_out(maxPool_wg_disp_stall_in[0]),
      .stall_in(maxPool_stall_out[0]),
      .valid_out(maxPool_valid_in[0]),
      .group_id_in(maxPool_wg_disp_group_id_out),
      .global_id_base_in(maxPool_wg_disp_global_id_base_out),
      .local_size(maxPool_local_size),
      .global_size(maxPool_global_size),
      .local_id(maxPool_local_id[0]),
      .global_id(maxPool_global_id[0]),
      .group_id(maxPool_group_id[0])
   );

   // INST maxPool_inst_0 of maxPool_top_wrapper_0
   maxPool_top_wrapper_0 maxPool_inst_0
   (
      .start(maxPool_start_kernel_copy[0]),
      .kernel_arguments(maxPool_kernel_arguments),
      .work_dim(maxPool_work_dim),
      .global_offset(maxPool_global_offset),
      .kernel_valid_out(maxPool_valid_out[0]),
      .has_a_write_pending(maxPool_pending_write[0]),
      .has_a_lsu_active(maxPool_lsu_active[0]),
      .global_id(maxPool_global_id[0]),
      .local_id(maxPool_local_id[0]),
      .group_id(maxPool_group_id[0]),
      .global_size(maxPool_global_size),
      .local_size(maxPool_local_size),
      .num_groups(maxPool_num_groups),
      .workgroup_size(maxPool_wg_size),
      .kernel_stall_out(maxPool_stall_out[0]),
      .kernel_valid_in(maxPool_valid_in[0]),
      .clock(clock),
      .resetn(resetn),
      .clock2x(clock2x),
      // AVST avm_channel_id_conv_ch_read
      .avm_channel_id_conv_ch_read_valid(avm_channel_id_conv_ch_read_valid),
      .avm_channel_id_conv_ch_read_ready(avm_channel_id_conv_ch_read_ready),
      .avm_channel_id_conv_ch_read_data(avm_channel_id_conv_ch_read_data),
      // AVST avm_channel_id_pool_ch_write
      .avm_channel_id_pool_ch_write_valid(avm_channel_id_pool_ch_write_valid),
      .avm_channel_id_pool_ch_write_ready(avm_channel_id_pool_ch_write_ready),
      .avm_channel_id_pool_ch_write_data(avm_channel_id_pool_ch_write_data),
      .avm_channel_id_pool_ch_write_almostfull(avm_channel_id_pool_ch_write_almostfull)
   );

   // INST maxPool_start_elem_inst_0 of acl_start_signal_chain_element
   acl_start_signal_chain_element maxPool_start_elem_inst_0
   (
      .clock(clock),
      .resetn(resetn),
      .start_in(maxPool_start_chain[0]),
      .start_kernel(maxPool_start_kernel_copy[0]),
      .start_finish_detector(maxPool_start_task_fd[0]),
      .start_finish_chain_element(maxPool_start_finish_element[0]),
      .start_chain()
   );

   assign memRead_start_chain[0] = memRead_start;
   assign memRead_finish_chain[0] = 1'b1;
   assign memRead_cra_pending_write = |memRead_pending_write;
   assign memRead_cra_lsu_active = |memRead_lsu_active;
   assign memRead_cra_valid_in = |memRead_valid_in;
   assign memRead_stall_in = 0;
   // INST memRead_cra_slave_inst of memRead_function_cra_slave
   memRead_function_cra_slave memRead_cra_slave_inst
   (
      .clock(clock),
      .resetn(resetn),
      .start(memRead_start),
      .finish(memRead_finish),
      .has_a_lsu_active(memRead_cra_lsu_active),
      .has_a_write_pending(memRead_cra_pending_write),
      .valid_in(memRead_cra_valid_in),
      .kernel_arguments(memRead_kernel_arguments),
      .cra_irq(kernel_irqs[3]),
      // AVS avs_cra
      .avs_cra_enable(avs_memRead_cra_enable),
      .avs_cra_read(avs_memRead_cra_read),
      .avs_cra_write(avs_memRead_cra_write),
      .avs_cra_address(avs_memRead_cra_address),
      .avs_cra_writedata(avs_memRead_cra_writedata),
      .avs_cra_byteenable(avs_memRead_cra_byteenable),
      .avs_cra_readdata(avs_memRead_cra_readdata),
      .avs_cra_readdatavalid(avs_memRead_cra_readdatavalid)
   );

   // INST memRead_inst_0 of memRead_top_wrapper_0
   memRead_top_wrapper_0 memRead_inst_0
   (
      .start(memRead_start_kernel_copy[0]),
      .kernel_arguments(memRead_kernel_arguments),
      .kernel_valid_in(memRead_valid_in[0]),
      .kernel_valid_out(memRead_valid_out[0]),
      .has_a_write_pending(memRead_pending_write[0]),
      .has_a_lsu_active(memRead_lsu_active[0]),
      .clock(clock),
      .resetn(resetn),
      .clock2x(clock2x),
      // AVM avm_local_bb10_ld__inst0
      .avm_local_bb10_ld__inst0_enable(avm_kernel_rd_enable[1]),
      .avm_local_bb10_ld__inst0_read(avm_kernel_rd_read[1]),
      .avm_local_bb10_ld__inst0_write(avm_kernel_rd_write[1]),
      .avm_local_bb10_ld__inst0_burstcount(avm_kernel_rd_burstcount[1]),
      .avm_local_bb10_ld__inst0_address(avm_kernel_rd_address[1]),
      .avm_local_bb10_ld__inst0_writedata(avm_kernel_rd_writedata[1]),
      .avm_local_bb10_ld__inst0_byteenable(avm_kernel_rd_byteenable[1]),
      .avm_local_bb10_ld__inst0_waitrequest(avm_kernel_rd_waitrequest[1]),
      .avm_local_bb10_ld__inst0_readdata(avm_kernel_rd_readdata[1]),
      .avm_local_bb10_ld__inst0_readdatavalid(avm_kernel_rd_readdatavalid[1]),
      .avm_local_bb10_ld__inst0_writeack(avm_kernel_rd_writeack[1]),
      // AVM avm_local_bb10_ld_memcoalesce_bias_load_0_inst0
      .avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_enable(avm_kernel_rd_enable[2]),
      .avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_read(avm_kernel_rd_read[2]),
      .avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_write(avm_kernel_rd_write[2]),
      .avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_burstcount(avm_kernel_rd_burstcount[2]),
      .avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_address(avm_kernel_rd_address[2]),
      .avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_writedata(avm_kernel_rd_writedata[2]),
      .avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_byteenable(avm_kernel_rd_byteenable[2]),
      .avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_waitrequest(avm_kernel_rd_waitrequest[2]),
      .avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_readdata(avm_kernel_rd_readdata[2]),
      .avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_readdatavalid(avm_kernel_rd_readdatavalid[2]),
      .avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_writeack(avm_kernel_rd_writeack[2]),
      // AVM avm_local_bb10_ld_memcoalesce_weights_load_0_inst0
      .avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_enable(avm_kernel_rd_enable[3]),
      .avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_read(avm_kernel_rd_read[3]),
      .avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_write(avm_kernel_rd_write[3]),
      .avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_burstcount(avm_kernel_rd_burstcount[3]),
      .avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_address(avm_kernel_rd_address[3]),
      .avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_writedata(avm_kernel_rd_writedata[3]),
      .avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_byteenable(avm_kernel_rd_byteenable[3]),
      .avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_waitrequest(avm_kernel_rd_waitrequest[3]),
      .avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_readdata(avm_kernel_rd_readdata[3]),
      .avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_readdatavalid(avm_kernel_rd_readdatavalid[3]),
      .avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_writeack(avm_kernel_rd_writeack[3]),
      // AVM avm_local_bb4_ld__inst0
      .avm_local_bb4_ld__inst0_enable(avm_kernel_rd_enable[4]),
      .avm_local_bb4_ld__inst0_read(avm_kernel_rd_read[4]),
      .avm_local_bb4_ld__inst0_write(avm_kernel_rd_write[4]),
      .avm_local_bb4_ld__inst0_burstcount(avm_kernel_rd_burstcount[4]),
      .avm_local_bb4_ld__inst0_address(avm_kernel_rd_address[4]),
      .avm_local_bb4_ld__inst0_writedata(avm_kernel_rd_writedata[4]),
      .avm_local_bb4_ld__inst0_byteenable(avm_kernel_rd_byteenable[4]),
      .avm_local_bb4_ld__inst0_waitrequest(avm_kernel_rd_waitrequest[4]),
      .avm_local_bb4_ld__inst0_readdata(avm_kernel_rd_readdata[4]),
      .avm_local_bb4_ld__inst0_readdatavalid(avm_kernel_rd_readdatavalid[4]),
      .avm_local_bb4_ld__inst0_writeack(avm_kernel_rd_writeack[4]),
      // AVST avm_channel_id_bias_ch_write
      .avm_channel_id_bias_ch_write_valid(avm_channel_id_bias_ch_write_valid),
      .avm_channel_id_bias_ch_write_ready(avm_channel_id_bias_ch_write_ready),
      .avm_channel_id_bias_ch_write_data(avm_channel_id_bias_ch_write_data),
      .avm_channel_id_bias_ch_write_almostfull(avm_channel_id_bias_ch_write_almostfull),
      // AVST avm_channel_id_data_ch_write
      .avm_channel_id_data_ch_write_valid(avm_channel_id_data_ch_write_valid),
      .avm_channel_id_data_ch_write_ready(avm_channel_id_data_ch_write_ready),
      .avm_channel_id_data_ch_write_data(avm_channel_id_data_ch_write_data),
      .avm_channel_id_data_ch_write_almostfull(avm_channel_id_data_ch_write_almostfull),
      // AVST avm_channel_id_weight_ch_write
      .avm_channel_id_weight_ch_write_valid(avm_channel_id_weight_ch_write_valid),
      .avm_channel_id_weight_ch_write_ready(avm_channel_id_weight_ch_write_ready),
      .avm_channel_id_weight_ch_write_data(avm_channel_id_weight_ch_write_data),
      .avm_channel_id_weight_ch_write_almostfull(avm_channel_id_weight_ch_write_almostfull)
   );

   // INST memRead_start_elem_inst_0 of acl_start_signal_chain_element
   acl_start_signal_chain_element memRead_start_elem_inst_0
   (
      .clock(clock),
      .resetn(resetn),
      .start_in(memRead_start_chain[0]),
      .start_kernel(memRead_start_kernel_copy[0]),
      .start_finish_detector(memRead_start_task_fd[0]),
      .start_finish_chain_element(memRead_start_finish_element[0]),
      .start_chain()
   );

   // INST memRead_task_finish_detector_inst_0 of acl_task_copy_finish_detector
   acl_task_copy_finish_detector memRead_task_finish_detector_inst_0
   (
      .clock(clock),
      .resetn(resetn),
      .start(memRead_start_task_fd[0]),
      .kernel_copy_valid_out(memRead_valid_out[0]),
      .kernel_copy_has_pending_write(memRead_pending_write[0]),
      .kernel_copy_finished(memRead_finish_kernel_copy[0])
   );

   // INST memRead_finish_elem_inst_0 of acl_finish_signal_chain_element
   acl_finish_signal_chain_element memRead_finish_elem_inst_0
   (
      .clock(clock),
      .resetn(resetn),
      .start(memRead_start_finish_element[0]),
      .kernel_copy_finished(memRead_finish_kernel_copy[0]),
      .finish_in(memRead_finish_chain[0]),
      .finish_out(memRead_finish)
   );

   assign memWrite_start_chain[0] = memWrite_start;
   assign memWrite_finish_chain[0] = 1'b1;
   assign memWrite_cra_pending_write = |memWrite_pending_write;
   assign memWrite_cra_lsu_active = |memWrite_lsu_active;
   assign memWrite_cra_valid_in = |memWrite_valid_in;
   assign memWrite_stall_in = 0;
   // INST memWrite_workgroup_dispatcher of acl_work_group_dispatcher
   acl_work_group_dispatcher
   #(
      .WIDTH(32),
      .NUM_COPIES(1),
      .RUN_FOREVER(0)
   )
   memWrite_workgroup_dispatcher
   (
      .clock(clock),
      .resetn(resetn),
      .start(memWrite_start),
      .num_groups(memWrite_num_groups),
      .local_size(memWrite_local_size),
      .stall_in(memWrite_wg_disp_stall_in),
      .valid_out(memWrite_wg_disp_valid_out),
      .group_id_out(memWrite_wg_disp_group_id_out),
      .global_id_base_out(memWrite_wg_disp_global_id_base_out),
      .start_out(memWrite_wg_disp_start_out),
      .dispatched_all_groups(memWrite_wg_disp_dispatched_all_groups)
   );

   // INST memWrite_finish_detector of acl_kernel_finish_detector
   acl_kernel_finish_detector
   #(
      .NUM_COPIES(1),
      .WG_SIZE_W(32),
      .GLOBAL_ID_W(32),
      .TESSELLATION_SIZE(0)
   )
   memWrite_finish_detector
   (
      .clock(clock),
      .resetn(resetn),
      .start(memWrite_start),
      .wg_size(memWrite_wg_size),
      .wg_dispatch_valid_out(memWrite_wg_disp_valid_out),
      .wg_dispatch_stall_in(memWrite_wg_disp_stall_in),
      .dispatched_all_groups(memWrite_wg_disp_dispatched_all_groups),
      .kernel_copy_valid_out(memWrite_valid_out),
      .kernel_copy_stall_in(memWrite_stall_in),
      .pending_writes(memWrite_cra_pending_write),
      .finish(memWrite_finish)
   );

   // INST memWrite_cra_slave_inst of memWrite_function_cra_slave
   memWrite_function_cra_slave memWrite_cra_slave_inst
   (
      .clock(clock),
      .resetn(resetn),
      .start(memWrite_start),
      .finish(memWrite_finish),
      .global_offset_0(memWrite_global_offset[0]),
      .global_offset_1(memWrite_global_offset[1]),
      .global_offset_2(memWrite_global_offset[2]),
      .work_dim(memWrite_work_dim),
      .has_a_lsu_active(memWrite_cra_lsu_active),
      .has_a_write_pending(memWrite_cra_pending_write),
      .valid_in(memWrite_cra_valid_in),
      .global_size_0(memWrite_global_size[0]),
      .global_size_1(memWrite_global_size[1]),
      .global_size_2(memWrite_global_size[2]),
      .num_groups_0(memWrite_num_groups[0]),
      .num_groups_1(memWrite_num_groups[1]),
      .num_groups_2(memWrite_num_groups[2]),
      .local_size_0(memWrite_local_size[0]),
      .local_size_1(memWrite_local_size[1]),
      .local_size_2(memWrite_local_size[2]),
      .workgroup_size(memWrite_wg_size),
      .kernel_arguments(memWrite_kernel_arguments),
      .cra_irq(kernel_irqs[4]),
      // AVS avs_cra
      .avs_cra_enable(avs_memWrite_cra_enable),
      .avs_cra_read(avs_memWrite_cra_read),
      .avs_cra_write(avs_memWrite_cra_write),
      .avs_cra_address(avs_memWrite_cra_address),
      .avs_cra_writedata(avs_memWrite_cra_writedata),
      .avs_cra_byteenable(avs_memWrite_cra_byteenable),
      .avs_cra_readdata(avs_memWrite_cra_readdata),
      .avs_cra_readdatavalid(avs_memWrite_cra_readdatavalid)
   );

   // INST memWrite_id_iter_inst_0 of acl_id_iterator
   acl_id_iterator
   #(
      .WIDTH(32),
      .LOCAL_WIDTH_X(1),
      .LOCAL_WIDTH_Y(1),
      .LOCAL_WIDTH_Z(4),
      .ENABLE_TESSELLATION(0)
   )
   memWrite_id_iter_inst_0
   (
      .clock(clock),
      .resetn(resetn),
      .start(memWrite_wg_disp_start_out),
      .valid_in(memWrite_wg_disp_valid_out[0]),
      .stall_out(memWrite_wg_disp_stall_in[0]),
      .stall_in(memWrite_stall_out[0]),
      .valid_out(memWrite_valid_in[0]),
      .group_id_in(memWrite_wg_disp_group_id_out),
      .global_id_base_in(memWrite_wg_disp_global_id_base_out),
      .local_size(memWrite_local_size),
      .global_size(memWrite_global_size),
      .local_id(memWrite_local_id[0]),
      .global_id(memWrite_global_id[0]),
      .group_id(memWrite_group_id[0])
   );

   // INST memWrite_inst_0 of memWrite_top_wrapper_0
   memWrite_top_wrapper_0 memWrite_inst_0
   (
      .start(memWrite_start_kernel_copy[0]),
      .kernel_arguments(memWrite_kernel_arguments),
      .work_dim(memWrite_work_dim),
      .global_offset(memWrite_global_offset),
      .kernel_valid_out(memWrite_valid_out[0]),
      .has_a_write_pending(memWrite_pending_write[0]),
      .has_a_lsu_active(memWrite_lsu_active[0]),
      .global_id(memWrite_global_id[0]),
      .local_id(memWrite_local_id[0]),
      .group_id(memWrite_group_id[0]),
      .global_size(memWrite_global_size),
      .local_size(memWrite_local_size),
      .num_groups(memWrite_num_groups),
      .workgroup_size(memWrite_wg_size),
      .kernel_stall_out(memWrite_stall_out[0]),
      .kernel_valid_in(memWrite_valid_in[0]),
      .clock(clock),
      .resetn(resetn),
      .clock2x(clock2x),
      // AVM avm_local_bb1_st_c0_exe2_inst0
      .avm_local_bb1_st_c0_exe2_inst0_enable(avm_kernel_wr_enable[1]),
      .avm_local_bb1_st_c0_exe2_inst0_read(avm_kernel_wr_read[1]),
      .avm_local_bb1_st_c0_exe2_inst0_write(avm_kernel_wr_write[1]),
      .avm_local_bb1_st_c0_exe2_inst0_burstcount(avm_kernel_wr_burstcount[1]),
      .avm_local_bb1_st_c0_exe2_inst0_address(avm_kernel_wr_address[1]),
      .avm_local_bb1_st_c0_exe2_inst0_writedata(avm_kernel_wr_writedata[1]),
      .avm_local_bb1_st_c0_exe2_inst0_byteenable(avm_kernel_wr_byteenable[1]),
      .avm_local_bb1_st_c0_exe2_inst0_waitrequest(avm_kernel_wr_waitrequest[1]),
      .avm_local_bb1_st_c0_exe2_inst0_readdata(avm_kernel_wr_readdata[1]),
      .avm_local_bb1_st_c0_exe2_inst0_readdatavalid(avm_kernel_wr_readdatavalid[1]),
      .avm_local_bb1_st_c0_exe2_inst0_writeack(avm_kernel_wr_writeack[1]),
      // AVST avm_channel_id_bypass_ch_read
      .avm_channel_id_bypass_ch_read_valid(avm_channel_id_bypass_ch_read_valid),
      .avm_channel_id_bypass_ch_read_ready(avm_channel_id_bypass_ch_read_ready),
      .avm_channel_id_bypass_ch_read_data(avm_channel_id_bypass_ch_read_data),
      // AVST avm_channel_id_pool_ch_read
      .avm_channel_id_pool_ch_read_valid(avm_channel_id_pool_ch_read_valid),
      .avm_channel_id_pool_ch_read_ready(avm_channel_id_pool_ch_read_ready),
      .avm_channel_id_pool_ch_read_data(avm_channel_id_pool_ch_read_data)
   );

   // INST memWrite_start_elem_inst_0 of acl_start_signal_chain_element
   acl_start_signal_chain_element memWrite_start_elem_inst_0
   (
      .clock(clock),
      .resetn(resetn),
      .start_in(memWrite_start_chain[0]),
      .start_kernel(memWrite_start_kernel_copy[0]),
      .start_finish_detector(memWrite_start_task_fd[0]),
      .start_finish_chain_element(memWrite_start_finish_element[0]),
      .start_chain()
   );

   assign kernel_irq = |kernel_irqs;
   // INST lsu_ic_top of lsu_ic_top
   lsu_ic_top
   #(
      .AWIDTH(30),
      .SHIFT(30),
      .MWIDTH_BYTES(32),
      .BURST_CNT_W(5),
      .NUM_RD_PORT(5),
      .NUM_WR_PORT(2),
      .NUM_DIMM(1),
      .ENABLE_DUAL_RING(0),
      .ENABLE_MULTIPLE_WR_RING(0),
      .ENABLE_LAST_WAIT(0),
      .ENABLE_REORDER(0),
      .NUM_REORDER(1)
   )
   lsu_ic_top
   (
      .clk(clock),
      .resetn(resetn),
      .i_rd_request(avm_kernel_rd_read),
      .i_rd_address(avm_kernel_rd_address),
      .i_rd_burstcount(avm_kernel_rd_burstcount),
      .i_wr_byteenable(avm_kernel_wr_byteenable),
      .i_wr_address(avm_kernel_wr_address),
      .i_wr_request(avm_kernel_wr_write),
      .i_wr_burstcount(avm_kernel_wr_burstcount),
      .i_wr_writedata(avm_kernel_wr_writedata),
      .i_avm_waitrequest(ic_avm_waitrequest),
      .i_avm_readdata(ic_avm_readdata),
      .i_avm_readdatavalid(ic_avm_readdatavalid),
      .o_avm_byteenable(ic_avm_byteenable),
      .o_avm_address(ic_avm_address),
      .o_avm_read(ic_avm_read),
      .o_avm_write(ic_avm_write),
      .o_avm_burstcount(ic_avm_burstcount),
      .o_wr_waitrequest(avm_kernel_wr_waitrequest),
      .o_avm_writedata(ic_avm_writedata),
      .o_avm_writeack(avm_kernel_wr_writeack),
      .o_rd_waitrequest(avm_kernel_rd_waitrequest),
      .o_avm_readdata(avm_kernel_rd_readdata),
      .o_avm_readdatavalid(avm_kernel_rd_readdatavalid)
   );

   assign avm_memgmem0_port_0_0_rw_read = ic_avm_read[0];
   assign avm_memgmem0_port_0_0_rw_write = ic_avm_write[0];
   assign avm_memgmem0_port_0_0_rw_burstcount = ic_avm_burstcount[0];
   assign avm_memgmem0_port_0_0_rw_address = ic_avm_address[0];
   assign avm_memgmem0_port_0_0_rw_writedata = ic_avm_writedata[0];
   assign avm_memgmem0_port_0_0_rw_byteenable = ic_avm_byteenable[0];
   assign ic_avm_waitrequest[0] = avm_memgmem0_port_0_0_rw_waitrequest;
   assign ic_avm_readdata[0] = avm_memgmem0_port_0_0_rw_readdata;
   assign ic_avm_readdatavalid[0] = avm_memgmem0_port_0_0_rw_readdatavalid;
   assign avs_channel_id_bias_ch_write_valid = avm_channel_id_bias_ch_write_valid;
   assign avm_channel_id_bias_ch_write_ready = avs_channel_id_bias_ch_write_ready;
   assign avs_channel_id_bias_ch_write_data = avm_channel_id_bias_ch_write_data;
   assign avm_channel_id_bias_ch_read_valid = avs_channel_id_bias_ch_read_valid;
   assign avs_channel_id_bias_ch_read_ready = avm_channel_id_bias_ch_read_ready;
   assign avm_channel_id_bias_ch_read_data = avs_channel_id_bias_ch_read_data;
   // INST channel_id_bias_ch_fifo of acl_channel_fifo
   acl_channel_fifo
   #(
      .FIFO_DEPTH(10),
      .DATA_W(128),
      .ADJUST_FOR_LATENCY(1),
      .INTENDED_DEVICE_FAMILY("Cyclone V")
   )
   channel_id_bias_ch_fifo
   (
      .clock(clock),
      .resetn(resetn),
      // AVST avst_in
      .avst_in_valid(avs_channel_id_bias_ch_write_valid),
      .avst_in_ready(avs_channel_id_bias_ch_write_ready),
      .avst_in_data(avs_channel_id_bias_ch_write_data),
      // AVST avst_out
      .avst_out_valid(avs_channel_id_bias_ch_read_valid),
      .avst_out_ready(avs_channel_id_bias_ch_read_ready),
      .avst_out_data(avs_channel_id_bias_ch_read_data),
      .profile_fifosize(avs_channel_id_bias_ch_fifosize),
      .almost_full(avs_channel_id_bias_ch_write_almostfull)
   );

   assign avm_channel_id_bias_ch_write_almostfull = avs_channel_id_bias_ch_write_almostfull;
   assign avs_channel_id_data_ch_write_valid = avm_channel_id_data_ch_write_valid;
   assign avm_channel_id_data_ch_write_ready = avs_channel_id_data_ch_write_ready;
   assign avs_channel_id_data_ch_write_data = avm_channel_id_data_ch_write_data;
   assign avm_channel_id_data_ch_read_valid = avs_channel_id_data_ch_read_valid;
   assign avs_channel_id_data_ch_read_ready = avm_channel_id_data_ch_read_ready;
   assign avm_channel_id_data_ch_read_data = avs_channel_id_data_ch_read_data;
   // INST channel_id_data_ch_fifo of acl_channel_fifo
   acl_channel_fifo
   #(
      .FIFO_DEPTH(0),
      .DATA_W(512),
      .ADJUST_FOR_LATENCY(1),
      .INTENDED_DEVICE_FAMILY("Cyclone V")
   )
   channel_id_data_ch_fifo
   (
      .clock(clock),
      .resetn(resetn),
      // AVST avst_in
      .avst_in_valid(avs_channel_id_data_ch_write_valid),
      .avst_in_ready(avs_channel_id_data_ch_write_ready),
      .avst_in_data(avs_channel_id_data_ch_write_data),
      // AVST avst_out
      .avst_out_valid(avs_channel_id_data_ch_read_valid),
      .avst_out_ready(avs_channel_id_data_ch_read_ready),
      .avst_out_data(avs_channel_id_data_ch_read_data),
      .profile_fifosize(avs_channel_id_data_ch_fifosize),
      .almost_full(avs_channel_id_data_ch_write_almostfull)
   );

   assign avm_channel_id_data_ch_write_almostfull = avs_channel_id_data_ch_write_almostfull;
   assign avs_channel_id_weight_ch_write_valid = avm_channel_id_weight_ch_write_valid;
   assign avm_channel_id_weight_ch_write_ready = avs_channel_id_weight_ch_write_ready;
   assign avs_channel_id_weight_ch_write_data = avm_channel_id_weight_ch_write_data;
   assign avm_channel_id_weight_ch_read_valid = avs_channel_id_weight_ch_read_valid;
   assign avs_channel_id_weight_ch_read_ready = avm_channel_id_weight_ch_read_ready;
   assign avm_channel_id_weight_ch_read_data = avs_channel_id_weight_ch_read_data;
   // INST channel_id_weight_ch_fifo of acl_channel_fifo
   acl_channel_fifo
   #(
      .FIFO_DEPTH(0),
      .DATA_W(512),
      .ADJUST_FOR_LATENCY(1),
      .INTENDED_DEVICE_FAMILY("Cyclone V")
   )
   channel_id_weight_ch_fifo
   (
      .clock(clock),
      .resetn(resetn),
      // AVST avst_in
      .avst_in_valid(avs_channel_id_weight_ch_write_valid),
      .avst_in_ready(avs_channel_id_weight_ch_write_ready),
      .avst_in_data(avs_channel_id_weight_ch_write_data),
      // AVST avst_out
      .avst_out_valid(avs_channel_id_weight_ch_read_valid),
      .avst_out_ready(avs_channel_id_weight_ch_read_ready),
      .avst_out_data(avs_channel_id_weight_ch_read_data),
      .profile_fifosize(avs_channel_id_weight_ch_fifosize),
      .almost_full(avs_channel_id_weight_ch_write_almostfull)
   );

   assign avm_channel_id_weight_ch_write_almostfull = avs_channel_id_weight_ch_write_almostfull;
   assign avs_channel_id_bypass_ch_write_valid = avm_channel_id_bypass_ch_write_valid;
   assign avm_channel_id_bypass_ch_write_ready = avs_channel_id_bypass_ch_write_ready;
   assign avs_channel_id_bypass_ch_write_data = avm_channel_id_bypass_ch_write_data;
   assign avm_channel_id_bypass_ch_read_valid = avs_channel_id_bypass_ch_read_valid;
   assign avs_channel_id_bypass_ch_read_ready = avm_channel_id_bypass_ch_read_ready;
   assign avm_channel_id_bypass_ch_read_data = avs_channel_id_bypass_ch_read_data;
   // INST channel_id_bypass_ch_fifo of acl_channel_fifo
   acl_channel_fifo
   #(
      .FIFO_DEPTH(15),
      .DATA_W(128),
      .ADJUST_FOR_LATENCY(1),
      .INTENDED_DEVICE_FAMILY("Cyclone V")
   )
   channel_id_bypass_ch_fifo
   (
      .clock(clock),
      .resetn(resetn),
      // AVST avst_in
      .avst_in_valid(avs_channel_id_bypass_ch_write_valid),
      .avst_in_ready(avs_channel_id_bypass_ch_write_ready),
      .avst_in_data(avs_channel_id_bypass_ch_write_data),
      // AVST avst_out
      .avst_out_valid(avs_channel_id_bypass_ch_read_valid),
      .avst_out_ready(avs_channel_id_bypass_ch_read_ready),
      .avst_out_data(avs_channel_id_bypass_ch_read_data),
      .profile_fifosize(avs_channel_id_bypass_ch_fifosize),
      .almost_full(avs_channel_id_bypass_ch_write_almostfull)
   );

   assign avm_channel_id_bypass_ch_write_almostfull = avs_channel_id_bypass_ch_write_almostfull;
   assign avs_channel_id_conv_ch_write_valid = avm_channel_id_conv_ch_write_valid;
   assign avm_channel_id_conv_ch_write_ready = avs_channel_id_conv_ch_write_ready;
   assign avs_channel_id_conv_ch_write_data = avm_channel_id_conv_ch_write_data;
   assign avm_channel_id_conv_ch_read_valid = avs_channel_id_conv_ch_read_valid;
   assign avs_channel_id_conv_ch_read_ready = avm_channel_id_conv_ch_read_ready;
   assign avm_channel_id_conv_ch_read_data = avs_channel_id_conv_ch_read_data;
   // INST channel_id_conv_ch_fifo of acl_channel_fifo
   acl_channel_fifo
   #(
      .FIFO_DEPTH(0),
      .DATA_W(128),
      .ADJUST_FOR_LATENCY(1),
      .INTENDED_DEVICE_FAMILY("Cyclone V")
   )
   channel_id_conv_ch_fifo
   (
      .clock(clock),
      .resetn(resetn),
      // AVST avst_in
      .avst_in_valid(avs_channel_id_conv_ch_write_valid),
      .avst_in_ready(avs_channel_id_conv_ch_write_ready),
      .avst_in_data(avs_channel_id_conv_ch_write_data),
      // AVST avst_out
      .avst_out_valid(avs_channel_id_conv_ch_read_valid),
      .avst_out_ready(avs_channel_id_conv_ch_read_ready),
      .avst_out_data(avs_channel_id_conv_ch_read_data),
      .profile_fifosize(avs_channel_id_conv_ch_fifosize),
      .almost_full(avs_channel_id_conv_ch_write_almostfull)
   );

   assign avm_channel_id_conv_ch_write_almostfull = avs_channel_id_conv_ch_write_almostfull;
   assign avs_channel_id_pool_ch_write_valid = avm_channel_id_pool_ch_write_valid;
   assign avm_channel_id_pool_ch_write_ready = avs_channel_id_pool_ch_write_ready;
   assign avs_channel_id_pool_ch_write_data = avm_channel_id_pool_ch_write_data;
   assign avm_channel_id_pool_ch_read_valid = avs_channel_id_pool_ch_read_valid;
   assign avs_channel_id_pool_ch_read_ready = avm_channel_id_pool_ch_read_ready;
   assign avm_channel_id_pool_ch_read_data = avs_channel_id_pool_ch_read_data;
   // INST channel_id_pool_ch_fifo of acl_channel_fifo
   acl_channel_fifo
   #(
      .FIFO_DEPTH(0),
      .DATA_W(128),
      .ADJUST_FOR_LATENCY(1),
      .INTENDED_DEVICE_FAMILY("Cyclone V")
   )
   channel_id_pool_ch_fifo
   (
      .clock(clock),
      .resetn(resetn),
      // AVST avst_in
      .avst_in_valid(avs_channel_id_pool_ch_write_valid),
      .avst_in_ready(avs_channel_id_pool_ch_write_ready),
      .avst_in_data(avs_channel_id_pool_ch_write_data),
      // AVST avst_out
      .avst_out_valid(avs_channel_id_pool_ch_read_valid),
      .avst_out_ready(avs_channel_id_pool_ch_read_ready),
      .avst_out_data(avs_channel_id_pool_ch_read_data),
      .profile_fifosize(avs_channel_id_pool_ch_fifosize),
      .almost_full(avs_channel_id_pool_ch_write_almostfull)
   );

   assign avm_channel_id_pool_ch_write_almostfull = avs_channel_id_pool_ch_write_almostfull;
endmodule

/////////////////////////////////////////////////////////////////
// MODULE coreConv_top_wrapper_0
/////////////////////////////////////////////////////////////////
module coreConv_top_wrapper_0
(
   input logic start,
   input logic [119:0] kernel_arguments,
   output logic kernel_valid_in,
   output logic kernel_valid_out,
   output logic has_a_write_pending,
   output logic has_a_lsu_active,
   input logic clock,
   input logic resetn,
   input logic clock2x,
   // AVST avm_channel_id_bias_ch_read
   input logic avm_channel_id_bias_ch_read_valid,
   output logic avm_channel_id_bias_ch_read_ready,
   input logic [127:0] avm_channel_id_bias_ch_read_data,
   // AVST avm_channel_id_data_ch_read
   input logic avm_channel_id_data_ch_read_valid,
   output logic avm_channel_id_data_ch_read_ready,
   input logic [511:0] avm_channel_id_data_ch_read_data,
   // AVST avm_channel_id_weight_ch_read
   input logic avm_channel_id_weight_ch_read_valid,
   output logic avm_channel_id_weight_ch_read_ready,
   input logic [511:0] avm_channel_id_weight_ch_read_data,
   // AVST avm_channel_id_bypass_ch_write
   output logic avm_channel_id_bypass_ch_write_valid,
   input logic avm_channel_id_bypass_ch_write_ready,
   output logic [127:0] avm_channel_id_bypass_ch_write_data,
   input logic avm_channel_id_bypass_ch_write_almostfull,
   // AVST avm_channel_id_conv_ch_write
   output logic avm_channel_id_conv_ch_write_valid,
   input logic avm_channel_id_conv_ch_write_ready,
   output logic [127:0] avm_channel_id_conv_ch_write_data,
   input logic avm_channel_id_conv_ch_write_almostfull
);
   logic lmem_invalid_single_bit;

   // INST kernel of coreConv_function_wrapper
   coreConv_function_wrapper kernel
   (
      .local_router_hang(lmem_invalid_single_bit),
      .start(start),
      .kernel_arguments(kernel_arguments),
      .kernel_valid_in(kernel_valid_in),
      .kernel_valid_out(kernel_valid_out),
      .has_a_write_pending(has_a_write_pending),
      .has_a_lsu_active(has_a_lsu_active),
      .clock(clock),
      .resetn(resetn),
      .clock2x(clock2x),
      // AVST avst_local_bb2__bias_ch_inst0
      .avst_local_bb2__bias_ch_inst0_valid(avm_channel_id_bias_ch_read_valid),
      .avst_local_bb2__bias_ch_inst0_ready(avm_channel_id_bias_ch_read_ready),
      .avst_local_bb2__bias_ch_inst0_data(avm_channel_id_bias_ch_read_data),
      // AVST avst_local_bb3__data_ch_inst0
      .avst_local_bb3__data_ch_inst0_valid(avm_channel_id_data_ch_read_valid),
      .avst_local_bb3__data_ch_inst0_ready(avm_channel_id_data_ch_read_ready),
      .avst_local_bb3__data_ch_inst0_data(avm_channel_id_data_ch_read_data),
      // AVST avst_local_bb3__weight_ch_inst0
      .avst_local_bb3__weight_ch_inst0_valid(avm_channel_id_weight_ch_read_valid),
      .avst_local_bb3__weight_ch_inst0_ready(avm_channel_id_weight_ch_read_ready),
      .avst_local_bb3__weight_ch_inst0_data(avm_channel_id_weight_ch_read_data),
      // AVST avst_local_bb4__bypass_ch_inst0
      .avst_local_bb4__bypass_ch_inst0_valid(avm_channel_id_bypass_ch_write_valid),
      .avst_local_bb4__bypass_ch_inst0_ready(avm_channel_id_bypass_ch_write_ready),
      .avst_local_bb4__bypass_ch_inst0_data(avm_channel_id_bypass_ch_write_data),
      .avst_local_bb4__bypass_ch_inst0_almostfull(avm_channel_id_bypass_ch_write_almostfull),
      // AVST avst_local_bb4__conv_ch_inst0
      .avst_local_bb4__conv_ch_inst0_valid(avm_channel_id_conv_ch_write_valid),
      .avst_local_bb4__conv_ch_inst0_ready(avm_channel_id_conv_ch_write_ready),
      .avst_local_bb4__conv_ch_inst0_data(avm_channel_id_conv_ch_write_data),
      .avst_local_bb4__conv_ch_inst0_almostfull(avm_channel_id_conv_ch_write_almostfull)
   );

   assign lmem_invalid_single_bit = 'b0;
endmodule

/////////////////////////////////////////////////////////////////
// MODULE lrn_top_wrapper_0
/////////////////////////////////////////////////////////////////
module lrn_top_wrapper_0
(
   input logic start,
   input logic [151:0] kernel_arguments,
   input logic [31:0] work_dim,
   input logic [31:0] global_offset [2:0],
   output logic kernel_valid_out,
   output logic has_a_write_pending,
   output logic has_a_lsu_active,
   input logic [31:0] global_id [2:0],
   input logic [31:0] local_id [2:0],
   input logic [31:0] group_id [2:0],
   input logic [31:0] global_size [2:0],
   input logic [31:0] local_size [2:0],
   input logic [31:0] num_groups [2:0],
   input logic [31:0] workgroup_size,
   output logic kernel_stall_out,
   input logic kernel_valid_in,
   input logic clock,
   input logic resetn,
   input logic clock2x,
   // AVM avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0
   output logic avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_enable,
   output logic avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_read,
   output logic avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_write,
   output logic [4:0] avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_burstcount,
   output logic [29:0] avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_address,
   output logic [255:0] avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_writedata,
   output logic [31:0] avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_byteenable,
   input logic avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_waitrequest,
   input logic [255:0] avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_readdata,
   input logic avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_readdatavalid,
   input logic avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_writeack,
   // AVM avm_local_bb3_st_c0_exe111_inst0
   output logic avm_local_bb3_st_c0_exe111_inst0_enable,
   output logic avm_local_bb3_st_c0_exe111_inst0_read,
   output logic avm_local_bb3_st_c0_exe111_inst0_write,
   output logic [4:0] avm_local_bb3_st_c0_exe111_inst0_burstcount,
   output logic [29:0] avm_local_bb3_st_c0_exe111_inst0_address,
   output logic [255:0] avm_local_bb3_st_c0_exe111_inst0_writedata,
   output logic [31:0] avm_local_bb3_st_c0_exe111_inst0_byteenable,
   input logic avm_local_bb3_st_c0_exe111_inst0_waitrequest,
   input logic [255:0] avm_local_bb3_st_c0_exe111_inst0_readdata,
   input logic avm_local_bb3_st_c0_exe111_inst0_readdatavalid,
   input logic avm_local_bb3_st_c0_exe111_inst0_writeack
);
   genvar __i;
   genvar __j;
   genvar __k;
   logic lmem_invalid_single_bit;
   logic [1:0] lmem_invalid_aspaces;
   logic local_avm_aspace42_enable [1][9];
   logic local_avm_aspace42_read [1][9];
   logic local_avm_aspace42_write [1][9];
   logic local_avm_aspace42_burstcount [1][9];
   logic [31:0] local_avm_aspace42_address [1][9];
   logic [15:0] local_avm_aspace42_writedata [1][9];
   logic [1:0] local_avm_aspace42_byteenable [1][9];
   logic local_avm_aspace42_waitrequest [1][9];
   logic [15:0] local_avm_aspace42_readdata [1][9];
   logic local_avm_aspace42_readdatavalid [1][9];
   logic local_avm_aspace42_writeack [1][9];
   logic local_avm_aspace43_enable [1][2];
   logic local_avm_aspace43_read [1][2];
   logic local_avm_aspace43_write [1][2];
   logic local_avm_aspace43_burstcount [1][2];
   logic [31:0] local_avm_aspace43_address [1][2];
   logic [31:0] local_avm_aspace43_writedata [1][2];
   logic [3:0] local_avm_aspace43_byteenable [1][2];
   logic local_avm_aspace43_waitrequest [1][2];
   logic [31:0] local_avm_aspace43_readdata [1][2];
   logic local_avm_aspace43_readdatavalid [1][2];
   logic local_avm_aspace43_writeack [1][2];

   // INST kernel of lrn_function_wrapper
   lrn_function_wrapper kernel
   (
      .local_router_hang(lmem_invalid_single_bit),
      .start(start),
      .kernel_arguments(kernel_arguments),
      .work_dim(work_dim),
      .global_offset_0(global_offset[0]),
      .global_offset_1(global_offset[1]),
      .global_offset_2(global_offset[2]),
      .kernel_valid_out(kernel_valid_out),
      .has_a_write_pending(has_a_write_pending),
      .has_a_lsu_active(has_a_lsu_active),
      .global_id_0(global_id[0]),
      .global_id_1(global_id[1]),
      .global_id_2(global_id[2]),
      .local_id_0(local_id[0]),
      .local_id_1(local_id[1]),
      .local_id_2(local_id[2]),
      .group_id_0(group_id[0]),
      .group_id_1(group_id[1]),
      .group_id_2(group_id[2]),
      .global_size_0(global_size[0]),
      .global_size_1(global_size[1]),
      .global_size_2(global_size[2]),
      .local_size_0(local_size[0]),
      .local_size_1(local_size[1]),
      .local_size_2(local_size[2]),
      .num_groups_0(num_groups[0]),
      .num_groups_1(num_groups[1]),
      .num_groups_2(num_groups[2]),
      .workgroup_size(workgroup_size),
      .kernel_stall_out(kernel_stall_out),
      .kernel_valid_in(kernel_valid_in),
      .clock(clock),
      .resetn(resetn),
      .clock2x(clock2x),
      // AVM avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0
      .avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_enable(avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_enable),
      .avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_read(avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_read),
      .avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_write(avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_write),
      .avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_burstcount(avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_burstcount),
      .avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_address(avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_address),
      .avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_writedata(avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_writedata),
      .avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_byteenable(avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_byteenable),
      .avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_waitrequest(avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_waitrequest),
      .avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_readdata(avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_readdata),
      .avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_readdatavalid(avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_readdatavalid),
      .avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_writeack(avm_local_bb1_ld_memcoalesce_bottom_load_0_inst0_writeack),
      // AVM avm_local_bb3_st_c0_exe111_inst0
      .avm_local_bb3_st_c0_exe111_inst0_enable(avm_local_bb3_st_c0_exe111_inst0_enable),
      .avm_local_bb3_st_c0_exe111_inst0_read(avm_local_bb3_st_c0_exe111_inst0_read),
      .avm_local_bb3_st_c0_exe111_inst0_write(avm_local_bb3_st_c0_exe111_inst0_write),
      .avm_local_bb3_st_c0_exe111_inst0_burstcount(avm_local_bb3_st_c0_exe111_inst0_burstcount),
      .avm_local_bb3_st_c0_exe111_inst0_address(avm_local_bb3_st_c0_exe111_inst0_address),
      .avm_local_bb3_st_c0_exe111_inst0_writedata(avm_local_bb3_st_c0_exe111_inst0_writedata),
      .avm_local_bb3_st_c0_exe111_inst0_byteenable(avm_local_bb3_st_c0_exe111_inst0_byteenable),
      .avm_local_bb3_st_c0_exe111_inst0_waitrequest(avm_local_bb3_st_c0_exe111_inst0_waitrequest),
      .avm_local_bb3_st_c0_exe111_inst0_readdata(avm_local_bb3_st_c0_exe111_inst0_readdata),
      .avm_local_bb3_st_c0_exe111_inst0_readdatavalid(avm_local_bb3_st_c0_exe111_inst0_readdatavalid),
      .avm_local_bb3_st_c0_exe111_inst0_writeack(avm_local_bb3_st_c0_exe111_inst0_writeack),
      // AVM avm_local_bb1_st__inst0
      .avm_local_bb1_st__inst0_enable(local_avm_aspace42_enable[0][0]),
      .avm_local_bb1_st__inst0_read(local_avm_aspace42_read[0][0]),
      .avm_local_bb1_st__inst0_write(local_avm_aspace42_write[0][0]),
      .avm_local_bb1_st__inst0_burstcount(local_avm_aspace42_burstcount[0][0]),
      .avm_local_bb1_st__inst0_address(local_avm_aspace42_address[0][0]),
      .avm_local_bb1_st__inst0_writedata(local_avm_aspace42_writedata[0][0]),
      .avm_local_bb1_st__inst0_byteenable(local_avm_aspace42_byteenable[0][0]),
      .avm_local_bb1_st__inst0_waitrequest(local_avm_aspace42_waitrequest[0][0]),
      .avm_local_bb1_st__inst0_readdata(local_avm_aspace42_readdata[0][0]),
      .avm_local_bb1_st__inst0_readdatavalid(local_avm_aspace42_readdatavalid[0][0]),
      .avm_local_bb1_st__inst0_writeack(local_avm_aspace42_writeack[0][0]),
      // AVM avm_local_bb1_st__u0_inst0
      .avm_local_bb1_st__u0_inst0_enable(local_avm_aspace42_enable[0][1]),
      .avm_local_bb1_st__u0_inst0_read(local_avm_aspace42_read[0][1]),
      .avm_local_bb1_st__u0_inst0_write(local_avm_aspace42_write[0][1]),
      .avm_local_bb1_st__u0_inst0_burstcount(local_avm_aspace42_burstcount[0][1]),
      .avm_local_bb1_st__u0_inst0_address(local_avm_aspace42_address[0][1]),
      .avm_local_bb1_st__u0_inst0_writedata(local_avm_aspace42_writedata[0][1]),
      .avm_local_bb1_st__u0_inst0_byteenable(local_avm_aspace42_byteenable[0][1]),
      .avm_local_bb1_st__u0_inst0_waitrequest(local_avm_aspace42_waitrequest[0][1]),
      .avm_local_bb1_st__u0_inst0_readdata(local_avm_aspace42_readdata[0][1]),
      .avm_local_bb1_st__u0_inst0_readdatavalid(local_avm_aspace42_readdatavalid[0][1]),
      .avm_local_bb1_st__u0_inst0_writeack(local_avm_aspace42_writeack[0][1]),
      // AVM avm_local_bb1_st_memcoalesce_null_insertValue_13_inst0
      .avm_local_bb1_st_memcoalesce_null_insertValue_13_inst0_enable(local_avm_aspace42_enable[0][2]),
      .avm_local_bb1_st_memcoalesce_null_insertValue_13_inst0_read(local_avm_aspace42_read[0][2]),
      .avm_local_bb1_st_memcoalesce_null_insertValue_13_inst0_write(local_avm_aspace42_write[0][2]),
      .avm_local_bb1_st_memcoalesce_null_insertValue_13_inst0_burstcount(local_avm_aspace42_burstcount[0][2]),
      .avm_local_bb1_st_memcoalesce_null_insertValue_13_inst0_address(local_avm_aspace42_address[0][2]),
      .avm_local_bb1_st_memcoalesce_null_insertValue_13_inst0_writedata(local_avm_aspace42_writedata[0][2]),
      .avm_local_bb1_st_memcoalesce_null_insertValue_13_inst0_byteenable(local_avm_aspace42_byteenable[0][2]),
      .avm_local_bb1_st_memcoalesce_null_insertValue_13_inst0_waitrequest(local_avm_aspace42_waitrequest[0][2]),
      .avm_local_bb1_st_memcoalesce_null_insertValue_13_inst0_readdata(local_avm_aspace42_readdata[0][2]),
      .avm_local_bb1_st_memcoalesce_null_insertValue_13_inst0_readdatavalid(local_avm_aspace42_readdatavalid[0][2]),
      .avm_local_bb1_st_memcoalesce_null_insertValue_13_inst0_writeack(local_avm_aspace42_writeack[0][2]),
      // AVM avm_local_bb1_st_memcoalesce_null_insertValue_1_inst0
      .avm_local_bb1_st_memcoalesce_null_insertValue_1_inst0_enable(local_avm_aspace42_enable[0][3]),
      .avm_local_bb1_st_memcoalesce_null_insertValue_1_inst0_read(local_avm_aspace42_read[0][3]),
      .avm_local_bb1_st_memcoalesce_null_insertValue_1_inst0_write(local_avm_aspace42_write[0][3]),
      .avm_local_bb1_st_memcoalesce_null_insertValue_1_inst0_burstcount(local_avm_aspace42_burstcount[0][3]),
      .avm_local_bb1_st_memcoalesce_null_insertValue_1_inst0_address(local_avm_aspace42_address[0][3]),
      .avm_local_bb1_st_memcoalesce_null_insertValue_1_inst0_writedata(local_avm_aspace42_writedata[0][3]),
      .avm_local_bb1_st_memcoalesce_null_insertValue_1_inst0_byteenable(local_avm_aspace42_byteenable[0][3]),
      .avm_local_bb1_st_memcoalesce_null_insertValue_1_inst0_waitrequest(local_avm_aspace42_waitrequest[0][3]),
      .avm_local_bb1_st_memcoalesce_null_insertValue_1_inst0_readdata(local_avm_aspace42_readdata[0][3]),
      .avm_local_bb1_st_memcoalesce_null_insertValue_1_inst0_readdatavalid(local_avm_aspace42_readdatavalid[0][3]),
      .avm_local_bb1_st_memcoalesce_null_insertValue_1_inst0_writeack(local_avm_aspace42_writeack[0][3]),
      // AVM avm_local_bb2_ld__inst0
      .avm_local_bb2_ld__inst0_enable(local_avm_aspace42_enable[0][4]),
      .avm_local_bb2_ld__inst0_read(local_avm_aspace42_read[0][4]),
      .avm_local_bb2_ld__inst0_write(local_avm_aspace42_write[0][4]),
      .avm_local_bb2_ld__inst0_burstcount(local_avm_aspace42_burstcount[0][4]),
      .avm_local_bb2_ld__inst0_address(local_avm_aspace42_address[0][4]),
      .avm_local_bb2_ld__inst0_writedata(local_avm_aspace42_writedata[0][4]),
      .avm_local_bb2_ld__inst0_byteenable(local_avm_aspace42_byteenable[0][4]),
      .avm_local_bb2_ld__inst0_waitrequest(local_avm_aspace42_waitrequest[0][4]),
      .avm_local_bb2_ld__inst0_readdata(local_avm_aspace42_readdata[0][4]),
      .avm_local_bb2_ld__inst0_readdatavalid(local_avm_aspace42_readdatavalid[0][4]),
      .avm_local_bb2_ld__inst0_writeack(local_avm_aspace42_writeack[0][4]),
      // AVM avm_local_bb2_ld__u1_inst0
      .avm_local_bb2_ld__u1_inst0_enable(local_avm_aspace42_enable[0][5]),
      .avm_local_bb2_ld__u1_inst0_read(local_avm_aspace42_read[0][5]),
      .avm_local_bb2_ld__u1_inst0_write(local_avm_aspace42_write[0][5]),
      .avm_local_bb2_ld__u1_inst0_burstcount(local_avm_aspace42_burstcount[0][5]),
      .avm_local_bb2_ld__u1_inst0_address(local_avm_aspace42_address[0][5]),
      .avm_local_bb2_ld__u1_inst0_writedata(local_avm_aspace42_writedata[0][5]),
      .avm_local_bb2_ld__u1_inst0_byteenable(local_avm_aspace42_byteenable[0][5]),
      .avm_local_bb2_ld__u1_inst0_waitrequest(local_avm_aspace42_waitrequest[0][5]),
      .avm_local_bb2_ld__u1_inst0_readdata(local_avm_aspace42_readdata[0][5]),
      .avm_local_bb2_ld__u1_inst0_readdatavalid(local_avm_aspace42_readdatavalid[0][5]),
      .avm_local_bb2_ld__u1_inst0_writeack(local_avm_aspace42_writeack[0][5]),
      // AVM avm_local_bb2_ld__u2_inst0
      .avm_local_bb2_ld__u2_inst0_enable(local_avm_aspace42_enable[0][6]),
      .avm_local_bb2_ld__u2_inst0_read(local_avm_aspace42_read[0][6]),
      .avm_local_bb2_ld__u2_inst0_write(local_avm_aspace42_write[0][6]),
      .avm_local_bb2_ld__u2_inst0_burstcount(local_avm_aspace42_burstcount[0][6]),
      .avm_local_bb2_ld__u2_inst0_address(local_avm_aspace42_address[0][6]),
      .avm_local_bb2_ld__u2_inst0_writedata(local_avm_aspace42_writedata[0][6]),
      .avm_local_bb2_ld__u2_inst0_byteenable(local_avm_aspace42_byteenable[0][6]),
      .avm_local_bb2_ld__u2_inst0_waitrequest(local_avm_aspace42_waitrequest[0][6]),
      .avm_local_bb2_ld__u2_inst0_readdata(local_avm_aspace42_readdata[0][6]),
      .avm_local_bb2_ld__u2_inst0_readdatavalid(local_avm_aspace42_readdatavalid[0][6]),
      .avm_local_bb2_ld__u2_inst0_writeack(local_avm_aspace42_writeack[0][6]),
      // AVM avm_local_bb2_ld__u3_inst0
      .avm_local_bb2_ld__u3_inst0_enable(local_avm_aspace42_enable[0][7]),
      .avm_local_bb2_ld__u3_inst0_read(local_avm_aspace42_read[0][7]),
      .avm_local_bb2_ld__u3_inst0_write(local_avm_aspace42_write[0][7]),
      .avm_local_bb2_ld__u3_inst0_burstcount(local_avm_aspace42_burstcount[0][7]),
      .avm_local_bb2_ld__u3_inst0_address(local_avm_aspace42_address[0][7]),
      .avm_local_bb2_ld__u3_inst0_writedata(local_avm_aspace42_writedata[0][7]),
      .avm_local_bb2_ld__u3_inst0_byteenable(local_avm_aspace42_byteenable[0][7]),
      .avm_local_bb2_ld__u3_inst0_waitrequest(local_avm_aspace42_waitrequest[0][7]),
      .avm_local_bb2_ld__u3_inst0_readdata(local_avm_aspace42_readdata[0][7]),
      .avm_local_bb2_ld__u3_inst0_readdatavalid(local_avm_aspace42_readdatavalid[0][7]),
      .avm_local_bb2_ld__u3_inst0_writeack(local_avm_aspace42_writeack[0][7]),
      // AVM avm_local_bb2_ld__u4_inst0
      .avm_local_bb2_ld__u4_inst0_enable(local_avm_aspace42_enable[0][8]),
      .avm_local_bb2_ld__u4_inst0_read(local_avm_aspace42_read[0][8]),
      .avm_local_bb2_ld__u4_inst0_write(local_avm_aspace42_write[0][8]),
      .avm_local_bb2_ld__u4_inst0_burstcount(local_avm_aspace42_burstcount[0][8]),
      .avm_local_bb2_ld__u4_inst0_address(local_avm_aspace42_address[0][8]),
      .avm_local_bb2_ld__u4_inst0_writedata(local_avm_aspace42_writedata[0][8]),
      .avm_local_bb2_ld__u4_inst0_byteenable(local_avm_aspace42_byteenable[0][8]),
      .avm_local_bb2_ld__u4_inst0_waitrequest(local_avm_aspace42_waitrequest[0][8]),
      .avm_local_bb2_ld__u4_inst0_readdata(local_avm_aspace42_readdata[0][8]),
      .avm_local_bb2_ld__u4_inst0_readdatavalid(local_avm_aspace42_readdatavalid[0][8]),
      .avm_local_bb2_ld__u4_inst0_writeack(local_avm_aspace42_writeack[0][8]),
      // AVM avm_local_bb2_st__fsi_inst0
      .avm_local_bb2_st__fsi_inst0_enable(local_avm_aspace43_enable[0][0]),
      .avm_local_bb2_st__fsi_inst0_read(local_avm_aspace43_read[0][0]),
      .avm_local_bb2_st__fsi_inst0_write(local_avm_aspace43_write[0][0]),
      .avm_local_bb2_st__fsi_inst0_burstcount(local_avm_aspace43_burstcount[0][0]),
      .avm_local_bb2_st__fsi_inst0_address(local_avm_aspace43_address[0][0]),
      .avm_local_bb2_st__fsi_inst0_writedata(local_avm_aspace43_writedata[0][0]),
      .avm_local_bb2_st__fsi_inst0_byteenable(local_avm_aspace43_byteenable[0][0]),
      .avm_local_bb2_st__fsi_inst0_waitrequest(local_avm_aspace43_waitrequest[0][0]),
      .avm_local_bb2_st__fsi_inst0_readdata(local_avm_aspace43_readdata[0][0]),
      .avm_local_bb2_st__fsi_inst0_readdatavalid(local_avm_aspace43_readdatavalid[0][0]),
      .avm_local_bb2_st__fsi_inst0_writeack(local_avm_aspace43_writeack[0][0]),
      // AVM avm_local_bb3_ld_memcoalesce_null_load_0_inst0
      .avm_local_bb3_ld_memcoalesce_null_load_0_inst0_enable(local_avm_aspace43_enable[0][1]),
      .avm_local_bb3_ld_memcoalesce_null_load_0_inst0_read(local_avm_aspace43_read[0][1]),
      .avm_local_bb3_ld_memcoalesce_null_load_0_inst0_write(local_avm_aspace43_write[0][1]),
      .avm_local_bb3_ld_memcoalesce_null_load_0_inst0_burstcount(local_avm_aspace43_burstcount[0][1]),
      .avm_local_bb3_ld_memcoalesce_null_load_0_inst0_address(local_avm_aspace43_address[0][1]),
      .avm_local_bb3_ld_memcoalesce_null_load_0_inst0_writedata(local_avm_aspace43_writedata[0][1]),
      .avm_local_bb3_ld_memcoalesce_null_load_0_inst0_byteenable(local_avm_aspace43_byteenable[0][1]),
      .avm_local_bb3_ld_memcoalesce_null_load_0_inst0_waitrequest(local_avm_aspace43_waitrequest[0][1]),
      .avm_local_bb3_ld_memcoalesce_null_load_0_inst0_readdata(local_avm_aspace43_readdata[0][1]),
      .avm_local_bb3_ld_memcoalesce_null_load_0_inst0_readdatavalid(local_avm_aspace43_readdatavalid[0][1]),
      .avm_local_bb3_ld_memcoalesce_null_load_0_inst0_writeack(local_avm_aspace43_writeack[0][1])
   );

   assign lmem_invalid_single_bit = |lmem_invalid_aspaces;
   generate
   begin:local_mem_system_aspace42
      logic local_icm_arb_request [1][9];
      logic local_icm_arb_enable [1][9];
      logic local_icm_arb_read [1][9];
      logic local_icm_arb_write [1][9];
      logic local_icm_arb_burstcount [1][9];
      logic [9:0] local_icm_arb_address [1][9];
      logic [15:0] local_icm_arb_writedata [1][9];
      logic [1:0] local_icm_arb_byteenable [1][9];
      logic local_icm_arb_stall [1][9];
      logic local_icm_wrp_ack [1][9];
      logic local_icm_rrp_datavalid [1][9];
      logic [15:0] local_icm_rrp_data [1][9];
      logic invalid_access_grps;

      for( __i = 0; __i < 1; __i = __i + 1 )
      begin:local_mem_group
         logic [8:0] invalid_access_terms;

         for( __j = 0; __j < 9; __j = __j + 1 )
         begin:master
            // INST avm_to_ic of acl_avm_to_ic
            acl_avm_to_ic
            #(
               .DATA_W(16),
               .WRITEDATA_W(16),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(32),
               .BYTEENA_W(2)
            )
            avm_to_ic
            (
               // AVM avm
               .avm_enable(local_avm_aspace42_enable[__i][__j]),
               .avm_read(local_avm_aspace42_read[__i][__j]),
               .avm_write(local_avm_aspace42_write[__i][__j]),
               .avm_burstcount(local_avm_aspace42_burstcount[__i][__j]),
               .avm_address(local_avm_aspace42_address[__i][__j]),
               .avm_writedata(local_avm_aspace42_writedata[__i][__j]),
               .avm_byteenable(local_avm_aspace42_byteenable[__i][__j]),
               .avm_waitrequest(local_avm_aspace42_waitrequest[__i][__j]),
               .avm_readdata(local_avm_aspace42_readdata[__i][__j]),
               .avm_readdatavalid(local_avm_aspace42_readdatavalid[__i][__j]),
               .avm_writeack(local_avm_aspace42_writeack[__i][__j]),
               // ICM ic
               .ic_arb_request(local_icm_arb_request[__i][__j]),
               .ic_arb_enable(local_icm_arb_enable[__i][__j]),
               .ic_arb_read(local_icm_arb_read[__i][__j]),
               .ic_arb_write(local_icm_arb_write[__i][__j]),
               .ic_arb_burstcount(local_icm_arb_burstcount[__i][__j]),
               .ic_arb_address(local_icm_arb_address[__i][__j]),
               .ic_arb_writedata(local_icm_arb_writedata[__i][__j]),
               .ic_arb_byteenable(local_icm_arb_byteenable[__i][__j]),
               .ic_arb_stall(local_icm_arb_stall[__i][__j]),
               .ic_wrp_ack(local_icm_wrp_ack[__i][__j]),
               .ic_rrp_datavalid(local_icm_rrp_datavalid[__i][__j]),
               .ic_rrp_data(local_icm_rrp_data[__i][__j])
            );

         end

         for( __j = 0; __j < 4; __j = __j + 1 )
         begin:bank
            logic port_enable [1:4];
            logic port_read [1:4];
            logic port_write [1:4];
            logic [7:0] port_address [1:4];
            logic [15:0] port_writedata [1:4];
            logic [1:0] port_byteenable [1:4];
            logic port_waitrequest [1:4];
            logic [15:0] port_readdata [1:4];
            logic port_readdatavalid [1:4];

            // INST mem0 of acl_mem2x
            acl_mem2x
            #(
               .INTENDED_DEVICE_FAMILY("Cyclone V"),
               .DEPTH_WORDS(192),
               .WIDTH(16),
               .ENABLED(0),
               .RDW_MODE("DONT_CARE"),
               .RAM_OPERATION_MODE("BIDIR_DUAL_PORT"),
               .PREFERRED_WIDTH(160),
               .RAM_BLOCK_TYPE("M10K")
            )
            mem0
            (
               .clk(clock),
               .clk2x(clock2x),
               .resetn(resetn),
               // AVS avs_port1
               .avs_port1_enable(port_enable[1]),
               .avs_port1_read(port_read[1]),
               .avs_port1_write(port_write[1]),
               .avs_port1_address(port_address[1]),
               .avs_port1_writedata(port_writedata[1]),
               .avs_port1_byteenable(port_byteenable[1]),
               .avs_port1_waitrequest(port_waitrequest[1]),
               .avs_port1_readdata(port_readdata[1]),
               .avs_port1_readdatavalid(port_readdatavalid[1]),
               // AVS avs_port2
               .avs_port2_enable(port_enable[2]),
               .avs_port2_read(port_read[2]),
               .avs_port2_write(port_write[2]),
               .avs_port2_address(port_address[2]),
               .avs_port2_writedata(port_writedata[2]),
               .avs_port2_byteenable(port_byteenable[2]),
               .avs_port2_waitrequest(port_waitrequest[2]),
               .avs_port2_readdata(port_readdata[2]),
               .avs_port2_readdatavalid(port_readdatavalid[2]),
               // AVS avs_port3
               .avs_port3_enable(port_enable[3]),
               .avs_port3_read(port_read[3]),
               .avs_port3_write(port_write[3]),
               .avs_port3_address(port_address[3]),
               .avs_port3_writedata(port_writedata[3]),
               .avs_port3_byteenable(port_byteenable[3]),
               .avs_port3_waitrequest(port_waitrequest[3]),
               .avs_port3_readdata(port_readdata[3]),
               .avs_port3_readdatavalid(port_readdatavalid[3]),
               // AVS avs_port4
               .avs_port4_enable(port_enable[4]),
               .avs_port4_read(port_read[4]),
               .avs_port4_write(port_write[4]),
               .avs_port4_address(port_address[4]),
               .avs_port4_writedata(port_writedata[4]),
               .avs_port4_byteenable(port_byteenable[4]),
               .avs_port4_waitrequest(port_waitrequest[4]),
               .avs_port4_readdata(port_readdata[4]),
               .avs_port4_readdatavalid(port_readdatavalid[4])
            );

         end

         for( __j = 0; __j < 9; __j = __j + 1 )
         begin:router
            logic b_arb_request [4];
            logic b_arb_enable [4];
            logic b_arb_read [4];
            logic b_arb_write [4];
            logic b_arb_burstcount [4];
            logic [7:0] b_arb_address [4];
            logic [15:0] b_arb_writedata [4];
            logic [1:0] b_arb_byteenable [4];
            logic b_arb_stall [4];
            logic b_wrp_ack [4];
            logic b_rrp_datavalid [4];
            logic [15:0] b_rrp_data [4];
            logic [3:0] bank_select;

            // INST router of acl_ic_local_mem_router
            acl_ic_local_mem_router
            #(
               .DATA_W(16),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(10),
               .BYTEENA_W(2),
               .NUM_BANKS(4)
            )
            router
            (
               .clock(clock),
               .resetn(resetn),
               .bank_select(bank_select),
               // ICM m
               .m_arb_request(local_icm_arb_request[__i][__j]),
               .m_arb_enable(local_icm_arb_enable[__i][__j]),
               .m_arb_read(local_icm_arb_read[__i][__j]),
               .m_arb_write(local_icm_arb_write[__i][__j]),
               .m_arb_burstcount(local_icm_arb_burstcount[__i][__j]),
               .m_arb_address(local_icm_arb_address[__i][__j]),
               .m_arb_writedata(local_icm_arb_writedata[__i][__j]),
               .m_arb_byteenable(local_icm_arb_byteenable[__i][__j]),
               .m_arb_stall(local_icm_arb_stall[__i][__j]),
               .m_wrp_ack(local_icm_wrp_ack[__i][__j]),
               .m_rrp_datavalid(local_icm_rrp_datavalid[__i][__j]),
               .m_rrp_data(local_icm_rrp_data[__i][__j]),
               // ICM b
               .b_arb_request(b_arb_request),
               .b_arb_enable(b_arb_enable),
               .b_arb_read(b_arb_read),
               .b_arb_write(b_arb_write),
               .b_arb_burstcount(b_arb_burstcount),
               .b_arb_address(b_arb_address),
               .b_arb_writedata(b_arb_writedata),
               .b_arb_byteenable(b_arb_byteenable),
               .b_arb_stall(b_arb_stall),
               .b_wrp_ack(b_wrp_ack),
               .b_rrp_datavalid(b_rrp_datavalid),
               .b_rrp_data(b_rrp_data)
            );

            assign bank_select[0] = (local_icm_arb_address[__i][__j][9:8] == 2'b00);
            assign bank_select[1] = (local_icm_arb_address[__i][__j][9:8] == 2'b01);
            assign bank_select[2] = (local_icm_arb_address[__i][__j][9:8] == 2'b10);
            assign bank_select[3] = (local_icm_arb_address[__i][__j][9:8] == 2'b11);
         end

         assign invalid_access_grps = |invalid_access_terms;
         // INST acl_ic_local_mem_router_terminator_m0b1 of acl_ic_local_mem_router_terminator
         acl_ic_local_mem_router_terminator
         #(
            .DATA_W(16)
         )
         acl_ic_local_mem_router_terminator_m0b1
         (
            .clock(clock),
            .resetn(resetn),
            .b_arb_request(router[0].b_arb_request[1]),
            .b_arb_read(router[0].b_arb_read[1]),
            .b_arb_write(router[0].b_arb_write[1]),
            .b_arb_stall(router[0].b_arb_stall[1]),
            .b_wrp_ack(router[0].b_wrp_ack[1]),
            .b_rrp_datavalid(router[0].b_rrp_datavalid[1]),
            .b_rrp_data(router[0].b_rrp_data[1]),
            .b_invalid_access(invalid_access_terms[0])
         );

         // INST acl_ic_local_mem_router_terminator_m0b2 of acl_ic_local_mem_router_terminator
         acl_ic_local_mem_router_terminator
         #(
            .DATA_W(16)
         )
         acl_ic_local_mem_router_terminator_m0b2
         (
            .clock(clock),
            .resetn(resetn),
            .b_arb_request(router[0].b_arb_request[2]),
            .b_arb_read(router[0].b_arb_read[2]),
            .b_arb_write(router[0].b_arb_write[2]),
            .b_arb_stall(router[0].b_arb_stall[2]),
            .b_wrp_ack(router[0].b_wrp_ack[2]),
            .b_rrp_datavalid(router[0].b_rrp_datavalid[2]),
            .b_rrp_data(router[0].b_rrp_data[2]),
            .b_invalid_access(invalid_access_terms[1])
         );

         // INST acl_ic_local_mem_router_terminator_m0b3 of acl_ic_local_mem_router_terminator
         acl_ic_local_mem_router_terminator
         #(
            .DATA_W(16)
         )
         acl_ic_local_mem_router_terminator_m0b3
         (
            .clock(clock),
            .resetn(resetn),
            .b_arb_request(router[0].b_arb_request[3]),
            .b_arb_read(router[0].b_arb_read[3]),
            .b_arb_write(router[0].b_arb_write[3]),
            .b_arb_stall(router[0].b_arb_stall[3]),
            .b_wrp_ack(router[0].b_wrp_ack[3]),
            .b_rrp_datavalid(router[0].b_rrp_datavalid[3]),
            .b_rrp_data(router[0].b_rrp_data[3]),
            .b_invalid_access(invalid_access_terms[2])
         );

         // INST acl_ic_local_mem_router_terminator_m1b0 of acl_ic_local_mem_router_terminator
         acl_ic_local_mem_router_terminator
         #(
            .DATA_W(16)
         )
         acl_ic_local_mem_router_terminator_m1b0
         (
            .clock(clock),
            .resetn(resetn),
            .b_arb_request(router[1].b_arb_request[0]),
            .b_arb_read(router[1].b_arb_read[0]),
            .b_arb_write(router[1].b_arb_write[0]),
            .b_arb_stall(router[1].b_arb_stall[0]),
            .b_wrp_ack(router[1].b_wrp_ack[0]),
            .b_rrp_datavalid(router[1].b_rrp_datavalid[0]),
            .b_rrp_data(router[1].b_rrp_data[0]),
            .b_invalid_access(invalid_access_terms[3])
         );

         // INST acl_ic_local_mem_router_terminator_m1b2 of acl_ic_local_mem_router_terminator
         acl_ic_local_mem_router_terminator
         #(
            .DATA_W(16)
         )
         acl_ic_local_mem_router_terminator_m1b2
         (
            .clock(clock),
            .resetn(resetn),
            .b_arb_request(router[1].b_arb_request[2]),
            .b_arb_read(router[1].b_arb_read[2]),
            .b_arb_write(router[1].b_arb_write[2]),
            .b_arb_stall(router[1].b_arb_stall[2]),
            .b_wrp_ack(router[1].b_wrp_ack[2]),
            .b_rrp_datavalid(router[1].b_rrp_datavalid[2]),
            .b_rrp_data(router[1].b_rrp_data[2]),
            .b_invalid_access(invalid_access_terms[4])
         );

         // INST acl_ic_local_mem_router_terminator_m2b1 of acl_ic_local_mem_router_terminator
         acl_ic_local_mem_router_terminator
         #(
            .DATA_W(16)
         )
         acl_ic_local_mem_router_terminator_m2b1
         (
            .clock(clock),
            .resetn(resetn),
            .b_arb_request(router[2].b_arb_request[1]),
            .b_arb_read(router[2].b_arb_read[1]),
            .b_arb_write(router[2].b_arb_write[1]),
            .b_arb_stall(router[2].b_arb_stall[1]),
            .b_wrp_ack(router[2].b_wrp_ack[1]),
            .b_rrp_datavalid(router[2].b_rrp_datavalid[1]),
            .b_rrp_data(router[2].b_rrp_data[1]),
            .b_invalid_access(invalid_access_terms[5])
         );

         // INST acl_ic_local_mem_router_terminator_m2b3 of acl_ic_local_mem_router_terminator
         acl_ic_local_mem_router_terminator
         #(
            .DATA_W(16)
         )
         acl_ic_local_mem_router_terminator_m2b3
         (
            .clock(clock),
            .resetn(resetn),
            .b_arb_request(router[2].b_arb_request[3]),
            .b_arb_read(router[2].b_arb_read[3]),
            .b_arb_write(router[2].b_arb_write[3]),
            .b_arb_stall(router[2].b_arb_stall[3]),
            .b_wrp_ack(router[2].b_wrp_ack[3]),
            .b_rrp_datavalid(router[2].b_rrp_datavalid[3]),
            .b_rrp_data(router[2].b_rrp_data[3]),
            .b_invalid_access(invalid_access_terms[6])
         );

         // INST acl_ic_local_mem_router_terminator_m3b0 of acl_ic_local_mem_router_terminator
         acl_ic_local_mem_router_terminator
         #(
            .DATA_W(16)
         )
         acl_ic_local_mem_router_terminator_m3b0
         (
            .clock(clock),
            .resetn(resetn),
            .b_arb_request(router[3].b_arb_request[0]),
            .b_arb_read(router[3].b_arb_read[0]),
            .b_arb_write(router[3].b_arb_write[0]),
            .b_arb_stall(router[3].b_arb_stall[0]),
            .b_wrp_ack(router[3].b_wrp_ack[0]),
            .b_rrp_datavalid(router[3].b_rrp_datavalid[0]),
            .b_rrp_data(router[3].b_rrp_data[0]),
            .b_invalid_access(invalid_access_terms[7])
         );

         // INST acl_ic_local_mem_router_terminator_m3b2 of acl_ic_local_mem_router_terminator
         acl_ic_local_mem_router_terminator
         #(
            .DATA_W(16)
         )
         acl_ic_local_mem_router_terminator_m3b2
         (
            .clock(clock),
            .resetn(resetn),
            .b_arb_request(router[3].b_arb_request[2]),
            .b_arb_read(router[3].b_arb_read[2]),
            .b_arb_write(router[3].b_arb_write[2]),
            .b_arb_stall(router[3].b_arb_stall[2]),
            .b_wrp_ack(router[3].b_wrp_ack[2]),
            .b_rrp_datavalid(router[3].b_rrp_datavalid[2]),
            .b_rrp_data(router[3].b_rrp_data[2]),
            .b_invalid_access(invalid_access_terms[8])
         );

         for( __j = 0; __j < 1; __j = __j + 1 )
         begin:port1bank0
            logic icm_in_arb_request [2];
            logic icm_in_arb_enable [2];
            logic icm_in_arb_read [2];
            logic icm_in_arb_write [2];
            logic icm_in_arb_burstcount [2];
            logic [7:0] icm_in_arb_address [2];
            logic [15:0] icm_in_arb_writedata [2];
            logic [1:0] icm_in_arb_byteenable [2];
            logic icm_in_arb_stall [2];
            logic icm_in_wrp_ack [2];
            logic icm_in_rrp_datavalid [2];
            logic [15:0] icm_in_rrp_data [2];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [15:0] icm_out_arb_writedata;
            logic [1:0] icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [15:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[2].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[2].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[2].b_arb_read[0];
            assign icm_in_arb_write[0] = router[2].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[2].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[2].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[2].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[2].b_arb_byteenable[0];
            assign router[2].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[2].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[2].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[2].b_rrp_data[0] = icm_in_rrp_data[0];
            assign icm_in_arb_request[1] = router[4].b_arb_request[0];
            assign icm_in_arb_enable[1] = router[4].b_arb_enable[0];
            assign icm_in_arb_read[1] = router[4].b_arb_read[0];
            assign icm_in_arb_write[1] = router[4].b_arb_write[0];
            assign icm_in_arb_burstcount[1] = router[4].b_arb_burstcount[0];
            assign icm_in_arb_address[1] = router[4].b_arb_address[0];
            assign icm_in_arb_writedata[1] = router[4].b_arb_writedata[0];
            assign icm_in_arb_byteenable[1] = router[4].b_arb_byteenable[0];
            assign router[4].b_arb_stall[0] = icm_in_arb_stall[1];
            assign router[4].b_wrp_ack[0] = icm_in_wrp_ack[1];
            assign router[4].b_rrp_datavalid[0] = icm_in_rrp_datavalid[1];
            assign router[4].b_rrp_data[0] = icm_in_rrp_data[1];
            // INST data_ic of conv_pipe_system_interconnect_0
            conv_pipe_system_interconnect_0 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[1] = icm_out_arb_enable;
            assign bank[0].port_read[1] = icm_out_arb_read;
            assign bank[0].port_write[1] = icm_out_arb_write;
            assign bank[0].port_address[1] = icm_out_arb_address;
            assign bank[0].port_writedata[1] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[1] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[1];
            assign icm_out_rrp_data = bank[0].port_readdata[1];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[1];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __j = 0; __j < 1; __j = __j + 1 )
         begin:port1bank1
            logic icm_in_arb_request [2];
            logic icm_in_arb_enable [2];
            logic icm_in_arb_read [2];
            logic icm_in_arb_write [2];
            logic icm_in_arb_burstcount [2];
            logic [7:0] icm_in_arb_address [2];
            logic [15:0] icm_in_arb_writedata [2];
            logic [1:0] icm_in_arb_byteenable [2];
            logic icm_in_arb_stall [2];
            logic icm_in_wrp_ack [2];
            logic icm_in_rrp_datavalid [2];
            logic [15:0] icm_in_rrp_data [2];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [15:0] icm_out_arb_writedata;
            logic [1:0] icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [15:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[3].b_arb_request[1];
            assign icm_in_arb_enable[0] = router[3].b_arb_enable[1];
            assign icm_in_arb_read[0] = router[3].b_arb_read[1];
            assign icm_in_arb_write[0] = router[3].b_arb_write[1];
            assign icm_in_arb_burstcount[0] = router[3].b_arb_burstcount[1];
            assign icm_in_arb_address[0] = router[3].b_arb_address[1];
            assign icm_in_arb_writedata[0] = router[3].b_arb_writedata[1];
            assign icm_in_arb_byteenable[0] = router[3].b_arb_byteenable[1];
            assign router[3].b_arb_stall[1] = icm_in_arb_stall[0];
            assign router[3].b_wrp_ack[1] = icm_in_wrp_ack[0];
            assign router[3].b_rrp_datavalid[1] = icm_in_rrp_datavalid[0];
            assign router[3].b_rrp_data[1] = icm_in_rrp_data[0];
            assign icm_in_arb_request[1] = router[4].b_arb_request[1];
            assign icm_in_arb_enable[1] = router[4].b_arb_enable[1];
            assign icm_in_arb_read[1] = router[4].b_arb_read[1];
            assign icm_in_arb_write[1] = router[4].b_arb_write[1];
            assign icm_in_arb_burstcount[1] = router[4].b_arb_burstcount[1];
            assign icm_in_arb_address[1] = router[4].b_arb_address[1];
            assign icm_in_arb_writedata[1] = router[4].b_arb_writedata[1];
            assign icm_in_arb_byteenable[1] = router[4].b_arb_byteenable[1];
            assign router[4].b_arb_stall[1] = icm_in_arb_stall[1];
            assign router[4].b_wrp_ack[1] = icm_in_wrp_ack[1];
            assign router[4].b_rrp_datavalid[1] = icm_in_rrp_datavalid[1];
            assign router[4].b_rrp_data[1] = icm_in_rrp_data[1];
            // INST data_ic of conv_pipe_system_interconnect_0
            conv_pipe_system_interconnect_0 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[1].port_enable[1] = icm_out_arb_enable;
            assign bank[1].port_read[1] = icm_out_arb_read;
            assign bank[1].port_write[1] = icm_out_arb_write;
            assign bank[1].port_address[1] = icm_out_arb_address;
            assign bank[1].port_writedata[1] = icm_out_arb_writedata;
            assign bank[1].port_byteenable[1] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[1].port_waitrequest[1];
            assign icm_out_rrp_data = bank[1].port_readdata[1];
            assign icm_out_rrp_datavalid = bank[1].port_readdatavalid[1];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __j = 0; __j < 1; __j = __j + 1 )
         begin:port1bank2
            logic icm_in_arb_request [2];
            logic icm_in_arb_enable [2];
            logic icm_in_arb_read [2];
            logic icm_in_arb_write [2];
            logic icm_in_arb_burstcount [2];
            logic [7:0] icm_in_arb_address [2];
            logic [15:0] icm_in_arb_writedata [2];
            logic [1:0] icm_in_arb_byteenable [2];
            logic icm_in_arb_stall [2];
            logic icm_in_wrp_ack [2];
            logic icm_in_rrp_datavalid [2];
            logic [15:0] icm_in_rrp_data [2];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [15:0] icm_out_arb_writedata;
            logic [1:0] icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [15:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[2].b_arb_request[2];
            assign icm_in_arb_enable[0] = router[2].b_arb_enable[2];
            assign icm_in_arb_read[0] = router[2].b_arb_read[2];
            assign icm_in_arb_write[0] = router[2].b_arb_write[2];
            assign icm_in_arb_burstcount[0] = router[2].b_arb_burstcount[2];
            assign icm_in_arb_address[0] = router[2].b_arb_address[2];
            assign icm_in_arb_writedata[0] = router[2].b_arb_writedata[2];
            assign icm_in_arb_byteenable[0] = router[2].b_arb_byteenable[2];
            assign router[2].b_arb_stall[2] = icm_in_arb_stall[0];
            assign router[2].b_wrp_ack[2] = icm_in_wrp_ack[0];
            assign router[2].b_rrp_datavalid[2] = icm_in_rrp_datavalid[0];
            assign router[2].b_rrp_data[2] = icm_in_rrp_data[0];
            assign icm_in_arb_request[1] = router[6].b_arb_request[2];
            assign icm_in_arb_enable[1] = router[6].b_arb_enable[2];
            assign icm_in_arb_read[1] = router[6].b_arb_read[2];
            assign icm_in_arb_write[1] = router[6].b_arb_write[2];
            assign icm_in_arb_burstcount[1] = router[6].b_arb_burstcount[2];
            assign icm_in_arb_address[1] = router[6].b_arb_address[2];
            assign icm_in_arb_writedata[1] = router[6].b_arb_writedata[2];
            assign icm_in_arb_byteenable[1] = router[6].b_arb_byteenable[2];
            assign router[6].b_arb_stall[2] = icm_in_arb_stall[1];
            assign router[6].b_wrp_ack[2] = icm_in_wrp_ack[1];
            assign router[6].b_rrp_datavalid[2] = icm_in_rrp_datavalid[1];
            assign router[6].b_rrp_data[2] = icm_in_rrp_data[1];
            // INST data_ic of conv_pipe_system_interconnect_0
            conv_pipe_system_interconnect_0 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[2].port_enable[1] = icm_out_arb_enable;
            assign bank[2].port_read[1] = icm_out_arb_read;
            assign bank[2].port_write[1] = icm_out_arb_write;
            assign bank[2].port_address[1] = icm_out_arb_address;
            assign bank[2].port_writedata[1] = icm_out_arb_writedata;
            assign bank[2].port_byteenable[1] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[2].port_waitrequest[1];
            assign icm_out_rrp_data = bank[2].port_readdata[1];
            assign icm_out_rrp_datavalid = bank[2].port_readdatavalid[1];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __j = 0; __j < 1; __j = __j + 1 )
         begin:port1bank3
            logic icm_in_arb_request [2];
            logic icm_in_arb_enable [2];
            logic icm_in_arb_read [2];
            logic icm_in_arb_write [2];
            logic icm_in_arb_burstcount [2];
            logic [7:0] icm_in_arb_address [2];
            logic [15:0] icm_in_arb_writedata [2];
            logic [1:0] icm_in_arb_byteenable [2];
            logic icm_in_arb_stall [2];
            logic icm_in_wrp_ack [2];
            logic icm_in_rrp_datavalid [2];
            logic [15:0] icm_in_rrp_data [2];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [15:0] icm_out_arb_writedata;
            logic [1:0] icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [15:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[3].b_arb_request[3];
            assign icm_in_arb_enable[0] = router[3].b_arb_enable[3];
            assign icm_in_arb_read[0] = router[3].b_arb_read[3];
            assign icm_in_arb_write[0] = router[3].b_arb_write[3];
            assign icm_in_arb_burstcount[0] = router[3].b_arb_burstcount[3];
            assign icm_in_arb_address[0] = router[3].b_arb_address[3];
            assign icm_in_arb_writedata[0] = router[3].b_arb_writedata[3];
            assign icm_in_arb_byteenable[0] = router[3].b_arb_byteenable[3];
            assign router[3].b_arb_stall[3] = icm_in_arb_stall[0];
            assign router[3].b_wrp_ack[3] = icm_in_wrp_ack[0];
            assign router[3].b_rrp_datavalid[3] = icm_in_rrp_datavalid[0];
            assign router[3].b_rrp_data[3] = icm_in_rrp_data[0];
            assign icm_in_arb_request[1] = router[4].b_arb_request[3];
            assign icm_in_arb_enable[1] = router[4].b_arb_enable[3];
            assign icm_in_arb_read[1] = router[4].b_arb_read[3];
            assign icm_in_arb_write[1] = router[4].b_arb_write[3];
            assign icm_in_arb_burstcount[1] = router[4].b_arb_burstcount[3];
            assign icm_in_arb_address[1] = router[4].b_arb_address[3];
            assign icm_in_arb_writedata[1] = router[4].b_arb_writedata[3];
            assign icm_in_arb_byteenable[1] = router[4].b_arb_byteenable[3];
            assign router[4].b_arb_stall[3] = icm_in_arb_stall[1];
            assign router[4].b_wrp_ack[3] = icm_in_wrp_ack[1];
            assign router[4].b_rrp_datavalid[3] = icm_in_rrp_datavalid[1];
            assign router[4].b_rrp_data[3] = icm_in_rrp_data[1];
            // INST data_ic of conv_pipe_system_interconnect_0
            conv_pipe_system_interconnect_0 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[3].port_enable[1] = icm_out_arb_enable;
            assign bank[3].port_read[1] = icm_out_arb_read;
            assign bank[3].port_write[1] = icm_out_arb_write;
            assign bank[3].port_address[1] = icm_out_arb_address;
            assign bank[3].port_writedata[1] = icm_out_arb_writedata;
            assign bank[3].port_byteenable[1] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[3].port_waitrequest[1];
            assign icm_out_rrp_data = bank[3].port_readdata[1];
            assign icm_out_rrp_datavalid = bank[3].port_readdatavalid[1];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __j = 0; __j < 1; __j = __j + 1 )
         begin:port2bank0
            logic icm_in_arb_request [2];
            logic icm_in_arb_enable [2];
            logic icm_in_arb_read [2];
            logic icm_in_arb_write [2];
            logic icm_in_arb_burstcount [2];
            logic [7:0] icm_in_arb_address [2];
            logic [15:0] icm_in_arb_writedata [2];
            logic [1:0] icm_in_arb_byteenable [2];
            logic icm_in_arb_stall [2];
            logic icm_in_wrp_ack [2];
            logic icm_in_rrp_datavalid [2];
            logic [15:0] icm_in_rrp_data [2];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [15:0] icm_out_arb_writedata;
            logic [1:0] icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [15:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[0].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[0].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[0].b_arb_read[0];
            assign icm_in_arb_write[0] = router[0].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[0].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[0].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[0].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[0].b_arb_byteenable[0];
            assign router[0].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[0].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[0].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[0].b_rrp_data[0] = icm_in_rrp_data[0];
            assign icm_in_arb_request[1] = router[6].b_arb_request[0];
            assign icm_in_arb_enable[1] = router[6].b_arb_enable[0];
            assign icm_in_arb_read[1] = router[6].b_arb_read[0];
            assign icm_in_arb_write[1] = router[6].b_arb_write[0];
            assign icm_in_arb_burstcount[1] = router[6].b_arb_burstcount[0];
            assign icm_in_arb_address[1] = router[6].b_arb_address[0];
            assign icm_in_arb_writedata[1] = router[6].b_arb_writedata[0];
            assign icm_in_arb_byteenable[1] = router[6].b_arb_byteenable[0];
            assign router[6].b_arb_stall[0] = icm_in_arb_stall[1];
            assign router[6].b_wrp_ack[0] = icm_in_wrp_ack[1];
            assign router[6].b_rrp_datavalid[0] = icm_in_rrp_datavalid[1];
            assign router[6].b_rrp_data[0] = icm_in_rrp_data[1];
            // INST data_ic of conv_pipe_system_interconnect_0
            conv_pipe_system_interconnect_0 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[2] = icm_out_arb_enable;
            assign bank[0].port_read[2] = icm_out_arb_read;
            assign bank[0].port_write[2] = icm_out_arb_write;
            assign bank[0].port_address[2] = icm_out_arb_address;
            assign bank[0].port_writedata[2] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[2] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[2];
            assign icm_out_rrp_data = bank[0].port_readdata[2];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[2];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __j = 0; __j < 1; __j = __j + 1 )
         begin:port2bank1
            logic icm_in_arb_request [2];
            logic icm_in_arb_enable [2];
            logic icm_in_arb_read [2];
            logic icm_in_arb_write [2];
            logic icm_in_arb_burstcount [2];
            logic [7:0] icm_in_arb_address [2];
            logic [15:0] icm_in_arb_writedata [2];
            logic [1:0] icm_in_arb_byteenable [2];
            logic icm_in_arb_stall [2];
            logic icm_in_wrp_ack [2];
            logic icm_in_rrp_datavalid [2];
            logic [15:0] icm_in_rrp_data [2];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [15:0] icm_out_arb_writedata;
            logic [1:0] icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [15:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[1].b_arb_request[1];
            assign icm_in_arb_enable[0] = router[1].b_arb_enable[1];
            assign icm_in_arb_read[0] = router[1].b_arb_read[1];
            assign icm_in_arb_write[0] = router[1].b_arb_write[1];
            assign icm_in_arb_burstcount[0] = router[1].b_arb_burstcount[1];
            assign icm_in_arb_address[0] = router[1].b_arb_address[1];
            assign icm_in_arb_writedata[0] = router[1].b_arb_writedata[1];
            assign icm_in_arb_byteenable[0] = router[1].b_arb_byteenable[1];
            assign router[1].b_arb_stall[1] = icm_in_arb_stall[0];
            assign router[1].b_wrp_ack[1] = icm_in_wrp_ack[0];
            assign router[1].b_rrp_datavalid[1] = icm_in_rrp_datavalid[0];
            assign router[1].b_rrp_data[1] = icm_in_rrp_data[0];
            assign icm_in_arb_request[1] = router[6].b_arb_request[1];
            assign icm_in_arb_enable[1] = router[6].b_arb_enable[1];
            assign icm_in_arb_read[1] = router[6].b_arb_read[1];
            assign icm_in_arb_write[1] = router[6].b_arb_write[1];
            assign icm_in_arb_burstcount[1] = router[6].b_arb_burstcount[1];
            assign icm_in_arb_address[1] = router[6].b_arb_address[1];
            assign icm_in_arb_writedata[1] = router[6].b_arb_writedata[1];
            assign icm_in_arb_byteenable[1] = router[6].b_arb_byteenable[1];
            assign router[6].b_arb_stall[1] = icm_in_arb_stall[1];
            assign router[6].b_wrp_ack[1] = icm_in_wrp_ack[1];
            assign router[6].b_rrp_datavalid[1] = icm_in_rrp_datavalid[1];
            assign router[6].b_rrp_data[1] = icm_in_rrp_data[1];
            // INST data_ic of conv_pipe_system_interconnect_0
            conv_pipe_system_interconnect_0 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[1].port_enable[2] = icm_out_arb_enable;
            assign bank[1].port_read[2] = icm_out_arb_read;
            assign bank[1].port_write[2] = icm_out_arb_write;
            assign bank[1].port_address[2] = icm_out_arb_address;
            assign bank[1].port_writedata[2] = icm_out_arb_writedata;
            assign bank[1].port_byteenable[2] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[1].port_waitrequest[2];
            assign icm_out_rrp_data = bank[1].port_readdata[2];
            assign icm_out_rrp_datavalid = bank[1].port_readdatavalid[2];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __j = 0; __j < 1; __j = __j + 1 )
         begin:port2bank2
            logic icm_in_arb_request [2];
            logic icm_in_arb_enable [2];
            logic icm_in_arb_read [2];
            logic icm_in_arb_write [2];
            logic icm_in_arb_burstcount [2];
            logic [7:0] icm_in_arb_address [2];
            logic [15:0] icm_in_arb_writedata [2];
            logic [1:0] icm_in_arb_byteenable [2];
            logic icm_in_arb_stall [2];
            logic icm_in_wrp_ack [2];
            logic icm_in_rrp_datavalid [2];
            logic [15:0] icm_in_rrp_data [2];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [15:0] icm_out_arb_writedata;
            logic [1:0] icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [15:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[5].b_arb_request[2];
            assign icm_in_arb_enable[0] = router[5].b_arb_enable[2];
            assign icm_in_arb_read[0] = router[5].b_arb_read[2];
            assign icm_in_arb_write[0] = router[5].b_arb_write[2];
            assign icm_in_arb_burstcount[0] = router[5].b_arb_burstcount[2];
            assign icm_in_arb_address[0] = router[5].b_arb_address[2];
            assign icm_in_arb_writedata[0] = router[5].b_arb_writedata[2];
            assign icm_in_arb_byteenable[0] = router[5].b_arb_byteenable[2];
            assign router[5].b_arb_stall[2] = icm_in_arb_stall[0];
            assign router[5].b_wrp_ack[2] = icm_in_wrp_ack[0];
            assign router[5].b_rrp_datavalid[2] = icm_in_rrp_datavalid[0];
            assign router[5].b_rrp_data[2] = icm_in_rrp_data[0];
            assign icm_in_arb_request[1] = router[7].b_arb_request[2];
            assign icm_in_arb_enable[1] = router[7].b_arb_enable[2];
            assign icm_in_arb_read[1] = router[7].b_arb_read[2];
            assign icm_in_arb_write[1] = router[7].b_arb_write[2];
            assign icm_in_arb_burstcount[1] = router[7].b_arb_burstcount[2];
            assign icm_in_arb_address[1] = router[7].b_arb_address[2];
            assign icm_in_arb_writedata[1] = router[7].b_arb_writedata[2];
            assign icm_in_arb_byteenable[1] = router[7].b_arb_byteenable[2];
            assign router[7].b_arb_stall[2] = icm_in_arb_stall[1];
            assign router[7].b_wrp_ack[2] = icm_in_wrp_ack[1];
            assign router[7].b_rrp_datavalid[2] = icm_in_rrp_datavalid[1];
            assign router[7].b_rrp_data[2] = icm_in_rrp_data[1];
            // INST data_ic of conv_pipe_system_interconnect_1
            conv_pipe_system_interconnect_1 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[2].port_enable[2] = icm_out_arb_enable;
            assign bank[2].port_read[2] = icm_out_arb_read;
            assign bank[2].port_write[2] = icm_out_arb_write;
            assign bank[2].port_address[2] = icm_out_arb_address;
            assign bank[2].port_writedata[2] = icm_out_arb_writedata;
            assign bank[2].port_byteenable[2] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[2].port_waitrequest[2];
            assign icm_out_rrp_data = bank[2].port_readdata[2];
            assign icm_out_rrp_datavalid = bank[2].port_readdatavalid[2];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __j = 0; __j < 1; __j = __j + 1 )
         begin:port2bank3
            logic icm_in_arb_request [2];
            logic icm_in_arb_enable [2];
            logic icm_in_arb_read [2];
            logic icm_in_arb_write [2];
            logic icm_in_arb_burstcount [2];
            logic [7:0] icm_in_arb_address [2];
            logic [15:0] icm_in_arb_writedata [2];
            logic [1:0] icm_in_arb_byteenable [2];
            logic icm_in_arb_stall [2];
            logic icm_in_wrp_ack [2];
            logic icm_in_rrp_datavalid [2];
            logic [15:0] icm_in_rrp_data [2];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [15:0] icm_out_arb_writedata;
            logic [1:0] icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [15:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[1].b_arb_request[3];
            assign icm_in_arb_enable[0] = router[1].b_arb_enable[3];
            assign icm_in_arb_read[0] = router[1].b_arb_read[3];
            assign icm_in_arb_write[0] = router[1].b_arb_write[3];
            assign icm_in_arb_burstcount[0] = router[1].b_arb_burstcount[3];
            assign icm_in_arb_address[0] = router[1].b_arb_address[3];
            assign icm_in_arb_writedata[0] = router[1].b_arb_writedata[3];
            assign icm_in_arb_byteenable[0] = router[1].b_arb_byteenable[3];
            assign router[1].b_arb_stall[3] = icm_in_arb_stall[0];
            assign router[1].b_wrp_ack[3] = icm_in_wrp_ack[0];
            assign router[1].b_rrp_datavalid[3] = icm_in_rrp_datavalid[0];
            assign router[1].b_rrp_data[3] = icm_in_rrp_data[0];
            assign icm_in_arb_request[1] = router[6].b_arb_request[3];
            assign icm_in_arb_enable[1] = router[6].b_arb_enable[3];
            assign icm_in_arb_read[1] = router[6].b_arb_read[3];
            assign icm_in_arb_write[1] = router[6].b_arb_write[3];
            assign icm_in_arb_burstcount[1] = router[6].b_arb_burstcount[3];
            assign icm_in_arb_address[1] = router[6].b_arb_address[3];
            assign icm_in_arb_writedata[1] = router[6].b_arb_writedata[3];
            assign icm_in_arb_byteenable[1] = router[6].b_arb_byteenable[3];
            assign router[6].b_arb_stall[3] = icm_in_arb_stall[1];
            assign router[6].b_wrp_ack[3] = icm_in_wrp_ack[1];
            assign router[6].b_rrp_datavalid[3] = icm_in_rrp_datavalid[1];
            assign router[6].b_rrp_data[3] = icm_in_rrp_data[1];
            // INST data_ic of conv_pipe_system_interconnect_0
            conv_pipe_system_interconnect_0 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[3].port_enable[2] = icm_out_arb_enable;
            assign bank[3].port_read[2] = icm_out_arb_read;
            assign bank[3].port_write[2] = icm_out_arb_write;
            assign bank[3].port_address[2] = icm_out_arb_address;
            assign bank[3].port_writedata[2] = icm_out_arb_writedata;
            assign bank[3].port_byteenable[2] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[3].port_waitrequest[2];
            assign icm_out_rrp_data = bank[3].port_readdata[2];
            assign icm_out_rrp_datavalid = bank[3].port_readdatavalid[2];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __j = 0; __j < 1; __j = __j + 1 )
         begin:port3bank0
            logic icm_in_arb_request [2];
            logic icm_in_arb_enable [2];
            logic icm_in_arb_read [2];
            logic icm_in_arb_write [2];
            logic icm_in_arb_burstcount [2];
            logic [7:0] icm_in_arb_address [2];
            logic [15:0] icm_in_arb_writedata [2];
            logic [1:0] icm_in_arb_byteenable [2];
            logic icm_in_arb_stall [2];
            logic icm_in_wrp_ack [2];
            logic icm_in_rrp_datavalid [2];
            logic [15:0] icm_in_rrp_data [2];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [15:0] icm_out_arb_writedata;
            logic [1:0] icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [15:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[5].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[5].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[5].b_arb_read[0];
            assign icm_in_arb_write[0] = router[5].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[5].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[5].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[5].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[5].b_arb_byteenable[0];
            assign router[5].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[5].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[5].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[5].b_rrp_data[0] = icm_in_rrp_data[0];
            assign icm_in_arb_request[1] = router[7].b_arb_request[0];
            assign icm_in_arb_enable[1] = router[7].b_arb_enable[0];
            assign icm_in_arb_read[1] = router[7].b_arb_read[0];
            assign icm_in_arb_write[1] = router[7].b_arb_write[0];
            assign icm_in_arb_burstcount[1] = router[7].b_arb_burstcount[0];
            assign icm_in_arb_address[1] = router[7].b_arb_address[0];
            assign icm_in_arb_writedata[1] = router[7].b_arb_writedata[0];
            assign icm_in_arb_byteenable[1] = router[7].b_arb_byteenable[0];
            assign router[7].b_arb_stall[0] = icm_in_arb_stall[1];
            assign router[7].b_wrp_ack[0] = icm_in_wrp_ack[1];
            assign router[7].b_rrp_datavalid[0] = icm_in_rrp_datavalid[1];
            assign router[7].b_rrp_data[0] = icm_in_rrp_data[1];
            // INST data_ic of conv_pipe_system_interconnect_1
            conv_pipe_system_interconnect_1 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[3] = icm_out_arb_enable;
            assign bank[0].port_read[3] = icm_out_arb_read;
            assign bank[0].port_write[3] = icm_out_arb_write;
            assign bank[0].port_address[3] = icm_out_arb_address;
            assign bank[0].port_writedata[3] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[3] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[3];
            assign icm_out_rrp_data = bank[0].port_readdata[3];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[3];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __j = 0; __j < 1; __j = __j + 1 )
         begin:port3bank1
            logic icm_in_arb_request [2];
            logic icm_in_arb_enable [2];
            logic icm_in_arb_read [2];
            logic icm_in_arb_write [2];
            logic icm_in_arb_burstcount [2];
            logic [7:0] icm_in_arb_address [2];
            logic [15:0] icm_in_arb_writedata [2];
            logic [1:0] icm_in_arb_byteenable [2];
            logic icm_in_arb_stall [2];
            logic icm_in_wrp_ack [2];
            logic icm_in_rrp_datavalid [2];
            logic [15:0] icm_in_rrp_data [2];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [15:0] icm_out_arb_writedata;
            logic [1:0] icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [15:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[5].b_arb_request[1];
            assign icm_in_arb_enable[0] = router[5].b_arb_enable[1];
            assign icm_in_arb_read[0] = router[5].b_arb_read[1];
            assign icm_in_arb_write[0] = router[5].b_arb_write[1];
            assign icm_in_arb_burstcount[0] = router[5].b_arb_burstcount[1];
            assign icm_in_arb_address[0] = router[5].b_arb_address[1];
            assign icm_in_arb_writedata[0] = router[5].b_arb_writedata[1];
            assign icm_in_arb_byteenable[0] = router[5].b_arb_byteenable[1];
            assign router[5].b_arb_stall[1] = icm_in_arb_stall[0];
            assign router[5].b_wrp_ack[1] = icm_in_wrp_ack[0];
            assign router[5].b_rrp_datavalid[1] = icm_in_rrp_datavalid[0];
            assign router[5].b_rrp_data[1] = icm_in_rrp_data[0];
            assign icm_in_arb_request[1] = router[7].b_arb_request[1];
            assign icm_in_arb_enable[1] = router[7].b_arb_enable[1];
            assign icm_in_arb_read[1] = router[7].b_arb_read[1];
            assign icm_in_arb_write[1] = router[7].b_arb_write[1];
            assign icm_in_arb_burstcount[1] = router[7].b_arb_burstcount[1];
            assign icm_in_arb_address[1] = router[7].b_arb_address[1];
            assign icm_in_arb_writedata[1] = router[7].b_arb_writedata[1];
            assign icm_in_arb_byteenable[1] = router[7].b_arb_byteenable[1];
            assign router[7].b_arb_stall[1] = icm_in_arb_stall[1];
            assign router[7].b_wrp_ack[1] = icm_in_wrp_ack[1];
            assign router[7].b_rrp_datavalid[1] = icm_in_rrp_datavalid[1];
            assign router[7].b_rrp_data[1] = icm_in_rrp_data[1];
            // INST data_ic of conv_pipe_system_interconnect_1
            conv_pipe_system_interconnect_1 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[1].port_enable[3] = icm_out_arb_enable;
            assign bank[1].port_read[3] = icm_out_arb_read;
            assign bank[1].port_write[3] = icm_out_arb_write;
            assign bank[1].port_address[3] = icm_out_arb_address;
            assign bank[1].port_writedata[3] = icm_out_arb_writedata;
            assign bank[1].port_byteenable[3] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[1].port_waitrequest[3];
            assign icm_out_rrp_data = bank[1].port_readdata[3];
            assign icm_out_rrp_datavalid = bank[1].port_readdatavalid[3];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __j = 0; __j < 1; __j = __j + 1 )
         begin:port3bank2
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [15:0] icm_in_arb_writedata [1];
            logic [1:0] icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [15:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [15:0] icm_out_arb_writedata;
            logic [1:0] icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [15:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[8].b_arb_request[2];
            assign icm_in_arb_enable[0] = router[8].b_arb_enable[2];
            assign icm_in_arb_read[0] = router[8].b_arb_read[2];
            assign icm_in_arb_write[0] = router[8].b_arb_write[2];
            assign icm_in_arb_burstcount[0] = router[8].b_arb_burstcount[2];
            assign icm_in_arb_address[0] = router[8].b_arb_address[2];
            assign icm_in_arb_writedata[0] = router[8].b_arb_writedata[2];
            assign icm_in_arb_byteenable[0] = router[8].b_arb_byteenable[2];
            assign router[8].b_arb_stall[2] = icm_in_arb_stall[0];
            assign router[8].b_wrp_ack[2] = icm_in_wrp_ack[0];
            assign router[8].b_rrp_datavalid[2] = icm_in_rrp_datavalid[0];
            assign router[8].b_rrp_data[2] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_2
            conv_pipe_system_interconnect_2 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[2].port_enable[3] = icm_out_arb_enable;
            assign bank[2].port_read[3] = icm_out_arb_read;
            assign bank[2].port_write[3] = icm_out_arb_write;
            assign bank[2].port_address[3] = icm_out_arb_address;
            assign bank[2].port_writedata[3] = icm_out_arb_writedata;
            assign bank[2].port_byteenable[3] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[2].port_waitrequest[3];
            assign icm_out_rrp_data = bank[2].port_readdata[3];
            assign icm_out_rrp_datavalid = bank[2].port_readdatavalid[3];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __j = 0; __j < 1; __j = __j + 1 )
         begin:port3bank3
            logic icm_in_arb_request [2];
            logic icm_in_arb_enable [2];
            logic icm_in_arb_read [2];
            logic icm_in_arb_write [2];
            logic icm_in_arb_burstcount [2];
            logic [7:0] icm_in_arb_address [2];
            logic [15:0] icm_in_arb_writedata [2];
            logic [1:0] icm_in_arb_byteenable [2];
            logic icm_in_arb_stall [2];
            logic icm_in_wrp_ack [2];
            logic icm_in_rrp_datavalid [2];
            logic [15:0] icm_in_rrp_data [2];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [15:0] icm_out_arb_writedata;
            logic [1:0] icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [15:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[5].b_arb_request[3];
            assign icm_in_arb_enable[0] = router[5].b_arb_enable[3];
            assign icm_in_arb_read[0] = router[5].b_arb_read[3];
            assign icm_in_arb_write[0] = router[5].b_arb_write[3];
            assign icm_in_arb_burstcount[0] = router[5].b_arb_burstcount[3];
            assign icm_in_arb_address[0] = router[5].b_arb_address[3];
            assign icm_in_arb_writedata[0] = router[5].b_arb_writedata[3];
            assign icm_in_arb_byteenable[0] = router[5].b_arb_byteenable[3];
            assign router[5].b_arb_stall[3] = icm_in_arb_stall[0];
            assign router[5].b_wrp_ack[3] = icm_in_wrp_ack[0];
            assign router[5].b_rrp_datavalid[3] = icm_in_rrp_datavalid[0];
            assign router[5].b_rrp_data[3] = icm_in_rrp_data[0];
            assign icm_in_arb_request[1] = router[7].b_arb_request[3];
            assign icm_in_arb_enable[1] = router[7].b_arb_enable[3];
            assign icm_in_arb_read[1] = router[7].b_arb_read[3];
            assign icm_in_arb_write[1] = router[7].b_arb_write[3];
            assign icm_in_arb_burstcount[1] = router[7].b_arb_burstcount[3];
            assign icm_in_arb_address[1] = router[7].b_arb_address[3];
            assign icm_in_arb_writedata[1] = router[7].b_arb_writedata[3];
            assign icm_in_arb_byteenable[1] = router[7].b_arb_byteenable[3];
            assign router[7].b_arb_stall[3] = icm_in_arb_stall[1];
            assign router[7].b_wrp_ack[3] = icm_in_wrp_ack[1];
            assign router[7].b_rrp_datavalid[3] = icm_in_rrp_datavalid[1];
            assign router[7].b_rrp_data[3] = icm_in_rrp_data[1];
            // INST data_ic of conv_pipe_system_interconnect_1
            conv_pipe_system_interconnect_1 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[3].port_enable[3] = icm_out_arb_enable;
            assign bank[3].port_read[3] = icm_out_arb_read;
            assign bank[3].port_write[3] = icm_out_arb_write;
            assign bank[3].port_address[3] = icm_out_arb_address;
            assign bank[3].port_writedata[3] = icm_out_arb_writedata;
            assign bank[3].port_byteenable[3] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[3].port_waitrequest[3];
            assign icm_out_rrp_data = bank[3].port_readdata[3];
            assign icm_out_rrp_datavalid = bank[3].port_readdatavalid[3];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __j = 0; __j < 1; __j = __j + 1 )
         begin:port4bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [15:0] icm_in_arb_writedata [1];
            logic [1:0] icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [15:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [15:0] icm_out_arb_writedata;
            logic [1:0] icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [15:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[8].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[8].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[8].b_arb_read[0];
            assign icm_in_arb_write[0] = router[8].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[8].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[8].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[8].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[8].b_arb_byteenable[0];
            assign router[8].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[8].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[8].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[8].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_2
            conv_pipe_system_interconnect_2 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[4] = icm_out_arb_enable;
            assign bank[0].port_read[4] = icm_out_arb_read;
            assign bank[0].port_write[4] = icm_out_arb_write;
            assign bank[0].port_address[4] = icm_out_arb_address;
            assign bank[0].port_writedata[4] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[4] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[4];
            assign icm_out_rrp_data = bank[0].port_readdata[4];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[4];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __j = 0; __j < 1; __j = __j + 1 )
         begin:port4bank1
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [15:0] icm_in_arb_writedata [1];
            logic [1:0] icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [15:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [15:0] icm_out_arb_writedata;
            logic [1:0] icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [15:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[8].b_arb_request[1];
            assign icm_in_arb_enable[0] = router[8].b_arb_enable[1];
            assign icm_in_arb_read[0] = router[8].b_arb_read[1];
            assign icm_in_arb_write[0] = router[8].b_arb_write[1];
            assign icm_in_arb_burstcount[0] = router[8].b_arb_burstcount[1];
            assign icm_in_arb_address[0] = router[8].b_arb_address[1];
            assign icm_in_arb_writedata[0] = router[8].b_arb_writedata[1];
            assign icm_in_arb_byteenable[0] = router[8].b_arb_byteenable[1];
            assign router[8].b_arb_stall[1] = icm_in_arb_stall[0];
            assign router[8].b_wrp_ack[1] = icm_in_wrp_ack[0];
            assign router[8].b_rrp_datavalid[1] = icm_in_rrp_datavalid[0];
            assign router[8].b_rrp_data[1] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_2
            conv_pipe_system_interconnect_2 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[1].port_enable[4] = icm_out_arb_enable;
            assign bank[1].port_read[4] = icm_out_arb_read;
            assign bank[1].port_write[4] = icm_out_arb_write;
            assign bank[1].port_address[4] = icm_out_arb_address;
            assign bank[1].port_writedata[4] = icm_out_arb_writedata;
            assign bank[1].port_byteenable[4] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[1].port_waitrequest[4];
            assign icm_out_rrp_data = bank[1].port_readdata[4];
            assign icm_out_rrp_datavalid = bank[1].port_readdatavalid[4];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __j = 0; __j < 1; __j = __j + 1 )
         begin:port4bank2
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [15:0] icm_in_arb_writedata [1];
            logic [1:0] icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [15:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [15:0] icm_out_arb_writedata;
            logic [1:0] icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [15:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[4].b_arb_request[2];
            assign icm_in_arb_enable[0] = router[4].b_arb_enable[2];
            assign icm_in_arb_read[0] = router[4].b_arb_read[2];
            assign icm_in_arb_write[0] = router[4].b_arb_write[2];
            assign icm_in_arb_burstcount[0] = router[4].b_arb_burstcount[2];
            assign icm_in_arb_address[0] = router[4].b_arb_address[2];
            assign icm_in_arb_writedata[0] = router[4].b_arb_writedata[2];
            assign icm_in_arb_byteenable[0] = router[4].b_arb_byteenable[2];
            assign router[4].b_arb_stall[2] = icm_in_arb_stall[0];
            assign router[4].b_wrp_ack[2] = icm_in_wrp_ack[0];
            assign router[4].b_rrp_datavalid[2] = icm_in_rrp_datavalid[0];
            assign router[4].b_rrp_data[2] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_3
            conv_pipe_system_interconnect_3 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[2].port_enable[4] = icm_out_arb_enable;
            assign bank[2].port_read[4] = icm_out_arb_read;
            assign bank[2].port_write[4] = icm_out_arb_write;
            assign bank[2].port_address[4] = icm_out_arb_address;
            assign bank[2].port_writedata[4] = icm_out_arb_writedata;
            assign bank[2].port_byteenable[4] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[2].port_waitrequest[4];
            assign icm_out_rrp_data = bank[2].port_readdata[4];
            assign icm_out_rrp_datavalid = bank[2].port_readdatavalid[4];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __j = 0; __j < 1; __j = __j + 1 )
         begin:port4bank3
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [15:0] icm_in_arb_writedata [1];
            logic [1:0] icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [15:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [15:0] icm_out_arb_writedata;
            logic [1:0] icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [15:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[8].b_arb_request[3];
            assign icm_in_arb_enable[0] = router[8].b_arb_enable[3];
            assign icm_in_arb_read[0] = router[8].b_arb_read[3];
            assign icm_in_arb_write[0] = router[8].b_arb_write[3];
            assign icm_in_arb_burstcount[0] = router[8].b_arb_burstcount[3];
            assign icm_in_arb_address[0] = router[8].b_arb_address[3];
            assign icm_in_arb_writedata[0] = router[8].b_arb_writedata[3];
            assign icm_in_arb_byteenable[0] = router[8].b_arb_byteenable[3];
            assign router[8].b_arb_stall[3] = icm_in_arb_stall[0];
            assign router[8].b_wrp_ack[3] = icm_in_wrp_ack[0];
            assign router[8].b_rrp_datavalid[3] = icm_in_rrp_datavalid[0];
            assign router[8].b_rrp_data[3] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_2
            conv_pipe_system_interconnect_2 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[3].port_enable[4] = icm_out_arb_enable;
            assign bank[3].port_read[4] = icm_out_arb_read;
            assign bank[3].port_write[4] = icm_out_arb_write;
            assign bank[3].port_address[4] = icm_out_arb_address;
            assign bank[3].port_writedata[4] = icm_out_arb_writedata;
            assign bank[3].port_byteenable[4] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[3].port_waitrequest[4];
            assign icm_out_rrp_data = bank[3].port_readdata[4];
            assign icm_out_rrp_datavalid = bank[3].port_readdatavalid[4];
            assign icm_out_wrp_ack = 'b0;
         end

      end

      assign lmem_invalid_aspaces[0] = |invalid_access_grps;
   end
   endgenerate

   generate
   begin:local_mem_system_aspace43
      logic local_icm_arb_request [1][2];
      logic local_icm_arb_enable [1][2];
      logic local_icm_arb_read [1][2];
      logic local_icm_arb_write [1][2];
      logic local_icm_arb_burstcount [1][2];
      logic [7:0] local_icm_arb_address [1][2];
      logic [31:0] local_icm_arb_writedata [1][2];
      logic [3:0] local_icm_arb_byteenable [1][2];
      logic local_icm_arb_stall [1][2];
      logic local_icm_wrp_ack [1][2];
      logic local_icm_rrp_datavalid [1][2];
      logic [31:0] local_icm_rrp_data [1][2];

      for( __j = 0; __j < 1; __j = __j + 1 )
      begin:local_mem_group
         for( __k = 0; __k < 2; __k = __k + 1 )
         begin:master
            // INST avm_to_ic of acl_avm_to_ic
            acl_avm_to_ic
            #(
               .DATA_W(32),
               .WRITEDATA_W(32),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(32),
               .BYTEENA_W(4)
            )
            avm_to_ic
            (
               // AVM avm
               .avm_enable(local_avm_aspace43_enable[__j][__k]),
               .avm_read(local_avm_aspace43_read[__j][__k]),
               .avm_write(local_avm_aspace43_write[__j][__k]),
               .avm_burstcount(local_avm_aspace43_burstcount[__j][__k]),
               .avm_address(local_avm_aspace43_address[__j][__k]),
               .avm_writedata(local_avm_aspace43_writedata[__j][__k]),
               .avm_byteenable(local_avm_aspace43_byteenable[__j][__k]),
               .avm_waitrequest(local_avm_aspace43_waitrequest[__j][__k]),
               .avm_readdata(local_avm_aspace43_readdata[__j][__k]),
               .avm_readdatavalid(local_avm_aspace43_readdatavalid[__j][__k]),
               .avm_writeack(local_avm_aspace43_writeack[__j][__k]),
               // ICM ic
               .ic_arb_request(local_icm_arb_request[__j][__k]),
               .ic_arb_enable(local_icm_arb_enable[__j][__k]),
               .ic_arb_read(local_icm_arb_read[__j][__k]),
               .ic_arb_write(local_icm_arb_write[__j][__k]),
               .ic_arb_burstcount(local_icm_arb_burstcount[__j][__k]),
               .ic_arb_address(local_icm_arb_address[__j][__k]),
               .ic_arb_writedata(local_icm_arb_writedata[__j][__k]),
               .ic_arb_byteenable(local_icm_arb_byteenable[__j][__k]),
               .ic_arb_stall(local_icm_arb_stall[__j][__k]),
               .ic_wrp_ack(local_icm_wrp_ack[__j][__k]),
               .ic_rrp_datavalid(local_icm_rrp_datavalid[__j][__k]),
               .ic_rrp_data(local_icm_rrp_data[__j][__k])
            );

         end

         for( __k = 0; __k < 1; __k = __k + 1 )
         begin:bank
            logic port_enable [1:2];
            logic port_read [1:2];
            logic port_write [1:2];
            logic [7:0] port_address [1:2];
            logic [31:0] port_writedata [1:2];
            logic [3:0] port_byteenable [1:2];
            logic port_waitrequest [1:2];
            logic [31:0] port_readdata [1:2];
            logic port_readdatavalid [1:2];

            // INST mem0 of acl_mem1x
            acl_mem1x
            #(
               .INTENDED_DEVICE_FAMILY("Cyclone V"),
               .DEPTH_WORDS(192),
               .WIDTH(32),
               .MEM_LATENCY(3),
               .ENABLED(0),
               .RDW_MODE("DONT_CARE"),
               .RAM_OPERATION_MODE("DUAL_PORT"),
               .PREFERRED_WIDTH(320),
               .RAM_BLOCK_TYPE("M10K")
            )
            mem0
            (
               .clk(clock),
               .resetn(resetn),
               // AVS avs_port1
               .avs_port1_enable(port_enable[1]),
               .avs_port1_read(port_read[1]),
               .avs_port1_write(port_write[1]),
               .avs_port1_address(port_address[1]),
               .avs_port1_writedata(port_writedata[1]),
               .avs_port1_byteenable(port_byteenable[1]),
               .avs_port1_waitrequest(port_waitrequest[1]),
               .avs_port1_readdata(port_readdata[1]),
               .avs_port1_readdatavalid(port_readdatavalid[1]),
               // AVS avs_port2
               .avs_port2_enable(port_enable[2]),
               .avs_port2_read(port_read[2]),
               .avs_port2_write(port_write[2]),
               .avs_port2_address(port_address[2]),
               .avs_port2_writedata(port_writedata[2]),
               .avs_port2_byteenable(port_byteenable[2]),
               .avs_port2_waitrequest(port_waitrequest[2]),
               .avs_port2_readdata(port_readdata[2]),
               .avs_port2_readdatavalid(port_readdatavalid[2])
            );

         end

         for( __k = 0; __k < 2; __k = __k + 1 )
         begin:router
            logic b_arb_request [1];
            logic b_arb_enable [1];
            logic b_arb_read [1];
            logic b_arb_write [1];
            logic b_arb_burstcount [1];
            logic [7:0] b_arb_address [1];
            logic [31:0] b_arb_writedata [1];
            logic [3:0] b_arb_byteenable [1];
            logic b_arb_stall [1];
            logic b_wrp_ack [1];
            logic b_rrp_datavalid [1];
            logic [31:0] b_rrp_data [1];
            logic bank_select;

            // INST router of acl_ic_local_mem_router
            acl_ic_local_mem_router
            #(
               .DATA_W(32),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(8),
               .BYTEENA_W(4),
               .NUM_BANKS(1)
            )
            router
            (
               .clock(clock),
               .resetn(resetn),
               .bank_select(bank_select),
               // ICM m
               .m_arb_request(local_icm_arb_request[__j][__k]),
               .m_arb_enable(local_icm_arb_enable[__j][__k]),
               .m_arb_read(local_icm_arb_read[__j][__k]),
               .m_arb_write(local_icm_arb_write[__j][__k]),
               .m_arb_burstcount(local_icm_arb_burstcount[__j][__k]),
               .m_arb_address(local_icm_arb_address[__j][__k]),
               .m_arb_writedata(local_icm_arb_writedata[__j][__k]),
               .m_arb_byteenable(local_icm_arb_byteenable[__j][__k]),
               .m_arb_stall(local_icm_arb_stall[__j][__k]),
               .m_wrp_ack(local_icm_wrp_ack[__j][__k]),
               .m_rrp_datavalid(local_icm_rrp_datavalid[__j][__k]),
               .m_rrp_data(local_icm_rrp_data[__j][__k]),
               // ICM b
               .b_arb_request(b_arb_request),
               .b_arb_enable(b_arb_enable),
               .b_arb_read(b_arb_read),
               .b_arb_write(b_arb_write),
               .b_arb_burstcount(b_arb_burstcount),
               .b_arb_address(b_arb_address),
               .b_arb_writedata(b_arb_writedata),
               .b_arb_byteenable(b_arb_byteenable),
               .b_arb_stall(b_arb_stall),
               .b_wrp_ack(b_wrp_ack),
               .b_rrp_datavalid(b_rrp_datavalid),
               .b_rrp_data(b_rrp_data)
            );

            assign bank_select = 1'b1;
         end

         for( __k = 0; __k < 1; __k = __k + 1 )
         begin:port1bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [31:0] icm_in_arb_writedata [1];
            logic [3:0] icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [31:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [31:0] icm_out_arb_writedata;
            logic [3:0] icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [31:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[0].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[0].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[0].b_arb_read[0];
            assign icm_in_arb_write[0] = router[0].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[0].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[0].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[0].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[0].b_arb_byteenable[0];
            assign router[0].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[0].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[0].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[0].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_4
            conv_pipe_system_interconnect_4 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[1] = icm_out_arb_enable;
            assign bank[0].port_read[1] = icm_out_arb_read;
            assign bank[0].port_write[1] = icm_out_arb_write;
            assign bank[0].port_address[1] = icm_out_arb_address;
            assign bank[0].port_writedata[1] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[1] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[1];
            assign icm_out_rrp_data = bank[0].port_readdata[1];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[1];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __k = 0; __k < 1; __k = __k + 1 )
         begin:port2bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [31:0] icm_in_arb_writedata [1];
            logic [3:0] icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [31:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [31:0] icm_out_arb_writedata;
            logic [3:0] icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [31:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[1].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[1].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[1].b_arb_read[0];
            assign icm_in_arb_write[0] = router[1].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[1].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[1].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[1].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[1].b_arb_byteenable[0];
            assign router[1].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[1].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[1].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[1].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_5
            conv_pipe_system_interconnect_5 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[2] = icm_out_arb_enable;
            assign bank[0].port_read[2] = icm_out_arb_read;
            assign bank[0].port_write[2] = icm_out_arb_write;
            assign bank[0].port_address[2] = icm_out_arb_address;
            assign bank[0].port_writedata[2] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[2] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[2];
            assign icm_out_rrp_data = bank[0].port_readdata[2];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[2];
            assign icm_out_wrp_ack = 'b0;
         end

      end

   end
   endgenerate

endmodule

/////////////////////////////////////////////////////////////////
// MODULE maxPool_top_wrapper_0
/////////////////////////////////////////////////////////////////
module maxPool_top_wrapper_0
(
   input logic start,
   input logic [55:0] kernel_arguments,
   input logic [31:0] work_dim,
   input logic [31:0] global_offset [2:0],
   output logic kernel_valid_out,
   output logic has_a_write_pending,
   output logic has_a_lsu_active,
   input logic [31:0] global_id [2:0],
   input logic [31:0] local_id [2:0],
   input logic [31:0] group_id [2:0],
   input logic [31:0] global_size [2:0],
   input logic [31:0] local_size [2:0],
   input logic [31:0] num_groups [2:0],
   input logic [31:0] workgroup_size,
   output logic kernel_stall_out,
   input logic kernel_valid_in,
   input logic clock,
   input logic resetn,
   input logic clock2x,
   // AVST avm_channel_id_conv_ch_read
   input logic avm_channel_id_conv_ch_read_valid,
   output logic avm_channel_id_conv_ch_read_ready,
   input logic [127:0] avm_channel_id_conv_ch_read_data,
   // AVST avm_channel_id_pool_ch_write
   output logic avm_channel_id_pool_ch_write_valid,
   input logic avm_channel_id_pool_ch_write_ready,
   output logic [127:0] avm_channel_id_pool_ch_write_data,
   input logic avm_channel_id_pool_ch_write_almostfull
);
   genvar __i;
   genvar __j;
   genvar __k;
   genvar __l;
   genvar __m;
   genvar __n;
   genvar __o;
   genvar __p;
   genvar __q;
   genvar __r;
   genvar __s;
   genvar __t;
   genvar __u;
   genvar __v;
   genvar __w;
   genvar __x;
   genvar __y;
   genvar __z;
   genvar __a;
   genvar __b;
   genvar __c;
   genvar __d;
   genvar __e;
   genvar __f;
   genvar __g;
   genvar __h;
   genvar __i1;
   genvar __j1;
   genvar __k1;
   genvar __l1;
   genvar __m1;
   genvar __n1;
   genvar __o1;
   logic lmem_invalid_single_bit;
   logic [31:0] lmem_invalid_aspaces;
   logic local_avm_aspace7_enable [1][2];
   logic local_avm_aspace7_read [1][2];
   logic local_avm_aspace7_write [1][2];
   logic local_avm_aspace7_burstcount [1][2];
   logic [31:0] local_avm_aspace7_address [1][2];
   logic [7:0] local_avm_aspace7_writedata [1][2];
   logic local_avm_aspace7_byteenable [1][2];
   logic local_avm_aspace7_waitrequest [1][2];
   logic [7:0] local_avm_aspace7_readdata [1][2];
   logic local_avm_aspace7_readdatavalid [1][2];
   logic local_avm_aspace7_writeack [1][2];
   logic local_avm_aspace8_enable [1][2];
   logic local_avm_aspace8_read [1][2];
   logic local_avm_aspace8_write [1][2];
   logic local_avm_aspace8_burstcount [1][2];
   logic [31:0] local_avm_aspace8_address [1][2];
   logic [7:0] local_avm_aspace8_writedata [1][2];
   logic local_avm_aspace8_byteenable [1][2];
   logic local_avm_aspace8_waitrequest [1][2];
   logic [7:0] local_avm_aspace8_readdata [1][2];
   logic local_avm_aspace8_readdatavalid [1][2];
   logic local_avm_aspace8_writeack [1][2];
   logic local_avm_aspace10_enable [1][2];
   logic local_avm_aspace10_read [1][2];
   logic local_avm_aspace10_write [1][2];
   logic local_avm_aspace10_burstcount [1][2];
   logic [31:0] local_avm_aspace10_address [1][2];
   logic [7:0] local_avm_aspace10_writedata [1][2];
   logic local_avm_aspace10_byteenable [1][2];
   logic local_avm_aspace10_waitrequest [1][2];
   logic [7:0] local_avm_aspace10_readdata [1][2];
   logic local_avm_aspace10_readdatavalid [1][2];
   logic local_avm_aspace10_writeack [1][2];
   logic local_avm_aspace11_enable [1][2];
   logic local_avm_aspace11_read [1][2];
   logic local_avm_aspace11_write [1][2];
   logic local_avm_aspace11_burstcount [1][2];
   logic [31:0] local_avm_aspace11_address [1][2];
   logic [7:0] local_avm_aspace11_writedata [1][2];
   logic local_avm_aspace11_byteenable [1][2];
   logic local_avm_aspace11_waitrequest [1][2];
   logic [7:0] local_avm_aspace11_readdata [1][2];
   logic local_avm_aspace11_readdatavalid [1][2];
   logic local_avm_aspace11_writeack [1][2];
   logic local_avm_aspace12_enable [1][2];
   logic local_avm_aspace12_read [1][2];
   logic local_avm_aspace12_write [1][2];
   logic local_avm_aspace12_burstcount [1][2];
   logic [31:0] local_avm_aspace12_address [1][2];
   logic [7:0] local_avm_aspace12_writedata [1][2];
   logic local_avm_aspace12_byteenable [1][2];
   logic local_avm_aspace12_waitrequest [1][2];
   logic [7:0] local_avm_aspace12_readdata [1][2];
   logic local_avm_aspace12_readdatavalid [1][2];
   logic local_avm_aspace12_writeack [1][2];
   logic local_avm_aspace13_enable [1][2];
   logic local_avm_aspace13_read [1][2];
   logic local_avm_aspace13_write [1][2];
   logic local_avm_aspace13_burstcount [1][2];
   logic [31:0] local_avm_aspace13_address [1][2];
   logic [7:0] local_avm_aspace13_writedata [1][2];
   logic local_avm_aspace13_byteenable [1][2];
   logic local_avm_aspace13_waitrequest [1][2];
   logic [7:0] local_avm_aspace13_readdata [1][2];
   logic local_avm_aspace13_readdatavalid [1][2];
   logic local_avm_aspace13_writeack [1][2];
   logic local_avm_aspace14_enable [1][2];
   logic local_avm_aspace14_read [1][2];
   logic local_avm_aspace14_write [1][2];
   logic local_avm_aspace14_burstcount [1][2];
   logic [31:0] local_avm_aspace14_address [1][2];
   logic [7:0] local_avm_aspace14_writedata [1][2];
   logic local_avm_aspace14_byteenable [1][2];
   logic local_avm_aspace14_waitrequest [1][2];
   logic [7:0] local_avm_aspace14_readdata [1][2];
   logic local_avm_aspace14_readdatavalid [1][2];
   logic local_avm_aspace14_writeack [1][2];
   logic local_avm_aspace15_enable [1][2];
   logic local_avm_aspace15_read [1][2];
   logic local_avm_aspace15_write [1][2];
   logic local_avm_aspace15_burstcount [1][2];
   logic [31:0] local_avm_aspace15_address [1][2];
   logic [7:0] local_avm_aspace15_writedata [1][2];
   logic local_avm_aspace15_byteenable [1][2];
   logic local_avm_aspace15_waitrequest [1][2];
   logic [7:0] local_avm_aspace15_readdata [1][2];
   logic local_avm_aspace15_readdatavalid [1][2];
   logic local_avm_aspace15_writeack [1][2];
   logic local_avm_aspace16_enable [1][2];
   logic local_avm_aspace16_read [1][2];
   logic local_avm_aspace16_write [1][2];
   logic local_avm_aspace16_burstcount [1][2];
   logic [31:0] local_avm_aspace16_address [1][2];
   logic [7:0] local_avm_aspace16_writedata [1][2];
   logic local_avm_aspace16_byteenable [1][2];
   logic local_avm_aspace16_waitrequest [1][2];
   logic [7:0] local_avm_aspace16_readdata [1][2];
   logic local_avm_aspace16_readdatavalid [1][2];
   logic local_avm_aspace16_writeack [1][2];
   logic local_avm_aspace17_enable [1][2];
   logic local_avm_aspace17_read [1][2];
   logic local_avm_aspace17_write [1][2];
   logic local_avm_aspace17_burstcount [1][2];
   logic [31:0] local_avm_aspace17_address [1][2];
   logic [7:0] local_avm_aspace17_writedata [1][2];
   logic local_avm_aspace17_byteenable [1][2];
   logic local_avm_aspace17_waitrequest [1][2];
   logic [7:0] local_avm_aspace17_readdata [1][2];
   logic local_avm_aspace17_readdatavalid [1][2];
   logic local_avm_aspace17_writeack [1][2];
   logic local_avm_aspace18_enable [1][2];
   logic local_avm_aspace18_read [1][2];
   logic local_avm_aspace18_write [1][2];
   logic local_avm_aspace18_burstcount [1][2];
   logic [31:0] local_avm_aspace18_address [1][2];
   logic [7:0] local_avm_aspace18_writedata [1][2];
   logic local_avm_aspace18_byteenable [1][2];
   logic local_avm_aspace18_waitrequest [1][2];
   logic [7:0] local_avm_aspace18_readdata [1][2];
   logic local_avm_aspace18_readdatavalid [1][2];
   logic local_avm_aspace18_writeack [1][2];
   logic local_avm_aspace19_enable [1][2];
   logic local_avm_aspace19_read [1][2];
   logic local_avm_aspace19_write [1][2];
   logic local_avm_aspace19_burstcount [1][2];
   logic [31:0] local_avm_aspace19_address [1][2];
   logic [7:0] local_avm_aspace19_writedata [1][2];
   logic local_avm_aspace19_byteenable [1][2];
   logic local_avm_aspace19_waitrequest [1][2];
   logic [7:0] local_avm_aspace19_readdata [1][2];
   logic local_avm_aspace19_readdatavalid [1][2];
   logic local_avm_aspace19_writeack [1][2];
   logic local_avm_aspace20_enable [1][2];
   logic local_avm_aspace20_read [1][2];
   logic local_avm_aspace20_write [1][2];
   logic local_avm_aspace20_burstcount [1][2];
   logic [31:0] local_avm_aspace20_address [1][2];
   logic [7:0] local_avm_aspace20_writedata [1][2];
   logic local_avm_aspace20_byteenable [1][2];
   logic local_avm_aspace20_waitrequest [1][2];
   logic [7:0] local_avm_aspace20_readdata [1][2];
   logic local_avm_aspace20_readdatavalid [1][2];
   logic local_avm_aspace20_writeack [1][2];
   logic local_avm_aspace21_enable [1][2];
   logic local_avm_aspace21_read [1][2];
   logic local_avm_aspace21_write [1][2];
   logic local_avm_aspace21_burstcount [1][2];
   logic [31:0] local_avm_aspace21_address [1][2];
   logic [7:0] local_avm_aspace21_writedata [1][2];
   logic local_avm_aspace21_byteenable [1][2];
   logic local_avm_aspace21_waitrequest [1][2];
   logic [7:0] local_avm_aspace21_readdata [1][2];
   logic local_avm_aspace21_readdatavalid [1][2];
   logic local_avm_aspace21_writeack [1][2];
   logic local_avm_aspace22_enable [1][2];
   logic local_avm_aspace22_read [1][2];
   logic local_avm_aspace22_write [1][2];
   logic local_avm_aspace22_burstcount [1][2];
   logic [31:0] local_avm_aspace22_address [1][2];
   logic [7:0] local_avm_aspace22_writedata [1][2];
   logic local_avm_aspace22_byteenable [1][2];
   logic local_avm_aspace22_waitrequest [1][2];
   logic [7:0] local_avm_aspace22_readdata [1][2];
   logic local_avm_aspace22_readdatavalid [1][2];
   logic local_avm_aspace22_writeack [1][2];
   logic local_avm_aspace23_enable [1][2];
   logic local_avm_aspace23_read [1][2];
   logic local_avm_aspace23_write [1][2];
   logic local_avm_aspace23_burstcount [1][2];
   logic [31:0] local_avm_aspace23_address [1][2];
   logic [7:0] local_avm_aspace23_writedata [1][2];
   logic local_avm_aspace23_byteenable [1][2];
   logic local_avm_aspace23_waitrequest [1][2];
   logic [7:0] local_avm_aspace23_readdata [1][2];
   logic local_avm_aspace23_readdatavalid [1][2];
   logic local_avm_aspace23_writeack [1][2];
   logic local_avm_aspace24_enable [1][2];
   logic local_avm_aspace24_read [1][2];
   logic local_avm_aspace24_write [1][2];
   logic local_avm_aspace24_burstcount [1][2];
   logic [31:0] local_avm_aspace24_address [1][2];
   logic [7:0] local_avm_aspace24_writedata [1][2];
   logic local_avm_aspace24_byteenable [1][2];
   logic local_avm_aspace24_waitrequest [1][2];
   logic [7:0] local_avm_aspace24_readdata [1][2];
   logic local_avm_aspace24_readdatavalid [1][2];
   logic local_avm_aspace24_writeack [1][2];
   logic local_avm_aspace25_enable [1][2];
   logic local_avm_aspace25_read [1][2];
   logic local_avm_aspace25_write [1][2];
   logic local_avm_aspace25_burstcount [1][2];
   logic [31:0] local_avm_aspace25_address [1][2];
   logic [7:0] local_avm_aspace25_writedata [1][2];
   logic local_avm_aspace25_byteenable [1][2];
   logic local_avm_aspace25_waitrequest [1][2];
   logic [7:0] local_avm_aspace25_readdata [1][2];
   logic local_avm_aspace25_readdatavalid [1][2];
   logic local_avm_aspace25_writeack [1][2];
   logic local_avm_aspace26_enable [1][2];
   logic local_avm_aspace26_read [1][2];
   logic local_avm_aspace26_write [1][2];
   logic local_avm_aspace26_burstcount [1][2];
   logic [31:0] local_avm_aspace26_address [1][2];
   logic [7:0] local_avm_aspace26_writedata [1][2];
   logic local_avm_aspace26_byteenable [1][2];
   logic local_avm_aspace26_waitrequest [1][2];
   logic [7:0] local_avm_aspace26_readdata [1][2];
   logic local_avm_aspace26_readdatavalid [1][2];
   logic local_avm_aspace26_writeack [1][2];
   logic local_avm_aspace27_enable [1][2];
   logic local_avm_aspace27_read [1][2];
   logic local_avm_aspace27_write [1][2];
   logic local_avm_aspace27_burstcount [1][2];
   logic [31:0] local_avm_aspace27_address [1][2];
   logic [7:0] local_avm_aspace27_writedata [1][2];
   logic local_avm_aspace27_byteenable [1][2];
   logic local_avm_aspace27_waitrequest [1][2];
   logic [7:0] local_avm_aspace27_readdata [1][2];
   logic local_avm_aspace27_readdatavalid [1][2];
   logic local_avm_aspace27_writeack [1][2];
   logic local_avm_aspace28_enable [1][2];
   logic local_avm_aspace28_read [1][2];
   logic local_avm_aspace28_write [1][2];
   logic local_avm_aspace28_burstcount [1][2];
   logic [31:0] local_avm_aspace28_address [1][2];
   logic [7:0] local_avm_aspace28_writedata [1][2];
   logic local_avm_aspace28_byteenable [1][2];
   logic local_avm_aspace28_waitrequest [1][2];
   logic [7:0] local_avm_aspace28_readdata [1][2];
   logic local_avm_aspace28_readdatavalid [1][2];
   logic local_avm_aspace28_writeack [1][2];
   logic local_avm_aspace29_enable [1][2];
   logic local_avm_aspace29_read [1][2];
   logic local_avm_aspace29_write [1][2];
   logic local_avm_aspace29_burstcount [1][2];
   logic [31:0] local_avm_aspace29_address [1][2];
   logic [7:0] local_avm_aspace29_writedata [1][2];
   logic local_avm_aspace29_byteenable [1][2];
   logic local_avm_aspace29_waitrequest [1][2];
   logic [7:0] local_avm_aspace29_readdata [1][2];
   logic local_avm_aspace29_readdatavalid [1][2];
   logic local_avm_aspace29_writeack [1][2];
   logic local_avm_aspace30_enable [1][2];
   logic local_avm_aspace30_read [1][2];
   logic local_avm_aspace30_write [1][2];
   logic local_avm_aspace30_burstcount [1][2];
   logic [31:0] local_avm_aspace30_address [1][2];
   logic [7:0] local_avm_aspace30_writedata [1][2];
   logic local_avm_aspace30_byteenable [1][2];
   logic local_avm_aspace30_waitrequest [1][2];
   logic [7:0] local_avm_aspace30_readdata [1][2];
   logic local_avm_aspace30_readdatavalid [1][2];
   logic local_avm_aspace30_writeack [1][2];
   logic local_avm_aspace31_enable [1][2];
   logic local_avm_aspace31_read [1][2];
   logic local_avm_aspace31_write [1][2];
   logic local_avm_aspace31_burstcount [1][2];
   logic [31:0] local_avm_aspace31_address [1][2];
   logic [7:0] local_avm_aspace31_writedata [1][2];
   logic local_avm_aspace31_byteenable [1][2];
   logic local_avm_aspace31_waitrequest [1][2];
   logic [7:0] local_avm_aspace31_readdata [1][2];
   logic local_avm_aspace31_readdatavalid [1][2];
   logic local_avm_aspace31_writeack [1][2];
   logic local_avm_aspace32_enable [1][2];
   logic local_avm_aspace32_read [1][2];
   logic local_avm_aspace32_write [1][2];
   logic local_avm_aspace32_burstcount [1][2];
   logic [31:0] local_avm_aspace32_address [1][2];
   logic [7:0] local_avm_aspace32_writedata [1][2];
   logic local_avm_aspace32_byteenable [1][2];
   logic local_avm_aspace32_waitrequest [1][2];
   logic [7:0] local_avm_aspace32_readdata [1][2];
   logic local_avm_aspace32_readdatavalid [1][2];
   logic local_avm_aspace32_writeack [1][2];
   logic local_avm_aspace33_enable [1][2];
   logic local_avm_aspace33_read [1][2];
   logic local_avm_aspace33_write [1][2];
   logic local_avm_aspace33_burstcount [1][2];
   logic [31:0] local_avm_aspace33_address [1][2];
   logic [7:0] local_avm_aspace33_writedata [1][2];
   logic local_avm_aspace33_byteenable [1][2];
   logic local_avm_aspace33_waitrequest [1][2];
   logic [7:0] local_avm_aspace33_readdata [1][2];
   logic local_avm_aspace33_readdatavalid [1][2];
   logic local_avm_aspace33_writeack [1][2];
   logic local_avm_aspace34_enable [1][2];
   logic local_avm_aspace34_read [1][2];
   logic local_avm_aspace34_write [1][2];
   logic local_avm_aspace34_burstcount [1][2];
   logic [31:0] local_avm_aspace34_address [1][2];
   logic [7:0] local_avm_aspace34_writedata [1][2];
   logic local_avm_aspace34_byteenable [1][2];
   logic local_avm_aspace34_waitrequest [1][2];
   logic [7:0] local_avm_aspace34_readdata [1][2];
   logic local_avm_aspace34_readdatavalid [1][2];
   logic local_avm_aspace34_writeack [1][2];
   logic local_avm_aspace35_enable [1][2];
   logic local_avm_aspace35_read [1][2];
   logic local_avm_aspace35_write [1][2];
   logic local_avm_aspace35_burstcount [1][2];
   logic [31:0] local_avm_aspace35_address [1][2];
   logic [7:0] local_avm_aspace35_writedata [1][2];
   logic local_avm_aspace35_byteenable [1][2];
   logic local_avm_aspace35_waitrequest [1][2];
   logic [7:0] local_avm_aspace35_readdata [1][2];
   logic local_avm_aspace35_readdatavalid [1][2];
   logic local_avm_aspace35_writeack [1][2];
   logic local_avm_aspace36_enable [1][2];
   logic local_avm_aspace36_read [1][2];
   logic local_avm_aspace36_write [1][2];
   logic local_avm_aspace36_burstcount [1][2];
   logic [31:0] local_avm_aspace36_address [1][2];
   logic [7:0] local_avm_aspace36_writedata [1][2];
   logic local_avm_aspace36_byteenable [1][2];
   logic local_avm_aspace36_waitrequest [1][2];
   logic [7:0] local_avm_aspace36_readdata [1][2];
   logic local_avm_aspace36_readdatavalid [1][2];
   logic local_avm_aspace36_writeack [1][2];
   logic local_avm_aspace37_enable [1][2];
   logic local_avm_aspace37_read [1][2];
   logic local_avm_aspace37_write [1][2];
   logic local_avm_aspace37_burstcount [1][2];
   logic [31:0] local_avm_aspace37_address [1][2];
   logic [7:0] local_avm_aspace37_writedata [1][2];
   logic local_avm_aspace37_byteenable [1][2];
   logic local_avm_aspace37_waitrequest [1][2];
   logic [7:0] local_avm_aspace37_readdata [1][2];
   logic local_avm_aspace37_readdatavalid [1][2];
   logic local_avm_aspace37_writeack [1][2];
   logic local_avm_aspace38_enable [1][2];
   logic local_avm_aspace38_read [1][2];
   logic local_avm_aspace38_write [1][2];
   logic local_avm_aspace38_burstcount [1][2];
   logic [31:0] local_avm_aspace38_address [1][2];
   logic [7:0] local_avm_aspace38_writedata [1][2];
   logic local_avm_aspace38_byteenable [1][2];
   logic local_avm_aspace38_waitrequest [1][2];
   logic [7:0] local_avm_aspace38_readdata [1][2];
   logic local_avm_aspace38_readdatavalid [1][2];
   logic local_avm_aspace38_writeack [1][2];
   logic local_avm_aspace39_enable [1][2];
   logic local_avm_aspace39_read [1][2];
   logic local_avm_aspace39_write [1][2];
   logic local_avm_aspace39_burstcount [1][2];
   logic [31:0] local_avm_aspace39_address [1][2];
   logic [7:0] local_avm_aspace39_writedata [1][2];
   logic local_avm_aspace39_byteenable [1][2];
   logic local_avm_aspace39_waitrequest [1][2];
   logic [7:0] local_avm_aspace39_readdata [1][2];
   logic local_avm_aspace39_readdatavalid [1][2];
   logic local_avm_aspace39_writeack [1][2];

   // INST kernel of maxPool_function_wrapper
   maxPool_function_wrapper kernel
   (
      .local_router_hang(lmem_invalid_single_bit),
      .start(start),
      .kernel_arguments(kernel_arguments),
      .work_dim(work_dim),
      .global_offset_0(global_offset[0]),
      .global_offset_1(global_offset[1]),
      .global_offset_2(global_offset[2]),
      .kernel_valid_out(kernel_valid_out),
      .has_a_write_pending(has_a_write_pending),
      .has_a_lsu_active(has_a_lsu_active),
      .global_id_0(global_id[0]),
      .global_id_1(global_id[1]),
      .global_id_2(global_id[2]),
      .local_id_0(local_id[0]),
      .local_id_1(local_id[1]),
      .local_id_2(local_id[2]),
      .group_id_0(group_id[0]),
      .group_id_1(group_id[1]),
      .group_id_2(group_id[2]),
      .global_size_0(global_size[0]),
      .global_size_1(global_size[1]),
      .global_size_2(global_size[2]),
      .local_size_0(local_size[0]),
      .local_size_1(local_size[1]),
      .local_size_2(local_size[2]),
      .num_groups_0(num_groups[0]),
      .num_groups_1(num_groups[1]),
      .num_groups_2(num_groups[2]),
      .workgroup_size(workgroup_size),
      .kernel_stall_out(kernel_stall_out),
      .kernel_valid_in(kernel_valid_in),
      .clock(clock),
      .resetn(resetn),
      .clock2x(clock2x),
      // AVM avm_local_bb2_ld__inst0
      .avm_local_bb2_ld__inst0_enable(local_avm_aspace7_enable[0][0]),
      .avm_local_bb2_ld__inst0_read(local_avm_aspace7_read[0][0]),
      .avm_local_bb2_ld__inst0_write(local_avm_aspace7_write[0][0]),
      .avm_local_bb2_ld__inst0_burstcount(local_avm_aspace7_burstcount[0][0]),
      .avm_local_bb2_ld__inst0_address(local_avm_aspace7_address[0][0]),
      .avm_local_bb2_ld__inst0_writedata(local_avm_aspace7_writedata[0][0]),
      .avm_local_bb2_ld__inst0_byteenable(local_avm_aspace7_byteenable[0][0]),
      .avm_local_bb2_ld__inst0_waitrequest(local_avm_aspace7_waitrequest[0][0]),
      .avm_local_bb2_ld__inst0_readdata(local_avm_aspace7_readdata[0][0]),
      .avm_local_bb2_ld__inst0_readdatavalid(local_avm_aspace7_readdatavalid[0][0]),
      .avm_local_bb2_ld__inst0_writeack(local_avm_aspace7_writeack[0][0]),
      // AVM avm_local_bb2_st__233_inst0
      .avm_local_bb2_st__233_inst0_enable(local_avm_aspace7_enable[0][1]),
      .avm_local_bb2_st__233_inst0_read(local_avm_aspace7_read[0][1]),
      .avm_local_bb2_st__233_inst0_write(local_avm_aspace7_write[0][1]),
      .avm_local_bb2_st__233_inst0_burstcount(local_avm_aspace7_burstcount[0][1]),
      .avm_local_bb2_st__233_inst0_address(local_avm_aspace7_address[0][1]),
      .avm_local_bb2_st__233_inst0_writedata(local_avm_aspace7_writedata[0][1]),
      .avm_local_bb2_st__233_inst0_byteenable(local_avm_aspace7_byteenable[0][1]),
      .avm_local_bb2_st__233_inst0_waitrequest(local_avm_aspace7_waitrequest[0][1]),
      .avm_local_bb2_st__233_inst0_readdata(local_avm_aspace7_readdata[0][1]),
      .avm_local_bb2_st__233_inst0_readdatavalid(local_avm_aspace7_readdatavalid[0][1]),
      .avm_local_bb2_st__233_inst0_writeack(local_avm_aspace7_writeack[0][1]),
      // AVM avm_local_bb2_ld__233_inst0
      .avm_local_bb2_ld__233_inst0_enable(local_avm_aspace8_enable[0][0]),
      .avm_local_bb2_ld__233_inst0_read(local_avm_aspace8_read[0][0]),
      .avm_local_bb2_ld__233_inst0_write(local_avm_aspace8_write[0][0]),
      .avm_local_bb2_ld__233_inst0_burstcount(local_avm_aspace8_burstcount[0][0]),
      .avm_local_bb2_ld__233_inst0_address(local_avm_aspace8_address[0][0]),
      .avm_local_bb2_ld__233_inst0_writedata(local_avm_aspace8_writedata[0][0]),
      .avm_local_bb2_ld__233_inst0_byteenable(local_avm_aspace8_byteenable[0][0]),
      .avm_local_bb2_ld__233_inst0_waitrequest(local_avm_aspace8_waitrequest[0][0]),
      .avm_local_bb2_ld__233_inst0_readdata(local_avm_aspace8_readdata[0][0]),
      .avm_local_bb2_ld__233_inst0_readdatavalid(local_avm_aspace8_readdatavalid[0][0]),
      .avm_local_bb2_ld__233_inst0_writeack(local_avm_aspace8_writeack[0][0]),
      // AVM avm_local_bb2_st__inst0
      .avm_local_bb2_st__inst0_enable(local_avm_aspace8_enable[0][1]),
      .avm_local_bb2_st__inst0_read(local_avm_aspace8_read[0][1]),
      .avm_local_bb2_st__inst0_write(local_avm_aspace8_write[0][1]),
      .avm_local_bb2_st__inst0_burstcount(local_avm_aspace8_burstcount[0][1]),
      .avm_local_bb2_st__inst0_address(local_avm_aspace8_address[0][1]),
      .avm_local_bb2_st__inst0_writedata(local_avm_aspace8_writedata[0][1]),
      .avm_local_bb2_st__inst0_byteenable(local_avm_aspace8_byteenable[0][1]),
      .avm_local_bb2_st__inst0_waitrequest(local_avm_aspace8_waitrequest[0][1]),
      .avm_local_bb2_st__inst0_readdata(local_avm_aspace8_readdata[0][1]),
      .avm_local_bb2_st__inst0_readdatavalid(local_avm_aspace8_readdatavalid[0][1]),
      .avm_local_bb2_st__inst0_writeack(local_avm_aspace8_writeack[0][1]),
      // AVM avm_local_bb2_ld__235_inst0
      .avm_local_bb2_ld__235_inst0_enable(local_avm_aspace10_enable[0][0]),
      .avm_local_bb2_ld__235_inst0_read(local_avm_aspace10_read[0][0]),
      .avm_local_bb2_ld__235_inst0_write(local_avm_aspace10_write[0][0]),
      .avm_local_bb2_ld__235_inst0_burstcount(local_avm_aspace10_burstcount[0][0]),
      .avm_local_bb2_ld__235_inst0_address(local_avm_aspace10_address[0][0]),
      .avm_local_bb2_ld__235_inst0_writedata(local_avm_aspace10_writedata[0][0]),
      .avm_local_bb2_ld__235_inst0_byteenable(local_avm_aspace10_byteenable[0][0]),
      .avm_local_bb2_ld__235_inst0_waitrequest(local_avm_aspace10_waitrequest[0][0]),
      .avm_local_bb2_ld__235_inst0_readdata(local_avm_aspace10_readdata[0][0]),
      .avm_local_bb2_ld__235_inst0_readdatavalid(local_avm_aspace10_readdatavalid[0][0]),
      .avm_local_bb2_ld__235_inst0_writeack(local_avm_aspace10_writeack[0][0]),
      // AVM avm_local_bb2_st__u21_inst0
      .avm_local_bb2_st__u21_inst0_enable(local_avm_aspace10_enable[0][1]),
      .avm_local_bb2_st__u21_inst0_read(local_avm_aspace10_read[0][1]),
      .avm_local_bb2_st__u21_inst0_write(local_avm_aspace10_write[0][1]),
      .avm_local_bb2_st__u21_inst0_burstcount(local_avm_aspace10_burstcount[0][1]),
      .avm_local_bb2_st__u21_inst0_address(local_avm_aspace10_address[0][1]),
      .avm_local_bb2_st__u21_inst0_writedata(local_avm_aspace10_writedata[0][1]),
      .avm_local_bb2_st__u21_inst0_byteenable(local_avm_aspace10_byteenable[0][1]),
      .avm_local_bb2_st__u21_inst0_waitrequest(local_avm_aspace10_waitrequest[0][1]),
      .avm_local_bb2_st__u21_inst0_readdata(local_avm_aspace10_readdata[0][1]),
      .avm_local_bb2_st__u21_inst0_readdatavalid(local_avm_aspace10_readdatavalid[0][1]),
      .avm_local_bb2_st__u21_inst0_writeack(local_avm_aspace10_writeack[0][1]),
      // AVM avm_local_bb2_ld__u36_inst0
      .avm_local_bb2_ld__u36_inst0_enable(local_avm_aspace11_enable[0][0]),
      .avm_local_bb2_ld__u36_inst0_read(local_avm_aspace11_read[0][0]),
      .avm_local_bb2_ld__u36_inst0_write(local_avm_aspace11_write[0][0]),
      .avm_local_bb2_ld__u36_inst0_burstcount(local_avm_aspace11_burstcount[0][0]),
      .avm_local_bb2_ld__u36_inst0_address(local_avm_aspace11_address[0][0]),
      .avm_local_bb2_ld__u36_inst0_writedata(local_avm_aspace11_writedata[0][0]),
      .avm_local_bb2_ld__u36_inst0_byteenable(local_avm_aspace11_byteenable[0][0]),
      .avm_local_bb2_ld__u36_inst0_waitrequest(local_avm_aspace11_waitrequest[0][0]),
      .avm_local_bb2_ld__u36_inst0_readdata(local_avm_aspace11_readdata[0][0]),
      .avm_local_bb2_ld__u36_inst0_readdatavalid(local_avm_aspace11_readdatavalid[0][0]),
      .avm_local_bb2_ld__u36_inst0_writeack(local_avm_aspace11_writeack[0][0]),
      // AVM avm_local_bb2_st__235_inst0
      .avm_local_bb2_st__235_inst0_enable(local_avm_aspace11_enable[0][1]),
      .avm_local_bb2_st__235_inst0_read(local_avm_aspace11_read[0][1]),
      .avm_local_bb2_st__235_inst0_write(local_avm_aspace11_write[0][1]),
      .avm_local_bb2_st__235_inst0_burstcount(local_avm_aspace11_burstcount[0][1]),
      .avm_local_bb2_st__235_inst0_address(local_avm_aspace11_address[0][1]),
      .avm_local_bb2_st__235_inst0_writedata(local_avm_aspace11_writedata[0][1]),
      .avm_local_bb2_st__235_inst0_byteenable(local_avm_aspace11_byteenable[0][1]),
      .avm_local_bb2_st__235_inst0_waitrequest(local_avm_aspace11_waitrequest[0][1]),
      .avm_local_bb2_st__235_inst0_readdata(local_avm_aspace11_readdata[0][1]),
      .avm_local_bb2_st__235_inst0_readdatavalid(local_avm_aspace11_readdatavalid[0][1]),
      .avm_local_bb2_st__235_inst0_writeack(local_avm_aspace11_writeack[0][1]),
      // AVM avm_local_bb2_ld__237_inst0
      .avm_local_bb2_ld__237_inst0_enable(local_avm_aspace12_enable[0][0]),
      .avm_local_bb2_ld__237_inst0_read(local_avm_aspace12_read[0][0]),
      .avm_local_bb2_ld__237_inst0_write(local_avm_aspace12_write[0][0]),
      .avm_local_bb2_ld__237_inst0_burstcount(local_avm_aspace12_burstcount[0][0]),
      .avm_local_bb2_ld__237_inst0_address(local_avm_aspace12_address[0][0]),
      .avm_local_bb2_ld__237_inst0_writedata(local_avm_aspace12_writedata[0][0]),
      .avm_local_bb2_ld__237_inst0_byteenable(local_avm_aspace12_byteenable[0][0]),
      .avm_local_bb2_ld__237_inst0_waitrequest(local_avm_aspace12_waitrequest[0][0]),
      .avm_local_bb2_ld__237_inst0_readdata(local_avm_aspace12_readdata[0][0]),
      .avm_local_bb2_ld__237_inst0_readdatavalid(local_avm_aspace12_readdatavalid[0][0]),
      .avm_local_bb2_ld__237_inst0_writeack(local_avm_aspace12_writeack[0][0]),
      // AVM avm_local_bb2_st__u22_inst0
      .avm_local_bb2_st__u22_inst0_enable(local_avm_aspace12_enable[0][1]),
      .avm_local_bb2_st__u22_inst0_read(local_avm_aspace12_read[0][1]),
      .avm_local_bb2_st__u22_inst0_write(local_avm_aspace12_write[0][1]),
      .avm_local_bb2_st__u22_inst0_burstcount(local_avm_aspace12_burstcount[0][1]),
      .avm_local_bb2_st__u22_inst0_address(local_avm_aspace12_address[0][1]),
      .avm_local_bb2_st__u22_inst0_writedata(local_avm_aspace12_writedata[0][1]),
      .avm_local_bb2_st__u22_inst0_byteenable(local_avm_aspace12_byteenable[0][1]),
      .avm_local_bb2_st__u22_inst0_waitrequest(local_avm_aspace12_waitrequest[0][1]),
      .avm_local_bb2_st__u22_inst0_readdata(local_avm_aspace12_readdata[0][1]),
      .avm_local_bb2_st__u22_inst0_readdatavalid(local_avm_aspace12_readdatavalid[0][1]),
      .avm_local_bb2_st__u22_inst0_writeack(local_avm_aspace12_writeack[0][1]),
      // AVM avm_local_bb2_ld__u37_inst0
      .avm_local_bb2_ld__u37_inst0_enable(local_avm_aspace13_enable[0][0]),
      .avm_local_bb2_ld__u37_inst0_read(local_avm_aspace13_read[0][0]),
      .avm_local_bb2_ld__u37_inst0_write(local_avm_aspace13_write[0][0]),
      .avm_local_bb2_ld__u37_inst0_burstcount(local_avm_aspace13_burstcount[0][0]),
      .avm_local_bb2_ld__u37_inst0_address(local_avm_aspace13_address[0][0]),
      .avm_local_bb2_ld__u37_inst0_writedata(local_avm_aspace13_writedata[0][0]),
      .avm_local_bb2_ld__u37_inst0_byteenable(local_avm_aspace13_byteenable[0][0]),
      .avm_local_bb2_ld__u37_inst0_waitrequest(local_avm_aspace13_waitrequest[0][0]),
      .avm_local_bb2_ld__u37_inst0_readdata(local_avm_aspace13_readdata[0][0]),
      .avm_local_bb2_ld__u37_inst0_readdatavalid(local_avm_aspace13_readdatavalid[0][0]),
      .avm_local_bb2_ld__u37_inst0_writeack(local_avm_aspace13_writeack[0][0]),
      // AVM avm_local_bb2_st__237_inst0
      .avm_local_bb2_st__237_inst0_enable(local_avm_aspace13_enable[0][1]),
      .avm_local_bb2_st__237_inst0_read(local_avm_aspace13_read[0][1]),
      .avm_local_bb2_st__237_inst0_write(local_avm_aspace13_write[0][1]),
      .avm_local_bb2_st__237_inst0_burstcount(local_avm_aspace13_burstcount[0][1]),
      .avm_local_bb2_st__237_inst0_address(local_avm_aspace13_address[0][1]),
      .avm_local_bb2_st__237_inst0_writedata(local_avm_aspace13_writedata[0][1]),
      .avm_local_bb2_st__237_inst0_byteenable(local_avm_aspace13_byteenable[0][1]),
      .avm_local_bb2_st__237_inst0_waitrequest(local_avm_aspace13_waitrequest[0][1]),
      .avm_local_bb2_st__237_inst0_readdata(local_avm_aspace13_readdata[0][1]),
      .avm_local_bb2_st__237_inst0_readdatavalid(local_avm_aspace13_readdatavalid[0][1]),
      .avm_local_bb2_st__237_inst0_writeack(local_avm_aspace13_writeack[0][1]),
      // AVM avm_local_bb2_ld__239_inst0
      .avm_local_bb2_ld__239_inst0_enable(local_avm_aspace14_enable[0][0]),
      .avm_local_bb2_ld__239_inst0_read(local_avm_aspace14_read[0][0]),
      .avm_local_bb2_ld__239_inst0_write(local_avm_aspace14_write[0][0]),
      .avm_local_bb2_ld__239_inst0_burstcount(local_avm_aspace14_burstcount[0][0]),
      .avm_local_bb2_ld__239_inst0_address(local_avm_aspace14_address[0][0]),
      .avm_local_bb2_ld__239_inst0_writedata(local_avm_aspace14_writedata[0][0]),
      .avm_local_bb2_ld__239_inst0_byteenable(local_avm_aspace14_byteenable[0][0]),
      .avm_local_bb2_ld__239_inst0_waitrequest(local_avm_aspace14_waitrequest[0][0]),
      .avm_local_bb2_ld__239_inst0_readdata(local_avm_aspace14_readdata[0][0]),
      .avm_local_bb2_ld__239_inst0_readdatavalid(local_avm_aspace14_readdatavalid[0][0]),
      .avm_local_bb2_ld__239_inst0_writeack(local_avm_aspace14_writeack[0][0]),
      // AVM avm_local_bb2_st__u23_inst0
      .avm_local_bb2_st__u23_inst0_enable(local_avm_aspace14_enable[0][1]),
      .avm_local_bb2_st__u23_inst0_read(local_avm_aspace14_read[0][1]),
      .avm_local_bb2_st__u23_inst0_write(local_avm_aspace14_write[0][1]),
      .avm_local_bb2_st__u23_inst0_burstcount(local_avm_aspace14_burstcount[0][1]),
      .avm_local_bb2_st__u23_inst0_address(local_avm_aspace14_address[0][1]),
      .avm_local_bb2_st__u23_inst0_writedata(local_avm_aspace14_writedata[0][1]),
      .avm_local_bb2_st__u23_inst0_byteenable(local_avm_aspace14_byteenable[0][1]),
      .avm_local_bb2_st__u23_inst0_waitrequest(local_avm_aspace14_waitrequest[0][1]),
      .avm_local_bb2_st__u23_inst0_readdata(local_avm_aspace14_readdata[0][1]),
      .avm_local_bb2_st__u23_inst0_readdatavalid(local_avm_aspace14_readdatavalid[0][1]),
      .avm_local_bb2_st__u23_inst0_writeack(local_avm_aspace14_writeack[0][1]),
      // AVM avm_local_bb2_ld__u38_inst0
      .avm_local_bb2_ld__u38_inst0_enable(local_avm_aspace15_enable[0][0]),
      .avm_local_bb2_ld__u38_inst0_read(local_avm_aspace15_read[0][0]),
      .avm_local_bb2_ld__u38_inst0_write(local_avm_aspace15_write[0][0]),
      .avm_local_bb2_ld__u38_inst0_burstcount(local_avm_aspace15_burstcount[0][0]),
      .avm_local_bb2_ld__u38_inst0_address(local_avm_aspace15_address[0][0]),
      .avm_local_bb2_ld__u38_inst0_writedata(local_avm_aspace15_writedata[0][0]),
      .avm_local_bb2_ld__u38_inst0_byteenable(local_avm_aspace15_byteenable[0][0]),
      .avm_local_bb2_ld__u38_inst0_waitrequest(local_avm_aspace15_waitrequest[0][0]),
      .avm_local_bb2_ld__u38_inst0_readdata(local_avm_aspace15_readdata[0][0]),
      .avm_local_bb2_ld__u38_inst0_readdatavalid(local_avm_aspace15_readdatavalid[0][0]),
      .avm_local_bb2_ld__u38_inst0_writeack(local_avm_aspace15_writeack[0][0]),
      // AVM avm_local_bb2_st__239_inst0
      .avm_local_bb2_st__239_inst0_enable(local_avm_aspace15_enable[0][1]),
      .avm_local_bb2_st__239_inst0_read(local_avm_aspace15_read[0][1]),
      .avm_local_bb2_st__239_inst0_write(local_avm_aspace15_write[0][1]),
      .avm_local_bb2_st__239_inst0_burstcount(local_avm_aspace15_burstcount[0][1]),
      .avm_local_bb2_st__239_inst0_address(local_avm_aspace15_address[0][1]),
      .avm_local_bb2_st__239_inst0_writedata(local_avm_aspace15_writedata[0][1]),
      .avm_local_bb2_st__239_inst0_byteenable(local_avm_aspace15_byteenable[0][1]),
      .avm_local_bb2_st__239_inst0_waitrequest(local_avm_aspace15_waitrequest[0][1]),
      .avm_local_bb2_st__239_inst0_readdata(local_avm_aspace15_readdata[0][1]),
      .avm_local_bb2_st__239_inst0_readdatavalid(local_avm_aspace15_readdatavalid[0][1]),
      .avm_local_bb2_st__239_inst0_writeack(local_avm_aspace15_writeack[0][1]),
      // AVM avm_local_bb2_ld__241_inst0
      .avm_local_bb2_ld__241_inst0_enable(local_avm_aspace16_enable[0][0]),
      .avm_local_bb2_ld__241_inst0_read(local_avm_aspace16_read[0][0]),
      .avm_local_bb2_ld__241_inst0_write(local_avm_aspace16_write[0][0]),
      .avm_local_bb2_ld__241_inst0_burstcount(local_avm_aspace16_burstcount[0][0]),
      .avm_local_bb2_ld__241_inst0_address(local_avm_aspace16_address[0][0]),
      .avm_local_bb2_ld__241_inst0_writedata(local_avm_aspace16_writedata[0][0]),
      .avm_local_bb2_ld__241_inst0_byteenable(local_avm_aspace16_byteenable[0][0]),
      .avm_local_bb2_ld__241_inst0_waitrequest(local_avm_aspace16_waitrequest[0][0]),
      .avm_local_bb2_ld__241_inst0_readdata(local_avm_aspace16_readdata[0][0]),
      .avm_local_bb2_ld__241_inst0_readdatavalid(local_avm_aspace16_readdatavalid[0][0]),
      .avm_local_bb2_ld__241_inst0_writeack(local_avm_aspace16_writeack[0][0]),
      // AVM avm_local_bb2_st__u24_inst0
      .avm_local_bb2_st__u24_inst0_enable(local_avm_aspace16_enable[0][1]),
      .avm_local_bb2_st__u24_inst0_read(local_avm_aspace16_read[0][1]),
      .avm_local_bb2_st__u24_inst0_write(local_avm_aspace16_write[0][1]),
      .avm_local_bb2_st__u24_inst0_burstcount(local_avm_aspace16_burstcount[0][1]),
      .avm_local_bb2_st__u24_inst0_address(local_avm_aspace16_address[0][1]),
      .avm_local_bb2_st__u24_inst0_writedata(local_avm_aspace16_writedata[0][1]),
      .avm_local_bb2_st__u24_inst0_byteenable(local_avm_aspace16_byteenable[0][1]),
      .avm_local_bb2_st__u24_inst0_waitrequest(local_avm_aspace16_waitrequest[0][1]),
      .avm_local_bb2_st__u24_inst0_readdata(local_avm_aspace16_readdata[0][1]),
      .avm_local_bb2_st__u24_inst0_readdatavalid(local_avm_aspace16_readdatavalid[0][1]),
      .avm_local_bb2_st__u24_inst0_writeack(local_avm_aspace16_writeack[0][1]),
      // AVM avm_local_bb2_ld__u39_inst0
      .avm_local_bb2_ld__u39_inst0_enable(local_avm_aspace17_enable[0][0]),
      .avm_local_bb2_ld__u39_inst0_read(local_avm_aspace17_read[0][0]),
      .avm_local_bb2_ld__u39_inst0_write(local_avm_aspace17_write[0][0]),
      .avm_local_bb2_ld__u39_inst0_burstcount(local_avm_aspace17_burstcount[0][0]),
      .avm_local_bb2_ld__u39_inst0_address(local_avm_aspace17_address[0][0]),
      .avm_local_bb2_ld__u39_inst0_writedata(local_avm_aspace17_writedata[0][0]),
      .avm_local_bb2_ld__u39_inst0_byteenable(local_avm_aspace17_byteenable[0][0]),
      .avm_local_bb2_ld__u39_inst0_waitrequest(local_avm_aspace17_waitrequest[0][0]),
      .avm_local_bb2_ld__u39_inst0_readdata(local_avm_aspace17_readdata[0][0]),
      .avm_local_bb2_ld__u39_inst0_readdatavalid(local_avm_aspace17_readdatavalid[0][0]),
      .avm_local_bb2_ld__u39_inst0_writeack(local_avm_aspace17_writeack[0][0]),
      // AVM avm_local_bb2_st__241_inst0
      .avm_local_bb2_st__241_inst0_enable(local_avm_aspace17_enable[0][1]),
      .avm_local_bb2_st__241_inst0_read(local_avm_aspace17_read[0][1]),
      .avm_local_bb2_st__241_inst0_write(local_avm_aspace17_write[0][1]),
      .avm_local_bb2_st__241_inst0_burstcount(local_avm_aspace17_burstcount[0][1]),
      .avm_local_bb2_st__241_inst0_address(local_avm_aspace17_address[0][1]),
      .avm_local_bb2_st__241_inst0_writedata(local_avm_aspace17_writedata[0][1]),
      .avm_local_bb2_st__241_inst0_byteenable(local_avm_aspace17_byteenable[0][1]),
      .avm_local_bb2_st__241_inst0_waitrequest(local_avm_aspace17_waitrequest[0][1]),
      .avm_local_bb2_st__241_inst0_readdata(local_avm_aspace17_readdata[0][1]),
      .avm_local_bb2_st__241_inst0_readdatavalid(local_avm_aspace17_readdatavalid[0][1]),
      .avm_local_bb2_st__241_inst0_writeack(local_avm_aspace17_writeack[0][1]),
      // AVM avm_local_bb2_ld__243_inst0
      .avm_local_bb2_ld__243_inst0_enable(local_avm_aspace18_enable[0][0]),
      .avm_local_bb2_ld__243_inst0_read(local_avm_aspace18_read[0][0]),
      .avm_local_bb2_ld__243_inst0_write(local_avm_aspace18_write[0][0]),
      .avm_local_bb2_ld__243_inst0_burstcount(local_avm_aspace18_burstcount[0][0]),
      .avm_local_bb2_ld__243_inst0_address(local_avm_aspace18_address[0][0]),
      .avm_local_bb2_ld__243_inst0_writedata(local_avm_aspace18_writedata[0][0]),
      .avm_local_bb2_ld__243_inst0_byteenable(local_avm_aspace18_byteenable[0][0]),
      .avm_local_bb2_ld__243_inst0_waitrequest(local_avm_aspace18_waitrequest[0][0]),
      .avm_local_bb2_ld__243_inst0_readdata(local_avm_aspace18_readdata[0][0]),
      .avm_local_bb2_ld__243_inst0_readdatavalid(local_avm_aspace18_readdatavalid[0][0]),
      .avm_local_bb2_ld__243_inst0_writeack(local_avm_aspace18_writeack[0][0]),
      // AVM avm_local_bb2_st__u25_inst0
      .avm_local_bb2_st__u25_inst0_enable(local_avm_aspace18_enable[0][1]),
      .avm_local_bb2_st__u25_inst0_read(local_avm_aspace18_read[0][1]),
      .avm_local_bb2_st__u25_inst0_write(local_avm_aspace18_write[0][1]),
      .avm_local_bb2_st__u25_inst0_burstcount(local_avm_aspace18_burstcount[0][1]),
      .avm_local_bb2_st__u25_inst0_address(local_avm_aspace18_address[0][1]),
      .avm_local_bb2_st__u25_inst0_writedata(local_avm_aspace18_writedata[0][1]),
      .avm_local_bb2_st__u25_inst0_byteenable(local_avm_aspace18_byteenable[0][1]),
      .avm_local_bb2_st__u25_inst0_waitrequest(local_avm_aspace18_waitrequest[0][1]),
      .avm_local_bb2_st__u25_inst0_readdata(local_avm_aspace18_readdata[0][1]),
      .avm_local_bb2_st__u25_inst0_readdatavalid(local_avm_aspace18_readdatavalid[0][1]),
      .avm_local_bb2_st__u25_inst0_writeack(local_avm_aspace18_writeack[0][1]),
      // AVM avm_local_bb2_ld__u40_inst0
      .avm_local_bb2_ld__u40_inst0_enable(local_avm_aspace19_enable[0][0]),
      .avm_local_bb2_ld__u40_inst0_read(local_avm_aspace19_read[0][0]),
      .avm_local_bb2_ld__u40_inst0_write(local_avm_aspace19_write[0][0]),
      .avm_local_bb2_ld__u40_inst0_burstcount(local_avm_aspace19_burstcount[0][0]),
      .avm_local_bb2_ld__u40_inst0_address(local_avm_aspace19_address[0][0]),
      .avm_local_bb2_ld__u40_inst0_writedata(local_avm_aspace19_writedata[0][0]),
      .avm_local_bb2_ld__u40_inst0_byteenable(local_avm_aspace19_byteenable[0][0]),
      .avm_local_bb2_ld__u40_inst0_waitrequest(local_avm_aspace19_waitrequest[0][0]),
      .avm_local_bb2_ld__u40_inst0_readdata(local_avm_aspace19_readdata[0][0]),
      .avm_local_bb2_ld__u40_inst0_readdatavalid(local_avm_aspace19_readdatavalid[0][0]),
      .avm_local_bb2_ld__u40_inst0_writeack(local_avm_aspace19_writeack[0][0]),
      // AVM avm_local_bb2_st__243_inst0
      .avm_local_bb2_st__243_inst0_enable(local_avm_aspace19_enable[0][1]),
      .avm_local_bb2_st__243_inst0_read(local_avm_aspace19_read[0][1]),
      .avm_local_bb2_st__243_inst0_write(local_avm_aspace19_write[0][1]),
      .avm_local_bb2_st__243_inst0_burstcount(local_avm_aspace19_burstcount[0][1]),
      .avm_local_bb2_st__243_inst0_address(local_avm_aspace19_address[0][1]),
      .avm_local_bb2_st__243_inst0_writedata(local_avm_aspace19_writedata[0][1]),
      .avm_local_bb2_st__243_inst0_byteenable(local_avm_aspace19_byteenable[0][1]),
      .avm_local_bb2_st__243_inst0_waitrequest(local_avm_aspace19_waitrequest[0][1]),
      .avm_local_bb2_st__243_inst0_readdata(local_avm_aspace19_readdata[0][1]),
      .avm_local_bb2_st__243_inst0_readdatavalid(local_avm_aspace19_readdatavalid[0][1]),
      .avm_local_bb2_st__243_inst0_writeack(local_avm_aspace19_writeack[0][1]),
      // AVM avm_local_bb2_ld__245_inst0
      .avm_local_bb2_ld__245_inst0_enable(local_avm_aspace20_enable[0][0]),
      .avm_local_bb2_ld__245_inst0_read(local_avm_aspace20_read[0][0]),
      .avm_local_bb2_ld__245_inst0_write(local_avm_aspace20_write[0][0]),
      .avm_local_bb2_ld__245_inst0_burstcount(local_avm_aspace20_burstcount[0][0]),
      .avm_local_bb2_ld__245_inst0_address(local_avm_aspace20_address[0][0]),
      .avm_local_bb2_ld__245_inst0_writedata(local_avm_aspace20_writedata[0][0]),
      .avm_local_bb2_ld__245_inst0_byteenable(local_avm_aspace20_byteenable[0][0]),
      .avm_local_bb2_ld__245_inst0_waitrequest(local_avm_aspace20_waitrequest[0][0]),
      .avm_local_bb2_ld__245_inst0_readdata(local_avm_aspace20_readdata[0][0]),
      .avm_local_bb2_ld__245_inst0_readdatavalid(local_avm_aspace20_readdatavalid[0][0]),
      .avm_local_bb2_ld__245_inst0_writeack(local_avm_aspace20_writeack[0][0]),
      // AVM avm_local_bb2_st__u26_inst0
      .avm_local_bb2_st__u26_inst0_enable(local_avm_aspace20_enable[0][1]),
      .avm_local_bb2_st__u26_inst0_read(local_avm_aspace20_read[0][1]),
      .avm_local_bb2_st__u26_inst0_write(local_avm_aspace20_write[0][1]),
      .avm_local_bb2_st__u26_inst0_burstcount(local_avm_aspace20_burstcount[0][1]),
      .avm_local_bb2_st__u26_inst0_address(local_avm_aspace20_address[0][1]),
      .avm_local_bb2_st__u26_inst0_writedata(local_avm_aspace20_writedata[0][1]),
      .avm_local_bb2_st__u26_inst0_byteenable(local_avm_aspace20_byteenable[0][1]),
      .avm_local_bb2_st__u26_inst0_waitrequest(local_avm_aspace20_waitrequest[0][1]),
      .avm_local_bb2_st__u26_inst0_readdata(local_avm_aspace20_readdata[0][1]),
      .avm_local_bb2_st__u26_inst0_readdatavalid(local_avm_aspace20_readdatavalid[0][1]),
      .avm_local_bb2_st__u26_inst0_writeack(local_avm_aspace20_writeack[0][1]),
      // AVM avm_local_bb2_ld__u41_inst0
      .avm_local_bb2_ld__u41_inst0_enable(local_avm_aspace21_enable[0][0]),
      .avm_local_bb2_ld__u41_inst0_read(local_avm_aspace21_read[0][0]),
      .avm_local_bb2_ld__u41_inst0_write(local_avm_aspace21_write[0][0]),
      .avm_local_bb2_ld__u41_inst0_burstcount(local_avm_aspace21_burstcount[0][0]),
      .avm_local_bb2_ld__u41_inst0_address(local_avm_aspace21_address[0][0]),
      .avm_local_bb2_ld__u41_inst0_writedata(local_avm_aspace21_writedata[0][0]),
      .avm_local_bb2_ld__u41_inst0_byteenable(local_avm_aspace21_byteenable[0][0]),
      .avm_local_bb2_ld__u41_inst0_waitrequest(local_avm_aspace21_waitrequest[0][0]),
      .avm_local_bb2_ld__u41_inst0_readdata(local_avm_aspace21_readdata[0][0]),
      .avm_local_bb2_ld__u41_inst0_readdatavalid(local_avm_aspace21_readdatavalid[0][0]),
      .avm_local_bb2_ld__u41_inst0_writeack(local_avm_aspace21_writeack[0][0]),
      // AVM avm_local_bb2_st__245_inst0
      .avm_local_bb2_st__245_inst0_enable(local_avm_aspace21_enable[0][1]),
      .avm_local_bb2_st__245_inst0_read(local_avm_aspace21_read[0][1]),
      .avm_local_bb2_st__245_inst0_write(local_avm_aspace21_write[0][1]),
      .avm_local_bb2_st__245_inst0_burstcount(local_avm_aspace21_burstcount[0][1]),
      .avm_local_bb2_st__245_inst0_address(local_avm_aspace21_address[0][1]),
      .avm_local_bb2_st__245_inst0_writedata(local_avm_aspace21_writedata[0][1]),
      .avm_local_bb2_st__245_inst0_byteenable(local_avm_aspace21_byteenable[0][1]),
      .avm_local_bb2_st__245_inst0_waitrequest(local_avm_aspace21_waitrequest[0][1]),
      .avm_local_bb2_st__245_inst0_readdata(local_avm_aspace21_readdata[0][1]),
      .avm_local_bb2_st__245_inst0_readdatavalid(local_avm_aspace21_readdatavalid[0][1]),
      .avm_local_bb2_st__245_inst0_writeack(local_avm_aspace21_writeack[0][1]),
      // AVM avm_local_bb2_ld__247_inst0
      .avm_local_bb2_ld__247_inst0_enable(local_avm_aspace22_enable[0][0]),
      .avm_local_bb2_ld__247_inst0_read(local_avm_aspace22_read[0][0]),
      .avm_local_bb2_ld__247_inst0_write(local_avm_aspace22_write[0][0]),
      .avm_local_bb2_ld__247_inst0_burstcount(local_avm_aspace22_burstcount[0][0]),
      .avm_local_bb2_ld__247_inst0_address(local_avm_aspace22_address[0][0]),
      .avm_local_bb2_ld__247_inst0_writedata(local_avm_aspace22_writedata[0][0]),
      .avm_local_bb2_ld__247_inst0_byteenable(local_avm_aspace22_byteenable[0][0]),
      .avm_local_bb2_ld__247_inst0_waitrequest(local_avm_aspace22_waitrequest[0][0]),
      .avm_local_bb2_ld__247_inst0_readdata(local_avm_aspace22_readdata[0][0]),
      .avm_local_bb2_ld__247_inst0_readdatavalid(local_avm_aspace22_readdatavalid[0][0]),
      .avm_local_bb2_ld__247_inst0_writeack(local_avm_aspace22_writeack[0][0]),
      // AVM avm_local_bb2_st__u27_inst0
      .avm_local_bb2_st__u27_inst0_enable(local_avm_aspace22_enable[0][1]),
      .avm_local_bb2_st__u27_inst0_read(local_avm_aspace22_read[0][1]),
      .avm_local_bb2_st__u27_inst0_write(local_avm_aspace22_write[0][1]),
      .avm_local_bb2_st__u27_inst0_burstcount(local_avm_aspace22_burstcount[0][1]),
      .avm_local_bb2_st__u27_inst0_address(local_avm_aspace22_address[0][1]),
      .avm_local_bb2_st__u27_inst0_writedata(local_avm_aspace22_writedata[0][1]),
      .avm_local_bb2_st__u27_inst0_byteenable(local_avm_aspace22_byteenable[0][1]),
      .avm_local_bb2_st__u27_inst0_waitrequest(local_avm_aspace22_waitrequest[0][1]),
      .avm_local_bb2_st__u27_inst0_readdata(local_avm_aspace22_readdata[0][1]),
      .avm_local_bb2_st__u27_inst0_readdatavalid(local_avm_aspace22_readdatavalid[0][1]),
      .avm_local_bb2_st__u27_inst0_writeack(local_avm_aspace22_writeack[0][1]),
      // AVM avm_local_bb2_ld__u42_inst0
      .avm_local_bb2_ld__u42_inst0_enable(local_avm_aspace23_enable[0][0]),
      .avm_local_bb2_ld__u42_inst0_read(local_avm_aspace23_read[0][0]),
      .avm_local_bb2_ld__u42_inst0_write(local_avm_aspace23_write[0][0]),
      .avm_local_bb2_ld__u42_inst0_burstcount(local_avm_aspace23_burstcount[0][0]),
      .avm_local_bb2_ld__u42_inst0_address(local_avm_aspace23_address[0][0]),
      .avm_local_bb2_ld__u42_inst0_writedata(local_avm_aspace23_writedata[0][0]),
      .avm_local_bb2_ld__u42_inst0_byteenable(local_avm_aspace23_byteenable[0][0]),
      .avm_local_bb2_ld__u42_inst0_waitrequest(local_avm_aspace23_waitrequest[0][0]),
      .avm_local_bb2_ld__u42_inst0_readdata(local_avm_aspace23_readdata[0][0]),
      .avm_local_bb2_ld__u42_inst0_readdatavalid(local_avm_aspace23_readdatavalid[0][0]),
      .avm_local_bb2_ld__u42_inst0_writeack(local_avm_aspace23_writeack[0][0]),
      // AVM avm_local_bb2_st__247_inst0
      .avm_local_bb2_st__247_inst0_enable(local_avm_aspace23_enable[0][1]),
      .avm_local_bb2_st__247_inst0_read(local_avm_aspace23_read[0][1]),
      .avm_local_bb2_st__247_inst0_write(local_avm_aspace23_write[0][1]),
      .avm_local_bb2_st__247_inst0_burstcount(local_avm_aspace23_burstcount[0][1]),
      .avm_local_bb2_st__247_inst0_address(local_avm_aspace23_address[0][1]),
      .avm_local_bb2_st__247_inst0_writedata(local_avm_aspace23_writedata[0][1]),
      .avm_local_bb2_st__247_inst0_byteenable(local_avm_aspace23_byteenable[0][1]),
      .avm_local_bb2_st__247_inst0_waitrequest(local_avm_aspace23_waitrequest[0][1]),
      .avm_local_bb2_st__247_inst0_readdata(local_avm_aspace23_readdata[0][1]),
      .avm_local_bb2_st__247_inst0_readdatavalid(local_avm_aspace23_readdatavalid[0][1]),
      .avm_local_bb2_st__247_inst0_writeack(local_avm_aspace23_writeack[0][1]),
      // AVM avm_local_bb2_ld__249_inst0
      .avm_local_bb2_ld__249_inst0_enable(local_avm_aspace24_enable[0][0]),
      .avm_local_bb2_ld__249_inst0_read(local_avm_aspace24_read[0][0]),
      .avm_local_bb2_ld__249_inst0_write(local_avm_aspace24_write[0][0]),
      .avm_local_bb2_ld__249_inst0_burstcount(local_avm_aspace24_burstcount[0][0]),
      .avm_local_bb2_ld__249_inst0_address(local_avm_aspace24_address[0][0]),
      .avm_local_bb2_ld__249_inst0_writedata(local_avm_aspace24_writedata[0][0]),
      .avm_local_bb2_ld__249_inst0_byteenable(local_avm_aspace24_byteenable[0][0]),
      .avm_local_bb2_ld__249_inst0_waitrequest(local_avm_aspace24_waitrequest[0][0]),
      .avm_local_bb2_ld__249_inst0_readdata(local_avm_aspace24_readdata[0][0]),
      .avm_local_bb2_ld__249_inst0_readdatavalid(local_avm_aspace24_readdatavalid[0][0]),
      .avm_local_bb2_ld__249_inst0_writeack(local_avm_aspace24_writeack[0][0]),
      // AVM avm_local_bb2_st__u28_inst0
      .avm_local_bb2_st__u28_inst0_enable(local_avm_aspace24_enable[0][1]),
      .avm_local_bb2_st__u28_inst0_read(local_avm_aspace24_read[0][1]),
      .avm_local_bb2_st__u28_inst0_write(local_avm_aspace24_write[0][1]),
      .avm_local_bb2_st__u28_inst0_burstcount(local_avm_aspace24_burstcount[0][1]),
      .avm_local_bb2_st__u28_inst0_address(local_avm_aspace24_address[0][1]),
      .avm_local_bb2_st__u28_inst0_writedata(local_avm_aspace24_writedata[0][1]),
      .avm_local_bb2_st__u28_inst0_byteenable(local_avm_aspace24_byteenable[0][1]),
      .avm_local_bb2_st__u28_inst0_waitrequest(local_avm_aspace24_waitrequest[0][1]),
      .avm_local_bb2_st__u28_inst0_readdata(local_avm_aspace24_readdata[0][1]),
      .avm_local_bb2_st__u28_inst0_readdatavalid(local_avm_aspace24_readdatavalid[0][1]),
      .avm_local_bb2_st__u28_inst0_writeack(local_avm_aspace24_writeack[0][1]),
      // AVM avm_local_bb2_ld__u43_inst0
      .avm_local_bb2_ld__u43_inst0_enable(local_avm_aspace25_enable[0][0]),
      .avm_local_bb2_ld__u43_inst0_read(local_avm_aspace25_read[0][0]),
      .avm_local_bb2_ld__u43_inst0_write(local_avm_aspace25_write[0][0]),
      .avm_local_bb2_ld__u43_inst0_burstcount(local_avm_aspace25_burstcount[0][0]),
      .avm_local_bb2_ld__u43_inst0_address(local_avm_aspace25_address[0][0]),
      .avm_local_bb2_ld__u43_inst0_writedata(local_avm_aspace25_writedata[0][0]),
      .avm_local_bb2_ld__u43_inst0_byteenable(local_avm_aspace25_byteenable[0][0]),
      .avm_local_bb2_ld__u43_inst0_waitrequest(local_avm_aspace25_waitrequest[0][0]),
      .avm_local_bb2_ld__u43_inst0_readdata(local_avm_aspace25_readdata[0][0]),
      .avm_local_bb2_ld__u43_inst0_readdatavalid(local_avm_aspace25_readdatavalid[0][0]),
      .avm_local_bb2_ld__u43_inst0_writeack(local_avm_aspace25_writeack[0][0]),
      // AVM avm_local_bb2_st__249_inst0
      .avm_local_bb2_st__249_inst0_enable(local_avm_aspace25_enable[0][1]),
      .avm_local_bb2_st__249_inst0_read(local_avm_aspace25_read[0][1]),
      .avm_local_bb2_st__249_inst0_write(local_avm_aspace25_write[0][1]),
      .avm_local_bb2_st__249_inst0_burstcount(local_avm_aspace25_burstcount[0][1]),
      .avm_local_bb2_st__249_inst0_address(local_avm_aspace25_address[0][1]),
      .avm_local_bb2_st__249_inst0_writedata(local_avm_aspace25_writedata[0][1]),
      .avm_local_bb2_st__249_inst0_byteenable(local_avm_aspace25_byteenable[0][1]),
      .avm_local_bb2_st__249_inst0_waitrequest(local_avm_aspace25_waitrequest[0][1]),
      .avm_local_bb2_st__249_inst0_readdata(local_avm_aspace25_readdata[0][1]),
      .avm_local_bb2_st__249_inst0_readdatavalid(local_avm_aspace25_readdatavalid[0][1]),
      .avm_local_bb2_st__249_inst0_writeack(local_avm_aspace25_writeack[0][1]),
      // AVM avm_local_bb2_ld__251_inst0
      .avm_local_bb2_ld__251_inst0_enable(local_avm_aspace26_enable[0][0]),
      .avm_local_bb2_ld__251_inst0_read(local_avm_aspace26_read[0][0]),
      .avm_local_bb2_ld__251_inst0_write(local_avm_aspace26_write[0][0]),
      .avm_local_bb2_ld__251_inst0_burstcount(local_avm_aspace26_burstcount[0][0]),
      .avm_local_bb2_ld__251_inst0_address(local_avm_aspace26_address[0][0]),
      .avm_local_bb2_ld__251_inst0_writedata(local_avm_aspace26_writedata[0][0]),
      .avm_local_bb2_ld__251_inst0_byteenable(local_avm_aspace26_byteenable[0][0]),
      .avm_local_bb2_ld__251_inst0_waitrequest(local_avm_aspace26_waitrequest[0][0]),
      .avm_local_bb2_ld__251_inst0_readdata(local_avm_aspace26_readdata[0][0]),
      .avm_local_bb2_ld__251_inst0_readdatavalid(local_avm_aspace26_readdatavalid[0][0]),
      .avm_local_bb2_ld__251_inst0_writeack(local_avm_aspace26_writeack[0][0]),
      // AVM avm_local_bb2_st__u29_inst0
      .avm_local_bb2_st__u29_inst0_enable(local_avm_aspace26_enable[0][1]),
      .avm_local_bb2_st__u29_inst0_read(local_avm_aspace26_read[0][1]),
      .avm_local_bb2_st__u29_inst0_write(local_avm_aspace26_write[0][1]),
      .avm_local_bb2_st__u29_inst0_burstcount(local_avm_aspace26_burstcount[0][1]),
      .avm_local_bb2_st__u29_inst0_address(local_avm_aspace26_address[0][1]),
      .avm_local_bb2_st__u29_inst0_writedata(local_avm_aspace26_writedata[0][1]),
      .avm_local_bb2_st__u29_inst0_byteenable(local_avm_aspace26_byteenable[0][1]),
      .avm_local_bb2_st__u29_inst0_waitrequest(local_avm_aspace26_waitrequest[0][1]),
      .avm_local_bb2_st__u29_inst0_readdata(local_avm_aspace26_readdata[0][1]),
      .avm_local_bb2_st__u29_inst0_readdatavalid(local_avm_aspace26_readdatavalid[0][1]),
      .avm_local_bb2_st__u29_inst0_writeack(local_avm_aspace26_writeack[0][1]),
      // AVM avm_local_bb2_ld__u44_inst0
      .avm_local_bb2_ld__u44_inst0_enable(local_avm_aspace27_enable[0][0]),
      .avm_local_bb2_ld__u44_inst0_read(local_avm_aspace27_read[0][0]),
      .avm_local_bb2_ld__u44_inst0_write(local_avm_aspace27_write[0][0]),
      .avm_local_bb2_ld__u44_inst0_burstcount(local_avm_aspace27_burstcount[0][0]),
      .avm_local_bb2_ld__u44_inst0_address(local_avm_aspace27_address[0][0]),
      .avm_local_bb2_ld__u44_inst0_writedata(local_avm_aspace27_writedata[0][0]),
      .avm_local_bb2_ld__u44_inst0_byteenable(local_avm_aspace27_byteenable[0][0]),
      .avm_local_bb2_ld__u44_inst0_waitrequest(local_avm_aspace27_waitrequest[0][0]),
      .avm_local_bb2_ld__u44_inst0_readdata(local_avm_aspace27_readdata[0][0]),
      .avm_local_bb2_ld__u44_inst0_readdatavalid(local_avm_aspace27_readdatavalid[0][0]),
      .avm_local_bb2_ld__u44_inst0_writeack(local_avm_aspace27_writeack[0][0]),
      // AVM avm_local_bb2_st__251_inst0
      .avm_local_bb2_st__251_inst0_enable(local_avm_aspace27_enable[0][1]),
      .avm_local_bb2_st__251_inst0_read(local_avm_aspace27_read[0][1]),
      .avm_local_bb2_st__251_inst0_write(local_avm_aspace27_write[0][1]),
      .avm_local_bb2_st__251_inst0_burstcount(local_avm_aspace27_burstcount[0][1]),
      .avm_local_bb2_st__251_inst0_address(local_avm_aspace27_address[0][1]),
      .avm_local_bb2_st__251_inst0_writedata(local_avm_aspace27_writedata[0][1]),
      .avm_local_bb2_st__251_inst0_byteenable(local_avm_aspace27_byteenable[0][1]),
      .avm_local_bb2_st__251_inst0_waitrequest(local_avm_aspace27_waitrequest[0][1]),
      .avm_local_bb2_st__251_inst0_readdata(local_avm_aspace27_readdata[0][1]),
      .avm_local_bb2_st__251_inst0_readdatavalid(local_avm_aspace27_readdatavalid[0][1]),
      .avm_local_bb2_st__251_inst0_writeack(local_avm_aspace27_writeack[0][1]),
      // AVM avm_local_bb2_ld__253_inst0
      .avm_local_bb2_ld__253_inst0_enable(local_avm_aspace28_enable[0][0]),
      .avm_local_bb2_ld__253_inst0_read(local_avm_aspace28_read[0][0]),
      .avm_local_bb2_ld__253_inst0_write(local_avm_aspace28_write[0][0]),
      .avm_local_bb2_ld__253_inst0_burstcount(local_avm_aspace28_burstcount[0][0]),
      .avm_local_bb2_ld__253_inst0_address(local_avm_aspace28_address[0][0]),
      .avm_local_bb2_ld__253_inst0_writedata(local_avm_aspace28_writedata[0][0]),
      .avm_local_bb2_ld__253_inst0_byteenable(local_avm_aspace28_byteenable[0][0]),
      .avm_local_bb2_ld__253_inst0_waitrequest(local_avm_aspace28_waitrequest[0][0]),
      .avm_local_bb2_ld__253_inst0_readdata(local_avm_aspace28_readdata[0][0]),
      .avm_local_bb2_ld__253_inst0_readdatavalid(local_avm_aspace28_readdatavalid[0][0]),
      .avm_local_bb2_ld__253_inst0_writeack(local_avm_aspace28_writeack[0][0]),
      // AVM avm_local_bb2_st__u30_inst0
      .avm_local_bb2_st__u30_inst0_enable(local_avm_aspace28_enable[0][1]),
      .avm_local_bb2_st__u30_inst0_read(local_avm_aspace28_read[0][1]),
      .avm_local_bb2_st__u30_inst0_write(local_avm_aspace28_write[0][1]),
      .avm_local_bb2_st__u30_inst0_burstcount(local_avm_aspace28_burstcount[0][1]),
      .avm_local_bb2_st__u30_inst0_address(local_avm_aspace28_address[0][1]),
      .avm_local_bb2_st__u30_inst0_writedata(local_avm_aspace28_writedata[0][1]),
      .avm_local_bb2_st__u30_inst0_byteenable(local_avm_aspace28_byteenable[0][1]),
      .avm_local_bb2_st__u30_inst0_waitrequest(local_avm_aspace28_waitrequest[0][1]),
      .avm_local_bb2_st__u30_inst0_readdata(local_avm_aspace28_readdata[0][1]),
      .avm_local_bb2_st__u30_inst0_readdatavalid(local_avm_aspace28_readdatavalid[0][1]),
      .avm_local_bb2_st__u30_inst0_writeack(local_avm_aspace28_writeack[0][1]),
      // AVM avm_local_bb2_ld__u45_inst0
      .avm_local_bb2_ld__u45_inst0_enable(local_avm_aspace29_enable[0][0]),
      .avm_local_bb2_ld__u45_inst0_read(local_avm_aspace29_read[0][0]),
      .avm_local_bb2_ld__u45_inst0_write(local_avm_aspace29_write[0][0]),
      .avm_local_bb2_ld__u45_inst0_burstcount(local_avm_aspace29_burstcount[0][0]),
      .avm_local_bb2_ld__u45_inst0_address(local_avm_aspace29_address[0][0]),
      .avm_local_bb2_ld__u45_inst0_writedata(local_avm_aspace29_writedata[0][0]),
      .avm_local_bb2_ld__u45_inst0_byteenable(local_avm_aspace29_byteenable[0][0]),
      .avm_local_bb2_ld__u45_inst0_waitrequest(local_avm_aspace29_waitrequest[0][0]),
      .avm_local_bb2_ld__u45_inst0_readdata(local_avm_aspace29_readdata[0][0]),
      .avm_local_bb2_ld__u45_inst0_readdatavalid(local_avm_aspace29_readdatavalid[0][0]),
      .avm_local_bb2_ld__u45_inst0_writeack(local_avm_aspace29_writeack[0][0]),
      // AVM avm_local_bb2_st__253_inst0
      .avm_local_bb2_st__253_inst0_enable(local_avm_aspace29_enable[0][1]),
      .avm_local_bb2_st__253_inst0_read(local_avm_aspace29_read[0][1]),
      .avm_local_bb2_st__253_inst0_write(local_avm_aspace29_write[0][1]),
      .avm_local_bb2_st__253_inst0_burstcount(local_avm_aspace29_burstcount[0][1]),
      .avm_local_bb2_st__253_inst0_address(local_avm_aspace29_address[0][1]),
      .avm_local_bb2_st__253_inst0_writedata(local_avm_aspace29_writedata[0][1]),
      .avm_local_bb2_st__253_inst0_byteenable(local_avm_aspace29_byteenable[0][1]),
      .avm_local_bb2_st__253_inst0_waitrequest(local_avm_aspace29_waitrequest[0][1]),
      .avm_local_bb2_st__253_inst0_readdata(local_avm_aspace29_readdata[0][1]),
      .avm_local_bb2_st__253_inst0_readdatavalid(local_avm_aspace29_readdatavalid[0][1]),
      .avm_local_bb2_st__253_inst0_writeack(local_avm_aspace29_writeack[0][1]),
      // AVM avm_local_bb2_ld__255_inst0
      .avm_local_bb2_ld__255_inst0_enable(local_avm_aspace30_enable[0][0]),
      .avm_local_bb2_ld__255_inst0_read(local_avm_aspace30_read[0][0]),
      .avm_local_bb2_ld__255_inst0_write(local_avm_aspace30_write[0][0]),
      .avm_local_bb2_ld__255_inst0_burstcount(local_avm_aspace30_burstcount[0][0]),
      .avm_local_bb2_ld__255_inst0_address(local_avm_aspace30_address[0][0]),
      .avm_local_bb2_ld__255_inst0_writedata(local_avm_aspace30_writedata[0][0]),
      .avm_local_bb2_ld__255_inst0_byteenable(local_avm_aspace30_byteenable[0][0]),
      .avm_local_bb2_ld__255_inst0_waitrequest(local_avm_aspace30_waitrequest[0][0]),
      .avm_local_bb2_ld__255_inst0_readdata(local_avm_aspace30_readdata[0][0]),
      .avm_local_bb2_ld__255_inst0_readdatavalid(local_avm_aspace30_readdatavalid[0][0]),
      .avm_local_bb2_ld__255_inst0_writeack(local_avm_aspace30_writeack[0][0]),
      // AVM avm_local_bb2_st__u31_inst0
      .avm_local_bb2_st__u31_inst0_enable(local_avm_aspace30_enable[0][1]),
      .avm_local_bb2_st__u31_inst0_read(local_avm_aspace30_read[0][1]),
      .avm_local_bb2_st__u31_inst0_write(local_avm_aspace30_write[0][1]),
      .avm_local_bb2_st__u31_inst0_burstcount(local_avm_aspace30_burstcount[0][1]),
      .avm_local_bb2_st__u31_inst0_address(local_avm_aspace30_address[0][1]),
      .avm_local_bb2_st__u31_inst0_writedata(local_avm_aspace30_writedata[0][1]),
      .avm_local_bb2_st__u31_inst0_byteenable(local_avm_aspace30_byteenable[0][1]),
      .avm_local_bb2_st__u31_inst0_waitrequest(local_avm_aspace30_waitrequest[0][1]),
      .avm_local_bb2_st__u31_inst0_readdata(local_avm_aspace30_readdata[0][1]),
      .avm_local_bb2_st__u31_inst0_readdatavalid(local_avm_aspace30_readdatavalid[0][1]),
      .avm_local_bb2_st__u31_inst0_writeack(local_avm_aspace30_writeack[0][1]),
      // AVM avm_local_bb2_ld__u46_inst0
      .avm_local_bb2_ld__u46_inst0_enable(local_avm_aspace31_enable[0][0]),
      .avm_local_bb2_ld__u46_inst0_read(local_avm_aspace31_read[0][0]),
      .avm_local_bb2_ld__u46_inst0_write(local_avm_aspace31_write[0][0]),
      .avm_local_bb2_ld__u46_inst0_burstcount(local_avm_aspace31_burstcount[0][0]),
      .avm_local_bb2_ld__u46_inst0_address(local_avm_aspace31_address[0][0]),
      .avm_local_bb2_ld__u46_inst0_writedata(local_avm_aspace31_writedata[0][0]),
      .avm_local_bb2_ld__u46_inst0_byteenable(local_avm_aspace31_byteenable[0][0]),
      .avm_local_bb2_ld__u46_inst0_waitrequest(local_avm_aspace31_waitrequest[0][0]),
      .avm_local_bb2_ld__u46_inst0_readdata(local_avm_aspace31_readdata[0][0]),
      .avm_local_bb2_ld__u46_inst0_readdatavalid(local_avm_aspace31_readdatavalid[0][0]),
      .avm_local_bb2_ld__u46_inst0_writeack(local_avm_aspace31_writeack[0][0]),
      // AVM avm_local_bb2_st__255_inst0
      .avm_local_bb2_st__255_inst0_enable(local_avm_aspace31_enable[0][1]),
      .avm_local_bb2_st__255_inst0_read(local_avm_aspace31_read[0][1]),
      .avm_local_bb2_st__255_inst0_write(local_avm_aspace31_write[0][1]),
      .avm_local_bb2_st__255_inst0_burstcount(local_avm_aspace31_burstcount[0][1]),
      .avm_local_bb2_st__255_inst0_address(local_avm_aspace31_address[0][1]),
      .avm_local_bb2_st__255_inst0_writedata(local_avm_aspace31_writedata[0][1]),
      .avm_local_bb2_st__255_inst0_byteenable(local_avm_aspace31_byteenable[0][1]),
      .avm_local_bb2_st__255_inst0_waitrequest(local_avm_aspace31_waitrequest[0][1]),
      .avm_local_bb2_st__255_inst0_readdata(local_avm_aspace31_readdata[0][1]),
      .avm_local_bb2_st__255_inst0_readdatavalid(local_avm_aspace31_readdatavalid[0][1]),
      .avm_local_bb2_st__255_inst0_writeack(local_avm_aspace31_writeack[0][1]),
      // AVM avm_local_bb2_ld__257_inst0
      .avm_local_bb2_ld__257_inst0_enable(local_avm_aspace32_enable[0][0]),
      .avm_local_bb2_ld__257_inst0_read(local_avm_aspace32_read[0][0]),
      .avm_local_bb2_ld__257_inst0_write(local_avm_aspace32_write[0][0]),
      .avm_local_bb2_ld__257_inst0_burstcount(local_avm_aspace32_burstcount[0][0]),
      .avm_local_bb2_ld__257_inst0_address(local_avm_aspace32_address[0][0]),
      .avm_local_bb2_ld__257_inst0_writedata(local_avm_aspace32_writedata[0][0]),
      .avm_local_bb2_ld__257_inst0_byteenable(local_avm_aspace32_byteenable[0][0]),
      .avm_local_bb2_ld__257_inst0_waitrequest(local_avm_aspace32_waitrequest[0][0]),
      .avm_local_bb2_ld__257_inst0_readdata(local_avm_aspace32_readdata[0][0]),
      .avm_local_bb2_ld__257_inst0_readdatavalid(local_avm_aspace32_readdatavalid[0][0]),
      .avm_local_bb2_ld__257_inst0_writeack(local_avm_aspace32_writeack[0][0]),
      // AVM avm_local_bb2_st__u32_inst0
      .avm_local_bb2_st__u32_inst0_enable(local_avm_aspace32_enable[0][1]),
      .avm_local_bb2_st__u32_inst0_read(local_avm_aspace32_read[0][1]),
      .avm_local_bb2_st__u32_inst0_write(local_avm_aspace32_write[0][1]),
      .avm_local_bb2_st__u32_inst0_burstcount(local_avm_aspace32_burstcount[0][1]),
      .avm_local_bb2_st__u32_inst0_address(local_avm_aspace32_address[0][1]),
      .avm_local_bb2_st__u32_inst0_writedata(local_avm_aspace32_writedata[0][1]),
      .avm_local_bb2_st__u32_inst0_byteenable(local_avm_aspace32_byteenable[0][1]),
      .avm_local_bb2_st__u32_inst0_waitrequest(local_avm_aspace32_waitrequest[0][1]),
      .avm_local_bb2_st__u32_inst0_readdata(local_avm_aspace32_readdata[0][1]),
      .avm_local_bb2_st__u32_inst0_readdatavalid(local_avm_aspace32_readdatavalid[0][1]),
      .avm_local_bb2_st__u32_inst0_writeack(local_avm_aspace32_writeack[0][1]),
      // AVM avm_local_bb2_ld__u47_inst0
      .avm_local_bb2_ld__u47_inst0_enable(local_avm_aspace33_enable[0][0]),
      .avm_local_bb2_ld__u47_inst0_read(local_avm_aspace33_read[0][0]),
      .avm_local_bb2_ld__u47_inst0_write(local_avm_aspace33_write[0][0]),
      .avm_local_bb2_ld__u47_inst0_burstcount(local_avm_aspace33_burstcount[0][0]),
      .avm_local_bb2_ld__u47_inst0_address(local_avm_aspace33_address[0][0]),
      .avm_local_bb2_ld__u47_inst0_writedata(local_avm_aspace33_writedata[0][0]),
      .avm_local_bb2_ld__u47_inst0_byteenable(local_avm_aspace33_byteenable[0][0]),
      .avm_local_bb2_ld__u47_inst0_waitrequest(local_avm_aspace33_waitrequest[0][0]),
      .avm_local_bb2_ld__u47_inst0_readdata(local_avm_aspace33_readdata[0][0]),
      .avm_local_bb2_ld__u47_inst0_readdatavalid(local_avm_aspace33_readdatavalid[0][0]),
      .avm_local_bb2_ld__u47_inst0_writeack(local_avm_aspace33_writeack[0][0]),
      // AVM avm_local_bb2_st__257_inst0
      .avm_local_bb2_st__257_inst0_enable(local_avm_aspace33_enable[0][1]),
      .avm_local_bb2_st__257_inst0_read(local_avm_aspace33_read[0][1]),
      .avm_local_bb2_st__257_inst0_write(local_avm_aspace33_write[0][1]),
      .avm_local_bb2_st__257_inst0_burstcount(local_avm_aspace33_burstcount[0][1]),
      .avm_local_bb2_st__257_inst0_address(local_avm_aspace33_address[0][1]),
      .avm_local_bb2_st__257_inst0_writedata(local_avm_aspace33_writedata[0][1]),
      .avm_local_bb2_st__257_inst0_byteenable(local_avm_aspace33_byteenable[0][1]),
      .avm_local_bb2_st__257_inst0_waitrequest(local_avm_aspace33_waitrequest[0][1]),
      .avm_local_bb2_st__257_inst0_readdata(local_avm_aspace33_readdata[0][1]),
      .avm_local_bb2_st__257_inst0_readdatavalid(local_avm_aspace33_readdatavalid[0][1]),
      .avm_local_bb2_st__257_inst0_writeack(local_avm_aspace33_writeack[0][1]),
      // AVM avm_local_bb2_ld__259_inst0
      .avm_local_bb2_ld__259_inst0_enable(local_avm_aspace34_enable[0][0]),
      .avm_local_bb2_ld__259_inst0_read(local_avm_aspace34_read[0][0]),
      .avm_local_bb2_ld__259_inst0_write(local_avm_aspace34_write[0][0]),
      .avm_local_bb2_ld__259_inst0_burstcount(local_avm_aspace34_burstcount[0][0]),
      .avm_local_bb2_ld__259_inst0_address(local_avm_aspace34_address[0][0]),
      .avm_local_bb2_ld__259_inst0_writedata(local_avm_aspace34_writedata[0][0]),
      .avm_local_bb2_ld__259_inst0_byteenable(local_avm_aspace34_byteenable[0][0]),
      .avm_local_bb2_ld__259_inst0_waitrequest(local_avm_aspace34_waitrequest[0][0]),
      .avm_local_bb2_ld__259_inst0_readdata(local_avm_aspace34_readdata[0][0]),
      .avm_local_bb2_ld__259_inst0_readdatavalid(local_avm_aspace34_readdatavalid[0][0]),
      .avm_local_bb2_ld__259_inst0_writeack(local_avm_aspace34_writeack[0][0]),
      // AVM avm_local_bb2_st__u33_inst0
      .avm_local_bb2_st__u33_inst0_enable(local_avm_aspace34_enable[0][1]),
      .avm_local_bb2_st__u33_inst0_read(local_avm_aspace34_read[0][1]),
      .avm_local_bb2_st__u33_inst0_write(local_avm_aspace34_write[0][1]),
      .avm_local_bb2_st__u33_inst0_burstcount(local_avm_aspace34_burstcount[0][1]),
      .avm_local_bb2_st__u33_inst0_address(local_avm_aspace34_address[0][1]),
      .avm_local_bb2_st__u33_inst0_writedata(local_avm_aspace34_writedata[0][1]),
      .avm_local_bb2_st__u33_inst0_byteenable(local_avm_aspace34_byteenable[0][1]),
      .avm_local_bb2_st__u33_inst0_waitrequest(local_avm_aspace34_waitrequest[0][1]),
      .avm_local_bb2_st__u33_inst0_readdata(local_avm_aspace34_readdata[0][1]),
      .avm_local_bb2_st__u33_inst0_readdatavalid(local_avm_aspace34_readdatavalid[0][1]),
      .avm_local_bb2_st__u33_inst0_writeack(local_avm_aspace34_writeack[0][1]),
      // AVM avm_local_bb2_ld__u48_inst0
      .avm_local_bb2_ld__u48_inst0_enable(local_avm_aspace35_enable[0][0]),
      .avm_local_bb2_ld__u48_inst0_read(local_avm_aspace35_read[0][0]),
      .avm_local_bb2_ld__u48_inst0_write(local_avm_aspace35_write[0][0]),
      .avm_local_bb2_ld__u48_inst0_burstcount(local_avm_aspace35_burstcount[0][0]),
      .avm_local_bb2_ld__u48_inst0_address(local_avm_aspace35_address[0][0]),
      .avm_local_bb2_ld__u48_inst0_writedata(local_avm_aspace35_writedata[0][0]),
      .avm_local_bb2_ld__u48_inst0_byteenable(local_avm_aspace35_byteenable[0][0]),
      .avm_local_bb2_ld__u48_inst0_waitrequest(local_avm_aspace35_waitrequest[0][0]),
      .avm_local_bb2_ld__u48_inst0_readdata(local_avm_aspace35_readdata[0][0]),
      .avm_local_bb2_ld__u48_inst0_readdatavalid(local_avm_aspace35_readdatavalid[0][0]),
      .avm_local_bb2_ld__u48_inst0_writeack(local_avm_aspace35_writeack[0][0]),
      // AVM avm_local_bb2_st__259_inst0
      .avm_local_bb2_st__259_inst0_enable(local_avm_aspace35_enable[0][1]),
      .avm_local_bb2_st__259_inst0_read(local_avm_aspace35_read[0][1]),
      .avm_local_bb2_st__259_inst0_write(local_avm_aspace35_write[0][1]),
      .avm_local_bb2_st__259_inst0_burstcount(local_avm_aspace35_burstcount[0][1]),
      .avm_local_bb2_st__259_inst0_address(local_avm_aspace35_address[0][1]),
      .avm_local_bb2_st__259_inst0_writedata(local_avm_aspace35_writedata[0][1]),
      .avm_local_bb2_st__259_inst0_byteenable(local_avm_aspace35_byteenable[0][1]),
      .avm_local_bb2_st__259_inst0_waitrequest(local_avm_aspace35_waitrequest[0][1]),
      .avm_local_bb2_st__259_inst0_readdata(local_avm_aspace35_readdata[0][1]),
      .avm_local_bb2_st__259_inst0_readdatavalid(local_avm_aspace35_readdatavalid[0][1]),
      .avm_local_bb2_st__259_inst0_writeack(local_avm_aspace35_writeack[0][1]),
      // AVM avm_local_bb2_ld__261_inst0
      .avm_local_bb2_ld__261_inst0_enable(local_avm_aspace36_enable[0][0]),
      .avm_local_bb2_ld__261_inst0_read(local_avm_aspace36_read[0][0]),
      .avm_local_bb2_ld__261_inst0_write(local_avm_aspace36_write[0][0]),
      .avm_local_bb2_ld__261_inst0_burstcount(local_avm_aspace36_burstcount[0][0]),
      .avm_local_bb2_ld__261_inst0_address(local_avm_aspace36_address[0][0]),
      .avm_local_bb2_ld__261_inst0_writedata(local_avm_aspace36_writedata[0][0]),
      .avm_local_bb2_ld__261_inst0_byteenable(local_avm_aspace36_byteenable[0][0]),
      .avm_local_bb2_ld__261_inst0_waitrequest(local_avm_aspace36_waitrequest[0][0]),
      .avm_local_bb2_ld__261_inst0_readdata(local_avm_aspace36_readdata[0][0]),
      .avm_local_bb2_ld__261_inst0_readdatavalid(local_avm_aspace36_readdatavalid[0][0]),
      .avm_local_bb2_ld__261_inst0_writeack(local_avm_aspace36_writeack[0][0]),
      // AVM avm_local_bb2_st__u34_inst0
      .avm_local_bb2_st__u34_inst0_enable(local_avm_aspace36_enable[0][1]),
      .avm_local_bb2_st__u34_inst0_read(local_avm_aspace36_read[0][1]),
      .avm_local_bb2_st__u34_inst0_write(local_avm_aspace36_write[0][1]),
      .avm_local_bb2_st__u34_inst0_burstcount(local_avm_aspace36_burstcount[0][1]),
      .avm_local_bb2_st__u34_inst0_address(local_avm_aspace36_address[0][1]),
      .avm_local_bb2_st__u34_inst0_writedata(local_avm_aspace36_writedata[0][1]),
      .avm_local_bb2_st__u34_inst0_byteenable(local_avm_aspace36_byteenable[0][1]),
      .avm_local_bb2_st__u34_inst0_waitrequest(local_avm_aspace36_waitrequest[0][1]),
      .avm_local_bb2_st__u34_inst0_readdata(local_avm_aspace36_readdata[0][1]),
      .avm_local_bb2_st__u34_inst0_readdatavalid(local_avm_aspace36_readdatavalid[0][1]),
      .avm_local_bb2_st__u34_inst0_writeack(local_avm_aspace36_writeack[0][1]),
      // AVM avm_local_bb2_ld__u49_inst0
      .avm_local_bb2_ld__u49_inst0_enable(local_avm_aspace37_enable[0][0]),
      .avm_local_bb2_ld__u49_inst0_read(local_avm_aspace37_read[0][0]),
      .avm_local_bb2_ld__u49_inst0_write(local_avm_aspace37_write[0][0]),
      .avm_local_bb2_ld__u49_inst0_burstcount(local_avm_aspace37_burstcount[0][0]),
      .avm_local_bb2_ld__u49_inst0_address(local_avm_aspace37_address[0][0]),
      .avm_local_bb2_ld__u49_inst0_writedata(local_avm_aspace37_writedata[0][0]),
      .avm_local_bb2_ld__u49_inst0_byteenable(local_avm_aspace37_byteenable[0][0]),
      .avm_local_bb2_ld__u49_inst0_waitrequest(local_avm_aspace37_waitrequest[0][0]),
      .avm_local_bb2_ld__u49_inst0_readdata(local_avm_aspace37_readdata[0][0]),
      .avm_local_bb2_ld__u49_inst0_readdatavalid(local_avm_aspace37_readdatavalid[0][0]),
      .avm_local_bb2_ld__u49_inst0_writeack(local_avm_aspace37_writeack[0][0]),
      // AVM avm_local_bb2_st__261_inst0
      .avm_local_bb2_st__261_inst0_enable(local_avm_aspace37_enable[0][1]),
      .avm_local_bb2_st__261_inst0_read(local_avm_aspace37_read[0][1]),
      .avm_local_bb2_st__261_inst0_write(local_avm_aspace37_write[0][1]),
      .avm_local_bb2_st__261_inst0_burstcount(local_avm_aspace37_burstcount[0][1]),
      .avm_local_bb2_st__261_inst0_address(local_avm_aspace37_address[0][1]),
      .avm_local_bb2_st__261_inst0_writedata(local_avm_aspace37_writedata[0][1]),
      .avm_local_bb2_st__261_inst0_byteenable(local_avm_aspace37_byteenable[0][1]),
      .avm_local_bb2_st__261_inst0_waitrequest(local_avm_aspace37_waitrequest[0][1]),
      .avm_local_bb2_st__261_inst0_readdata(local_avm_aspace37_readdata[0][1]),
      .avm_local_bb2_st__261_inst0_readdatavalid(local_avm_aspace37_readdatavalid[0][1]),
      .avm_local_bb2_st__261_inst0_writeack(local_avm_aspace37_writeack[0][1]),
      // AVM avm_local_bb2_ld__263_inst0
      .avm_local_bb2_ld__263_inst0_enable(local_avm_aspace38_enable[0][0]),
      .avm_local_bb2_ld__263_inst0_read(local_avm_aspace38_read[0][0]),
      .avm_local_bb2_ld__263_inst0_write(local_avm_aspace38_write[0][0]),
      .avm_local_bb2_ld__263_inst0_burstcount(local_avm_aspace38_burstcount[0][0]),
      .avm_local_bb2_ld__263_inst0_address(local_avm_aspace38_address[0][0]),
      .avm_local_bb2_ld__263_inst0_writedata(local_avm_aspace38_writedata[0][0]),
      .avm_local_bb2_ld__263_inst0_byteenable(local_avm_aspace38_byteenable[0][0]),
      .avm_local_bb2_ld__263_inst0_waitrequest(local_avm_aspace38_waitrequest[0][0]),
      .avm_local_bb2_ld__263_inst0_readdata(local_avm_aspace38_readdata[0][0]),
      .avm_local_bb2_ld__263_inst0_readdatavalid(local_avm_aspace38_readdatavalid[0][0]),
      .avm_local_bb2_ld__263_inst0_writeack(local_avm_aspace38_writeack[0][0]),
      // AVM avm_local_bb2_st__u35_inst0
      .avm_local_bb2_st__u35_inst0_enable(local_avm_aspace38_enable[0][1]),
      .avm_local_bb2_st__u35_inst0_read(local_avm_aspace38_read[0][1]),
      .avm_local_bb2_st__u35_inst0_write(local_avm_aspace38_write[0][1]),
      .avm_local_bb2_st__u35_inst0_burstcount(local_avm_aspace38_burstcount[0][1]),
      .avm_local_bb2_st__u35_inst0_address(local_avm_aspace38_address[0][1]),
      .avm_local_bb2_st__u35_inst0_writedata(local_avm_aspace38_writedata[0][1]),
      .avm_local_bb2_st__u35_inst0_byteenable(local_avm_aspace38_byteenable[0][1]),
      .avm_local_bb2_st__u35_inst0_waitrequest(local_avm_aspace38_waitrequest[0][1]),
      .avm_local_bb2_st__u35_inst0_readdata(local_avm_aspace38_readdata[0][1]),
      .avm_local_bb2_st__u35_inst0_readdatavalid(local_avm_aspace38_readdatavalid[0][1]),
      .avm_local_bb2_st__u35_inst0_writeack(local_avm_aspace38_writeack[0][1]),
      // AVM avm_local_bb2_ld__u50_inst0
      .avm_local_bb2_ld__u50_inst0_enable(local_avm_aspace39_enable[0][0]),
      .avm_local_bb2_ld__u50_inst0_read(local_avm_aspace39_read[0][0]),
      .avm_local_bb2_ld__u50_inst0_write(local_avm_aspace39_write[0][0]),
      .avm_local_bb2_ld__u50_inst0_burstcount(local_avm_aspace39_burstcount[0][0]),
      .avm_local_bb2_ld__u50_inst0_address(local_avm_aspace39_address[0][0]),
      .avm_local_bb2_ld__u50_inst0_writedata(local_avm_aspace39_writedata[0][0]),
      .avm_local_bb2_ld__u50_inst0_byteenable(local_avm_aspace39_byteenable[0][0]),
      .avm_local_bb2_ld__u50_inst0_waitrequest(local_avm_aspace39_waitrequest[0][0]),
      .avm_local_bb2_ld__u50_inst0_readdata(local_avm_aspace39_readdata[0][0]),
      .avm_local_bb2_ld__u50_inst0_readdatavalid(local_avm_aspace39_readdatavalid[0][0]),
      .avm_local_bb2_ld__u50_inst0_writeack(local_avm_aspace39_writeack[0][0]),
      // AVM avm_local_bb2_st__263_inst0
      .avm_local_bb2_st__263_inst0_enable(local_avm_aspace39_enable[0][1]),
      .avm_local_bb2_st__263_inst0_read(local_avm_aspace39_read[0][1]),
      .avm_local_bb2_st__263_inst0_write(local_avm_aspace39_write[0][1]),
      .avm_local_bb2_st__263_inst0_burstcount(local_avm_aspace39_burstcount[0][1]),
      .avm_local_bb2_st__263_inst0_address(local_avm_aspace39_address[0][1]),
      .avm_local_bb2_st__263_inst0_writedata(local_avm_aspace39_writedata[0][1]),
      .avm_local_bb2_st__263_inst0_byteenable(local_avm_aspace39_byteenable[0][1]),
      .avm_local_bb2_st__263_inst0_waitrequest(local_avm_aspace39_waitrequest[0][1]),
      .avm_local_bb2_st__263_inst0_readdata(local_avm_aspace39_readdata[0][1]),
      .avm_local_bb2_st__263_inst0_readdatavalid(local_avm_aspace39_readdatavalid[0][1]),
      .avm_local_bb2_st__263_inst0_writeack(local_avm_aspace39_writeack[0][1]),
      // AVST avst_local_bb2__conv_ch_inst0
      .avst_local_bb2__conv_ch_inst0_valid(avm_channel_id_conv_ch_read_valid),
      .avst_local_bb2__conv_ch_inst0_ready(avm_channel_id_conv_ch_read_ready),
      .avst_local_bb2__conv_ch_inst0_data(avm_channel_id_conv_ch_read_data),
      // AVST avst_local_bb2__pool_ch_inst0
      .avst_local_bb2__pool_ch_inst0_valid(avm_channel_id_pool_ch_write_valid),
      .avst_local_bb2__pool_ch_inst0_ready(avm_channel_id_pool_ch_write_ready),
      .avst_local_bb2__pool_ch_inst0_data(avm_channel_id_pool_ch_write_data),
      .avst_local_bb2__pool_ch_inst0_almostfull(avm_channel_id_pool_ch_write_almostfull)
   );

   assign lmem_invalid_single_bit = |lmem_invalid_aspaces;
   generate
   begin:local_mem_system_aspace7
      logic local_icm_arb_request [1][2];
      logic local_icm_arb_enable [1][2];
      logic local_icm_arb_read [1][2];
      logic local_icm_arb_write [1][2];
      logic local_icm_arb_burstcount [1][2];
      logic [7:0] local_icm_arb_address [1][2];
      logic [7:0] local_icm_arb_writedata [1][2];
      logic local_icm_arb_byteenable [1][2];
      logic local_icm_arb_stall [1][2];
      logic local_icm_wrp_ack [1][2];
      logic local_icm_rrp_datavalid [1][2];
      logic [7:0] local_icm_rrp_data [1][2];

      for( __i = 0; __i < 1; __i = __i + 1 )
      begin:local_mem_group
         for( __j = 0; __j < 2; __j = __j + 1 )
         begin:master
            // INST avm_to_ic of acl_avm_to_ic
            acl_avm_to_ic
            #(
               .DATA_W(8),
               .WRITEDATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(32),
               .BYTEENA_W(1)
            )
            avm_to_ic
            (
               // AVM avm
               .avm_enable(local_avm_aspace7_enable[__i][__j]),
               .avm_read(local_avm_aspace7_read[__i][__j]),
               .avm_write(local_avm_aspace7_write[__i][__j]),
               .avm_burstcount(local_avm_aspace7_burstcount[__i][__j]),
               .avm_address(local_avm_aspace7_address[__i][__j]),
               .avm_writedata(local_avm_aspace7_writedata[__i][__j]),
               .avm_byteenable(local_avm_aspace7_byteenable[__i][__j]),
               .avm_waitrequest(local_avm_aspace7_waitrequest[__i][__j]),
               .avm_readdata(local_avm_aspace7_readdata[__i][__j]),
               .avm_readdatavalid(local_avm_aspace7_readdatavalid[__i][__j]),
               .avm_writeack(local_avm_aspace7_writeack[__i][__j]),
               // ICM ic
               .ic_arb_request(local_icm_arb_request[__i][__j]),
               .ic_arb_enable(local_icm_arb_enable[__i][__j]),
               .ic_arb_read(local_icm_arb_read[__i][__j]),
               .ic_arb_write(local_icm_arb_write[__i][__j]),
               .ic_arb_burstcount(local_icm_arb_burstcount[__i][__j]),
               .ic_arb_address(local_icm_arb_address[__i][__j]),
               .ic_arb_writedata(local_icm_arb_writedata[__i][__j]),
               .ic_arb_byteenable(local_icm_arb_byteenable[__i][__j]),
               .ic_arb_stall(local_icm_arb_stall[__i][__j]),
               .ic_wrp_ack(local_icm_wrp_ack[__i][__j]),
               .ic_rrp_datavalid(local_icm_rrp_datavalid[__i][__j]),
               .ic_rrp_data(local_icm_rrp_data[__i][__j])
            );

         end

         for( __j = 0; __j < 1; __j = __j + 1 )
         begin:bank
            logic port_enable [1:2];
            logic port_read [1:2];
            logic port_write [1:2];
            logic [7:0] port_address [1:2];
            logic [7:0] port_writedata [1:2];
            logic port_byteenable [1:2];
            logic port_waitrequest [1:2];
            logic [7:0] port_readdata [1:2];
            logic port_readdatavalid [1:2];

            // INST mem0 of acl_mem1x
            acl_mem1x
            #(
               .INTENDED_DEVICE_FAMILY("Cyclone V"),
               .DEPTH_WORDS(256),
               .WIDTH(8),
               .MEM_LATENCY(3),
               .ENABLED(0),
               .RDW_MODE("OLD_DATA"),
               .RAM_OPERATION_MODE("BIDIR_DUAL_PORT"),
               .PREFERRED_WIDTH(160),
               .RAM_BLOCK_TYPE("M10K")
            )
            mem0
            (
               .clk(clock),
               .resetn(resetn),
               // AVS avs_port1
               .avs_port1_enable(port_enable[1]),
               .avs_port1_read(port_read[1]),
               .avs_port1_write(port_write[1]),
               .avs_port1_address(port_address[1]),
               .avs_port1_writedata(port_writedata[1]),
               .avs_port1_byteenable(port_byteenable[1]),
               .avs_port1_waitrequest(port_waitrequest[1]),
               .avs_port1_readdata(port_readdata[1]),
               .avs_port1_readdatavalid(port_readdatavalid[1]),
               // AVS avs_port2
               .avs_port2_enable(port_enable[2]),
               .avs_port2_read(port_read[2]),
               .avs_port2_write(port_write[2]),
               .avs_port2_address(port_address[2]),
               .avs_port2_writedata(port_writedata[2]),
               .avs_port2_byteenable(port_byteenable[2]),
               .avs_port2_waitrequest(port_waitrequest[2]),
               .avs_port2_readdata(port_readdata[2]),
               .avs_port2_readdatavalid(port_readdatavalid[2])
            );

         end

         for( __j = 0; __j < 2; __j = __j + 1 )
         begin:router
            logic b_arb_request [1];
            logic b_arb_enable [1];
            logic b_arb_read [1];
            logic b_arb_write [1];
            logic b_arb_burstcount [1];
            logic [7:0] b_arb_address [1];
            logic [7:0] b_arb_writedata [1];
            logic b_arb_byteenable [1];
            logic b_arb_stall [1];
            logic b_wrp_ack [1];
            logic b_rrp_datavalid [1];
            logic [7:0] b_rrp_data [1];
            logic bank_select;

            // INST router of acl_ic_local_mem_router
            acl_ic_local_mem_router
            #(
               .DATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(8),
               .BYTEENA_W(1),
               .NUM_BANKS(1)
            )
            router
            (
               .clock(clock),
               .resetn(resetn),
               .bank_select(bank_select),
               // ICM m
               .m_arb_request(local_icm_arb_request[__i][__j]),
               .m_arb_enable(local_icm_arb_enable[__i][__j]),
               .m_arb_read(local_icm_arb_read[__i][__j]),
               .m_arb_write(local_icm_arb_write[__i][__j]),
               .m_arb_burstcount(local_icm_arb_burstcount[__i][__j]),
               .m_arb_address(local_icm_arb_address[__i][__j]),
               .m_arb_writedata(local_icm_arb_writedata[__i][__j]),
               .m_arb_byteenable(local_icm_arb_byteenable[__i][__j]),
               .m_arb_stall(local_icm_arb_stall[__i][__j]),
               .m_wrp_ack(local_icm_wrp_ack[__i][__j]),
               .m_rrp_datavalid(local_icm_rrp_datavalid[__i][__j]),
               .m_rrp_data(local_icm_rrp_data[__i][__j]),
               // ICM b
               .b_arb_request(b_arb_request),
               .b_arb_enable(b_arb_enable),
               .b_arb_read(b_arb_read),
               .b_arb_write(b_arb_write),
               .b_arb_burstcount(b_arb_burstcount),
               .b_arb_address(b_arb_address),
               .b_arb_writedata(b_arb_writedata),
               .b_arb_byteenable(b_arb_byteenable),
               .b_arb_stall(b_arb_stall),
               .b_wrp_ack(b_wrp_ack),
               .b_rrp_datavalid(b_rrp_datavalid),
               .b_rrp_data(b_rrp_data)
            );

            assign bank_select = 1'b1;
         end

         for( __j = 0; __j < 1; __j = __j + 1 )
         begin:port1bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[0].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[0].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[0].b_arb_read[0];
            assign icm_in_arb_write[0] = router[0].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[0].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[0].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[0].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[0].b_arb_byteenable[0];
            assign router[0].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[0].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[0].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[0].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_6
            conv_pipe_system_interconnect_6 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[1] = icm_out_arb_enable;
            assign bank[0].port_read[1] = icm_out_arb_read;
            assign bank[0].port_write[1] = icm_out_arb_write;
            assign bank[0].port_address[1] = icm_out_arb_address;
            assign bank[0].port_writedata[1] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[1] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[1];
            assign icm_out_rrp_data = bank[0].port_readdata[1];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[1];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __j = 0; __j < 1; __j = __j + 1 )
         begin:port2bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[1].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[1].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[1].b_arb_read[0];
            assign icm_in_arb_write[0] = router[1].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[1].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[1].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[1].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[1].b_arb_byteenable[0];
            assign router[1].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[1].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[1].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[1].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_7
            conv_pipe_system_interconnect_7 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[2] = icm_out_arb_enable;
            assign bank[0].port_read[2] = icm_out_arb_read;
            assign bank[0].port_write[2] = icm_out_arb_write;
            assign bank[0].port_address[2] = icm_out_arb_address;
            assign bank[0].port_writedata[2] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[2] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[2];
            assign icm_out_rrp_data = bank[0].port_readdata[2];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[2];
            assign icm_out_wrp_ack = 'b0;
         end

      end

   end
   endgenerate

   generate
   begin:local_mem_system_aspace8
      logic local_icm_arb_request [1][2];
      logic local_icm_arb_enable [1][2];
      logic local_icm_arb_read [1][2];
      logic local_icm_arb_write [1][2];
      logic local_icm_arb_burstcount [1][2];
      logic [7:0] local_icm_arb_address [1][2];
      logic [7:0] local_icm_arb_writedata [1][2];
      logic local_icm_arb_byteenable [1][2];
      logic local_icm_arb_stall [1][2];
      logic local_icm_wrp_ack [1][2];
      logic local_icm_rrp_datavalid [1][2];
      logic [7:0] local_icm_rrp_data [1][2];

      for( __j = 0; __j < 1; __j = __j + 1 )
      begin:local_mem_group
         for( __k = 0; __k < 2; __k = __k + 1 )
         begin:master
            // INST avm_to_ic of acl_avm_to_ic
            acl_avm_to_ic
            #(
               .DATA_W(8),
               .WRITEDATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(32),
               .BYTEENA_W(1)
            )
            avm_to_ic
            (
               // AVM avm
               .avm_enable(local_avm_aspace8_enable[__j][__k]),
               .avm_read(local_avm_aspace8_read[__j][__k]),
               .avm_write(local_avm_aspace8_write[__j][__k]),
               .avm_burstcount(local_avm_aspace8_burstcount[__j][__k]),
               .avm_address(local_avm_aspace8_address[__j][__k]),
               .avm_writedata(local_avm_aspace8_writedata[__j][__k]),
               .avm_byteenable(local_avm_aspace8_byteenable[__j][__k]),
               .avm_waitrequest(local_avm_aspace8_waitrequest[__j][__k]),
               .avm_readdata(local_avm_aspace8_readdata[__j][__k]),
               .avm_readdatavalid(local_avm_aspace8_readdatavalid[__j][__k]),
               .avm_writeack(local_avm_aspace8_writeack[__j][__k]),
               // ICM ic
               .ic_arb_request(local_icm_arb_request[__j][__k]),
               .ic_arb_enable(local_icm_arb_enable[__j][__k]),
               .ic_arb_read(local_icm_arb_read[__j][__k]),
               .ic_arb_write(local_icm_arb_write[__j][__k]),
               .ic_arb_burstcount(local_icm_arb_burstcount[__j][__k]),
               .ic_arb_address(local_icm_arb_address[__j][__k]),
               .ic_arb_writedata(local_icm_arb_writedata[__j][__k]),
               .ic_arb_byteenable(local_icm_arb_byteenable[__j][__k]),
               .ic_arb_stall(local_icm_arb_stall[__j][__k]),
               .ic_wrp_ack(local_icm_wrp_ack[__j][__k]),
               .ic_rrp_datavalid(local_icm_rrp_datavalid[__j][__k]),
               .ic_rrp_data(local_icm_rrp_data[__j][__k])
            );

         end

         for( __k = 0; __k < 1; __k = __k + 1 )
         begin:bank
            logic port_enable [1:2];
            logic port_read [1:2];
            logic port_write [1:2];
            logic [7:0] port_address [1:2];
            logic [7:0] port_writedata [1:2];
            logic port_byteenable [1:2];
            logic port_waitrequest [1:2];
            logic [7:0] port_readdata [1:2];
            logic port_readdatavalid [1:2];

            // INST mem0 of acl_mem1x
            acl_mem1x
            #(
               .INTENDED_DEVICE_FAMILY("Cyclone V"),
               .DEPTH_WORDS(256),
               .WIDTH(8),
               .MEM_LATENCY(3),
               .ENABLED(0),
               .RDW_MODE("OLD_DATA"),
               .RAM_OPERATION_MODE("BIDIR_DUAL_PORT"),
               .PREFERRED_WIDTH(160),
               .RAM_BLOCK_TYPE("M10K")
            )
            mem0
            (
               .clk(clock),
               .resetn(resetn),
               // AVS avs_port1
               .avs_port1_enable(port_enable[1]),
               .avs_port1_read(port_read[1]),
               .avs_port1_write(port_write[1]),
               .avs_port1_address(port_address[1]),
               .avs_port1_writedata(port_writedata[1]),
               .avs_port1_byteenable(port_byteenable[1]),
               .avs_port1_waitrequest(port_waitrequest[1]),
               .avs_port1_readdata(port_readdata[1]),
               .avs_port1_readdatavalid(port_readdatavalid[1]),
               // AVS avs_port2
               .avs_port2_enable(port_enable[2]),
               .avs_port2_read(port_read[2]),
               .avs_port2_write(port_write[2]),
               .avs_port2_address(port_address[2]),
               .avs_port2_writedata(port_writedata[2]),
               .avs_port2_byteenable(port_byteenable[2]),
               .avs_port2_waitrequest(port_waitrequest[2]),
               .avs_port2_readdata(port_readdata[2]),
               .avs_port2_readdatavalid(port_readdatavalid[2])
            );

         end

         for( __k = 0; __k < 2; __k = __k + 1 )
         begin:router
            logic b_arb_request [1];
            logic b_arb_enable [1];
            logic b_arb_read [1];
            logic b_arb_write [1];
            logic b_arb_burstcount [1];
            logic [7:0] b_arb_address [1];
            logic [7:0] b_arb_writedata [1];
            logic b_arb_byteenable [1];
            logic b_arb_stall [1];
            logic b_wrp_ack [1];
            logic b_rrp_datavalid [1];
            logic [7:0] b_rrp_data [1];
            logic bank_select;

            // INST router of acl_ic_local_mem_router
            acl_ic_local_mem_router
            #(
               .DATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(8),
               .BYTEENA_W(1),
               .NUM_BANKS(1)
            )
            router
            (
               .clock(clock),
               .resetn(resetn),
               .bank_select(bank_select),
               // ICM m
               .m_arb_request(local_icm_arb_request[__j][__k]),
               .m_arb_enable(local_icm_arb_enable[__j][__k]),
               .m_arb_read(local_icm_arb_read[__j][__k]),
               .m_arb_write(local_icm_arb_write[__j][__k]),
               .m_arb_burstcount(local_icm_arb_burstcount[__j][__k]),
               .m_arb_address(local_icm_arb_address[__j][__k]),
               .m_arb_writedata(local_icm_arb_writedata[__j][__k]),
               .m_arb_byteenable(local_icm_arb_byteenable[__j][__k]),
               .m_arb_stall(local_icm_arb_stall[__j][__k]),
               .m_wrp_ack(local_icm_wrp_ack[__j][__k]),
               .m_rrp_datavalid(local_icm_rrp_datavalid[__j][__k]),
               .m_rrp_data(local_icm_rrp_data[__j][__k]),
               // ICM b
               .b_arb_request(b_arb_request),
               .b_arb_enable(b_arb_enable),
               .b_arb_read(b_arb_read),
               .b_arb_write(b_arb_write),
               .b_arb_burstcount(b_arb_burstcount),
               .b_arb_address(b_arb_address),
               .b_arb_writedata(b_arb_writedata),
               .b_arb_byteenable(b_arb_byteenable),
               .b_arb_stall(b_arb_stall),
               .b_wrp_ack(b_wrp_ack),
               .b_rrp_datavalid(b_rrp_datavalid),
               .b_rrp_data(b_rrp_data)
            );

            assign bank_select = 1'b1;
         end

         for( __k = 0; __k < 1; __k = __k + 1 )
         begin:port1bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[0].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[0].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[0].b_arb_read[0];
            assign icm_in_arb_write[0] = router[0].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[0].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[0].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[0].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[0].b_arb_byteenable[0];
            assign router[0].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[0].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[0].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[0].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_6
            conv_pipe_system_interconnect_6 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[1] = icm_out_arb_enable;
            assign bank[0].port_read[1] = icm_out_arb_read;
            assign bank[0].port_write[1] = icm_out_arb_write;
            assign bank[0].port_address[1] = icm_out_arb_address;
            assign bank[0].port_writedata[1] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[1] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[1];
            assign icm_out_rrp_data = bank[0].port_readdata[1];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[1];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __k = 0; __k < 1; __k = __k + 1 )
         begin:port2bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[1].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[1].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[1].b_arb_read[0];
            assign icm_in_arb_write[0] = router[1].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[1].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[1].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[1].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[1].b_arb_byteenable[0];
            assign router[1].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[1].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[1].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[1].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_7
            conv_pipe_system_interconnect_7 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[2] = icm_out_arb_enable;
            assign bank[0].port_read[2] = icm_out_arb_read;
            assign bank[0].port_write[2] = icm_out_arb_write;
            assign bank[0].port_address[2] = icm_out_arb_address;
            assign bank[0].port_writedata[2] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[2] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[2];
            assign icm_out_rrp_data = bank[0].port_readdata[2];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[2];
            assign icm_out_wrp_ack = 'b0;
         end

      end

   end
   endgenerate

   generate
   begin:local_mem_system_aspace10
      logic local_icm_arb_request [1][2];
      logic local_icm_arb_enable [1][2];
      logic local_icm_arb_read [1][2];
      logic local_icm_arb_write [1][2];
      logic local_icm_arb_burstcount [1][2];
      logic [7:0] local_icm_arb_address [1][2];
      logic [7:0] local_icm_arb_writedata [1][2];
      logic local_icm_arb_byteenable [1][2];
      logic local_icm_arb_stall [1][2];
      logic local_icm_wrp_ack [1][2];
      logic local_icm_rrp_datavalid [1][2];
      logic [7:0] local_icm_rrp_data [1][2];

      for( __k = 0; __k < 1; __k = __k + 1 )
      begin:local_mem_group
         for( __l = 0; __l < 2; __l = __l + 1 )
         begin:master
            // INST avm_to_ic of acl_avm_to_ic
            acl_avm_to_ic
            #(
               .DATA_W(8),
               .WRITEDATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(32),
               .BYTEENA_W(1)
            )
            avm_to_ic
            (
               // AVM avm
               .avm_enable(local_avm_aspace10_enable[__k][__l]),
               .avm_read(local_avm_aspace10_read[__k][__l]),
               .avm_write(local_avm_aspace10_write[__k][__l]),
               .avm_burstcount(local_avm_aspace10_burstcount[__k][__l]),
               .avm_address(local_avm_aspace10_address[__k][__l]),
               .avm_writedata(local_avm_aspace10_writedata[__k][__l]),
               .avm_byteenable(local_avm_aspace10_byteenable[__k][__l]),
               .avm_waitrequest(local_avm_aspace10_waitrequest[__k][__l]),
               .avm_readdata(local_avm_aspace10_readdata[__k][__l]),
               .avm_readdatavalid(local_avm_aspace10_readdatavalid[__k][__l]),
               .avm_writeack(local_avm_aspace10_writeack[__k][__l]),
               // ICM ic
               .ic_arb_request(local_icm_arb_request[__k][__l]),
               .ic_arb_enable(local_icm_arb_enable[__k][__l]),
               .ic_arb_read(local_icm_arb_read[__k][__l]),
               .ic_arb_write(local_icm_arb_write[__k][__l]),
               .ic_arb_burstcount(local_icm_arb_burstcount[__k][__l]),
               .ic_arb_address(local_icm_arb_address[__k][__l]),
               .ic_arb_writedata(local_icm_arb_writedata[__k][__l]),
               .ic_arb_byteenable(local_icm_arb_byteenable[__k][__l]),
               .ic_arb_stall(local_icm_arb_stall[__k][__l]),
               .ic_wrp_ack(local_icm_wrp_ack[__k][__l]),
               .ic_rrp_datavalid(local_icm_rrp_datavalid[__k][__l]),
               .ic_rrp_data(local_icm_rrp_data[__k][__l])
            );

         end

         for( __l = 0; __l < 1; __l = __l + 1 )
         begin:bank
            logic port_enable [1:2];
            logic port_read [1:2];
            logic port_write [1:2];
            logic [7:0] port_address [1:2];
            logic [7:0] port_writedata [1:2];
            logic port_byteenable [1:2];
            logic port_waitrequest [1:2];
            logic [7:0] port_readdata [1:2];
            logic port_readdatavalid [1:2];

            // INST mem0 of acl_mem1x
            acl_mem1x
            #(
               .INTENDED_DEVICE_FAMILY("Cyclone V"),
               .DEPTH_WORDS(256),
               .WIDTH(8),
               .MEM_LATENCY(3),
               .ENABLED(0),
               .RDW_MODE("OLD_DATA"),
               .RAM_OPERATION_MODE("BIDIR_DUAL_PORT"),
               .PREFERRED_WIDTH(160),
               .RAM_BLOCK_TYPE("M10K")
            )
            mem0
            (
               .clk(clock),
               .resetn(resetn),
               // AVS avs_port1
               .avs_port1_enable(port_enable[1]),
               .avs_port1_read(port_read[1]),
               .avs_port1_write(port_write[1]),
               .avs_port1_address(port_address[1]),
               .avs_port1_writedata(port_writedata[1]),
               .avs_port1_byteenable(port_byteenable[1]),
               .avs_port1_waitrequest(port_waitrequest[1]),
               .avs_port1_readdata(port_readdata[1]),
               .avs_port1_readdatavalid(port_readdatavalid[1]),
               // AVS avs_port2
               .avs_port2_enable(port_enable[2]),
               .avs_port2_read(port_read[2]),
               .avs_port2_write(port_write[2]),
               .avs_port2_address(port_address[2]),
               .avs_port2_writedata(port_writedata[2]),
               .avs_port2_byteenable(port_byteenable[2]),
               .avs_port2_waitrequest(port_waitrequest[2]),
               .avs_port2_readdata(port_readdata[2]),
               .avs_port2_readdatavalid(port_readdatavalid[2])
            );

         end

         for( __l = 0; __l < 2; __l = __l + 1 )
         begin:router
            logic b_arb_request [1];
            logic b_arb_enable [1];
            logic b_arb_read [1];
            logic b_arb_write [1];
            logic b_arb_burstcount [1];
            logic [7:0] b_arb_address [1];
            logic [7:0] b_arb_writedata [1];
            logic b_arb_byteenable [1];
            logic b_arb_stall [1];
            logic b_wrp_ack [1];
            logic b_rrp_datavalid [1];
            logic [7:0] b_rrp_data [1];
            logic bank_select;

            // INST router of acl_ic_local_mem_router
            acl_ic_local_mem_router
            #(
               .DATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(8),
               .BYTEENA_W(1),
               .NUM_BANKS(1)
            )
            router
            (
               .clock(clock),
               .resetn(resetn),
               .bank_select(bank_select),
               // ICM m
               .m_arb_request(local_icm_arb_request[__k][__l]),
               .m_arb_enable(local_icm_arb_enable[__k][__l]),
               .m_arb_read(local_icm_arb_read[__k][__l]),
               .m_arb_write(local_icm_arb_write[__k][__l]),
               .m_arb_burstcount(local_icm_arb_burstcount[__k][__l]),
               .m_arb_address(local_icm_arb_address[__k][__l]),
               .m_arb_writedata(local_icm_arb_writedata[__k][__l]),
               .m_arb_byteenable(local_icm_arb_byteenable[__k][__l]),
               .m_arb_stall(local_icm_arb_stall[__k][__l]),
               .m_wrp_ack(local_icm_wrp_ack[__k][__l]),
               .m_rrp_datavalid(local_icm_rrp_datavalid[__k][__l]),
               .m_rrp_data(local_icm_rrp_data[__k][__l]),
               // ICM b
               .b_arb_request(b_arb_request),
               .b_arb_enable(b_arb_enable),
               .b_arb_read(b_arb_read),
               .b_arb_write(b_arb_write),
               .b_arb_burstcount(b_arb_burstcount),
               .b_arb_address(b_arb_address),
               .b_arb_writedata(b_arb_writedata),
               .b_arb_byteenable(b_arb_byteenable),
               .b_arb_stall(b_arb_stall),
               .b_wrp_ack(b_wrp_ack),
               .b_rrp_datavalid(b_rrp_datavalid),
               .b_rrp_data(b_rrp_data)
            );

            assign bank_select = 1'b1;
         end

         for( __l = 0; __l < 1; __l = __l + 1 )
         begin:port1bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[0].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[0].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[0].b_arb_read[0];
            assign icm_in_arb_write[0] = router[0].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[0].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[0].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[0].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[0].b_arb_byteenable[0];
            assign router[0].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[0].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[0].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[0].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_6
            conv_pipe_system_interconnect_6 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[1] = icm_out_arb_enable;
            assign bank[0].port_read[1] = icm_out_arb_read;
            assign bank[0].port_write[1] = icm_out_arb_write;
            assign bank[0].port_address[1] = icm_out_arb_address;
            assign bank[0].port_writedata[1] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[1] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[1];
            assign icm_out_rrp_data = bank[0].port_readdata[1];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[1];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __l = 0; __l < 1; __l = __l + 1 )
         begin:port2bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[1].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[1].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[1].b_arb_read[0];
            assign icm_in_arb_write[0] = router[1].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[1].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[1].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[1].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[1].b_arb_byteenable[0];
            assign router[1].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[1].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[1].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[1].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_7
            conv_pipe_system_interconnect_7 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[2] = icm_out_arb_enable;
            assign bank[0].port_read[2] = icm_out_arb_read;
            assign bank[0].port_write[2] = icm_out_arb_write;
            assign bank[0].port_address[2] = icm_out_arb_address;
            assign bank[0].port_writedata[2] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[2] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[2];
            assign icm_out_rrp_data = bank[0].port_readdata[2];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[2];
            assign icm_out_wrp_ack = 'b0;
         end

      end

   end
   endgenerate

   generate
   begin:local_mem_system_aspace11
      logic local_icm_arb_request [1][2];
      logic local_icm_arb_enable [1][2];
      logic local_icm_arb_read [1][2];
      logic local_icm_arb_write [1][2];
      logic local_icm_arb_burstcount [1][2];
      logic [7:0] local_icm_arb_address [1][2];
      logic [7:0] local_icm_arb_writedata [1][2];
      logic local_icm_arb_byteenable [1][2];
      logic local_icm_arb_stall [1][2];
      logic local_icm_wrp_ack [1][2];
      logic local_icm_rrp_datavalid [1][2];
      logic [7:0] local_icm_rrp_data [1][2];

      for( __l = 0; __l < 1; __l = __l + 1 )
      begin:local_mem_group
         for( __m = 0; __m < 2; __m = __m + 1 )
         begin:master
            // INST avm_to_ic of acl_avm_to_ic
            acl_avm_to_ic
            #(
               .DATA_W(8),
               .WRITEDATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(32),
               .BYTEENA_W(1)
            )
            avm_to_ic
            (
               // AVM avm
               .avm_enable(local_avm_aspace11_enable[__l][__m]),
               .avm_read(local_avm_aspace11_read[__l][__m]),
               .avm_write(local_avm_aspace11_write[__l][__m]),
               .avm_burstcount(local_avm_aspace11_burstcount[__l][__m]),
               .avm_address(local_avm_aspace11_address[__l][__m]),
               .avm_writedata(local_avm_aspace11_writedata[__l][__m]),
               .avm_byteenable(local_avm_aspace11_byteenable[__l][__m]),
               .avm_waitrequest(local_avm_aspace11_waitrequest[__l][__m]),
               .avm_readdata(local_avm_aspace11_readdata[__l][__m]),
               .avm_readdatavalid(local_avm_aspace11_readdatavalid[__l][__m]),
               .avm_writeack(local_avm_aspace11_writeack[__l][__m]),
               // ICM ic
               .ic_arb_request(local_icm_arb_request[__l][__m]),
               .ic_arb_enable(local_icm_arb_enable[__l][__m]),
               .ic_arb_read(local_icm_arb_read[__l][__m]),
               .ic_arb_write(local_icm_arb_write[__l][__m]),
               .ic_arb_burstcount(local_icm_arb_burstcount[__l][__m]),
               .ic_arb_address(local_icm_arb_address[__l][__m]),
               .ic_arb_writedata(local_icm_arb_writedata[__l][__m]),
               .ic_arb_byteenable(local_icm_arb_byteenable[__l][__m]),
               .ic_arb_stall(local_icm_arb_stall[__l][__m]),
               .ic_wrp_ack(local_icm_wrp_ack[__l][__m]),
               .ic_rrp_datavalid(local_icm_rrp_datavalid[__l][__m]),
               .ic_rrp_data(local_icm_rrp_data[__l][__m])
            );

         end

         for( __m = 0; __m < 1; __m = __m + 1 )
         begin:bank
            logic port_enable [1:2];
            logic port_read [1:2];
            logic port_write [1:2];
            logic [7:0] port_address [1:2];
            logic [7:0] port_writedata [1:2];
            logic port_byteenable [1:2];
            logic port_waitrequest [1:2];
            logic [7:0] port_readdata [1:2];
            logic port_readdatavalid [1:2];

            // INST mem0 of acl_mem1x
            acl_mem1x
            #(
               .INTENDED_DEVICE_FAMILY("Cyclone V"),
               .DEPTH_WORDS(256),
               .WIDTH(8),
               .MEM_LATENCY(3),
               .ENABLED(0),
               .RDW_MODE("OLD_DATA"),
               .RAM_OPERATION_MODE("BIDIR_DUAL_PORT"),
               .PREFERRED_WIDTH(160),
               .RAM_BLOCK_TYPE("M10K")
            )
            mem0
            (
               .clk(clock),
               .resetn(resetn),
               // AVS avs_port1
               .avs_port1_enable(port_enable[1]),
               .avs_port1_read(port_read[1]),
               .avs_port1_write(port_write[1]),
               .avs_port1_address(port_address[1]),
               .avs_port1_writedata(port_writedata[1]),
               .avs_port1_byteenable(port_byteenable[1]),
               .avs_port1_waitrequest(port_waitrequest[1]),
               .avs_port1_readdata(port_readdata[1]),
               .avs_port1_readdatavalid(port_readdatavalid[1]),
               // AVS avs_port2
               .avs_port2_enable(port_enable[2]),
               .avs_port2_read(port_read[2]),
               .avs_port2_write(port_write[2]),
               .avs_port2_address(port_address[2]),
               .avs_port2_writedata(port_writedata[2]),
               .avs_port2_byteenable(port_byteenable[2]),
               .avs_port2_waitrequest(port_waitrequest[2]),
               .avs_port2_readdata(port_readdata[2]),
               .avs_port2_readdatavalid(port_readdatavalid[2])
            );

         end

         for( __m = 0; __m < 2; __m = __m + 1 )
         begin:router
            logic b_arb_request [1];
            logic b_arb_enable [1];
            logic b_arb_read [1];
            logic b_arb_write [1];
            logic b_arb_burstcount [1];
            logic [7:0] b_arb_address [1];
            logic [7:0] b_arb_writedata [1];
            logic b_arb_byteenable [1];
            logic b_arb_stall [1];
            logic b_wrp_ack [1];
            logic b_rrp_datavalid [1];
            logic [7:0] b_rrp_data [1];
            logic bank_select;

            // INST router of acl_ic_local_mem_router
            acl_ic_local_mem_router
            #(
               .DATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(8),
               .BYTEENA_W(1),
               .NUM_BANKS(1)
            )
            router
            (
               .clock(clock),
               .resetn(resetn),
               .bank_select(bank_select),
               // ICM m
               .m_arb_request(local_icm_arb_request[__l][__m]),
               .m_arb_enable(local_icm_arb_enable[__l][__m]),
               .m_arb_read(local_icm_arb_read[__l][__m]),
               .m_arb_write(local_icm_arb_write[__l][__m]),
               .m_arb_burstcount(local_icm_arb_burstcount[__l][__m]),
               .m_arb_address(local_icm_arb_address[__l][__m]),
               .m_arb_writedata(local_icm_arb_writedata[__l][__m]),
               .m_arb_byteenable(local_icm_arb_byteenable[__l][__m]),
               .m_arb_stall(local_icm_arb_stall[__l][__m]),
               .m_wrp_ack(local_icm_wrp_ack[__l][__m]),
               .m_rrp_datavalid(local_icm_rrp_datavalid[__l][__m]),
               .m_rrp_data(local_icm_rrp_data[__l][__m]),
               // ICM b
               .b_arb_request(b_arb_request),
               .b_arb_enable(b_arb_enable),
               .b_arb_read(b_arb_read),
               .b_arb_write(b_arb_write),
               .b_arb_burstcount(b_arb_burstcount),
               .b_arb_address(b_arb_address),
               .b_arb_writedata(b_arb_writedata),
               .b_arb_byteenable(b_arb_byteenable),
               .b_arb_stall(b_arb_stall),
               .b_wrp_ack(b_wrp_ack),
               .b_rrp_datavalid(b_rrp_datavalid),
               .b_rrp_data(b_rrp_data)
            );

            assign bank_select = 1'b1;
         end

         for( __m = 0; __m < 1; __m = __m + 1 )
         begin:port1bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[0].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[0].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[0].b_arb_read[0];
            assign icm_in_arb_write[0] = router[0].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[0].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[0].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[0].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[0].b_arb_byteenable[0];
            assign router[0].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[0].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[0].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[0].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_6
            conv_pipe_system_interconnect_6 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[1] = icm_out_arb_enable;
            assign bank[0].port_read[1] = icm_out_arb_read;
            assign bank[0].port_write[1] = icm_out_arb_write;
            assign bank[0].port_address[1] = icm_out_arb_address;
            assign bank[0].port_writedata[1] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[1] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[1];
            assign icm_out_rrp_data = bank[0].port_readdata[1];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[1];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __m = 0; __m < 1; __m = __m + 1 )
         begin:port2bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[1].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[1].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[1].b_arb_read[0];
            assign icm_in_arb_write[0] = router[1].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[1].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[1].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[1].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[1].b_arb_byteenable[0];
            assign router[1].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[1].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[1].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[1].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_7
            conv_pipe_system_interconnect_7 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[2] = icm_out_arb_enable;
            assign bank[0].port_read[2] = icm_out_arb_read;
            assign bank[0].port_write[2] = icm_out_arb_write;
            assign bank[0].port_address[2] = icm_out_arb_address;
            assign bank[0].port_writedata[2] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[2] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[2];
            assign icm_out_rrp_data = bank[0].port_readdata[2];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[2];
            assign icm_out_wrp_ack = 'b0;
         end

      end

   end
   endgenerate

   generate
   begin:local_mem_system_aspace12
      logic local_icm_arb_request [1][2];
      logic local_icm_arb_enable [1][2];
      logic local_icm_arb_read [1][2];
      logic local_icm_arb_write [1][2];
      logic local_icm_arb_burstcount [1][2];
      logic [7:0] local_icm_arb_address [1][2];
      logic [7:0] local_icm_arb_writedata [1][2];
      logic local_icm_arb_byteenable [1][2];
      logic local_icm_arb_stall [1][2];
      logic local_icm_wrp_ack [1][2];
      logic local_icm_rrp_datavalid [1][2];
      logic [7:0] local_icm_rrp_data [1][2];

      for( __m = 0; __m < 1; __m = __m + 1 )
      begin:local_mem_group
         for( __n = 0; __n < 2; __n = __n + 1 )
         begin:master
            // INST avm_to_ic of acl_avm_to_ic
            acl_avm_to_ic
            #(
               .DATA_W(8),
               .WRITEDATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(32),
               .BYTEENA_W(1)
            )
            avm_to_ic
            (
               // AVM avm
               .avm_enable(local_avm_aspace12_enable[__m][__n]),
               .avm_read(local_avm_aspace12_read[__m][__n]),
               .avm_write(local_avm_aspace12_write[__m][__n]),
               .avm_burstcount(local_avm_aspace12_burstcount[__m][__n]),
               .avm_address(local_avm_aspace12_address[__m][__n]),
               .avm_writedata(local_avm_aspace12_writedata[__m][__n]),
               .avm_byteenable(local_avm_aspace12_byteenable[__m][__n]),
               .avm_waitrequest(local_avm_aspace12_waitrequest[__m][__n]),
               .avm_readdata(local_avm_aspace12_readdata[__m][__n]),
               .avm_readdatavalid(local_avm_aspace12_readdatavalid[__m][__n]),
               .avm_writeack(local_avm_aspace12_writeack[__m][__n]),
               // ICM ic
               .ic_arb_request(local_icm_arb_request[__m][__n]),
               .ic_arb_enable(local_icm_arb_enable[__m][__n]),
               .ic_arb_read(local_icm_arb_read[__m][__n]),
               .ic_arb_write(local_icm_arb_write[__m][__n]),
               .ic_arb_burstcount(local_icm_arb_burstcount[__m][__n]),
               .ic_arb_address(local_icm_arb_address[__m][__n]),
               .ic_arb_writedata(local_icm_arb_writedata[__m][__n]),
               .ic_arb_byteenable(local_icm_arb_byteenable[__m][__n]),
               .ic_arb_stall(local_icm_arb_stall[__m][__n]),
               .ic_wrp_ack(local_icm_wrp_ack[__m][__n]),
               .ic_rrp_datavalid(local_icm_rrp_datavalid[__m][__n]),
               .ic_rrp_data(local_icm_rrp_data[__m][__n])
            );

         end

         for( __n = 0; __n < 1; __n = __n + 1 )
         begin:bank
            logic port_enable [1:2];
            logic port_read [1:2];
            logic port_write [1:2];
            logic [7:0] port_address [1:2];
            logic [7:0] port_writedata [1:2];
            logic port_byteenable [1:2];
            logic port_waitrequest [1:2];
            logic [7:0] port_readdata [1:2];
            logic port_readdatavalid [1:2];

            // INST mem0 of acl_mem1x
            acl_mem1x
            #(
               .INTENDED_DEVICE_FAMILY("Cyclone V"),
               .DEPTH_WORDS(256),
               .WIDTH(8),
               .MEM_LATENCY(3),
               .ENABLED(0),
               .RDW_MODE("OLD_DATA"),
               .RAM_OPERATION_MODE("BIDIR_DUAL_PORT"),
               .PREFERRED_WIDTH(160),
               .RAM_BLOCK_TYPE("M10K")
            )
            mem0
            (
               .clk(clock),
               .resetn(resetn),
               // AVS avs_port1
               .avs_port1_enable(port_enable[1]),
               .avs_port1_read(port_read[1]),
               .avs_port1_write(port_write[1]),
               .avs_port1_address(port_address[1]),
               .avs_port1_writedata(port_writedata[1]),
               .avs_port1_byteenable(port_byteenable[1]),
               .avs_port1_waitrequest(port_waitrequest[1]),
               .avs_port1_readdata(port_readdata[1]),
               .avs_port1_readdatavalid(port_readdatavalid[1]),
               // AVS avs_port2
               .avs_port2_enable(port_enable[2]),
               .avs_port2_read(port_read[2]),
               .avs_port2_write(port_write[2]),
               .avs_port2_address(port_address[2]),
               .avs_port2_writedata(port_writedata[2]),
               .avs_port2_byteenable(port_byteenable[2]),
               .avs_port2_waitrequest(port_waitrequest[2]),
               .avs_port2_readdata(port_readdata[2]),
               .avs_port2_readdatavalid(port_readdatavalid[2])
            );

         end

         for( __n = 0; __n < 2; __n = __n + 1 )
         begin:router
            logic b_arb_request [1];
            logic b_arb_enable [1];
            logic b_arb_read [1];
            logic b_arb_write [1];
            logic b_arb_burstcount [1];
            logic [7:0] b_arb_address [1];
            logic [7:0] b_arb_writedata [1];
            logic b_arb_byteenable [1];
            logic b_arb_stall [1];
            logic b_wrp_ack [1];
            logic b_rrp_datavalid [1];
            logic [7:0] b_rrp_data [1];
            logic bank_select;

            // INST router of acl_ic_local_mem_router
            acl_ic_local_mem_router
            #(
               .DATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(8),
               .BYTEENA_W(1),
               .NUM_BANKS(1)
            )
            router
            (
               .clock(clock),
               .resetn(resetn),
               .bank_select(bank_select),
               // ICM m
               .m_arb_request(local_icm_arb_request[__m][__n]),
               .m_arb_enable(local_icm_arb_enable[__m][__n]),
               .m_arb_read(local_icm_arb_read[__m][__n]),
               .m_arb_write(local_icm_arb_write[__m][__n]),
               .m_arb_burstcount(local_icm_arb_burstcount[__m][__n]),
               .m_arb_address(local_icm_arb_address[__m][__n]),
               .m_arb_writedata(local_icm_arb_writedata[__m][__n]),
               .m_arb_byteenable(local_icm_arb_byteenable[__m][__n]),
               .m_arb_stall(local_icm_arb_stall[__m][__n]),
               .m_wrp_ack(local_icm_wrp_ack[__m][__n]),
               .m_rrp_datavalid(local_icm_rrp_datavalid[__m][__n]),
               .m_rrp_data(local_icm_rrp_data[__m][__n]),
               // ICM b
               .b_arb_request(b_arb_request),
               .b_arb_enable(b_arb_enable),
               .b_arb_read(b_arb_read),
               .b_arb_write(b_arb_write),
               .b_arb_burstcount(b_arb_burstcount),
               .b_arb_address(b_arb_address),
               .b_arb_writedata(b_arb_writedata),
               .b_arb_byteenable(b_arb_byteenable),
               .b_arb_stall(b_arb_stall),
               .b_wrp_ack(b_wrp_ack),
               .b_rrp_datavalid(b_rrp_datavalid),
               .b_rrp_data(b_rrp_data)
            );

            assign bank_select = 1'b1;
         end

         for( __n = 0; __n < 1; __n = __n + 1 )
         begin:port1bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[0].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[0].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[0].b_arb_read[0];
            assign icm_in_arb_write[0] = router[0].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[0].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[0].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[0].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[0].b_arb_byteenable[0];
            assign router[0].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[0].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[0].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[0].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_6
            conv_pipe_system_interconnect_6 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[1] = icm_out_arb_enable;
            assign bank[0].port_read[1] = icm_out_arb_read;
            assign bank[0].port_write[1] = icm_out_arb_write;
            assign bank[0].port_address[1] = icm_out_arb_address;
            assign bank[0].port_writedata[1] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[1] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[1];
            assign icm_out_rrp_data = bank[0].port_readdata[1];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[1];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __n = 0; __n < 1; __n = __n + 1 )
         begin:port2bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[1].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[1].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[1].b_arb_read[0];
            assign icm_in_arb_write[0] = router[1].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[1].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[1].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[1].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[1].b_arb_byteenable[0];
            assign router[1].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[1].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[1].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[1].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_7
            conv_pipe_system_interconnect_7 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[2] = icm_out_arb_enable;
            assign bank[0].port_read[2] = icm_out_arb_read;
            assign bank[0].port_write[2] = icm_out_arb_write;
            assign bank[0].port_address[2] = icm_out_arb_address;
            assign bank[0].port_writedata[2] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[2] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[2];
            assign icm_out_rrp_data = bank[0].port_readdata[2];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[2];
            assign icm_out_wrp_ack = 'b0;
         end

      end

   end
   endgenerate

   generate
   begin:local_mem_system_aspace13
      logic local_icm_arb_request [1][2];
      logic local_icm_arb_enable [1][2];
      logic local_icm_arb_read [1][2];
      logic local_icm_arb_write [1][2];
      logic local_icm_arb_burstcount [1][2];
      logic [7:0] local_icm_arb_address [1][2];
      logic [7:0] local_icm_arb_writedata [1][2];
      logic local_icm_arb_byteenable [1][2];
      logic local_icm_arb_stall [1][2];
      logic local_icm_wrp_ack [1][2];
      logic local_icm_rrp_datavalid [1][2];
      logic [7:0] local_icm_rrp_data [1][2];

      for( __n = 0; __n < 1; __n = __n + 1 )
      begin:local_mem_group
         for( __o = 0; __o < 2; __o = __o + 1 )
         begin:master
            // INST avm_to_ic of acl_avm_to_ic
            acl_avm_to_ic
            #(
               .DATA_W(8),
               .WRITEDATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(32),
               .BYTEENA_W(1)
            )
            avm_to_ic
            (
               // AVM avm
               .avm_enable(local_avm_aspace13_enable[__n][__o]),
               .avm_read(local_avm_aspace13_read[__n][__o]),
               .avm_write(local_avm_aspace13_write[__n][__o]),
               .avm_burstcount(local_avm_aspace13_burstcount[__n][__o]),
               .avm_address(local_avm_aspace13_address[__n][__o]),
               .avm_writedata(local_avm_aspace13_writedata[__n][__o]),
               .avm_byteenable(local_avm_aspace13_byteenable[__n][__o]),
               .avm_waitrequest(local_avm_aspace13_waitrequest[__n][__o]),
               .avm_readdata(local_avm_aspace13_readdata[__n][__o]),
               .avm_readdatavalid(local_avm_aspace13_readdatavalid[__n][__o]),
               .avm_writeack(local_avm_aspace13_writeack[__n][__o]),
               // ICM ic
               .ic_arb_request(local_icm_arb_request[__n][__o]),
               .ic_arb_enable(local_icm_arb_enable[__n][__o]),
               .ic_arb_read(local_icm_arb_read[__n][__o]),
               .ic_arb_write(local_icm_arb_write[__n][__o]),
               .ic_arb_burstcount(local_icm_arb_burstcount[__n][__o]),
               .ic_arb_address(local_icm_arb_address[__n][__o]),
               .ic_arb_writedata(local_icm_arb_writedata[__n][__o]),
               .ic_arb_byteenable(local_icm_arb_byteenable[__n][__o]),
               .ic_arb_stall(local_icm_arb_stall[__n][__o]),
               .ic_wrp_ack(local_icm_wrp_ack[__n][__o]),
               .ic_rrp_datavalid(local_icm_rrp_datavalid[__n][__o]),
               .ic_rrp_data(local_icm_rrp_data[__n][__o])
            );

         end

         for( __o = 0; __o < 1; __o = __o + 1 )
         begin:bank
            logic port_enable [1:2];
            logic port_read [1:2];
            logic port_write [1:2];
            logic [7:0] port_address [1:2];
            logic [7:0] port_writedata [1:2];
            logic port_byteenable [1:2];
            logic port_waitrequest [1:2];
            logic [7:0] port_readdata [1:2];
            logic port_readdatavalid [1:2];

            // INST mem0 of acl_mem1x
            acl_mem1x
            #(
               .INTENDED_DEVICE_FAMILY("Cyclone V"),
               .DEPTH_WORDS(256),
               .WIDTH(8),
               .MEM_LATENCY(3),
               .ENABLED(0),
               .RDW_MODE("OLD_DATA"),
               .RAM_OPERATION_MODE("BIDIR_DUAL_PORT"),
               .PREFERRED_WIDTH(160),
               .RAM_BLOCK_TYPE("M10K")
            )
            mem0
            (
               .clk(clock),
               .resetn(resetn),
               // AVS avs_port1
               .avs_port1_enable(port_enable[1]),
               .avs_port1_read(port_read[1]),
               .avs_port1_write(port_write[1]),
               .avs_port1_address(port_address[1]),
               .avs_port1_writedata(port_writedata[1]),
               .avs_port1_byteenable(port_byteenable[1]),
               .avs_port1_waitrequest(port_waitrequest[1]),
               .avs_port1_readdata(port_readdata[1]),
               .avs_port1_readdatavalid(port_readdatavalid[1]),
               // AVS avs_port2
               .avs_port2_enable(port_enable[2]),
               .avs_port2_read(port_read[2]),
               .avs_port2_write(port_write[2]),
               .avs_port2_address(port_address[2]),
               .avs_port2_writedata(port_writedata[2]),
               .avs_port2_byteenable(port_byteenable[2]),
               .avs_port2_waitrequest(port_waitrequest[2]),
               .avs_port2_readdata(port_readdata[2]),
               .avs_port2_readdatavalid(port_readdatavalid[2])
            );

         end

         for( __o = 0; __o < 2; __o = __o + 1 )
         begin:router
            logic b_arb_request [1];
            logic b_arb_enable [1];
            logic b_arb_read [1];
            logic b_arb_write [1];
            logic b_arb_burstcount [1];
            logic [7:0] b_arb_address [1];
            logic [7:0] b_arb_writedata [1];
            logic b_arb_byteenable [1];
            logic b_arb_stall [1];
            logic b_wrp_ack [1];
            logic b_rrp_datavalid [1];
            logic [7:0] b_rrp_data [1];
            logic bank_select;

            // INST router of acl_ic_local_mem_router
            acl_ic_local_mem_router
            #(
               .DATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(8),
               .BYTEENA_W(1),
               .NUM_BANKS(1)
            )
            router
            (
               .clock(clock),
               .resetn(resetn),
               .bank_select(bank_select),
               // ICM m
               .m_arb_request(local_icm_arb_request[__n][__o]),
               .m_arb_enable(local_icm_arb_enable[__n][__o]),
               .m_arb_read(local_icm_arb_read[__n][__o]),
               .m_arb_write(local_icm_arb_write[__n][__o]),
               .m_arb_burstcount(local_icm_arb_burstcount[__n][__o]),
               .m_arb_address(local_icm_arb_address[__n][__o]),
               .m_arb_writedata(local_icm_arb_writedata[__n][__o]),
               .m_arb_byteenable(local_icm_arb_byteenable[__n][__o]),
               .m_arb_stall(local_icm_arb_stall[__n][__o]),
               .m_wrp_ack(local_icm_wrp_ack[__n][__o]),
               .m_rrp_datavalid(local_icm_rrp_datavalid[__n][__o]),
               .m_rrp_data(local_icm_rrp_data[__n][__o]),
               // ICM b
               .b_arb_request(b_arb_request),
               .b_arb_enable(b_arb_enable),
               .b_arb_read(b_arb_read),
               .b_arb_write(b_arb_write),
               .b_arb_burstcount(b_arb_burstcount),
               .b_arb_address(b_arb_address),
               .b_arb_writedata(b_arb_writedata),
               .b_arb_byteenable(b_arb_byteenable),
               .b_arb_stall(b_arb_stall),
               .b_wrp_ack(b_wrp_ack),
               .b_rrp_datavalid(b_rrp_datavalid),
               .b_rrp_data(b_rrp_data)
            );

            assign bank_select = 1'b1;
         end

         for( __o = 0; __o < 1; __o = __o + 1 )
         begin:port1bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[0].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[0].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[0].b_arb_read[0];
            assign icm_in_arb_write[0] = router[0].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[0].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[0].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[0].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[0].b_arb_byteenable[0];
            assign router[0].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[0].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[0].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[0].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_6
            conv_pipe_system_interconnect_6 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[1] = icm_out_arb_enable;
            assign bank[0].port_read[1] = icm_out_arb_read;
            assign bank[0].port_write[1] = icm_out_arb_write;
            assign bank[0].port_address[1] = icm_out_arb_address;
            assign bank[0].port_writedata[1] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[1] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[1];
            assign icm_out_rrp_data = bank[0].port_readdata[1];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[1];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __o = 0; __o < 1; __o = __o + 1 )
         begin:port2bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[1].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[1].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[1].b_arb_read[0];
            assign icm_in_arb_write[0] = router[1].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[1].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[1].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[1].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[1].b_arb_byteenable[0];
            assign router[1].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[1].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[1].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[1].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_7
            conv_pipe_system_interconnect_7 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[2] = icm_out_arb_enable;
            assign bank[0].port_read[2] = icm_out_arb_read;
            assign bank[0].port_write[2] = icm_out_arb_write;
            assign bank[0].port_address[2] = icm_out_arb_address;
            assign bank[0].port_writedata[2] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[2] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[2];
            assign icm_out_rrp_data = bank[0].port_readdata[2];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[2];
            assign icm_out_wrp_ack = 'b0;
         end

      end

   end
   endgenerate

   generate
   begin:local_mem_system_aspace14
      logic local_icm_arb_request [1][2];
      logic local_icm_arb_enable [1][2];
      logic local_icm_arb_read [1][2];
      logic local_icm_arb_write [1][2];
      logic local_icm_arb_burstcount [1][2];
      logic [7:0] local_icm_arb_address [1][2];
      logic [7:0] local_icm_arb_writedata [1][2];
      logic local_icm_arb_byteenable [1][2];
      logic local_icm_arb_stall [1][2];
      logic local_icm_wrp_ack [1][2];
      logic local_icm_rrp_datavalid [1][2];
      logic [7:0] local_icm_rrp_data [1][2];

      for( __o = 0; __o < 1; __o = __o + 1 )
      begin:local_mem_group
         for( __p = 0; __p < 2; __p = __p + 1 )
         begin:master
            // INST avm_to_ic of acl_avm_to_ic
            acl_avm_to_ic
            #(
               .DATA_W(8),
               .WRITEDATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(32),
               .BYTEENA_W(1)
            )
            avm_to_ic
            (
               // AVM avm
               .avm_enable(local_avm_aspace14_enable[__o][__p]),
               .avm_read(local_avm_aspace14_read[__o][__p]),
               .avm_write(local_avm_aspace14_write[__o][__p]),
               .avm_burstcount(local_avm_aspace14_burstcount[__o][__p]),
               .avm_address(local_avm_aspace14_address[__o][__p]),
               .avm_writedata(local_avm_aspace14_writedata[__o][__p]),
               .avm_byteenable(local_avm_aspace14_byteenable[__o][__p]),
               .avm_waitrequest(local_avm_aspace14_waitrequest[__o][__p]),
               .avm_readdata(local_avm_aspace14_readdata[__o][__p]),
               .avm_readdatavalid(local_avm_aspace14_readdatavalid[__o][__p]),
               .avm_writeack(local_avm_aspace14_writeack[__o][__p]),
               // ICM ic
               .ic_arb_request(local_icm_arb_request[__o][__p]),
               .ic_arb_enable(local_icm_arb_enable[__o][__p]),
               .ic_arb_read(local_icm_arb_read[__o][__p]),
               .ic_arb_write(local_icm_arb_write[__o][__p]),
               .ic_arb_burstcount(local_icm_arb_burstcount[__o][__p]),
               .ic_arb_address(local_icm_arb_address[__o][__p]),
               .ic_arb_writedata(local_icm_arb_writedata[__o][__p]),
               .ic_arb_byteenable(local_icm_arb_byteenable[__o][__p]),
               .ic_arb_stall(local_icm_arb_stall[__o][__p]),
               .ic_wrp_ack(local_icm_wrp_ack[__o][__p]),
               .ic_rrp_datavalid(local_icm_rrp_datavalid[__o][__p]),
               .ic_rrp_data(local_icm_rrp_data[__o][__p])
            );

         end

         for( __p = 0; __p < 1; __p = __p + 1 )
         begin:bank
            logic port_enable [1:2];
            logic port_read [1:2];
            logic port_write [1:2];
            logic [7:0] port_address [1:2];
            logic [7:0] port_writedata [1:2];
            logic port_byteenable [1:2];
            logic port_waitrequest [1:2];
            logic [7:0] port_readdata [1:2];
            logic port_readdatavalid [1:2];

            // INST mem0 of acl_mem1x
            acl_mem1x
            #(
               .INTENDED_DEVICE_FAMILY("Cyclone V"),
               .DEPTH_WORDS(256),
               .WIDTH(8),
               .MEM_LATENCY(3),
               .ENABLED(0),
               .RDW_MODE("OLD_DATA"),
               .RAM_OPERATION_MODE("BIDIR_DUAL_PORT"),
               .PREFERRED_WIDTH(160),
               .RAM_BLOCK_TYPE("M10K")
            )
            mem0
            (
               .clk(clock),
               .resetn(resetn),
               // AVS avs_port1
               .avs_port1_enable(port_enable[1]),
               .avs_port1_read(port_read[1]),
               .avs_port1_write(port_write[1]),
               .avs_port1_address(port_address[1]),
               .avs_port1_writedata(port_writedata[1]),
               .avs_port1_byteenable(port_byteenable[1]),
               .avs_port1_waitrequest(port_waitrequest[1]),
               .avs_port1_readdata(port_readdata[1]),
               .avs_port1_readdatavalid(port_readdatavalid[1]),
               // AVS avs_port2
               .avs_port2_enable(port_enable[2]),
               .avs_port2_read(port_read[2]),
               .avs_port2_write(port_write[2]),
               .avs_port2_address(port_address[2]),
               .avs_port2_writedata(port_writedata[2]),
               .avs_port2_byteenable(port_byteenable[2]),
               .avs_port2_waitrequest(port_waitrequest[2]),
               .avs_port2_readdata(port_readdata[2]),
               .avs_port2_readdatavalid(port_readdatavalid[2])
            );

         end

         for( __p = 0; __p < 2; __p = __p + 1 )
         begin:router
            logic b_arb_request [1];
            logic b_arb_enable [1];
            logic b_arb_read [1];
            logic b_arb_write [1];
            logic b_arb_burstcount [1];
            logic [7:0] b_arb_address [1];
            logic [7:0] b_arb_writedata [1];
            logic b_arb_byteenable [1];
            logic b_arb_stall [1];
            logic b_wrp_ack [1];
            logic b_rrp_datavalid [1];
            logic [7:0] b_rrp_data [1];
            logic bank_select;

            // INST router of acl_ic_local_mem_router
            acl_ic_local_mem_router
            #(
               .DATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(8),
               .BYTEENA_W(1),
               .NUM_BANKS(1)
            )
            router
            (
               .clock(clock),
               .resetn(resetn),
               .bank_select(bank_select),
               // ICM m
               .m_arb_request(local_icm_arb_request[__o][__p]),
               .m_arb_enable(local_icm_arb_enable[__o][__p]),
               .m_arb_read(local_icm_arb_read[__o][__p]),
               .m_arb_write(local_icm_arb_write[__o][__p]),
               .m_arb_burstcount(local_icm_arb_burstcount[__o][__p]),
               .m_arb_address(local_icm_arb_address[__o][__p]),
               .m_arb_writedata(local_icm_arb_writedata[__o][__p]),
               .m_arb_byteenable(local_icm_arb_byteenable[__o][__p]),
               .m_arb_stall(local_icm_arb_stall[__o][__p]),
               .m_wrp_ack(local_icm_wrp_ack[__o][__p]),
               .m_rrp_datavalid(local_icm_rrp_datavalid[__o][__p]),
               .m_rrp_data(local_icm_rrp_data[__o][__p]),
               // ICM b
               .b_arb_request(b_arb_request),
               .b_arb_enable(b_arb_enable),
               .b_arb_read(b_arb_read),
               .b_arb_write(b_arb_write),
               .b_arb_burstcount(b_arb_burstcount),
               .b_arb_address(b_arb_address),
               .b_arb_writedata(b_arb_writedata),
               .b_arb_byteenable(b_arb_byteenable),
               .b_arb_stall(b_arb_stall),
               .b_wrp_ack(b_wrp_ack),
               .b_rrp_datavalid(b_rrp_datavalid),
               .b_rrp_data(b_rrp_data)
            );

            assign bank_select = 1'b1;
         end

         for( __p = 0; __p < 1; __p = __p + 1 )
         begin:port1bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[0].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[0].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[0].b_arb_read[0];
            assign icm_in_arb_write[0] = router[0].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[0].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[0].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[0].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[0].b_arb_byteenable[0];
            assign router[0].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[0].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[0].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[0].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_6
            conv_pipe_system_interconnect_6 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[1] = icm_out_arb_enable;
            assign bank[0].port_read[1] = icm_out_arb_read;
            assign bank[0].port_write[1] = icm_out_arb_write;
            assign bank[0].port_address[1] = icm_out_arb_address;
            assign bank[0].port_writedata[1] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[1] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[1];
            assign icm_out_rrp_data = bank[0].port_readdata[1];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[1];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __p = 0; __p < 1; __p = __p + 1 )
         begin:port2bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[1].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[1].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[1].b_arb_read[0];
            assign icm_in_arb_write[0] = router[1].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[1].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[1].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[1].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[1].b_arb_byteenable[0];
            assign router[1].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[1].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[1].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[1].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_7
            conv_pipe_system_interconnect_7 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[2] = icm_out_arb_enable;
            assign bank[0].port_read[2] = icm_out_arb_read;
            assign bank[0].port_write[2] = icm_out_arb_write;
            assign bank[0].port_address[2] = icm_out_arb_address;
            assign bank[0].port_writedata[2] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[2] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[2];
            assign icm_out_rrp_data = bank[0].port_readdata[2];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[2];
            assign icm_out_wrp_ack = 'b0;
         end

      end

   end
   endgenerate

   generate
   begin:local_mem_system_aspace15
      logic local_icm_arb_request [1][2];
      logic local_icm_arb_enable [1][2];
      logic local_icm_arb_read [1][2];
      logic local_icm_arb_write [1][2];
      logic local_icm_arb_burstcount [1][2];
      logic [7:0] local_icm_arb_address [1][2];
      logic [7:0] local_icm_arb_writedata [1][2];
      logic local_icm_arb_byteenable [1][2];
      logic local_icm_arb_stall [1][2];
      logic local_icm_wrp_ack [1][2];
      logic local_icm_rrp_datavalid [1][2];
      logic [7:0] local_icm_rrp_data [1][2];

      for( __p = 0; __p < 1; __p = __p + 1 )
      begin:local_mem_group
         for( __q = 0; __q < 2; __q = __q + 1 )
         begin:master
            // INST avm_to_ic of acl_avm_to_ic
            acl_avm_to_ic
            #(
               .DATA_W(8),
               .WRITEDATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(32),
               .BYTEENA_W(1)
            )
            avm_to_ic
            (
               // AVM avm
               .avm_enable(local_avm_aspace15_enable[__p][__q]),
               .avm_read(local_avm_aspace15_read[__p][__q]),
               .avm_write(local_avm_aspace15_write[__p][__q]),
               .avm_burstcount(local_avm_aspace15_burstcount[__p][__q]),
               .avm_address(local_avm_aspace15_address[__p][__q]),
               .avm_writedata(local_avm_aspace15_writedata[__p][__q]),
               .avm_byteenable(local_avm_aspace15_byteenable[__p][__q]),
               .avm_waitrequest(local_avm_aspace15_waitrequest[__p][__q]),
               .avm_readdata(local_avm_aspace15_readdata[__p][__q]),
               .avm_readdatavalid(local_avm_aspace15_readdatavalid[__p][__q]),
               .avm_writeack(local_avm_aspace15_writeack[__p][__q]),
               // ICM ic
               .ic_arb_request(local_icm_arb_request[__p][__q]),
               .ic_arb_enable(local_icm_arb_enable[__p][__q]),
               .ic_arb_read(local_icm_arb_read[__p][__q]),
               .ic_arb_write(local_icm_arb_write[__p][__q]),
               .ic_arb_burstcount(local_icm_arb_burstcount[__p][__q]),
               .ic_arb_address(local_icm_arb_address[__p][__q]),
               .ic_arb_writedata(local_icm_arb_writedata[__p][__q]),
               .ic_arb_byteenable(local_icm_arb_byteenable[__p][__q]),
               .ic_arb_stall(local_icm_arb_stall[__p][__q]),
               .ic_wrp_ack(local_icm_wrp_ack[__p][__q]),
               .ic_rrp_datavalid(local_icm_rrp_datavalid[__p][__q]),
               .ic_rrp_data(local_icm_rrp_data[__p][__q])
            );

         end

         for( __q = 0; __q < 1; __q = __q + 1 )
         begin:bank
            logic port_enable [1:2];
            logic port_read [1:2];
            logic port_write [1:2];
            logic [7:0] port_address [1:2];
            logic [7:0] port_writedata [1:2];
            logic port_byteenable [1:2];
            logic port_waitrequest [1:2];
            logic [7:0] port_readdata [1:2];
            logic port_readdatavalid [1:2];

            // INST mem0 of acl_mem1x
            acl_mem1x
            #(
               .INTENDED_DEVICE_FAMILY("Cyclone V"),
               .DEPTH_WORDS(256),
               .WIDTH(8),
               .MEM_LATENCY(3),
               .ENABLED(0),
               .RDW_MODE("OLD_DATA"),
               .RAM_OPERATION_MODE("BIDIR_DUAL_PORT"),
               .PREFERRED_WIDTH(160),
               .RAM_BLOCK_TYPE("M10K")
            )
            mem0
            (
               .clk(clock),
               .resetn(resetn),
               // AVS avs_port1
               .avs_port1_enable(port_enable[1]),
               .avs_port1_read(port_read[1]),
               .avs_port1_write(port_write[1]),
               .avs_port1_address(port_address[1]),
               .avs_port1_writedata(port_writedata[1]),
               .avs_port1_byteenable(port_byteenable[1]),
               .avs_port1_waitrequest(port_waitrequest[1]),
               .avs_port1_readdata(port_readdata[1]),
               .avs_port1_readdatavalid(port_readdatavalid[1]),
               // AVS avs_port2
               .avs_port2_enable(port_enable[2]),
               .avs_port2_read(port_read[2]),
               .avs_port2_write(port_write[2]),
               .avs_port2_address(port_address[2]),
               .avs_port2_writedata(port_writedata[2]),
               .avs_port2_byteenable(port_byteenable[2]),
               .avs_port2_waitrequest(port_waitrequest[2]),
               .avs_port2_readdata(port_readdata[2]),
               .avs_port2_readdatavalid(port_readdatavalid[2])
            );

         end

         for( __q = 0; __q < 2; __q = __q + 1 )
         begin:router
            logic b_arb_request [1];
            logic b_arb_enable [1];
            logic b_arb_read [1];
            logic b_arb_write [1];
            logic b_arb_burstcount [1];
            logic [7:0] b_arb_address [1];
            logic [7:0] b_arb_writedata [1];
            logic b_arb_byteenable [1];
            logic b_arb_stall [1];
            logic b_wrp_ack [1];
            logic b_rrp_datavalid [1];
            logic [7:0] b_rrp_data [1];
            logic bank_select;

            // INST router of acl_ic_local_mem_router
            acl_ic_local_mem_router
            #(
               .DATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(8),
               .BYTEENA_W(1),
               .NUM_BANKS(1)
            )
            router
            (
               .clock(clock),
               .resetn(resetn),
               .bank_select(bank_select),
               // ICM m
               .m_arb_request(local_icm_arb_request[__p][__q]),
               .m_arb_enable(local_icm_arb_enable[__p][__q]),
               .m_arb_read(local_icm_arb_read[__p][__q]),
               .m_arb_write(local_icm_arb_write[__p][__q]),
               .m_arb_burstcount(local_icm_arb_burstcount[__p][__q]),
               .m_arb_address(local_icm_arb_address[__p][__q]),
               .m_arb_writedata(local_icm_arb_writedata[__p][__q]),
               .m_arb_byteenable(local_icm_arb_byteenable[__p][__q]),
               .m_arb_stall(local_icm_arb_stall[__p][__q]),
               .m_wrp_ack(local_icm_wrp_ack[__p][__q]),
               .m_rrp_datavalid(local_icm_rrp_datavalid[__p][__q]),
               .m_rrp_data(local_icm_rrp_data[__p][__q]),
               // ICM b
               .b_arb_request(b_arb_request),
               .b_arb_enable(b_arb_enable),
               .b_arb_read(b_arb_read),
               .b_arb_write(b_arb_write),
               .b_arb_burstcount(b_arb_burstcount),
               .b_arb_address(b_arb_address),
               .b_arb_writedata(b_arb_writedata),
               .b_arb_byteenable(b_arb_byteenable),
               .b_arb_stall(b_arb_stall),
               .b_wrp_ack(b_wrp_ack),
               .b_rrp_datavalid(b_rrp_datavalid),
               .b_rrp_data(b_rrp_data)
            );

            assign bank_select = 1'b1;
         end

         for( __q = 0; __q < 1; __q = __q + 1 )
         begin:port1bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[0].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[0].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[0].b_arb_read[0];
            assign icm_in_arb_write[0] = router[0].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[0].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[0].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[0].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[0].b_arb_byteenable[0];
            assign router[0].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[0].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[0].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[0].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_6
            conv_pipe_system_interconnect_6 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[1] = icm_out_arb_enable;
            assign bank[0].port_read[1] = icm_out_arb_read;
            assign bank[0].port_write[1] = icm_out_arb_write;
            assign bank[0].port_address[1] = icm_out_arb_address;
            assign bank[0].port_writedata[1] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[1] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[1];
            assign icm_out_rrp_data = bank[0].port_readdata[1];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[1];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __q = 0; __q < 1; __q = __q + 1 )
         begin:port2bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[1].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[1].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[1].b_arb_read[0];
            assign icm_in_arb_write[0] = router[1].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[1].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[1].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[1].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[1].b_arb_byteenable[0];
            assign router[1].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[1].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[1].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[1].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_7
            conv_pipe_system_interconnect_7 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[2] = icm_out_arb_enable;
            assign bank[0].port_read[2] = icm_out_arb_read;
            assign bank[0].port_write[2] = icm_out_arb_write;
            assign bank[0].port_address[2] = icm_out_arb_address;
            assign bank[0].port_writedata[2] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[2] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[2];
            assign icm_out_rrp_data = bank[0].port_readdata[2];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[2];
            assign icm_out_wrp_ack = 'b0;
         end

      end

   end
   endgenerate

   generate
   begin:local_mem_system_aspace16
      logic local_icm_arb_request [1][2];
      logic local_icm_arb_enable [1][2];
      logic local_icm_arb_read [1][2];
      logic local_icm_arb_write [1][2];
      logic local_icm_arb_burstcount [1][2];
      logic [7:0] local_icm_arb_address [1][2];
      logic [7:0] local_icm_arb_writedata [1][2];
      logic local_icm_arb_byteenable [1][2];
      logic local_icm_arb_stall [1][2];
      logic local_icm_wrp_ack [1][2];
      logic local_icm_rrp_datavalid [1][2];
      logic [7:0] local_icm_rrp_data [1][2];

      for( __q = 0; __q < 1; __q = __q + 1 )
      begin:local_mem_group
         for( __r = 0; __r < 2; __r = __r + 1 )
         begin:master
            // INST avm_to_ic of acl_avm_to_ic
            acl_avm_to_ic
            #(
               .DATA_W(8),
               .WRITEDATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(32),
               .BYTEENA_W(1)
            )
            avm_to_ic
            (
               // AVM avm
               .avm_enable(local_avm_aspace16_enable[__q][__r]),
               .avm_read(local_avm_aspace16_read[__q][__r]),
               .avm_write(local_avm_aspace16_write[__q][__r]),
               .avm_burstcount(local_avm_aspace16_burstcount[__q][__r]),
               .avm_address(local_avm_aspace16_address[__q][__r]),
               .avm_writedata(local_avm_aspace16_writedata[__q][__r]),
               .avm_byteenable(local_avm_aspace16_byteenable[__q][__r]),
               .avm_waitrequest(local_avm_aspace16_waitrequest[__q][__r]),
               .avm_readdata(local_avm_aspace16_readdata[__q][__r]),
               .avm_readdatavalid(local_avm_aspace16_readdatavalid[__q][__r]),
               .avm_writeack(local_avm_aspace16_writeack[__q][__r]),
               // ICM ic
               .ic_arb_request(local_icm_arb_request[__q][__r]),
               .ic_arb_enable(local_icm_arb_enable[__q][__r]),
               .ic_arb_read(local_icm_arb_read[__q][__r]),
               .ic_arb_write(local_icm_arb_write[__q][__r]),
               .ic_arb_burstcount(local_icm_arb_burstcount[__q][__r]),
               .ic_arb_address(local_icm_arb_address[__q][__r]),
               .ic_arb_writedata(local_icm_arb_writedata[__q][__r]),
               .ic_arb_byteenable(local_icm_arb_byteenable[__q][__r]),
               .ic_arb_stall(local_icm_arb_stall[__q][__r]),
               .ic_wrp_ack(local_icm_wrp_ack[__q][__r]),
               .ic_rrp_datavalid(local_icm_rrp_datavalid[__q][__r]),
               .ic_rrp_data(local_icm_rrp_data[__q][__r])
            );

         end

         for( __r = 0; __r < 1; __r = __r + 1 )
         begin:bank
            logic port_enable [1:2];
            logic port_read [1:2];
            logic port_write [1:2];
            logic [7:0] port_address [1:2];
            logic [7:0] port_writedata [1:2];
            logic port_byteenable [1:2];
            logic port_waitrequest [1:2];
            logic [7:0] port_readdata [1:2];
            logic port_readdatavalid [1:2];

            // INST mem0 of acl_mem1x
            acl_mem1x
            #(
               .INTENDED_DEVICE_FAMILY("Cyclone V"),
               .DEPTH_WORDS(256),
               .WIDTH(8),
               .MEM_LATENCY(3),
               .ENABLED(0),
               .RDW_MODE("OLD_DATA"),
               .RAM_OPERATION_MODE("BIDIR_DUAL_PORT"),
               .PREFERRED_WIDTH(160),
               .RAM_BLOCK_TYPE("M10K")
            )
            mem0
            (
               .clk(clock),
               .resetn(resetn),
               // AVS avs_port1
               .avs_port1_enable(port_enable[1]),
               .avs_port1_read(port_read[1]),
               .avs_port1_write(port_write[1]),
               .avs_port1_address(port_address[1]),
               .avs_port1_writedata(port_writedata[1]),
               .avs_port1_byteenable(port_byteenable[1]),
               .avs_port1_waitrequest(port_waitrequest[1]),
               .avs_port1_readdata(port_readdata[1]),
               .avs_port1_readdatavalid(port_readdatavalid[1]),
               // AVS avs_port2
               .avs_port2_enable(port_enable[2]),
               .avs_port2_read(port_read[2]),
               .avs_port2_write(port_write[2]),
               .avs_port2_address(port_address[2]),
               .avs_port2_writedata(port_writedata[2]),
               .avs_port2_byteenable(port_byteenable[2]),
               .avs_port2_waitrequest(port_waitrequest[2]),
               .avs_port2_readdata(port_readdata[2]),
               .avs_port2_readdatavalid(port_readdatavalid[2])
            );

         end

         for( __r = 0; __r < 2; __r = __r + 1 )
         begin:router
            logic b_arb_request [1];
            logic b_arb_enable [1];
            logic b_arb_read [1];
            logic b_arb_write [1];
            logic b_arb_burstcount [1];
            logic [7:0] b_arb_address [1];
            logic [7:0] b_arb_writedata [1];
            logic b_arb_byteenable [1];
            logic b_arb_stall [1];
            logic b_wrp_ack [1];
            logic b_rrp_datavalid [1];
            logic [7:0] b_rrp_data [1];
            logic bank_select;

            // INST router of acl_ic_local_mem_router
            acl_ic_local_mem_router
            #(
               .DATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(8),
               .BYTEENA_W(1),
               .NUM_BANKS(1)
            )
            router
            (
               .clock(clock),
               .resetn(resetn),
               .bank_select(bank_select),
               // ICM m
               .m_arb_request(local_icm_arb_request[__q][__r]),
               .m_arb_enable(local_icm_arb_enable[__q][__r]),
               .m_arb_read(local_icm_arb_read[__q][__r]),
               .m_arb_write(local_icm_arb_write[__q][__r]),
               .m_arb_burstcount(local_icm_arb_burstcount[__q][__r]),
               .m_arb_address(local_icm_arb_address[__q][__r]),
               .m_arb_writedata(local_icm_arb_writedata[__q][__r]),
               .m_arb_byteenable(local_icm_arb_byteenable[__q][__r]),
               .m_arb_stall(local_icm_arb_stall[__q][__r]),
               .m_wrp_ack(local_icm_wrp_ack[__q][__r]),
               .m_rrp_datavalid(local_icm_rrp_datavalid[__q][__r]),
               .m_rrp_data(local_icm_rrp_data[__q][__r]),
               // ICM b
               .b_arb_request(b_arb_request),
               .b_arb_enable(b_arb_enable),
               .b_arb_read(b_arb_read),
               .b_arb_write(b_arb_write),
               .b_arb_burstcount(b_arb_burstcount),
               .b_arb_address(b_arb_address),
               .b_arb_writedata(b_arb_writedata),
               .b_arb_byteenable(b_arb_byteenable),
               .b_arb_stall(b_arb_stall),
               .b_wrp_ack(b_wrp_ack),
               .b_rrp_datavalid(b_rrp_datavalid),
               .b_rrp_data(b_rrp_data)
            );

            assign bank_select = 1'b1;
         end

         for( __r = 0; __r < 1; __r = __r + 1 )
         begin:port1bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[0].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[0].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[0].b_arb_read[0];
            assign icm_in_arb_write[0] = router[0].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[0].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[0].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[0].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[0].b_arb_byteenable[0];
            assign router[0].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[0].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[0].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[0].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_6
            conv_pipe_system_interconnect_6 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[1] = icm_out_arb_enable;
            assign bank[0].port_read[1] = icm_out_arb_read;
            assign bank[0].port_write[1] = icm_out_arb_write;
            assign bank[0].port_address[1] = icm_out_arb_address;
            assign bank[0].port_writedata[1] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[1] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[1];
            assign icm_out_rrp_data = bank[0].port_readdata[1];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[1];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __r = 0; __r < 1; __r = __r + 1 )
         begin:port2bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[1].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[1].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[1].b_arb_read[0];
            assign icm_in_arb_write[0] = router[1].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[1].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[1].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[1].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[1].b_arb_byteenable[0];
            assign router[1].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[1].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[1].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[1].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_7
            conv_pipe_system_interconnect_7 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[2] = icm_out_arb_enable;
            assign bank[0].port_read[2] = icm_out_arb_read;
            assign bank[0].port_write[2] = icm_out_arb_write;
            assign bank[0].port_address[2] = icm_out_arb_address;
            assign bank[0].port_writedata[2] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[2] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[2];
            assign icm_out_rrp_data = bank[0].port_readdata[2];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[2];
            assign icm_out_wrp_ack = 'b0;
         end

      end

   end
   endgenerate

   generate
   begin:local_mem_system_aspace17
      logic local_icm_arb_request [1][2];
      logic local_icm_arb_enable [1][2];
      logic local_icm_arb_read [1][2];
      logic local_icm_arb_write [1][2];
      logic local_icm_arb_burstcount [1][2];
      logic [7:0] local_icm_arb_address [1][2];
      logic [7:0] local_icm_arb_writedata [1][2];
      logic local_icm_arb_byteenable [1][2];
      logic local_icm_arb_stall [1][2];
      logic local_icm_wrp_ack [1][2];
      logic local_icm_rrp_datavalid [1][2];
      logic [7:0] local_icm_rrp_data [1][2];

      for( __r = 0; __r < 1; __r = __r + 1 )
      begin:local_mem_group
         for( __s = 0; __s < 2; __s = __s + 1 )
         begin:master
            // INST avm_to_ic of acl_avm_to_ic
            acl_avm_to_ic
            #(
               .DATA_W(8),
               .WRITEDATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(32),
               .BYTEENA_W(1)
            )
            avm_to_ic
            (
               // AVM avm
               .avm_enable(local_avm_aspace17_enable[__r][__s]),
               .avm_read(local_avm_aspace17_read[__r][__s]),
               .avm_write(local_avm_aspace17_write[__r][__s]),
               .avm_burstcount(local_avm_aspace17_burstcount[__r][__s]),
               .avm_address(local_avm_aspace17_address[__r][__s]),
               .avm_writedata(local_avm_aspace17_writedata[__r][__s]),
               .avm_byteenable(local_avm_aspace17_byteenable[__r][__s]),
               .avm_waitrequest(local_avm_aspace17_waitrequest[__r][__s]),
               .avm_readdata(local_avm_aspace17_readdata[__r][__s]),
               .avm_readdatavalid(local_avm_aspace17_readdatavalid[__r][__s]),
               .avm_writeack(local_avm_aspace17_writeack[__r][__s]),
               // ICM ic
               .ic_arb_request(local_icm_arb_request[__r][__s]),
               .ic_arb_enable(local_icm_arb_enable[__r][__s]),
               .ic_arb_read(local_icm_arb_read[__r][__s]),
               .ic_arb_write(local_icm_arb_write[__r][__s]),
               .ic_arb_burstcount(local_icm_arb_burstcount[__r][__s]),
               .ic_arb_address(local_icm_arb_address[__r][__s]),
               .ic_arb_writedata(local_icm_arb_writedata[__r][__s]),
               .ic_arb_byteenable(local_icm_arb_byteenable[__r][__s]),
               .ic_arb_stall(local_icm_arb_stall[__r][__s]),
               .ic_wrp_ack(local_icm_wrp_ack[__r][__s]),
               .ic_rrp_datavalid(local_icm_rrp_datavalid[__r][__s]),
               .ic_rrp_data(local_icm_rrp_data[__r][__s])
            );

         end

         for( __s = 0; __s < 1; __s = __s + 1 )
         begin:bank
            logic port_enable [1:2];
            logic port_read [1:2];
            logic port_write [1:2];
            logic [7:0] port_address [1:2];
            logic [7:0] port_writedata [1:2];
            logic port_byteenable [1:2];
            logic port_waitrequest [1:2];
            logic [7:0] port_readdata [1:2];
            logic port_readdatavalid [1:2];

            // INST mem0 of acl_mem1x
            acl_mem1x
            #(
               .INTENDED_DEVICE_FAMILY("Cyclone V"),
               .DEPTH_WORDS(256),
               .WIDTH(8),
               .MEM_LATENCY(3),
               .ENABLED(0),
               .RDW_MODE("OLD_DATA"),
               .RAM_OPERATION_MODE("BIDIR_DUAL_PORT"),
               .PREFERRED_WIDTH(160),
               .RAM_BLOCK_TYPE("M10K")
            )
            mem0
            (
               .clk(clock),
               .resetn(resetn),
               // AVS avs_port1
               .avs_port1_enable(port_enable[1]),
               .avs_port1_read(port_read[1]),
               .avs_port1_write(port_write[1]),
               .avs_port1_address(port_address[1]),
               .avs_port1_writedata(port_writedata[1]),
               .avs_port1_byteenable(port_byteenable[1]),
               .avs_port1_waitrequest(port_waitrequest[1]),
               .avs_port1_readdata(port_readdata[1]),
               .avs_port1_readdatavalid(port_readdatavalid[1]),
               // AVS avs_port2
               .avs_port2_enable(port_enable[2]),
               .avs_port2_read(port_read[2]),
               .avs_port2_write(port_write[2]),
               .avs_port2_address(port_address[2]),
               .avs_port2_writedata(port_writedata[2]),
               .avs_port2_byteenable(port_byteenable[2]),
               .avs_port2_waitrequest(port_waitrequest[2]),
               .avs_port2_readdata(port_readdata[2]),
               .avs_port2_readdatavalid(port_readdatavalid[2])
            );

         end

         for( __s = 0; __s < 2; __s = __s + 1 )
         begin:router
            logic b_arb_request [1];
            logic b_arb_enable [1];
            logic b_arb_read [1];
            logic b_arb_write [1];
            logic b_arb_burstcount [1];
            logic [7:0] b_arb_address [1];
            logic [7:0] b_arb_writedata [1];
            logic b_arb_byteenable [1];
            logic b_arb_stall [1];
            logic b_wrp_ack [1];
            logic b_rrp_datavalid [1];
            logic [7:0] b_rrp_data [1];
            logic bank_select;

            // INST router of acl_ic_local_mem_router
            acl_ic_local_mem_router
            #(
               .DATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(8),
               .BYTEENA_W(1),
               .NUM_BANKS(1)
            )
            router
            (
               .clock(clock),
               .resetn(resetn),
               .bank_select(bank_select),
               // ICM m
               .m_arb_request(local_icm_arb_request[__r][__s]),
               .m_arb_enable(local_icm_arb_enable[__r][__s]),
               .m_arb_read(local_icm_arb_read[__r][__s]),
               .m_arb_write(local_icm_arb_write[__r][__s]),
               .m_arb_burstcount(local_icm_arb_burstcount[__r][__s]),
               .m_arb_address(local_icm_arb_address[__r][__s]),
               .m_arb_writedata(local_icm_arb_writedata[__r][__s]),
               .m_arb_byteenable(local_icm_arb_byteenable[__r][__s]),
               .m_arb_stall(local_icm_arb_stall[__r][__s]),
               .m_wrp_ack(local_icm_wrp_ack[__r][__s]),
               .m_rrp_datavalid(local_icm_rrp_datavalid[__r][__s]),
               .m_rrp_data(local_icm_rrp_data[__r][__s]),
               // ICM b
               .b_arb_request(b_arb_request),
               .b_arb_enable(b_arb_enable),
               .b_arb_read(b_arb_read),
               .b_arb_write(b_arb_write),
               .b_arb_burstcount(b_arb_burstcount),
               .b_arb_address(b_arb_address),
               .b_arb_writedata(b_arb_writedata),
               .b_arb_byteenable(b_arb_byteenable),
               .b_arb_stall(b_arb_stall),
               .b_wrp_ack(b_wrp_ack),
               .b_rrp_datavalid(b_rrp_datavalid),
               .b_rrp_data(b_rrp_data)
            );

            assign bank_select = 1'b1;
         end

         for( __s = 0; __s < 1; __s = __s + 1 )
         begin:port1bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[0].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[0].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[0].b_arb_read[0];
            assign icm_in_arb_write[0] = router[0].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[0].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[0].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[0].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[0].b_arb_byteenable[0];
            assign router[0].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[0].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[0].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[0].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_6
            conv_pipe_system_interconnect_6 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[1] = icm_out_arb_enable;
            assign bank[0].port_read[1] = icm_out_arb_read;
            assign bank[0].port_write[1] = icm_out_arb_write;
            assign bank[0].port_address[1] = icm_out_arb_address;
            assign bank[0].port_writedata[1] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[1] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[1];
            assign icm_out_rrp_data = bank[0].port_readdata[1];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[1];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __s = 0; __s < 1; __s = __s + 1 )
         begin:port2bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[1].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[1].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[1].b_arb_read[0];
            assign icm_in_arb_write[0] = router[1].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[1].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[1].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[1].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[1].b_arb_byteenable[0];
            assign router[1].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[1].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[1].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[1].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_7
            conv_pipe_system_interconnect_7 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[2] = icm_out_arb_enable;
            assign bank[0].port_read[2] = icm_out_arb_read;
            assign bank[0].port_write[2] = icm_out_arb_write;
            assign bank[0].port_address[2] = icm_out_arb_address;
            assign bank[0].port_writedata[2] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[2] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[2];
            assign icm_out_rrp_data = bank[0].port_readdata[2];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[2];
            assign icm_out_wrp_ack = 'b0;
         end

      end

   end
   endgenerate

   generate
   begin:local_mem_system_aspace18
      logic local_icm_arb_request [1][2];
      logic local_icm_arb_enable [1][2];
      logic local_icm_arb_read [1][2];
      logic local_icm_arb_write [1][2];
      logic local_icm_arb_burstcount [1][2];
      logic [7:0] local_icm_arb_address [1][2];
      logic [7:0] local_icm_arb_writedata [1][2];
      logic local_icm_arb_byteenable [1][2];
      logic local_icm_arb_stall [1][2];
      logic local_icm_wrp_ack [1][2];
      logic local_icm_rrp_datavalid [1][2];
      logic [7:0] local_icm_rrp_data [1][2];

      for( __s = 0; __s < 1; __s = __s + 1 )
      begin:local_mem_group
         for( __t = 0; __t < 2; __t = __t + 1 )
         begin:master
            // INST avm_to_ic of acl_avm_to_ic
            acl_avm_to_ic
            #(
               .DATA_W(8),
               .WRITEDATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(32),
               .BYTEENA_W(1)
            )
            avm_to_ic
            (
               // AVM avm
               .avm_enable(local_avm_aspace18_enable[__s][__t]),
               .avm_read(local_avm_aspace18_read[__s][__t]),
               .avm_write(local_avm_aspace18_write[__s][__t]),
               .avm_burstcount(local_avm_aspace18_burstcount[__s][__t]),
               .avm_address(local_avm_aspace18_address[__s][__t]),
               .avm_writedata(local_avm_aspace18_writedata[__s][__t]),
               .avm_byteenable(local_avm_aspace18_byteenable[__s][__t]),
               .avm_waitrequest(local_avm_aspace18_waitrequest[__s][__t]),
               .avm_readdata(local_avm_aspace18_readdata[__s][__t]),
               .avm_readdatavalid(local_avm_aspace18_readdatavalid[__s][__t]),
               .avm_writeack(local_avm_aspace18_writeack[__s][__t]),
               // ICM ic
               .ic_arb_request(local_icm_arb_request[__s][__t]),
               .ic_arb_enable(local_icm_arb_enable[__s][__t]),
               .ic_arb_read(local_icm_arb_read[__s][__t]),
               .ic_arb_write(local_icm_arb_write[__s][__t]),
               .ic_arb_burstcount(local_icm_arb_burstcount[__s][__t]),
               .ic_arb_address(local_icm_arb_address[__s][__t]),
               .ic_arb_writedata(local_icm_arb_writedata[__s][__t]),
               .ic_arb_byteenable(local_icm_arb_byteenable[__s][__t]),
               .ic_arb_stall(local_icm_arb_stall[__s][__t]),
               .ic_wrp_ack(local_icm_wrp_ack[__s][__t]),
               .ic_rrp_datavalid(local_icm_rrp_datavalid[__s][__t]),
               .ic_rrp_data(local_icm_rrp_data[__s][__t])
            );

         end

         for( __t = 0; __t < 1; __t = __t + 1 )
         begin:bank
            logic port_enable [1:2];
            logic port_read [1:2];
            logic port_write [1:2];
            logic [7:0] port_address [1:2];
            logic [7:0] port_writedata [1:2];
            logic port_byteenable [1:2];
            logic port_waitrequest [1:2];
            logic [7:0] port_readdata [1:2];
            logic port_readdatavalid [1:2];

            // INST mem0 of acl_mem1x
            acl_mem1x
            #(
               .INTENDED_DEVICE_FAMILY("Cyclone V"),
               .DEPTH_WORDS(256),
               .WIDTH(8),
               .MEM_LATENCY(3),
               .ENABLED(0),
               .RDW_MODE("OLD_DATA"),
               .RAM_OPERATION_MODE("BIDIR_DUAL_PORT"),
               .PREFERRED_WIDTH(160),
               .RAM_BLOCK_TYPE("M10K")
            )
            mem0
            (
               .clk(clock),
               .resetn(resetn),
               // AVS avs_port1
               .avs_port1_enable(port_enable[1]),
               .avs_port1_read(port_read[1]),
               .avs_port1_write(port_write[1]),
               .avs_port1_address(port_address[1]),
               .avs_port1_writedata(port_writedata[1]),
               .avs_port1_byteenable(port_byteenable[1]),
               .avs_port1_waitrequest(port_waitrequest[1]),
               .avs_port1_readdata(port_readdata[1]),
               .avs_port1_readdatavalid(port_readdatavalid[1]),
               // AVS avs_port2
               .avs_port2_enable(port_enable[2]),
               .avs_port2_read(port_read[2]),
               .avs_port2_write(port_write[2]),
               .avs_port2_address(port_address[2]),
               .avs_port2_writedata(port_writedata[2]),
               .avs_port2_byteenable(port_byteenable[2]),
               .avs_port2_waitrequest(port_waitrequest[2]),
               .avs_port2_readdata(port_readdata[2]),
               .avs_port2_readdatavalid(port_readdatavalid[2])
            );

         end

         for( __t = 0; __t < 2; __t = __t + 1 )
         begin:router
            logic b_arb_request [1];
            logic b_arb_enable [1];
            logic b_arb_read [1];
            logic b_arb_write [1];
            logic b_arb_burstcount [1];
            logic [7:0] b_arb_address [1];
            logic [7:0] b_arb_writedata [1];
            logic b_arb_byteenable [1];
            logic b_arb_stall [1];
            logic b_wrp_ack [1];
            logic b_rrp_datavalid [1];
            logic [7:0] b_rrp_data [1];
            logic bank_select;

            // INST router of acl_ic_local_mem_router
            acl_ic_local_mem_router
            #(
               .DATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(8),
               .BYTEENA_W(1),
               .NUM_BANKS(1)
            )
            router
            (
               .clock(clock),
               .resetn(resetn),
               .bank_select(bank_select),
               // ICM m
               .m_arb_request(local_icm_arb_request[__s][__t]),
               .m_arb_enable(local_icm_arb_enable[__s][__t]),
               .m_arb_read(local_icm_arb_read[__s][__t]),
               .m_arb_write(local_icm_arb_write[__s][__t]),
               .m_arb_burstcount(local_icm_arb_burstcount[__s][__t]),
               .m_arb_address(local_icm_arb_address[__s][__t]),
               .m_arb_writedata(local_icm_arb_writedata[__s][__t]),
               .m_arb_byteenable(local_icm_arb_byteenable[__s][__t]),
               .m_arb_stall(local_icm_arb_stall[__s][__t]),
               .m_wrp_ack(local_icm_wrp_ack[__s][__t]),
               .m_rrp_datavalid(local_icm_rrp_datavalid[__s][__t]),
               .m_rrp_data(local_icm_rrp_data[__s][__t]),
               // ICM b
               .b_arb_request(b_arb_request),
               .b_arb_enable(b_arb_enable),
               .b_arb_read(b_arb_read),
               .b_arb_write(b_arb_write),
               .b_arb_burstcount(b_arb_burstcount),
               .b_arb_address(b_arb_address),
               .b_arb_writedata(b_arb_writedata),
               .b_arb_byteenable(b_arb_byteenable),
               .b_arb_stall(b_arb_stall),
               .b_wrp_ack(b_wrp_ack),
               .b_rrp_datavalid(b_rrp_datavalid),
               .b_rrp_data(b_rrp_data)
            );

            assign bank_select = 1'b1;
         end

         for( __t = 0; __t < 1; __t = __t + 1 )
         begin:port1bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[0].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[0].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[0].b_arb_read[0];
            assign icm_in_arb_write[0] = router[0].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[0].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[0].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[0].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[0].b_arb_byteenable[0];
            assign router[0].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[0].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[0].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[0].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_6
            conv_pipe_system_interconnect_6 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[1] = icm_out_arb_enable;
            assign bank[0].port_read[1] = icm_out_arb_read;
            assign bank[0].port_write[1] = icm_out_arb_write;
            assign bank[0].port_address[1] = icm_out_arb_address;
            assign bank[0].port_writedata[1] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[1] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[1];
            assign icm_out_rrp_data = bank[0].port_readdata[1];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[1];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __t = 0; __t < 1; __t = __t + 1 )
         begin:port2bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[1].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[1].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[1].b_arb_read[0];
            assign icm_in_arb_write[0] = router[1].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[1].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[1].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[1].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[1].b_arb_byteenable[0];
            assign router[1].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[1].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[1].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[1].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_7
            conv_pipe_system_interconnect_7 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[2] = icm_out_arb_enable;
            assign bank[0].port_read[2] = icm_out_arb_read;
            assign bank[0].port_write[2] = icm_out_arb_write;
            assign bank[0].port_address[2] = icm_out_arb_address;
            assign bank[0].port_writedata[2] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[2] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[2];
            assign icm_out_rrp_data = bank[0].port_readdata[2];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[2];
            assign icm_out_wrp_ack = 'b0;
         end

      end

   end
   endgenerate

   generate
   begin:local_mem_system_aspace19
      logic local_icm_arb_request [1][2];
      logic local_icm_arb_enable [1][2];
      logic local_icm_arb_read [1][2];
      logic local_icm_arb_write [1][2];
      logic local_icm_arb_burstcount [1][2];
      logic [7:0] local_icm_arb_address [1][2];
      logic [7:0] local_icm_arb_writedata [1][2];
      logic local_icm_arb_byteenable [1][2];
      logic local_icm_arb_stall [1][2];
      logic local_icm_wrp_ack [1][2];
      logic local_icm_rrp_datavalid [1][2];
      logic [7:0] local_icm_rrp_data [1][2];

      for( __t = 0; __t < 1; __t = __t + 1 )
      begin:local_mem_group
         for( __u = 0; __u < 2; __u = __u + 1 )
         begin:master
            // INST avm_to_ic of acl_avm_to_ic
            acl_avm_to_ic
            #(
               .DATA_W(8),
               .WRITEDATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(32),
               .BYTEENA_W(1)
            )
            avm_to_ic
            (
               // AVM avm
               .avm_enable(local_avm_aspace19_enable[__t][__u]),
               .avm_read(local_avm_aspace19_read[__t][__u]),
               .avm_write(local_avm_aspace19_write[__t][__u]),
               .avm_burstcount(local_avm_aspace19_burstcount[__t][__u]),
               .avm_address(local_avm_aspace19_address[__t][__u]),
               .avm_writedata(local_avm_aspace19_writedata[__t][__u]),
               .avm_byteenable(local_avm_aspace19_byteenable[__t][__u]),
               .avm_waitrequest(local_avm_aspace19_waitrequest[__t][__u]),
               .avm_readdata(local_avm_aspace19_readdata[__t][__u]),
               .avm_readdatavalid(local_avm_aspace19_readdatavalid[__t][__u]),
               .avm_writeack(local_avm_aspace19_writeack[__t][__u]),
               // ICM ic
               .ic_arb_request(local_icm_arb_request[__t][__u]),
               .ic_arb_enable(local_icm_arb_enable[__t][__u]),
               .ic_arb_read(local_icm_arb_read[__t][__u]),
               .ic_arb_write(local_icm_arb_write[__t][__u]),
               .ic_arb_burstcount(local_icm_arb_burstcount[__t][__u]),
               .ic_arb_address(local_icm_arb_address[__t][__u]),
               .ic_arb_writedata(local_icm_arb_writedata[__t][__u]),
               .ic_arb_byteenable(local_icm_arb_byteenable[__t][__u]),
               .ic_arb_stall(local_icm_arb_stall[__t][__u]),
               .ic_wrp_ack(local_icm_wrp_ack[__t][__u]),
               .ic_rrp_datavalid(local_icm_rrp_datavalid[__t][__u]),
               .ic_rrp_data(local_icm_rrp_data[__t][__u])
            );

         end

         for( __u = 0; __u < 1; __u = __u + 1 )
         begin:bank
            logic port_enable [1:2];
            logic port_read [1:2];
            logic port_write [1:2];
            logic [7:0] port_address [1:2];
            logic [7:0] port_writedata [1:2];
            logic port_byteenable [1:2];
            logic port_waitrequest [1:2];
            logic [7:0] port_readdata [1:2];
            logic port_readdatavalid [1:2];

            // INST mem0 of acl_mem1x
            acl_mem1x
            #(
               .INTENDED_DEVICE_FAMILY("Cyclone V"),
               .DEPTH_WORDS(256),
               .WIDTH(8),
               .MEM_LATENCY(3),
               .ENABLED(0),
               .RDW_MODE("OLD_DATA"),
               .RAM_OPERATION_MODE("BIDIR_DUAL_PORT"),
               .PREFERRED_WIDTH(160),
               .RAM_BLOCK_TYPE("M10K")
            )
            mem0
            (
               .clk(clock),
               .resetn(resetn),
               // AVS avs_port1
               .avs_port1_enable(port_enable[1]),
               .avs_port1_read(port_read[1]),
               .avs_port1_write(port_write[1]),
               .avs_port1_address(port_address[1]),
               .avs_port1_writedata(port_writedata[1]),
               .avs_port1_byteenable(port_byteenable[1]),
               .avs_port1_waitrequest(port_waitrequest[1]),
               .avs_port1_readdata(port_readdata[1]),
               .avs_port1_readdatavalid(port_readdatavalid[1]),
               // AVS avs_port2
               .avs_port2_enable(port_enable[2]),
               .avs_port2_read(port_read[2]),
               .avs_port2_write(port_write[2]),
               .avs_port2_address(port_address[2]),
               .avs_port2_writedata(port_writedata[2]),
               .avs_port2_byteenable(port_byteenable[2]),
               .avs_port2_waitrequest(port_waitrequest[2]),
               .avs_port2_readdata(port_readdata[2]),
               .avs_port2_readdatavalid(port_readdatavalid[2])
            );

         end

         for( __u = 0; __u < 2; __u = __u + 1 )
         begin:router
            logic b_arb_request [1];
            logic b_arb_enable [1];
            logic b_arb_read [1];
            logic b_arb_write [1];
            logic b_arb_burstcount [1];
            logic [7:0] b_arb_address [1];
            logic [7:0] b_arb_writedata [1];
            logic b_arb_byteenable [1];
            logic b_arb_stall [1];
            logic b_wrp_ack [1];
            logic b_rrp_datavalid [1];
            logic [7:0] b_rrp_data [1];
            logic bank_select;

            // INST router of acl_ic_local_mem_router
            acl_ic_local_mem_router
            #(
               .DATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(8),
               .BYTEENA_W(1),
               .NUM_BANKS(1)
            )
            router
            (
               .clock(clock),
               .resetn(resetn),
               .bank_select(bank_select),
               // ICM m
               .m_arb_request(local_icm_arb_request[__t][__u]),
               .m_arb_enable(local_icm_arb_enable[__t][__u]),
               .m_arb_read(local_icm_arb_read[__t][__u]),
               .m_arb_write(local_icm_arb_write[__t][__u]),
               .m_arb_burstcount(local_icm_arb_burstcount[__t][__u]),
               .m_arb_address(local_icm_arb_address[__t][__u]),
               .m_arb_writedata(local_icm_arb_writedata[__t][__u]),
               .m_arb_byteenable(local_icm_arb_byteenable[__t][__u]),
               .m_arb_stall(local_icm_arb_stall[__t][__u]),
               .m_wrp_ack(local_icm_wrp_ack[__t][__u]),
               .m_rrp_datavalid(local_icm_rrp_datavalid[__t][__u]),
               .m_rrp_data(local_icm_rrp_data[__t][__u]),
               // ICM b
               .b_arb_request(b_arb_request),
               .b_arb_enable(b_arb_enable),
               .b_arb_read(b_arb_read),
               .b_arb_write(b_arb_write),
               .b_arb_burstcount(b_arb_burstcount),
               .b_arb_address(b_arb_address),
               .b_arb_writedata(b_arb_writedata),
               .b_arb_byteenable(b_arb_byteenable),
               .b_arb_stall(b_arb_stall),
               .b_wrp_ack(b_wrp_ack),
               .b_rrp_datavalid(b_rrp_datavalid),
               .b_rrp_data(b_rrp_data)
            );

            assign bank_select = 1'b1;
         end

         for( __u = 0; __u < 1; __u = __u + 1 )
         begin:port1bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[0].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[0].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[0].b_arb_read[0];
            assign icm_in_arb_write[0] = router[0].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[0].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[0].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[0].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[0].b_arb_byteenable[0];
            assign router[0].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[0].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[0].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[0].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_6
            conv_pipe_system_interconnect_6 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[1] = icm_out_arb_enable;
            assign bank[0].port_read[1] = icm_out_arb_read;
            assign bank[0].port_write[1] = icm_out_arb_write;
            assign bank[0].port_address[1] = icm_out_arb_address;
            assign bank[0].port_writedata[1] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[1] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[1];
            assign icm_out_rrp_data = bank[0].port_readdata[1];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[1];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __u = 0; __u < 1; __u = __u + 1 )
         begin:port2bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[1].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[1].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[1].b_arb_read[0];
            assign icm_in_arb_write[0] = router[1].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[1].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[1].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[1].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[1].b_arb_byteenable[0];
            assign router[1].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[1].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[1].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[1].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_7
            conv_pipe_system_interconnect_7 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[2] = icm_out_arb_enable;
            assign bank[0].port_read[2] = icm_out_arb_read;
            assign bank[0].port_write[2] = icm_out_arb_write;
            assign bank[0].port_address[2] = icm_out_arb_address;
            assign bank[0].port_writedata[2] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[2] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[2];
            assign icm_out_rrp_data = bank[0].port_readdata[2];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[2];
            assign icm_out_wrp_ack = 'b0;
         end

      end

   end
   endgenerate

   generate
   begin:local_mem_system_aspace20
      logic local_icm_arb_request [1][2];
      logic local_icm_arb_enable [1][2];
      logic local_icm_arb_read [1][2];
      logic local_icm_arb_write [1][2];
      logic local_icm_arb_burstcount [1][2];
      logic [7:0] local_icm_arb_address [1][2];
      logic [7:0] local_icm_arb_writedata [1][2];
      logic local_icm_arb_byteenable [1][2];
      logic local_icm_arb_stall [1][2];
      logic local_icm_wrp_ack [1][2];
      logic local_icm_rrp_datavalid [1][2];
      logic [7:0] local_icm_rrp_data [1][2];

      for( __u = 0; __u < 1; __u = __u + 1 )
      begin:local_mem_group
         for( __v = 0; __v < 2; __v = __v + 1 )
         begin:master
            // INST avm_to_ic of acl_avm_to_ic
            acl_avm_to_ic
            #(
               .DATA_W(8),
               .WRITEDATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(32),
               .BYTEENA_W(1)
            )
            avm_to_ic
            (
               // AVM avm
               .avm_enable(local_avm_aspace20_enable[__u][__v]),
               .avm_read(local_avm_aspace20_read[__u][__v]),
               .avm_write(local_avm_aspace20_write[__u][__v]),
               .avm_burstcount(local_avm_aspace20_burstcount[__u][__v]),
               .avm_address(local_avm_aspace20_address[__u][__v]),
               .avm_writedata(local_avm_aspace20_writedata[__u][__v]),
               .avm_byteenable(local_avm_aspace20_byteenable[__u][__v]),
               .avm_waitrequest(local_avm_aspace20_waitrequest[__u][__v]),
               .avm_readdata(local_avm_aspace20_readdata[__u][__v]),
               .avm_readdatavalid(local_avm_aspace20_readdatavalid[__u][__v]),
               .avm_writeack(local_avm_aspace20_writeack[__u][__v]),
               // ICM ic
               .ic_arb_request(local_icm_arb_request[__u][__v]),
               .ic_arb_enable(local_icm_arb_enable[__u][__v]),
               .ic_arb_read(local_icm_arb_read[__u][__v]),
               .ic_arb_write(local_icm_arb_write[__u][__v]),
               .ic_arb_burstcount(local_icm_arb_burstcount[__u][__v]),
               .ic_arb_address(local_icm_arb_address[__u][__v]),
               .ic_arb_writedata(local_icm_arb_writedata[__u][__v]),
               .ic_arb_byteenable(local_icm_arb_byteenable[__u][__v]),
               .ic_arb_stall(local_icm_arb_stall[__u][__v]),
               .ic_wrp_ack(local_icm_wrp_ack[__u][__v]),
               .ic_rrp_datavalid(local_icm_rrp_datavalid[__u][__v]),
               .ic_rrp_data(local_icm_rrp_data[__u][__v])
            );

         end

         for( __v = 0; __v < 1; __v = __v + 1 )
         begin:bank
            logic port_enable [1:2];
            logic port_read [1:2];
            logic port_write [1:2];
            logic [7:0] port_address [1:2];
            logic [7:0] port_writedata [1:2];
            logic port_byteenable [1:2];
            logic port_waitrequest [1:2];
            logic [7:0] port_readdata [1:2];
            logic port_readdatavalid [1:2];

            // INST mem0 of acl_mem1x
            acl_mem1x
            #(
               .INTENDED_DEVICE_FAMILY("Cyclone V"),
               .DEPTH_WORDS(256),
               .WIDTH(8),
               .MEM_LATENCY(3),
               .ENABLED(0),
               .RDW_MODE("OLD_DATA"),
               .RAM_OPERATION_MODE("BIDIR_DUAL_PORT"),
               .PREFERRED_WIDTH(160),
               .RAM_BLOCK_TYPE("M10K")
            )
            mem0
            (
               .clk(clock),
               .resetn(resetn),
               // AVS avs_port1
               .avs_port1_enable(port_enable[1]),
               .avs_port1_read(port_read[1]),
               .avs_port1_write(port_write[1]),
               .avs_port1_address(port_address[1]),
               .avs_port1_writedata(port_writedata[1]),
               .avs_port1_byteenable(port_byteenable[1]),
               .avs_port1_waitrequest(port_waitrequest[1]),
               .avs_port1_readdata(port_readdata[1]),
               .avs_port1_readdatavalid(port_readdatavalid[1]),
               // AVS avs_port2
               .avs_port2_enable(port_enable[2]),
               .avs_port2_read(port_read[2]),
               .avs_port2_write(port_write[2]),
               .avs_port2_address(port_address[2]),
               .avs_port2_writedata(port_writedata[2]),
               .avs_port2_byteenable(port_byteenable[2]),
               .avs_port2_waitrequest(port_waitrequest[2]),
               .avs_port2_readdata(port_readdata[2]),
               .avs_port2_readdatavalid(port_readdatavalid[2])
            );

         end

         for( __v = 0; __v < 2; __v = __v + 1 )
         begin:router
            logic b_arb_request [1];
            logic b_arb_enable [1];
            logic b_arb_read [1];
            logic b_arb_write [1];
            logic b_arb_burstcount [1];
            logic [7:0] b_arb_address [1];
            logic [7:0] b_arb_writedata [1];
            logic b_arb_byteenable [1];
            logic b_arb_stall [1];
            logic b_wrp_ack [1];
            logic b_rrp_datavalid [1];
            logic [7:0] b_rrp_data [1];
            logic bank_select;

            // INST router of acl_ic_local_mem_router
            acl_ic_local_mem_router
            #(
               .DATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(8),
               .BYTEENA_W(1),
               .NUM_BANKS(1)
            )
            router
            (
               .clock(clock),
               .resetn(resetn),
               .bank_select(bank_select),
               // ICM m
               .m_arb_request(local_icm_arb_request[__u][__v]),
               .m_arb_enable(local_icm_arb_enable[__u][__v]),
               .m_arb_read(local_icm_arb_read[__u][__v]),
               .m_arb_write(local_icm_arb_write[__u][__v]),
               .m_arb_burstcount(local_icm_arb_burstcount[__u][__v]),
               .m_arb_address(local_icm_arb_address[__u][__v]),
               .m_arb_writedata(local_icm_arb_writedata[__u][__v]),
               .m_arb_byteenable(local_icm_arb_byteenable[__u][__v]),
               .m_arb_stall(local_icm_arb_stall[__u][__v]),
               .m_wrp_ack(local_icm_wrp_ack[__u][__v]),
               .m_rrp_datavalid(local_icm_rrp_datavalid[__u][__v]),
               .m_rrp_data(local_icm_rrp_data[__u][__v]),
               // ICM b
               .b_arb_request(b_arb_request),
               .b_arb_enable(b_arb_enable),
               .b_arb_read(b_arb_read),
               .b_arb_write(b_arb_write),
               .b_arb_burstcount(b_arb_burstcount),
               .b_arb_address(b_arb_address),
               .b_arb_writedata(b_arb_writedata),
               .b_arb_byteenable(b_arb_byteenable),
               .b_arb_stall(b_arb_stall),
               .b_wrp_ack(b_wrp_ack),
               .b_rrp_datavalid(b_rrp_datavalid),
               .b_rrp_data(b_rrp_data)
            );

            assign bank_select = 1'b1;
         end

         for( __v = 0; __v < 1; __v = __v + 1 )
         begin:port1bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[0].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[0].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[0].b_arb_read[0];
            assign icm_in_arb_write[0] = router[0].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[0].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[0].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[0].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[0].b_arb_byteenable[0];
            assign router[0].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[0].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[0].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[0].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_6
            conv_pipe_system_interconnect_6 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[1] = icm_out_arb_enable;
            assign bank[0].port_read[1] = icm_out_arb_read;
            assign bank[0].port_write[1] = icm_out_arb_write;
            assign bank[0].port_address[1] = icm_out_arb_address;
            assign bank[0].port_writedata[1] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[1] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[1];
            assign icm_out_rrp_data = bank[0].port_readdata[1];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[1];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __v = 0; __v < 1; __v = __v + 1 )
         begin:port2bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[1].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[1].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[1].b_arb_read[0];
            assign icm_in_arb_write[0] = router[1].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[1].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[1].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[1].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[1].b_arb_byteenable[0];
            assign router[1].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[1].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[1].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[1].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_7
            conv_pipe_system_interconnect_7 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[2] = icm_out_arb_enable;
            assign bank[0].port_read[2] = icm_out_arb_read;
            assign bank[0].port_write[2] = icm_out_arb_write;
            assign bank[0].port_address[2] = icm_out_arb_address;
            assign bank[0].port_writedata[2] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[2] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[2];
            assign icm_out_rrp_data = bank[0].port_readdata[2];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[2];
            assign icm_out_wrp_ack = 'b0;
         end

      end

   end
   endgenerate

   generate
   begin:local_mem_system_aspace21
      logic local_icm_arb_request [1][2];
      logic local_icm_arb_enable [1][2];
      logic local_icm_arb_read [1][2];
      logic local_icm_arb_write [1][2];
      logic local_icm_arb_burstcount [1][2];
      logic [7:0] local_icm_arb_address [1][2];
      logic [7:0] local_icm_arb_writedata [1][2];
      logic local_icm_arb_byteenable [1][2];
      logic local_icm_arb_stall [1][2];
      logic local_icm_wrp_ack [1][2];
      logic local_icm_rrp_datavalid [1][2];
      logic [7:0] local_icm_rrp_data [1][2];

      for( __v = 0; __v < 1; __v = __v + 1 )
      begin:local_mem_group
         for( __w = 0; __w < 2; __w = __w + 1 )
         begin:master
            // INST avm_to_ic of acl_avm_to_ic
            acl_avm_to_ic
            #(
               .DATA_W(8),
               .WRITEDATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(32),
               .BYTEENA_W(1)
            )
            avm_to_ic
            (
               // AVM avm
               .avm_enable(local_avm_aspace21_enable[__v][__w]),
               .avm_read(local_avm_aspace21_read[__v][__w]),
               .avm_write(local_avm_aspace21_write[__v][__w]),
               .avm_burstcount(local_avm_aspace21_burstcount[__v][__w]),
               .avm_address(local_avm_aspace21_address[__v][__w]),
               .avm_writedata(local_avm_aspace21_writedata[__v][__w]),
               .avm_byteenable(local_avm_aspace21_byteenable[__v][__w]),
               .avm_waitrequest(local_avm_aspace21_waitrequest[__v][__w]),
               .avm_readdata(local_avm_aspace21_readdata[__v][__w]),
               .avm_readdatavalid(local_avm_aspace21_readdatavalid[__v][__w]),
               .avm_writeack(local_avm_aspace21_writeack[__v][__w]),
               // ICM ic
               .ic_arb_request(local_icm_arb_request[__v][__w]),
               .ic_arb_enable(local_icm_arb_enable[__v][__w]),
               .ic_arb_read(local_icm_arb_read[__v][__w]),
               .ic_arb_write(local_icm_arb_write[__v][__w]),
               .ic_arb_burstcount(local_icm_arb_burstcount[__v][__w]),
               .ic_arb_address(local_icm_arb_address[__v][__w]),
               .ic_arb_writedata(local_icm_arb_writedata[__v][__w]),
               .ic_arb_byteenable(local_icm_arb_byteenable[__v][__w]),
               .ic_arb_stall(local_icm_arb_stall[__v][__w]),
               .ic_wrp_ack(local_icm_wrp_ack[__v][__w]),
               .ic_rrp_datavalid(local_icm_rrp_datavalid[__v][__w]),
               .ic_rrp_data(local_icm_rrp_data[__v][__w])
            );

         end

         for( __w = 0; __w < 1; __w = __w + 1 )
         begin:bank
            logic port_enable [1:2];
            logic port_read [1:2];
            logic port_write [1:2];
            logic [7:0] port_address [1:2];
            logic [7:0] port_writedata [1:2];
            logic port_byteenable [1:2];
            logic port_waitrequest [1:2];
            logic [7:0] port_readdata [1:2];
            logic port_readdatavalid [1:2];

            // INST mem0 of acl_mem1x
            acl_mem1x
            #(
               .INTENDED_DEVICE_FAMILY("Cyclone V"),
               .DEPTH_WORDS(256),
               .WIDTH(8),
               .MEM_LATENCY(3),
               .ENABLED(0),
               .RDW_MODE("OLD_DATA"),
               .RAM_OPERATION_MODE("BIDIR_DUAL_PORT"),
               .PREFERRED_WIDTH(160),
               .RAM_BLOCK_TYPE("M10K")
            )
            mem0
            (
               .clk(clock),
               .resetn(resetn),
               // AVS avs_port1
               .avs_port1_enable(port_enable[1]),
               .avs_port1_read(port_read[1]),
               .avs_port1_write(port_write[1]),
               .avs_port1_address(port_address[1]),
               .avs_port1_writedata(port_writedata[1]),
               .avs_port1_byteenable(port_byteenable[1]),
               .avs_port1_waitrequest(port_waitrequest[1]),
               .avs_port1_readdata(port_readdata[1]),
               .avs_port1_readdatavalid(port_readdatavalid[1]),
               // AVS avs_port2
               .avs_port2_enable(port_enable[2]),
               .avs_port2_read(port_read[2]),
               .avs_port2_write(port_write[2]),
               .avs_port2_address(port_address[2]),
               .avs_port2_writedata(port_writedata[2]),
               .avs_port2_byteenable(port_byteenable[2]),
               .avs_port2_waitrequest(port_waitrequest[2]),
               .avs_port2_readdata(port_readdata[2]),
               .avs_port2_readdatavalid(port_readdatavalid[2])
            );

         end

         for( __w = 0; __w < 2; __w = __w + 1 )
         begin:router
            logic b_arb_request [1];
            logic b_arb_enable [1];
            logic b_arb_read [1];
            logic b_arb_write [1];
            logic b_arb_burstcount [1];
            logic [7:0] b_arb_address [1];
            logic [7:0] b_arb_writedata [1];
            logic b_arb_byteenable [1];
            logic b_arb_stall [1];
            logic b_wrp_ack [1];
            logic b_rrp_datavalid [1];
            logic [7:0] b_rrp_data [1];
            logic bank_select;

            // INST router of acl_ic_local_mem_router
            acl_ic_local_mem_router
            #(
               .DATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(8),
               .BYTEENA_W(1),
               .NUM_BANKS(1)
            )
            router
            (
               .clock(clock),
               .resetn(resetn),
               .bank_select(bank_select),
               // ICM m
               .m_arb_request(local_icm_arb_request[__v][__w]),
               .m_arb_enable(local_icm_arb_enable[__v][__w]),
               .m_arb_read(local_icm_arb_read[__v][__w]),
               .m_arb_write(local_icm_arb_write[__v][__w]),
               .m_arb_burstcount(local_icm_arb_burstcount[__v][__w]),
               .m_arb_address(local_icm_arb_address[__v][__w]),
               .m_arb_writedata(local_icm_arb_writedata[__v][__w]),
               .m_arb_byteenable(local_icm_arb_byteenable[__v][__w]),
               .m_arb_stall(local_icm_arb_stall[__v][__w]),
               .m_wrp_ack(local_icm_wrp_ack[__v][__w]),
               .m_rrp_datavalid(local_icm_rrp_datavalid[__v][__w]),
               .m_rrp_data(local_icm_rrp_data[__v][__w]),
               // ICM b
               .b_arb_request(b_arb_request),
               .b_arb_enable(b_arb_enable),
               .b_arb_read(b_arb_read),
               .b_arb_write(b_arb_write),
               .b_arb_burstcount(b_arb_burstcount),
               .b_arb_address(b_arb_address),
               .b_arb_writedata(b_arb_writedata),
               .b_arb_byteenable(b_arb_byteenable),
               .b_arb_stall(b_arb_stall),
               .b_wrp_ack(b_wrp_ack),
               .b_rrp_datavalid(b_rrp_datavalid),
               .b_rrp_data(b_rrp_data)
            );

            assign bank_select = 1'b1;
         end

         for( __w = 0; __w < 1; __w = __w + 1 )
         begin:port1bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[0].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[0].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[0].b_arb_read[0];
            assign icm_in_arb_write[0] = router[0].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[0].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[0].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[0].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[0].b_arb_byteenable[0];
            assign router[0].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[0].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[0].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[0].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_6
            conv_pipe_system_interconnect_6 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[1] = icm_out_arb_enable;
            assign bank[0].port_read[1] = icm_out_arb_read;
            assign bank[0].port_write[1] = icm_out_arb_write;
            assign bank[0].port_address[1] = icm_out_arb_address;
            assign bank[0].port_writedata[1] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[1] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[1];
            assign icm_out_rrp_data = bank[0].port_readdata[1];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[1];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __w = 0; __w < 1; __w = __w + 1 )
         begin:port2bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[1].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[1].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[1].b_arb_read[0];
            assign icm_in_arb_write[0] = router[1].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[1].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[1].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[1].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[1].b_arb_byteenable[0];
            assign router[1].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[1].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[1].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[1].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_7
            conv_pipe_system_interconnect_7 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[2] = icm_out_arb_enable;
            assign bank[0].port_read[2] = icm_out_arb_read;
            assign bank[0].port_write[2] = icm_out_arb_write;
            assign bank[0].port_address[2] = icm_out_arb_address;
            assign bank[0].port_writedata[2] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[2] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[2];
            assign icm_out_rrp_data = bank[0].port_readdata[2];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[2];
            assign icm_out_wrp_ack = 'b0;
         end

      end

   end
   endgenerate

   generate
   begin:local_mem_system_aspace22
      logic local_icm_arb_request [1][2];
      logic local_icm_arb_enable [1][2];
      logic local_icm_arb_read [1][2];
      logic local_icm_arb_write [1][2];
      logic local_icm_arb_burstcount [1][2];
      logic [7:0] local_icm_arb_address [1][2];
      logic [7:0] local_icm_arb_writedata [1][2];
      logic local_icm_arb_byteenable [1][2];
      logic local_icm_arb_stall [1][2];
      logic local_icm_wrp_ack [1][2];
      logic local_icm_rrp_datavalid [1][2];
      logic [7:0] local_icm_rrp_data [1][2];

      for( __w = 0; __w < 1; __w = __w + 1 )
      begin:local_mem_group
         for( __x = 0; __x < 2; __x = __x + 1 )
         begin:master
            // INST avm_to_ic of acl_avm_to_ic
            acl_avm_to_ic
            #(
               .DATA_W(8),
               .WRITEDATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(32),
               .BYTEENA_W(1)
            )
            avm_to_ic
            (
               // AVM avm
               .avm_enable(local_avm_aspace22_enable[__w][__x]),
               .avm_read(local_avm_aspace22_read[__w][__x]),
               .avm_write(local_avm_aspace22_write[__w][__x]),
               .avm_burstcount(local_avm_aspace22_burstcount[__w][__x]),
               .avm_address(local_avm_aspace22_address[__w][__x]),
               .avm_writedata(local_avm_aspace22_writedata[__w][__x]),
               .avm_byteenable(local_avm_aspace22_byteenable[__w][__x]),
               .avm_waitrequest(local_avm_aspace22_waitrequest[__w][__x]),
               .avm_readdata(local_avm_aspace22_readdata[__w][__x]),
               .avm_readdatavalid(local_avm_aspace22_readdatavalid[__w][__x]),
               .avm_writeack(local_avm_aspace22_writeack[__w][__x]),
               // ICM ic
               .ic_arb_request(local_icm_arb_request[__w][__x]),
               .ic_arb_enable(local_icm_arb_enable[__w][__x]),
               .ic_arb_read(local_icm_arb_read[__w][__x]),
               .ic_arb_write(local_icm_arb_write[__w][__x]),
               .ic_arb_burstcount(local_icm_arb_burstcount[__w][__x]),
               .ic_arb_address(local_icm_arb_address[__w][__x]),
               .ic_arb_writedata(local_icm_arb_writedata[__w][__x]),
               .ic_arb_byteenable(local_icm_arb_byteenable[__w][__x]),
               .ic_arb_stall(local_icm_arb_stall[__w][__x]),
               .ic_wrp_ack(local_icm_wrp_ack[__w][__x]),
               .ic_rrp_datavalid(local_icm_rrp_datavalid[__w][__x]),
               .ic_rrp_data(local_icm_rrp_data[__w][__x])
            );

         end

         for( __x = 0; __x < 1; __x = __x + 1 )
         begin:bank
            logic port_enable [1:2];
            logic port_read [1:2];
            logic port_write [1:2];
            logic [7:0] port_address [1:2];
            logic [7:0] port_writedata [1:2];
            logic port_byteenable [1:2];
            logic port_waitrequest [1:2];
            logic [7:0] port_readdata [1:2];
            logic port_readdatavalid [1:2];

            // INST mem0 of acl_mem1x
            acl_mem1x
            #(
               .INTENDED_DEVICE_FAMILY("Cyclone V"),
               .DEPTH_WORDS(256),
               .WIDTH(8),
               .MEM_LATENCY(3),
               .ENABLED(0),
               .RDW_MODE("OLD_DATA"),
               .RAM_OPERATION_MODE("BIDIR_DUAL_PORT"),
               .PREFERRED_WIDTH(160),
               .RAM_BLOCK_TYPE("M10K")
            )
            mem0
            (
               .clk(clock),
               .resetn(resetn),
               // AVS avs_port1
               .avs_port1_enable(port_enable[1]),
               .avs_port1_read(port_read[1]),
               .avs_port1_write(port_write[1]),
               .avs_port1_address(port_address[1]),
               .avs_port1_writedata(port_writedata[1]),
               .avs_port1_byteenable(port_byteenable[1]),
               .avs_port1_waitrequest(port_waitrequest[1]),
               .avs_port1_readdata(port_readdata[1]),
               .avs_port1_readdatavalid(port_readdatavalid[1]),
               // AVS avs_port2
               .avs_port2_enable(port_enable[2]),
               .avs_port2_read(port_read[2]),
               .avs_port2_write(port_write[2]),
               .avs_port2_address(port_address[2]),
               .avs_port2_writedata(port_writedata[2]),
               .avs_port2_byteenable(port_byteenable[2]),
               .avs_port2_waitrequest(port_waitrequest[2]),
               .avs_port2_readdata(port_readdata[2]),
               .avs_port2_readdatavalid(port_readdatavalid[2])
            );

         end

         for( __x = 0; __x < 2; __x = __x + 1 )
         begin:router
            logic b_arb_request [1];
            logic b_arb_enable [1];
            logic b_arb_read [1];
            logic b_arb_write [1];
            logic b_arb_burstcount [1];
            logic [7:0] b_arb_address [1];
            logic [7:0] b_arb_writedata [1];
            logic b_arb_byteenable [1];
            logic b_arb_stall [1];
            logic b_wrp_ack [1];
            logic b_rrp_datavalid [1];
            logic [7:0] b_rrp_data [1];
            logic bank_select;

            // INST router of acl_ic_local_mem_router
            acl_ic_local_mem_router
            #(
               .DATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(8),
               .BYTEENA_W(1),
               .NUM_BANKS(1)
            )
            router
            (
               .clock(clock),
               .resetn(resetn),
               .bank_select(bank_select),
               // ICM m
               .m_arb_request(local_icm_arb_request[__w][__x]),
               .m_arb_enable(local_icm_arb_enable[__w][__x]),
               .m_arb_read(local_icm_arb_read[__w][__x]),
               .m_arb_write(local_icm_arb_write[__w][__x]),
               .m_arb_burstcount(local_icm_arb_burstcount[__w][__x]),
               .m_arb_address(local_icm_arb_address[__w][__x]),
               .m_arb_writedata(local_icm_arb_writedata[__w][__x]),
               .m_arb_byteenable(local_icm_arb_byteenable[__w][__x]),
               .m_arb_stall(local_icm_arb_stall[__w][__x]),
               .m_wrp_ack(local_icm_wrp_ack[__w][__x]),
               .m_rrp_datavalid(local_icm_rrp_datavalid[__w][__x]),
               .m_rrp_data(local_icm_rrp_data[__w][__x]),
               // ICM b
               .b_arb_request(b_arb_request),
               .b_arb_enable(b_arb_enable),
               .b_arb_read(b_arb_read),
               .b_arb_write(b_arb_write),
               .b_arb_burstcount(b_arb_burstcount),
               .b_arb_address(b_arb_address),
               .b_arb_writedata(b_arb_writedata),
               .b_arb_byteenable(b_arb_byteenable),
               .b_arb_stall(b_arb_stall),
               .b_wrp_ack(b_wrp_ack),
               .b_rrp_datavalid(b_rrp_datavalid),
               .b_rrp_data(b_rrp_data)
            );

            assign bank_select = 1'b1;
         end

         for( __x = 0; __x < 1; __x = __x + 1 )
         begin:port1bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[0].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[0].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[0].b_arb_read[0];
            assign icm_in_arb_write[0] = router[0].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[0].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[0].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[0].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[0].b_arb_byteenable[0];
            assign router[0].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[0].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[0].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[0].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_6
            conv_pipe_system_interconnect_6 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[1] = icm_out_arb_enable;
            assign bank[0].port_read[1] = icm_out_arb_read;
            assign bank[0].port_write[1] = icm_out_arb_write;
            assign bank[0].port_address[1] = icm_out_arb_address;
            assign bank[0].port_writedata[1] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[1] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[1];
            assign icm_out_rrp_data = bank[0].port_readdata[1];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[1];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __x = 0; __x < 1; __x = __x + 1 )
         begin:port2bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[1].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[1].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[1].b_arb_read[0];
            assign icm_in_arb_write[0] = router[1].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[1].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[1].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[1].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[1].b_arb_byteenable[0];
            assign router[1].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[1].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[1].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[1].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_7
            conv_pipe_system_interconnect_7 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[2] = icm_out_arb_enable;
            assign bank[0].port_read[2] = icm_out_arb_read;
            assign bank[0].port_write[2] = icm_out_arb_write;
            assign bank[0].port_address[2] = icm_out_arb_address;
            assign bank[0].port_writedata[2] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[2] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[2];
            assign icm_out_rrp_data = bank[0].port_readdata[2];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[2];
            assign icm_out_wrp_ack = 'b0;
         end

      end

   end
   endgenerate

   generate
   begin:local_mem_system_aspace23
      logic local_icm_arb_request [1][2];
      logic local_icm_arb_enable [1][2];
      logic local_icm_arb_read [1][2];
      logic local_icm_arb_write [1][2];
      logic local_icm_arb_burstcount [1][2];
      logic [7:0] local_icm_arb_address [1][2];
      logic [7:0] local_icm_arb_writedata [1][2];
      logic local_icm_arb_byteenable [1][2];
      logic local_icm_arb_stall [1][2];
      logic local_icm_wrp_ack [1][2];
      logic local_icm_rrp_datavalid [1][2];
      logic [7:0] local_icm_rrp_data [1][2];

      for( __x = 0; __x < 1; __x = __x + 1 )
      begin:local_mem_group
         for( __y = 0; __y < 2; __y = __y + 1 )
         begin:master
            // INST avm_to_ic of acl_avm_to_ic
            acl_avm_to_ic
            #(
               .DATA_W(8),
               .WRITEDATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(32),
               .BYTEENA_W(1)
            )
            avm_to_ic
            (
               // AVM avm
               .avm_enable(local_avm_aspace23_enable[__x][__y]),
               .avm_read(local_avm_aspace23_read[__x][__y]),
               .avm_write(local_avm_aspace23_write[__x][__y]),
               .avm_burstcount(local_avm_aspace23_burstcount[__x][__y]),
               .avm_address(local_avm_aspace23_address[__x][__y]),
               .avm_writedata(local_avm_aspace23_writedata[__x][__y]),
               .avm_byteenable(local_avm_aspace23_byteenable[__x][__y]),
               .avm_waitrequest(local_avm_aspace23_waitrequest[__x][__y]),
               .avm_readdata(local_avm_aspace23_readdata[__x][__y]),
               .avm_readdatavalid(local_avm_aspace23_readdatavalid[__x][__y]),
               .avm_writeack(local_avm_aspace23_writeack[__x][__y]),
               // ICM ic
               .ic_arb_request(local_icm_arb_request[__x][__y]),
               .ic_arb_enable(local_icm_arb_enable[__x][__y]),
               .ic_arb_read(local_icm_arb_read[__x][__y]),
               .ic_arb_write(local_icm_arb_write[__x][__y]),
               .ic_arb_burstcount(local_icm_arb_burstcount[__x][__y]),
               .ic_arb_address(local_icm_arb_address[__x][__y]),
               .ic_arb_writedata(local_icm_arb_writedata[__x][__y]),
               .ic_arb_byteenable(local_icm_arb_byteenable[__x][__y]),
               .ic_arb_stall(local_icm_arb_stall[__x][__y]),
               .ic_wrp_ack(local_icm_wrp_ack[__x][__y]),
               .ic_rrp_datavalid(local_icm_rrp_datavalid[__x][__y]),
               .ic_rrp_data(local_icm_rrp_data[__x][__y])
            );

         end

         for( __y = 0; __y < 1; __y = __y + 1 )
         begin:bank
            logic port_enable [1:2];
            logic port_read [1:2];
            logic port_write [1:2];
            logic [7:0] port_address [1:2];
            logic [7:0] port_writedata [1:2];
            logic port_byteenable [1:2];
            logic port_waitrequest [1:2];
            logic [7:0] port_readdata [1:2];
            logic port_readdatavalid [1:2];

            // INST mem0 of acl_mem1x
            acl_mem1x
            #(
               .INTENDED_DEVICE_FAMILY("Cyclone V"),
               .DEPTH_WORDS(256),
               .WIDTH(8),
               .MEM_LATENCY(3),
               .ENABLED(0),
               .RDW_MODE("OLD_DATA"),
               .RAM_OPERATION_MODE("BIDIR_DUAL_PORT"),
               .PREFERRED_WIDTH(160),
               .RAM_BLOCK_TYPE("M10K")
            )
            mem0
            (
               .clk(clock),
               .resetn(resetn),
               // AVS avs_port1
               .avs_port1_enable(port_enable[1]),
               .avs_port1_read(port_read[1]),
               .avs_port1_write(port_write[1]),
               .avs_port1_address(port_address[1]),
               .avs_port1_writedata(port_writedata[1]),
               .avs_port1_byteenable(port_byteenable[1]),
               .avs_port1_waitrequest(port_waitrequest[1]),
               .avs_port1_readdata(port_readdata[1]),
               .avs_port1_readdatavalid(port_readdatavalid[1]),
               // AVS avs_port2
               .avs_port2_enable(port_enable[2]),
               .avs_port2_read(port_read[2]),
               .avs_port2_write(port_write[2]),
               .avs_port2_address(port_address[2]),
               .avs_port2_writedata(port_writedata[2]),
               .avs_port2_byteenable(port_byteenable[2]),
               .avs_port2_waitrequest(port_waitrequest[2]),
               .avs_port2_readdata(port_readdata[2]),
               .avs_port2_readdatavalid(port_readdatavalid[2])
            );

         end

         for( __y = 0; __y < 2; __y = __y + 1 )
         begin:router
            logic b_arb_request [1];
            logic b_arb_enable [1];
            logic b_arb_read [1];
            logic b_arb_write [1];
            logic b_arb_burstcount [1];
            logic [7:0] b_arb_address [1];
            logic [7:0] b_arb_writedata [1];
            logic b_arb_byteenable [1];
            logic b_arb_stall [1];
            logic b_wrp_ack [1];
            logic b_rrp_datavalid [1];
            logic [7:0] b_rrp_data [1];
            logic bank_select;

            // INST router of acl_ic_local_mem_router
            acl_ic_local_mem_router
            #(
               .DATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(8),
               .BYTEENA_W(1),
               .NUM_BANKS(1)
            )
            router
            (
               .clock(clock),
               .resetn(resetn),
               .bank_select(bank_select),
               // ICM m
               .m_arb_request(local_icm_arb_request[__x][__y]),
               .m_arb_enable(local_icm_arb_enable[__x][__y]),
               .m_arb_read(local_icm_arb_read[__x][__y]),
               .m_arb_write(local_icm_arb_write[__x][__y]),
               .m_arb_burstcount(local_icm_arb_burstcount[__x][__y]),
               .m_arb_address(local_icm_arb_address[__x][__y]),
               .m_arb_writedata(local_icm_arb_writedata[__x][__y]),
               .m_arb_byteenable(local_icm_arb_byteenable[__x][__y]),
               .m_arb_stall(local_icm_arb_stall[__x][__y]),
               .m_wrp_ack(local_icm_wrp_ack[__x][__y]),
               .m_rrp_datavalid(local_icm_rrp_datavalid[__x][__y]),
               .m_rrp_data(local_icm_rrp_data[__x][__y]),
               // ICM b
               .b_arb_request(b_arb_request),
               .b_arb_enable(b_arb_enable),
               .b_arb_read(b_arb_read),
               .b_arb_write(b_arb_write),
               .b_arb_burstcount(b_arb_burstcount),
               .b_arb_address(b_arb_address),
               .b_arb_writedata(b_arb_writedata),
               .b_arb_byteenable(b_arb_byteenable),
               .b_arb_stall(b_arb_stall),
               .b_wrp_ack(b_wrp_ack),
               .b_rrp_datavalid(b_rrp_datavalid),
               .b_rrp_data(b_rrp_data)
            );

            assign bank_select = 1'b1;
         end

         for( __y = 0; __y < 1; __y = __y + 1 )
         begin:port1bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[0].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[0].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[0].b_arb_read[0];
            assign icm_in_arb_write[0] = router[0].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[0].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[0].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[0].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[0].b_arb_byteenable[0];
            assign router[0].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[0].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[0].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[0].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_6
            conv_pipe_system_interconnect_6 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[1] = icm_out_arb_enable;
            assign bank[0].port_read[1] = icm_out_arb_read;
            assign bank[0].port_write[1] = icm_out_arb_write;
            assign bank[0].port_address[1] = icm_out_arb_address;
            assign bank[0].port_writedata[1] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[1] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[1];
            assign icm_out_rrp_data = bank[0].port_readdata[1];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[1];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __y = 0; __y < 1; __y = __y + 1 )
         begin:port2bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[1].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[1].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[1].b_arb_read[0];
            assign icm_in_arb_write[0] = router[1].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[1].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[1].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[1].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[1].b_arb_byteenable[0];
            assign router[1].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[1].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[1].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[1].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_7
            conv_pipe_system_interconnect_7 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[2] = icm_out_arb_enable;
            assign bank[0].port_read[2] = icm_out_arb_read;
            assign bank[0].port_write[2] = icm_out_arb_write;
            assign bank[0].port_address[2] = icm_out_arb_address;
            assign bank[0].port_writedata[2] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[2] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[2];
            assign icm_out_rrp_data = bank[0].port_readdata[2];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[2];
            assign icm_out_wrp_ack = 'b0;
         end

      end

   end
   endgenerate

   generate
   begin:local_mem_system_aspace24
      logic local_icm_arb_request [1][2];
      logic local_icm_arb_enable [1][2];
      logic local_icm_arb_read [1][2];
      logic local_icm_arb_write [1][2];
      logic local_icm_arb_burstcount [1][2];
      logic [7:0] local_icm_arb_address [1][2];
      logic [7:0] local_icm_arb_writedata [1][2];
      logic local_icm_arb_byteenable [1][2];
      logic local_icm_arb_stall [1][2];
      logic local_icm_wrp_ack [1][2];
      logic local_icm_rrp_datavalid [1][2];
      logic [7:0] local_icm_rrp_data [1][2];

      for( __y = 0; __y < 1; __y = __y + 1 )
      begin:local_mem_group
         for( __z = 0; __z < 2; __z = __z + 1 )
         begin:master
            // INST avm_to_ic of acl_avm_to_ic
            acl_avm_to_ic
            #(
               .DATA_W(8),
               .WRITEDATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(32),
               .BYTEENA_W(1)
            )
            avm_to_ic
            (
               // AVM avm
               .avm_enable(local_avm_aspace24_enable[__y][__z]),
               .avm_read(local_avm_aspace24_read[__y][__z]),
               .avm_write(local_avm_aspace24_write[__y][__z]),
               .avm_burstcount(local_avm_aspace24_burstcount[__y][__z]),
               .avm_address(local_avm_aspace24_address[__y][__z]),
               .avm_writedata(local_avm_aspace24_writedata[__y][__z]),
               .avm_byteenable(local_avm_aspace24_byteenable[__y][__z]),
               .avm_waitrequest(local_avm_aspace24_waitrequest[__y][__z]),
               .avm_readdata(local_avm_aspace24_readdata[__y][__z]),
               .avm_readdatavalid(local_avm_aspace24_readdatavalid[__y][__z]),
               .avm_writeack(local_avm_aspace24_writeack[__y][__z]),
               // ICM ic
               .ic_arb_request(local_icm_arb_request[__y][__z]),
               .ic_arb_enable(local_icm_arb_enable[__y][__z]),
               .ic_arb_read(local_icm_arb_read[__y][__z]),
               .ic_arb_write(local_icm_arb_write[__y][__z]),
               .ic_arb_burstcount(local_icm_arb_burstcount[__y][__z]),
               .ic_arb_address(local_icm_arb_address[__y][__z]),
               .ic_arb_writedata(local_icm_arb_writedata[__y][__z]),
               .ic_arb_byteenable(local_icm_arb_byteenable[__y][__z]),
               .ic_arb_stall(local_icm_arb_stall[__y][__z]),
               .ic_wrp_ack(local_icm_wrp_ack[__y][__z]),
               .ic_rrp_datavalid(local_icm_rrp_datavalid[__y][__z]),
               .ic_rrp_data(local_icm_rrp_data[__y][__z])
            );

         end

         for( __z = 0; __z < 1; __z = __z + 1 )
         begin:bank
            logic port_enable [1:2];
            logic port_read [1:2];
            logic port_write [1:2];
            logic [7:0] port_address [1:2];
            logic [7:0] port_writedata [1:2];
            logic port_byteenable [1:2];
            logic port_waitrequest [1:2];
            logic [7:0] port_readdata [1:2];
            logic port_readdatavalid [1:2];

            // INST mem0 of acl_mem1x
            acl_mem1x
            #(
               .INTENDED_DEVICE_FAMILY("Cyclone V"),
               .DEPTH_WORDS(256),
               .WIDTH(8),
               .MEM_LATENCY(3),
               .ENABLED(0),
               .RDW_MODE("OLD_DATA"),
               .RAM_OPERATION_MODE("BIDIR_DUAL_PORT"),
               .PREFERRED_WIDTH(160),
               .RAM_BLOCK_TYPE("M10K")
            )
            mem0
            (
               .clk(clock),
               .resetn(resetn),
               // AVS avs_port1
               .avs_port1_enable(port_enable[1]),
               .avs_port1_read(port_read[1]),
               .avs_port1_write(port_write[1]),
               .avs_port1_address(port_address[1]),
               .avs_port1_writedata(port_writedata[1]),
               .avs_port1_byteenable(port_byteenable[1]),
               .avs_port1_waitrequest(port_waitrequest[1]),
               .avs_port1_readdata(port_readdata[1]),
               .avs_port1_readdatavalid(port_readdatavalid[1]),
               // AVS avs_port2
               .avs_port2_enable(port_enable[2]),
               .avs_port2_read(port_read[2]),
               .avs_port2_write(port_write[2]),
               .avs_port2_address(port_address[2]),
               .avs_port2_writedata(port_writedata[2]),
               .avs_port2_byteenable(port_byteenable[2]),
               .avs_port2_waitrequest(port_waitrequest[2]),
               .avs_port2_readdata(port_readdata[2]),
               .avs_port2_readdatavalid(port_readdatavalid[2])
            );

         end

         for( __z = 0; __z < 2; __z = __z + 1 )
         begin:router
            logic b_arb_request [1];
            logic b_arb_enable [1];
            logic b_arb_read [1];
            logic b_arb_write [1];
            logic b_arb_burstcount [1];
            logic [7:0] b_arb_address [1];
            logic [7:0] b_arb_writedata [1];
            logic b_arb_byteenable [1];
            logic b_arb_stall [1];
            logic b_wrp_ack [1];
            logic b_rrp_datavalid [1];
            logic [7:0] b_rrp_data [1];
            logic bank_select;

            // INST router of acl_ic_local_mem_router
            acl_ic_local_mem_router
            #(
               .DATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(8),
               .BYTEENA_W(1),
               .NUM_BANKS(1)
            )
            router
            (
               .clock(clock),
               .resetn(resetn),
               .bank_select(bank_select),
               // ICM m
               .m_arb_request(local_icm_arb_request[__y][__z]),
               .m_arb_enable(local_icm_arb_enable[__y][__z]),
               .m_arb_read(local_icm_arb_read[__y][__z]),
               .m_arb_write(local_icm_arb_write[__y][__z]),
               .m_arb_burstcount(local_icm_arb_burstcount[__y][__z]),
               .m_arb_address(local_icm_arb_address[__y][__z]),
               .m_arb_writedata(local_icm_arb_writedata[__y][__z]),
               .m_arb_byteenable(local_icm_arb_byteenable[__y][__z]),
               .m_arb_stall(local_icm_arb_stall[__y][__z]),
               .m_wrp_ack(local_icm_wrp_ack[__y][__z]),
               .m_rrp_datavalid(local_icm_rrp_datavalid[__y][__z]),
               .m_rrp_data(local_icm_rrp_data[__y][__z]),
               // ICM b
               .b_arb_request(b_arb_request),
               .b_arb_enable(b_arb_enable),
               .b_arb_read(b_arb_read),
               .b_arb_write(b_arb_write),
               .b_arb_burstcount(b_arb_burstcount),
               .b_arb_address(b_arb_address),
               .b_arb_writedata(b_arb_writedata),
               .b_arb_byteenable(b_arb_byteenable),
               .b_arb_stall(b_arb_stall),
               .b_wrp_ack(b_wrp_ack),
               .b_rrp_datavalid(b_rrp_datavalid),
               .b_rrp_data(b_rrp_data)
            );

            assign bank_select = 1'b1;
         end

         for( __z = 0; __z < 1; __z = __z + 1 )
         begin:port1bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[0].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[0].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[0].b_arb_read[0];
            assign icm_in_arb_write[0] = router[0].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[0].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[0].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[0].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[0].b_arb_byteenable[0];
            assign router[0].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[0].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[0].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[0].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_6
            conv_pipe_system_interconnect_6 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[1] = icm_out_arb_enable;
            assign bank[0].port_read[1] = icm_out_arb_read;
            assign bank[0].port_write[1] = icm_out_arb_write;
            assign bank[0].port_address[1] = icm_out_arb_address;
            assign bank[0].port_writedata[1] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[1] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[1];
            assign icm_out_rrp_data = bank[0].port_readdata[1];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[1];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __z = 0; __z < 1; __z = __z + 1 )
         begin:port2bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[1].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[1].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[1].b_arb_read[0];
            assign icm_in_arb_write[0] = router[1].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[1].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[1].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[1].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[1].b_arb_byteenable[0];
            assign router[1].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[1].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[1].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[1].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_7
            conv_pipe_system_interconnect_7 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[2] = icm_out_arb_enable;
            assign bank[0].port_read[2] = icm_out_arb_read;
            assign bank[0].port_write[2] = icm_out_arb_write;
            assign bank[0].port_address[2] = icm_out_arb_address;
            assign bank[0].port_writedata[2] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[2] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[2];
            assign icm_out_rrp_data = bank[0].port_readdata[2];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[2];
            assign icm_out_wrp_ack = 'b0;
         end

      end

   end
   endgenerate

   generate
   begin:local_mem_system_aspace25
      logic local_icm_arb_request [1][2];
      logic local_icm_arb_enable [1][2];
      logic local_icm_arb_read [1][2];
      logic local_icm_arb_write [1][2];
      logic local_icm_arb_burstcount [1][2];
      logic [7:0] local_icm_arb_address [1][2];
      logic [7:0] local_icm_arb_writedata [1][2];
      logic local_icm_arb_byteenable [1][2];
      logic local_icm_arb_stall [1][2];
      logic local_icm_wrp_ack [1][2];
      logic local_icm_rrp_datavalid [1][2];
      logic [7:0] local_icm_rrp_data [1][2];

      for( __z = 0; __z < 1; __z = __z + 1 )
      begin:local_mem_group
         for( __a = 0; __a < 2; __a = __a + 1 )
         begin:master
            // INST avm_to_ic of acl_avm_to_ic
            acl_avm_to_ic
            #(
               .DATA_W(8),
               .WRITEDATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(32),
               .BYTEENA_W(1)
            )
            avm_to_ic
            (
               // AVM avm
               .avm_enable(local_avm_aspace25_enable[__z][__a]),
               .avm_read(local_avm_aspace25_read[__z][__a]),
               .avm_write(local_avm_aspace25_write[__z][__a]),
               .avm_burstcount(local_avm_aspace25_burstcount[__z][__a]),
               .avm_address(local_avm_aspace25_address[__z][__a]),
               .avm_writedata(local_avm_aspace25_writedata[__z][__a]),
               .avm_byteenable(local_avm_aspace25_byteenable[__z][__a]),
               .avm_waitrequest(local_avm_aspace25_waitrequest[__z][__a]),
               .avm_readdata(local_avm_aspace25_readdata[__z][__a]),
               .avm_readdatavalid(local_avm_aspace25_readdatavalid[__z][__a]),
               .avm_writeack(local_avm_aspace25_writeack[__z][__a]),
               // ICM ic
               .ic_arb_request(local_icm_arb_request[__z][__a]),
               .ic_arb_enable(local_icm_arb_enable[__z][__a]),
               .ic_arb_read(local_icm_arb_read[__z][__a]),
               .ic_arb_write(local_icm_arb_write[__z][__a]),
               .ic_arb_burstcount(local_icm_arb_burstcount[__z][__a]),
               .ic_arb_address(local_icm_arb_address[__z][__a]),
               .ic_arb_writedata(local_icm_arb_writedata[__z][__a]),
               .ic_arb_byteenable(local_icm_arb_byteenable[__z][__a]),
               .ic_arb_stall(local_icm_arb_stall[__z][__a]),
               .ic_wrp_ack(local_icm_wrp_ack[__z][__a]),
               .ic_rrp_datavalid(local_icm_rrp_datavalid[__z][__a]),
               .ic_rrp_data(local_icm_rrp_data[__z][__a])
            );

         end

         for( __a = 0; __a < 1; __a = __a + 1 )
         begin:bank
            logic port_enable [1:2];
            logic port_read [1:2];
            logic port_write [1:2];
            logic [7:0] port_address [1:2];
            logic [7:0] port_writedata [1:2];
            logic port_byteenable [1:2];
            logic port_waitrequest [1:2];
            logic [7:0] port_readdata [1:2];
            logic port_readdatavalid [1:2];

            // INST mem0 of acl_mem1x
            acl_mem1x
            #(
               .INTENDED_DEVICE_FAMILY("Cyclone V"),
               .DEPTH_WORDS(256),
               .WIDTH(8),
               .MEM_LATENCY(3),
               .ENABLED(0),
               .RDW_MODE("OLD_DATA"),
               .RAM_OPERATION_MODE("BIDIR_DUAL_PORT"),
               .PREFERRED_WIDTH(160),
               .RAM_BLOCK_TYPE("M10K")
            )
            mem0
            (
               .clk(clock),
               .resetn(resetn),
               // AVS avs_port1
               .avs_port1_enable(port_enable[1]),
               .avs_port1_read(port_read[1]),
               .avs_port1_write(port_write[1]),
               .avs_port1_address(port_address[1]),
               .avs_port1_writedata(port_writedata[1]),
               .avs_port1_byteenable(port_byteenable[1]),
               .avs_port1_waitrequest(port_waitrequest[1]),
               .avs_port1_readdata(port_readdata[1]),
               .avs_port1_readdatavalid(port_readdatavalid[1]),
               // AVS avs_port2
               .avs_port2_enable(port_enable[2]),
               .avs_port2_read(port_read[2]),
               .avs_port2_write(port_write[2]),
               .avs_port2_address(port_address[2]),
               .avs_port2_writedata(port_writedata[2]),
               .avs_port2_byteenable(port_byteenable[2]),
               .avs_port2_waitrequest(port_waitrequest[2]),
               .avs_port2_readdata(port_readdata[2]),
               .avs_port2_readdatavalid(port_readdatavalid[2])
            );

         end

         for( __a = 0; __a < 2; __a = __a + 1 )
         begin:router
            logic b_arb_request [1];
            logic b_arb_enable [1];
            logic b_arb_read [1];
            logic b_arb_write [1];
            logic b_arb_burstcount [1];
            logic [7:0] b_arb_address [1];
            logic [7:0] b_arb_writedata [1];
            logic b_arb_byteenable [1];
            logic b_arb_stall [1];
            logic b_wrp_ack [1];
            logic b_rrp_datavalid [1];
            logic [7:0] b_rrp_data [1];
            logic bank_select;

            // INST router of acl_ic_local_mem_router
            acl_ic_local_mem_router
            #(
               .DATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(8),
               .BYTEENA_W(1),
               .NUM_BANKS(1)
            )
            router
            (
               .clock(clock),
               .resetn(resetn),
               .bank_select(bank_select),
               // ICM m
               .m_arb_request(local_icm_arb_request[__z][__a]),
               .m_arb_enable(local_icm_arb_enable[__z][__a]),
               .m_arb_read(local_icm_arb_read[__z][__a]),
               .m_arb_write(local_icm_arb_write[__z][__a]),
               .m_arb_burstcount(local_icm_arb_burstcount[__z][__a]),
               .m_arb_address(local_icm_arb_address[__z][__a]),
               .m_arb_writedata(local_icm_arb_writedata[__z][__a]),
               .m_arb_byteenable(local_icm_arb_byteenable[__z][__a]),
               .m_arb_stall(local_icm_arb_stall[__z][__a]),
               .m_wrp_ack(local_icm_wrp_ack[__z][__a]),
               .m_rrp_datavalid(local_icm_rrp_datavalid[__z][__a]),
               .m_rrp_data(local_icm_rrp_data[__z][__a]),
               // ICM b
               .b_arb_request(b_arb_request),
               .b_arb_enable(b_arb_enable),
               .b_arb_read(b_arb_read),
               .b_arb_write(b_arb_write),
               .b_arb_burstcount(b_arb_burstcount),
               .b_arb_address(b_arb_address),
               .b_arb_writedata(b_arb_writedata),
               .b_arb_byteenable(b_arb_byteenable),
               .b_arb_stall(b_arb_stall),
               .b_wrp_ack(b_wrp_ack),
               .b_rrp_datavalid(b_rrp_datavalid),
               .b_rrp_data(b_rrp_data)
            );

            assign bank_select = 1'b1;
         end

         for( __a = 0; __a < 1; __a = __a + 1 )
         begin:port1bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[0].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[0].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[0].b_arb_read[0];
            assign icm_in_arb_write[0] = router[0].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[0].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[0].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[0].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[0].b_arb_byteenable[0];
            assign router[0].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[0].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[0].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[0].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_6
            conv_pipe_system_interconnect_6 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[1] = icm_out_arb_enable;
            assign bank[0].port_read[1] = icm_out_arb_read;
            assign bank[0].port_write[1] = icm_out_arb_write;
            assign bank[0].port_address[1] = icm_out_arb_address;
            assign bank[0].port_writedata[1] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[1] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[1];
            assign icm_out_rrp_data = bank[0].port_readdata[1];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[1];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __a = 0; __a < 1; __a = __a + 1 )
         begin:port2bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[1].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[1].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[1].b_arb_read[0];
            assign icm_in_arb_write[0] = router[1].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[1].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[1].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[1].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[1].b_arb_byteenable[0];
            assign router[1].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[1].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[1].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[1].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_7
            conv_pipe_system_interconnect_7 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[2] = icm_out_arb_enable;
            assign bank[0].port_read[2] = icm_out_arb_read;
            assign bank[0].port_write[2] = icm_out_arb_write;
            assign bank[0].port_address[2] = icm_out_arb_address;
            assign bank[0].port_writedata[2] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[2] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[2];
            assign icm_out_rrp_data = bank[0].port_readdata[2];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[2];
            assign icm_out_wrp_ack = 'b0;
         end

      end

   end
   endgenerate

   generate
   begin:local_mem_system_aspace26
      logic local_icm_arb_request [1][2];
      logic local_icm_arb_enable [1][2];
      logic local_icm_arb_read [1][2];
      logic local_icm_arb_write [1][2];
      logic local_icm_arb_burstcount [1][2];
      logic [7:0] local_icm_arb_address [1][2];
      logic [7:0] local_icm_arb_writedata [1][2];
      logic local_icm_arb_byteenable [1][2];
      logic local_icm_arb_stall [1][2];
      logic local_icm_wrp_ack [1][2];
      logic local_icm_rrp_datavalid [1][2];
      logic [7:0] local_icm_rrp_data [1][2];

      for( __a = 0; __a < 1; __a = __a + 1 )
      begin:local_mem_group
         for( __b = 0; __b < 2; __b = __b + 1 )
         begin:master
            // INST avm_to_ic of acl_avm_to_ic
            acl_avm_to_ic
            #(
               .DATA_W(8),
               .WRITEDATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(32),
               .BYTEENA_W(1)
            )
            avm_to_ic
            (
               // AVM avm
               .avm_enable(local_avm_aspace26_enable[__a][__b]),
               .avm_read(local_avm_aspace26_read[__a][__b]),
               .avm_write(local_avm_aspace26_write[__a][__b]),
               .avm_burstcount(local_avm_aspace26_burstcount[__a][__b]),
               .avm_address(local_avm_aspace26_address[__a][__b]),
               .avm_writedata(local_avm_aspace26_writedata[__a][__b]),
               .avm_byteenable(local_avm_aspace26_byteenable[__a][__b]),
               .avm_waitrequest(local_avm_aspace26_waitrequest[__a][__b]),
               .avm_readdata(local_avm_aspace26_readdata[__a][__b]),
               .avm_readdatavalid(local_avm_aspace26_readdatavalid[__a][__b]),
               .avm_writeack(local_avm_aspace26_writeack[__a][__b]),
               // ICM ic
               .ic_arb_request(local_icm_arb_request[__a][__b]),
               .ic_arb_enable(local_icm_arb_enable[__a][__b]),
               .ic_arb_read(local_icm_arb_read[__a][__b]),
               .ic_arb_write(local_icm_arb_write[__a][__b]),
               .ic_arb_burstcount(local_icm_arb_burstcount[__a][__b]),
               .ic_arb_address(local_icm_arb_address[__a][__b]),
               .ic_arb_writedata(local_icm_arb_writedata[__a][__b]),
               .ic_arb_byteenable(local_icm_arb_byteenable[__a][__b]),
               .ic_arb_stall(local_icm_arb_stall[__a][__b]),
               .ic_wrp_ack(local_icm_wrp_ack[__a][__b]),
               .ic_rrp_datavalid(local_icm_rrp_datavalid[__a][__b]),
               .ic_rrp_data(local_icm_rrp_data[__a][__b])
            );

         end

         for( __b = 0; __b < 1; __b = __b + 1 )
         begin:bank
            logic port_enable [1:2];
            logic port_read [1:2];
            logic port_write [1:2];
            logic [7:0] port_address [1:2];
            logic [7:0] port_writedata [1:2];
            logic port_byteenable [1:2];
            logic port_waitrequest [1:2];
            logic [7:0] port_readdata [1:2];
            logic port_readdatavalid [1:2];

            // INST mem0 of acl_mem1x
            acl_mem1x
            #(
               .INTENDED_DEVICE_FAMILY("Cyclone V"),
               .DEPTH_WORDS(256),
               .WIDTH(8),
               .MEM_LATENCY(3),
               .ENABLED(0),
               .RDW_MODE("OLD_DATA"),
               .RAM_OPERATION_MODE("BIDIR_DUAL_PORT"),
               .PREFERRED_WIDTH(160),
               .RAM_BLOCK_TYPE("M10K")
            )
            mem0
            (
               .clk(clock),
               .resetn(resetn),
               // AVS avs_port1
               .avs_port1_enable(port_enable[1]),
               .avs_port1_read(port_read[1]),
               .avs_port1_write(port_write[1]),
               .avs_port1_address(port_address[1]),
               .avs_port1_writedata(port_writedata[1]),
               .avs_port1_byteenable(port_byteenable[1]),
               .avs_port1_waitrequest(port_waitrequest[1]),
               .avs_port1_readdata(port_readdata[1]),
               .avs_port1_readdatavalid(port_readdatavalid[1]),
               // AVS avs_port2
               .avs_port2_enable(port_enable[2]),
               .avs_port2_read(port_read[2]),
               .avs_port2_write(port_write[2]),
               .avs_port2_address(port_address[2]),
               .avs_port2_writedata(port_writedata[2]),
               .avs_port2_byteenable(port_byteenable[2]),
               .avs_port2_waitrequest(port_waitrequest[2]),
               .avs_port2_readdata(port_readdata[2]),
               .avs_port2_readdatavalid(port_readdatavalid[2])
            );

         end

         for( __b = 0; __b < 2; __b = __b + 1 )
         begin:router
            logic b_arb_request [1];
            logic b_arb_enable [1];
            logic b_arb_read [1];
            logic b_arb_write [1];
            logic b_arb_burstcount [1];
            logic [7:0] b_arb_address [1];
            logic [7:0] b_arb_writedata [1];
            logic b_arb_byteenable [1];
            logic b_arb_stall [1];
            logic b_wrp_ack [1];
            logic b_rrp_datavalid [1];
            logic [7:0] b_rrp_data [1];
            logic bank_select;

            // INST router of acl_ic_local_mem_router
            acl_ic_local_mem_router
            #(
               .DATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(8),
               .BYTEENA_W(1),
               .NUM_BANKS(1)
            )
            router
            (
               .clock(clock),
               .resetn(resetn),
               .bank_select(bank_select),
               // ICM m
               .m_arb_request(local_icm_arb_request[__a][__b]),
               .m_arb_enable(local_icm_arb_enable[__a][__b]),
               .m_arb_read(local_icm_arb_read[__a][__b]),
               .m_arb_write(local_icm_arb_write[__a][__b]),
               .m_arb_burstcount(local_icm_arb_burstcount[__a][__b]),
               .m_arb_address(local_icm_arb_address[__a][__b]),
               .m_arb_writedata(local_icm_arb_writedata[__a][__b]),
               .m_arb_byteenable(local_icm_arb_byteenable[__a][__b]),
               .m_arb_stall(local_icm_arb_stall[__a][__b]),
               .m_wrp_ack(local_icm_wrp_ack[__a][__b]),
               .m_rrp_datavalid(local_icm_rrp_datavalid[__a][__b]),
               .m_rrp_data(local_icm_rrp_data[__a][__b]),
               // ICM b
               .b_arb_request(b_arb_request),
               .b_arb_enable(b_arb_enable),
               .b_arb_read(b_arb_read),
               .b_arb_write(b_arb_write),
               .b_arb_burstcount(b_arb_burstcount),
               .b_arb_address(b_arb_address),
               .b_arb_writedata(b_arb_writedata),
               .b_arb_byteenable(b_arb_byteenable),
               .b_arb_stall(b_arb_stall),
               .b_wrp_ack(b_wrp_ack),
               .b_rrp_datavalid(b_rrp_datavalid),
               .b_rrp_data(b_rrp_data)
            );

            assign bank_select = 1'b1;
         end

         for( __b = 0; __b < 1; __b = __b + 1 )
         begin:port1bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[0].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[0].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[0].b_arb_read[0];
            assign icm_in_arb_write[0] = router[0].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[0].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[0].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[0].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[0].b_arb_byteenable[0];
            assign router[0].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[0].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[0].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[0].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_6
            conv_pipe_system_interconnect_6 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[1] = icm_out_arb_enable;
            assign bank[0].port_read[1] = icm_out_arb_read;
            assign bank[0].port_write[1] = icm_out_arb_write;
            assign bank[0].port_address[1] = icm_out_arb_address;
            assign bank[0].port_writedata[1] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[1] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[1];
            assign icm_out_rrp_data = bank[0].port_readdata[1];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[1];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __b = 0; __b < 1; __b = __b + 1 )
         begin:port2bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[1].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[1].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[1].b_arb_read[0];
            assign icm_in_arb_write[0] = router[1].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[1].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[1].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[1].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[1].b_arb_byteenable[0];
            assign router[1].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[1].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[1].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[1].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_7
            conv_pipe_system_interconnect_7 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[2] = icm_out_arb_enable;
            assign bank[0].port_read[2] = icm_out_arb_read;
            assign bank[0].port_write[2] = icm_out_arb_write;
            assign bank[0].port_address[2] = icm_out_arb_address;
            assign bank[0].port_writedata[2] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[2] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[2];
            assign icm_out_rrp_data = bank[0].port_readdata[2];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[2];
            assign icm_out_wrp_ack = 'b0;
         end

      end

   end
   endgenerate

   generate
   begin:local_mem_system_aspace27
      logic local_icm_arb_request [1][2];
      logic local_icm_arb_enable [1][2];
      logic local_icm_arb_read [1][2];
      logic local_icm_arb_write [1][2];
      logic local_icm_arb_burstcount [1][2];
      logic [7:0] local_icm_arb_address [1][2];
      logic [7:0] local_icm_arb_writedata [1][2];
      logic local_icm_arb_byteenable [1][2];
      logic local_icm_arb_stall [1][2];
      logic local_icm_wrp_ack [1][2];
      logic local_icm_rrp_datavalid [1][2];
      logic [7:0] local_icm_rrp_data [1][2];

      for( __b = 0; __b < 1; __b = __b + 1 )
      begin:local_mem_group
         for( __c = 0; __c < 2; __c = __c + 1 )
         begin:master
            // INST avm_to_ic of acl_avm_to_ic
            acl_avm_to_ic
            #(
               .DATA_W(8),
               .WRITEDATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(32),
               .BYTEENA_W(1)
            )
            avm_to_ic
            (
               // AVM avm
               .avm_enable(local_avm_aspace27_enable[__b][__c]),
               .avm_read(local_avm_aspace27_read[__b][__c]),
               .avm_write(local_avm_aspace27_write[__b][__c]),
               .avm_burstcount(local_avm_aspace27_burstcount[__b][__c]),
               .avm_address(local_avm_aspace27_address[__b][__c]),
               .avm_writedata(local_avm_aspace27_writedata[__b][__c]),
               .avm_byteenable(local_avm_aspace27_byteenable[__b][__c]),
               .avm_waitrequest(local_avm_aspace27_waitrequest[__b][__c]),
               .avm_readdata(local_avm_aspace27_readdata[__b][__c]),
               .avm_readdatavalid(local_avm_aspace27_readdatavalid[__b][__c]),
               .avm_writeack(local_avm_aspace27_writeack[__b][__c]),
               // ICM ic
               .ic_arb_request(local_icm_arb_request[__b][__c]),
               .ic_arb_enable(local_icm_arb_enable[__b][__c]),
               .ic_arb_read(local_icm_arb_read[__b][__c]),
               .ic_arb_write(local_icm_arb_write[__b][__c]),
               .ic_arb_burstcount(local_icm_arb_burstcount[__b][__c]),
               .ic_arb_address(local_icm_arb_address[__b][__c]),
               .ic_arb_writedata(local_icm_arb_writedata[__b][__c]),
               .ic_arb_byteenable(local_icm_arb_byteenable[__b][__c]),
               .ic_arb_stall(local_icm_arb_stall[__b][__c]),
               .ic_wrp_ack(local_icm_wrp_ack[__b][__c]),
               .ic_rrp_datavalid(local_icm_rrp_datavalid[__b][__c]),
               .ic_rrp_data(local_icm_rrp_data[__b][__c])
            );

         end

         for( __c = 0; __c < 1; __c = __c + 1 )
         begin:bank
            logic port_enable [1:2];
            logic port_read [1:2];
            logic port_write [1:2];
            logic [7:0] port_address [1:2];
            logic [7:0] port_writedata [1:2];
            logic port_byteenable [1:2];
            logic port_waitrequest [1:2];
            logic [7:0] port_readdata [1:2];
            logic port_readdatavalid [1:2];

            // INST mem0 of acl_mem1x
            acl_mem1x
            #(
               .INTENDED_DEVICE_FAMILY("Cyclone V"),
               .DEPTH_WORDS(256),
               .WIDTH(8),
               .MEM_LATENCY(3),
               .ENABLED(0),
               .RDW_MODE("OLD_DATA"),
               .RAM_OPERATION_MODE("BIDIR_DUAL_PORT"),
               .PREFERRED_WIDTH(160),
               .RAM_BLOCK_TYPE("M10K")
            )
            mem0
            (
               .clk(clock),
               .resetn(resetn),
               // AVS avs_port1
               .avs_port1_enable(port_enable[1]),
               .avs_port1_read(port_read[1]),
               .avs_port1_write(port_write[1]),
               .avs_port1_address(port_address[1]),
               .avs_port1_writedata(port_writedata[1]),
               .avs_port1_byteenable(port_byteenable[1]),
               .avs_port1_waitrequest(port_waitrequest[1]),
               .avs_port1_readdata(port_readdata[1]),
               .avs_port1_readdatavalid(port_readdatavalid[1]),
               // AVS avs_port2
               .avs_port2_enable(port_enable[2]),
               .avs_port2_read(port_read[2]),
               .avs_port2_write(port_write[2]),
               .avs_port2_address(port_address[2]),
               .avs_port2_writedata(port_writedata[2]),
               .avs_port2_byteenable(port_byteenable[2]),
               .avs_port2_waitrequest(port_waitrequest[2]),
               .avs_port2_readdata(port_readdata[2]),
               .avs_port2_readdatavalid(port_readdatavalid[2])
            );

         end

         for( __c = 0; __c < 2; __c = __c + 1 )
         begin:router
            logic b_arb_request [1];
            logic b_arb_enable [1];
            logic b_arb_read [1];
            logic b_arb_write [1];
            logic b_arb_burstcount [1];
            logic [7:0] b_arb_address [1];
            logic [7:0] b_arb_writedata [1];
            logic b_arb_byteenable [1];
            logic b_arb_stall [1];
            logic b_wrp_ack [1];
            logic b_rrp_datavalid [1];
            logic [7:0] b_rrp_data [1];
            logic bank_select;

            // INST router of acl_ic_local_mem_router
            acl_ic_local_mem_router
            #(
               .DATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(8),
               .BYTEENA_W(1),
               .NUM_BANKS(1)
            )
            router
            (
               .clock(clock),
               .resetn(resetn),
               .bank_select(bank_select),
               // ICM m
               .m_arb_request(local_icm_arb_request[__b][__c]),
               .m_arb_enable(local_icm_arb_enable[__b][__c]),
               .m_arb_read(local_icm_arb_read[__b][__c]),
               .m_arb_write(local_icm_arb_write[__b][__c]),
               .m_arb_burstcount(local_icm_arb_burstcount[__b][__c]),
               .m_arb_address(local_icm_arb_address[__b][__c]),
               .m_arb_writedata(local_icm_arb_writedata[__b][__c]),
               .m_arb_byteenable(local_icm_arb_byteenable[__b][__c]),
               .m_arb_stall(local_icm_arb_stall[__b][__c]),
               .m_wrp_ack(local_icm_wrp_ack[__b][__c]),
               .m_rrp_datavalid(local_icm_rrp_datavalid[__b][__c]),
               .m_rrp_data(local_icm_rrp_data[__b][__c]),
               // ICM b
               .b_arb_request(b_arb_request),
               .b_arb_enable(b_arb_enable),
               .b_arb_read(b_arb_read),
               .b_arb_write(b_arb_write),
               .b_arb_burstcount(b_arb_burstcount),
               .b_arb_address(b_arb_address),
               .b_arb_writedata(b_arb_writedata),
               .b_arb_byteenable(b_arb_byteenable),
               .b_arb_stall(b_arb_stall),
               .b_wrp_ack(b_wrp_ack),
               .b_rrp_datavalid(b_rrp_datavalid),
               .b_rrp_data(b_rrp_data)
            );

            assign bank_select = 1'b1;
         end

         for( __c = 0; __c < 1; __c = __c + 1 )
         begin:port1bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[0].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[0].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[0].b_arb_read[0];
            assign icm_in_arb_write[0] = router[0].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[0].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[0].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[0].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[0].b_arb_byteenable[0];
            assign router[0].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[0].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[0].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[0].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_6
            conv_pipe_system_interconnect_6 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[1] = icm_out_arb_enable;
            assign bank[0].port_read[1] = icm_out_arb_read;
            assign bank[0].port_write[1] = icm_out_arb_write;
            assign bank[0].port_address[1] = icm_out_arb_address;
            assign bank[0].port_writedata[1] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[1] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[1];
            assign icm_out_rrp_data = bank[0].port_readdata[1];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[1];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __c = 0; __c < 1; __c = __c + 1 )
         begin:port2bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[1].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[1].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[1].b_arb_read[0];
            assign icm_in_arb_write[0] = router[1].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[1].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[1].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[1].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[1].b_arb_byteenable[0];
            assign router[1].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[1].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[1].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[1].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_7
            conv_pipe_system_interconnect_7 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[2] = icm_out_arb_enable;
            assign bank[0].port_read[2] = icm_out_arb_read;
            assign bank[0].port_write[2] = icm_out_arb_write;
            assign bank[0].port_address[2] = icm_out_arb_address;
            assign bank[0].port_writedata[2] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[2] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[2];
            assign icm_out_rrp_data = bank[0].port_readdata[2];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[2];
            assign icm_out_wrp_ack = 'b0;
         end

      end

   end
   endgenerate

   generate
   begin:local_mem_system_aspace28
      logic local_icm_arb_request [1][2];
      logic local_icm_arb_enable [1][2];
      logic local_icm_arb_read [1][2];
      logic local_icm_arb_write [1][2];
      logic local_icm_arb_burstcount [1][2];
      logic [7:0] local_icm_arb_address [1][2];
      logic [7:0] local_icm_arb_writedata [1][2];
      logic local_icm_arb_byteenable [1][2];
      logic local_icm_arb_stall [1][2];
      logic local_icm_wrp_ack [1][2];
      logic local_icm_rrp_datavalid [1][2];
      logic [7:0] local_icm_rrp_data [1][2];

      for( __c = 0; __c < 1; __c = __c + 1 )
      begin:local_mem_group
         for( __d = 0; __d < 2; __d = __d + 1 )
         begin:master
            // INST avm_to_ic of acl_avm_to_ic
            acl_avm_to_ic
            #(
               .DATA_W(8),
               .WRITEDATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(32),
               .BYTEENA_W(1)
            )
            avm_to_ic
            (
               // AVM avm
               .avm_enable(local_avm_aspace28_enable[__c][__d]),
               .avm_read(local_avm_aspace28_read[__c][__d]),
               .avm_write(local_avm_aspace28_write[__c][__d]),
               .avm_burstcount(local_avm_aspace28_burstcount[__c][__d]),
               .avm_address(local_avm_aspace28_address[__c][__d]),
               .avm_writedata(local_avm_aspace28_writedata[__c][__d]),
               .avm_byteenable(local_avm_aspace28_byteenable[__c][__d]),
               .avm_waitrequest(local_avm_aspace28_waitrequest[__c][__d]),
               .avm_readdata(local_avm_aspace28_readdata[__c][__d]),
               .avm_readdatavalid(local_avm_aspace28_readdatavalid[__c][__d]),
               .avm_writeack(local_avm_aspace28_writeack[__c][__d]),
               // ICM ic
               .ic_arb_request(local_icm_arb_request[__c][__d]),
               .ic_arb_enable(local_icm_arb_enable[__c][__d]),
               .ic_arb_read(local_icm_arb_read[__c][__d]),
               .ic_arb_write(local_icm_arb_write[__c][__d]),
               .ic_arb_burstcount(local_icm_arb_burstcount[__c][__d]),
               .ic_arb_address(local_icm_arb_address[__c][__d]),
               .ic_arb_writedata(local_icm_arb_writedata[__c][__d]),
               .ic_arb_byteenable(local_icm_arb_byteenable[__c][__d]),
               .ic_arb_stall(local_icm_arb_stall[__c][__d]),
               .ic_wrp_ack(local_icm_wrp_ack[__c][__d]),
               .ic_rrp_datavalid(local_icm_rrp_datavalid[__c][__d]),
               .ic_rrp_data(local_icm_rrp_data[__c][__d])
            );

         end

         for( __d = 0; __d < 1; __d = __d + 1 )
         begin:bank
            logic port_enable [1:2];
            logic port_read [1:2];
            logic port_write [1:2];
            logic [7:0] port_address [1:2];
            logic [7:0] port_writedata [1:2];
            logic port_byteenable [1:2];
            logic port_waitrequest [1:2];
            logic [7:0] port_readdata [1:2];
            logic port_readdatavalid [1:2];

            // INST mem0 of acl_mem1x
            acl_mem1x
            #(
               .INTENDED_DEVICE_FAMILY("Cyclone V"),
               .DEPTH_WORDS(256),
               .WIDTH(8),
               .MEM_LATENCY(3),
               .ENABLED(0),
               .RDW_MODE("OLD_DATA"),
               .RAM_OPERATION_MODE("BIDIR_DUAL_PORT"),
               .PREFERRED_WIDTH(160),
               .RAM_BLOCK_TYPE("M10K")
            )
            mem0
            (
               .clk(clock),
               .resetn(resetn),
               // AVS avs_port1
               .avs_port1_enable(port_enable[1]),
               .avs_port1_read(port_read[1]),
               .avs_port1_write(port_write[1]),
               .avs_port1_address(port_address[1]),
               .avs_port1_writedata(port_writedata[1]),
               .avs_port1_byteenable(port_byteenable[1]),
               .avs_port1_waitrequest(port_waitrequest[1]),
               .avs_port1_readdata(port_readdata[1]),
               .avs_port1_readdatavalid(port_readdatavalid[1]),
               // AVS avs_port2
               .avs_port2_enable(port_enable[2]),
               .avs_port2_read(port_read[2]),
               .avs_port2_write(port_write[2]),
               .avs_port2_address(port_address[2]),
               .avs_port2_writedata(port_writedata[2]),
               .avs_port2_byteenable(port_byteenable[2]),
               .avs_port2_waitrequest(port_waitrequest[2]),
               .avs_port2_readdata(port_readdata[2]),
               .avs_port2_readdatavalid(port_readdatavalid[2])
            );

         end

         for( __d = 0; __d < 2; __d = __d + 1 )
         begin:router
            logic b_arb_request [1];
            logic b_arb_enable [1];
            logic b_arb_read [1];
            logic b_arb_write [1];
            logic b_arb_burstcount [1];
            logic [7:0] b_arb_address [1];
            logic [7:0] b_arb_writedata [1];
            logic b_arb_byteenable [1];
            logic b_arb_stall [1];
            logic b_wrp_ack [1];
            logic b_rrp_datavalid [1];
            logic [7:0] b_rrp_data [1];
            logic bank_select;

            // INST router of acl_ic_local_mem_router
            acl_ic_local_mem_router
            #(
               .DATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(8),
               .BYTEENA_W(1),
               .NUM_BANKS(1)
            )
            router
            (
               .clock(clock),
               .resetn(resetn),
               .bank_select(bank_select),
               // ICM m
               .m_arb_request(local_icm_arb_request[__c][__d]),
               .m_arb_enable(local_icm_arb_enable[__c][__d]),
               .m_arb_read(local_icm_arb_read[__c][__d]),
               .m_arb_write(local_icm_arb_write[__c][__d]),
               .m_arb_burstcount(local_icm_arb_burstcount[__c][__d]),
               .m_arb_address(local_icm_arb_address[__c][__d]),
               .m_arb_writedata(local_icm_arb_writedata[__c][__d]),
               .m_arb_byteenable(local_icm_arb_byteenable[__c][__d]),
               .m_arb_stall(local_icm_arb_stall[__c][__d]),
               .m_wrp_ack(local_icm_wrp_ack[__c][__d]),
               .m_rrp_datavalid(local_icm_rrp_datavalid[__c][__d]),
               .m_rrp_data(local_icm_rrp_data[__c][__d]),
               // ICM b
               .b_arb_request(b_arb_request),
               .b_arb_enable(b_arb_enable),
               .b_arb_read(b_arb_read),
               .b_arb_write(b_arb_write),
               .b_arb_burstcount(b_arb_burstcount),
               .b_arb_address(b_arb_address),
               .b_arb_writedata(b_arb_writedata),
               .b_arb_byteenable(b_arb_byteenable),
               .b_arb_stall(b_arb_stall),
               .b_wrp_ack(b_wrp_ack),
               .b_rrp_datavalid(b_rrp_datavalid),
               .b_rrp_data(b_rrp_data)
            );

            assign bank_select = 1'b1;
         end

         for( __d = 0; __d < 1; __d = __d + 1 )
         begin:port1bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[0].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[0].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[0].b_arb_read[0];
            assign icm_in_arb_write[0] = router[0].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[0].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[0].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[0].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[0].b_arb_byteenable[0];
            assign router[0].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[0].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[0].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[0].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_6
            conv_pipe_system_interconnect_6 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[1] = icm_out_arb_enable;
            assign bank[0].port_read[1] = icm_out_arb_read;
            assign bank[0].port_write[1] = icm_out_arb_write;
            assign bank[0].port_address[1] = icm_out_arb_address;
            assign bank[0].port_writedata[1] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[1] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[1];
            assign icm_out_rrp_data = bank[0].port_readdata[1];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[1];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __d = 0; __d < 1; __d = __d + 1 )
         begin:port2bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[1].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[1].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[1].b_arb_read[0];
            assign icm_in_arb_write[0] = router[1].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[1].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[1].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[1].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[1].b_arb_byteenable[0];
            assign router[1].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[1].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[1].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[1].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_7
            conv_pipe_system_interconnect_7 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[2] = icm_out_arb_enable;
            assign bank[0].port_read[2] = icm_out_arb_read;
            assign bank[0].port_write[2] = icm_out_arb_write;
            assign bank[0].port_address[2] = icm_out_arb_address;
            assign bank[0].port_writedata[2] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[2] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[2];
            assign icm_out_rrp_data = bank[0].port_readdata[2];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[2];
            assign icm_out_wrp_ack = 'b0;
         end

      end

   end
   endgenerate

   generate
   begin:local_mem_system_aspace29
      logic local_icm_arb_request [1][2];
      logic local_icm_arb_enable [1][2];
      logic local_icm_arb_read [1][2];
      logic local_icm_arb_write [1][2];
      logic local_icm_arb_burstcount [1][2];
      logic [7:0] local_icm_arb_address [1][2];
      logic [7:0] local_icm_arb_writedata [1][2];
      logic local_icm_arb_byteenable [1][2];
      logic local_icm_arb_stall [1][2];
      logic local_icm_wrp_ack [1][2];
      logic local_icm_rrp_datavalid [1][2];
      logic [7:0] local_icm_rrp_data [1][2];

      for( __d = 0; __d < 1; __d = __d + 1 )
      begin:local_mem_group
         for( __e = 0; __e < 2; __e = __e + 1 )
         begin:master
            // INST avm_to_ic of acl_avm_to_ic
            acl_avm_to_ic
            #(
               .DATA_W(8),
               .WRITEDATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(32),
               .BYTEENA_W(1)
            )
            avm_to_ic
            (
               // AVM avm
               .avm_enable(local_avm_aspace29_enable[__d][__e]),
               .avm_read(local_avm_aspace29_read[__d][__e]),
               .avm_write(local_avm_aspace29_write[__d][__e]),
               .avm_burstcount(local_avm_aspace29_burstcount[__d][__e]),
               .avm_address(local_avm_aspace29_address[__d][__e]),
               .avm_writedata(local_avm_aspace29_writedata[__d][__e]),
               .avm_byteenable(local_avm_aspace29_byteenable[__d][__e]),
               .avm_waitrequest(local_avm_aspace29_waitrequest[__d][__e]),
               .avm_readdata(local_avm_aspace29_readdata[__d][__e]),
               .avm_readdatavalid(local_avm_aspace29_readdatavalid[__d][__e]),
               .avm_writeack(local_avm_aspace29_writeack[__d][__e]),
               // ICM ic
               .ic_arb_request(local_icm_arb_request[__d][__e]),
               .ic_arb_enable(local_icm_arb_enable[__d][__e]),
               .ic_arb_read(local_icm_arb_read[__d][__e]),
               .ic_arb_write(local_icm_arb_write[__d][__e]),
               .ic_arb_burstcount(local_icm_arb_burstcount[__d][__e]),
               .ic_arb_address(local_icm_arb_address[__d][__e]),
               .ic_arb_writedata(local_icm_arb_writedata[__d][__e]),
               .ic_arb_byteenable(local_icm_arb_byteenable[__d][__e]),
               .ic_arb_stall(local_icm_arb_stall[__d][__e]),
               .ic_wrp_ack(local_icm_wrp_ack[__d][__e]),
               .ic_rrp_datavalid(local_icm_rrp_datavalid[__d][__e]),
               .ic_rrp_data(local_icm_rrp_data[__d][__e])
            );

         end

         for( __e = 0; __e < 1; __e = __e + 1 )
         begin:bank
            logic port_enable [1:2];
            logic port_read [1:2];
            logic port_write [1:2];
            logic [7:0] port_address [1:2];
            logic [7:0] port_writedata [1:2];
            logic port_byteenable [1:2];
            logic port_waitrequest [1:2];
            logic [7:0] port_readdata [1:2];
            logic port_readdatavalid [1:2];

            // INST mem0 of acl_mem1x
            acl_mem1x
            #(
               .INTENDED_DEVICE_FAMILY("Cyclone V"),
               .DEPTH_WORDS(256),
               .WIDTH(8),
               .MEM_LATENCY(3),
               .ENABLED(0),
               .RDW_MODE("OLD_DATA"),
               .RAM_OPERATION_MODE("BIDIR_DUAL_PORT"),
               .PREFERRED_WIDTH(160),
               .RAM_BLOCK_TYPE("M10K")
            )
            mem0
            (
               .clk(clock),
               .resetn(resetn),
               // AVS avs_port1
               .avs_port1_enable(port_enable[1]),
               .avs_port1_read(port_read[1]),
               .avs_port1_write(port_write[1]),
               .avs_port1_address(port_address[1]),
               .avs_port1_writedata(port_writedata[1]),
               .avs_port1_byteenable(port_byteenable[1]),
               .avs_port1_waitrequest(port_waitrequest[1]),
               .avs_port1_readdata(port_readdata[1]),
               .avs_port1_readdatavalid(port_readdatavalid[1]),
               // AVS avs_port2
               .avs_port2_enable(port_enable[2]),
               .avs_port2_read(port_read[2]),
               .avs_port2_write(port_write[2]),
               .avs_port2_address(port_address[2]),
               .avs_port2_writedata(port_writedata[2]),
               .avs_port2_byteenable(port_byteenable[2]),
               .avs_port2_waitrequest(port_waitrequest[2]),
               .avs_port2_readdata(port_readdata[2]),
               .avs_port2_readdatavalid(port_readdatavalid[2])
            );

         end

         for( __e = 0; __e < 2; __e = __e + 1 )
         begin:router
            logic b_arb_request [1];
            logic b_arb_enable [1];
            logic b_arb_read [1];
            logic b_arb_write [1];
            logic b_arb_burstcount [1];
            logic [7:0] b_arb_address [1];
            logic [7:0] b_arb_writedata [1];
            logic b_arb_byteenable [1];
            logic b_arb_stall [1];
            logic b_wrp_ack [1];
            logic b_rrp_datavalid [1];
            logic [7:0] b_rrp_data [1];
            logic bank_select;

            // INST router of acl_ic_local_mem_router
            acl_ic_local_mem_router
            #(
               .DATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(8),
               .BYTEENA_W(1),
               .NUM_BANKS(1)
            )
            router
            (
               .clock(clock),
               .resetn(resetn),
               .bank_select(bank_select),
               // ICM m
               .m_arb_request(local_icm_arb_request[__d][__e]),
               .m_arb_enable(local_icm_arb_enable[__d][__e]),
               .m_arb_read(local_icm_arb_read[__d][__e]),
               .m_arb_write(local_icm_arb_write[__d][__e]),
               .m_arb_burstcount(local_icm_arb_burstcount[__d][__e]),
               .m_arb_address(local_icm_arb_address[__d][__e]),
               .m_arb_writedata(local_icm_arb_writedata[__d][__e]),
               .m_arb_byteenable(local_icm_arb_byteenable[__d][__e]),
               .m_arb_stall(local_icm_arb_stall[__d][__e]),
               .m_wrp_ack(local_icm_wrp_ack[__d][__e]),
               .m_rrp_datavalid(local_icm_rrp_datavalid[__d][__e]),
               .m_rrp_data(local_icm_rrp_data[__d][__e]),
               // ICM b
               .b_arb_request(b_arb_request),
               .b_arb_enable(b_arb_enable),
               .b_arb_read(b_arb_read),
               .b_arb_write(b_arb_write),
               .b_arb_burstcount(b_arb_burstcount),
               .b_arb_address(b_arb_address),
               .b_arb_writedata(b_arb_writedata),
               .b_arb_byteenable(b_arb_byteenable),
               .b_arb_stall(b_arb_stall),
               .b_wrp_ack(b_wrp_ack),
               .b_rrp_datavalid(b_rrp_datavalid),
               .b_rrp_data(b_rrp_data)
            );

            assign bank_select = 1'b1;
         end

         for( __e = 0; __e < 1; __e = __e + 1 )
         begin:port1bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[0].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[0].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[0].b_arb_read[0];
            assign icm_in_arb_write[0] = router[0].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[0].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[0].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[0].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[0].b_arb_byteenable[0];
            assign router[0].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[0].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[0].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[0].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_6
            conv_pipe_system_interconnect_6 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[1] = icm_out_arb_enable;
            assign bank[0].port_read[1] = icm_out_arb_read;
            assign bank[0].port_write[1] = icm_out_arb_write;
            assign bank[0].port_address[1] = icm_out_arb_address;
            assign bank[0].port_writedata[1] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[1] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[1];
            assign icm_out_rrp_data = bank[0].port_readdata[1];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[1];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __e = 0; __e < 1; __e = __e + 1 )
         begin:port2bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[1].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[1].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[1].b_arb_read[0];
            assign icm_in_arb_write[0] = router[1].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[1].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[1].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[1].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[1].b_arb_byteenable[0];
            assign router[1].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[1].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[1].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[1].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_7
            conv_pipe_system_interconnect_7 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[2] = icm_out_arb_enable;
            assign bank[0].port_read[2] = icm_out_arb_read;
            assign bank[0].port_write[2] = icm_out_arb_write;
            assign bank[0].port_address[2] = icm_out_arb_address;
            assign bank[0].port_writedata[2] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[2] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[2];
            assign icm_out_rrp_data = bank[0].port_readdata[2];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[2];
            assign icm_out_wrp_ack = 'b0;
         end

      end

   end
   endgenerate

   generate
   begin:local_mem_system_aspace30
      logic local_icm_arb_request [1][2];
      logic local_icm_arb_enable [1][2];
      logic local_icm_arb_read [1][2];
      logic local_icm_arb_write [1][2];
      logic local_icm_arb_burstcount [1][2];
      logic [7:0] local_icm_arb_address [1][2];
      logic [7:0] local_icm_arb_writedata [1][2];
      logic local_icm_arb_byteenable [1][2];
      logic local_icm_arb_stall [1][2];
      logic local_icm_wrp_ack [1][2];
      logic local_icm_rrp_datavalid [1][2];
      logic [7:0] local_icm_rrp_data [1][2];

      for( __e = 0; __e < 1; __e = __e + 1 )
      begin:local_mem_group
         for( __f = 0; __f < 2; __f = __f + 1 )
         begin:master
            // INST avm_to_ic of acl_avm_to_ic
            acl_avm_to_ic
            #(
               .DATA_W(8),
               .WRITEDATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(32),
               .BYTEENA_W(1)
            )
            avm_to_ic
            (
               // AVM avm
               .avm_enable(local_avm_aspace30_enable[__e][__f]),
               .avm_read(local_avm_aspace30_read[__e][__f]),
               .avm_write(local_avm_aspace30_write[__e][__f]),
               .avm_burstcount(local_avm_aspace30_burstcount[__e][__f]),
               .avm_address(local_avm_aspace30_address[__e][__f]),
               .avm_writedata(local_avm_aspace30_writedata[__e][__f]),
               .avm_byteenable(local_avm_aspace30_byteenable[__e][__f]),
               .avm_waitrequest(local_avm_aspace30_waitrequest[__e][__f]),
               .avm_readdata(local_avm_aspace30_readdata[__e][__f]),
               .avm_readdatavalid(local_avm_aspace30_readdatavalid[__e][__f]),
               .avm_writeack(local_avm_aspace30_writeack[__e][__f]),
               // ICM ic
               .ic_arb_request(local_icm_arb_request[__e][__f]),
               .ic_arb_enable(local_icm_arb_enable[__e][__f]),
               .ic_arb_read(local_icm_arb_read[__e][__f]),
               .ic_arb_write(local_icm_arb_write[__e][__f]),
               .ic_arb_burstcount(local_icm_arb_burstcount[__e][__f]),
               .ic_arb_address(local_icm_arb_address[__e][__f]),
               .ic_arb_writedata(local_icm_arb_writedata[__e][__f]),
               .ic_arb_byteenable(local_icm_arb_byteenable[__e][__f]),
               .ic_arb_stall(local_icm_arb_stall[__e][__f]),
               .ic_wrp_ack(local_icm_wrp_ack[__e][__f]),
               .ic_rrp_datavalid(local_icm_rrp_datavalid[__e][__f]),
               .ic_rrp_data(local_icm_rrp_data[__e][__f])
            );

         end

         for( __f = 0; __f < 1; __f = __f + 1 )
         begin:bank
            logic port_enable [1:2];
            logic port_read [1:2];
            logic port_write [1:2];
            logic [7:0] port_address [1:2];
            logic [7:0] port_writedata [1:2];
            logic port_byteenable [1:2];
            logic port_waitrequest [1:2];
            logic [7:0] port_readdata [1:2];
            logic port_readdatavalid [1:2];

            // INST mem0 of acl_mem1x
            acl_mem1x
            #(
               .INTENDED_DEVICE_FAMILY("Cyclone V"),
               .DEPTH_WORDS(256),
               .WIDTH(8),
               .MEM_LATENCY(3),
               .ENABLED(0),
               .RDW_MODE("OLD_DATA"),
               .RAM_OPERATION_MODE("BIDIR_DUAL_PORT"),
               .PREFERRED_WIDTH(160),
               .RAM_BLOCK_TYPE("M10K")
            )
            mem0
            (
               .clk(clock),
               .resetn(resetn),
               // AVS avs_port1
               .avs_port1_enable(port_enable[1]),
               .avs_port1_read(port_read[1]),
               .avs_port1_write(port_write[1]),
               .avs_port1_address(port_address[1]),
               .avs_port1_writedata(port_writedata[1]),
               .avs_port1_byteenable(port_byteenable[1]),
               .avs_port1_waitrequest(port_waitrequest[1]),
               .avs_port1_readdata(port_readdata[1]),
               .avs_port1_readdatavalid(port_readdatavalid[1]),
               // AVS avs_port2
               .avs_port2_enable(port_enable[2]),
               .avs_port2_read(port_read[2]),
               .avs_port2_write(port_write[2]),
               .avs_port2_address(port_address[2]),
               .avs_port2_writedata(port_writedata[2]),
               .avs_port2_byteenable(port_byteenable[2]),
               .avs_port2_waitrequest(port_waitrequest[2]),
               .avs_port2_readdata(port_readdata[2]),
               .avs_port2_readdatavalid(port_readdatavalid[2])
            );

         end

         for( __f = 0; __f < 2; __f = __f + 1 )
         begin:router
            logic b_arb_request [1];
            logic b_arb_enable [1];
            logic b_arb_read [1];
            logic b_arb_write [1];
            logic b_arb_burstcount [1];
            logic [7:0] b_arb_address [1];
            logic [7:0] b_arb_writedata [1];
            logic b_arb_byteenable [1];
            logic b_arb_stall [1];
            logic b_wrp_ack [1];
            logic b_rrp_datavalid [1];
            logic [7:0] b_rrp_data [1];
            logic bank_select;

            // INST router of acl_ic_local_mem_router
            acl_ic_local_mem_router
            #(
               .DATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(8),
               .BYTEENA_W(1),
               .NUM_BANKS(1)
            )
            router
            (
               .clock(clock),
               .resetn(resetn),
               .bank_select(bank_select),
               // ICM m
               .m_arb_request(local_icm_arb_request[__e][__f]),
               .m_arb_enable(local_icm_arb_enable[__e][__f]),
               .m_arb_read(local_icm_arb_read[__e][__f]),
               .m_arb_write(local_icm_arb_write[__e][__f]),
               .m_arb_burstcount(local_icm_arb_burstcount[__e][__f]),
               .m_arb_address(local_icm_arb_address[__e][__f]),
               .m_arb_writedata(local_icm_arb_writedata[__e][__f]),
               .m_arb_byteenable(local_icm_arb_byteenable[__e][__f]),
               .m_arb_stall(local_icm_arb_stall[__e][__f]),
               .m_wrp_ack(local_icm_wrp_ack[__e][__f]),
               .m_rrp_datavalid(local_icm_rrp_datavalid[__e][__f]),
               .m_rrp_data(local_icm_rrp_data[__e][__f]),
               // ICM b
               .b_arb_request(b_arb_request),
               .b_arb_enable(b_arb_enable),
               .b_arb_read(b_arb_read),
               .b_arb_write(b_arb_write),
               .b_arb_burstcount(b_arb_burstcount),
               .b_arb_address(b_arb_address),
               .b_arb_writedata(b_arb_writedata),
               .b_arb_byteenable(b_arb_byteenable),
               .b_arb_stall(b_arb_stall),
               .b_wrp_ack(b_wrp_ack),
               .b_rrp_datavalid(b_rrp_datavalid),
               .b_rrp_data(b_rrp_data)
            );

            assign bank_select = 1'b1;
         end

         for( __f = 0; __f < 1; __f = __f + 1 )
         begin:port1bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[0].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[0].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[0].b_arb_read[0];
            assign icm_in_arb_write[0] = router[0].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[0].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[0].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[0].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[0].b_arb_byteenable[0];
            assign router[0].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[0].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[0].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[0].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_6
            conv_pipe_system_interconnect_6 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[1] = icm_out_arb_enable;
            assign bank[0].port_read[1] = icm_out_arb_read;
            assign bank[0].port_write[1] = icm_out_arb_write;
            assign bank[0].port_address[1] = icm_out_arb_address;
            assign bank[0].port_writedata[1] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[1] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[1];
            assign icm_out_rrp_data = bank[0].port_readdata[1];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[1];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __f = 0; __f < 1; __f = __f + 1 )
         begin:port2bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[1].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[1].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[1].b_arb_read[0];
            assign icm_in_arb_write[0] = router[1].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[1].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[1].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[1].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[1].b_arb_byteenable[0];
            assign router[1].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[1].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[1].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[1].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_7
            conv_pipe_system_interconnect_7 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[2] = icm_out_arb_enable;
            assign bank[0].port_read[2] = icm_out_arb_read;
            assign bank[0].port_write[2] = icm_out_arb_write;
            assign bank[0].port_address[2] = icm_out_arb_address;
            assign bank[0].port_writedata[2] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[2] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[2];
            assign icm_out_rrp_data = bank[0].port_readdata[2];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[2];
            assign icm_out_wrp_ack = 'b0;
         end

      end

   end
   endgenerate

   generate
   begin:local_mem_system_aspace31
      logic local_icm_arb_request [1][2];
      logic local_icm_arb_enable [1][2];
      logic local_icm_arb_read [1][2];
      logic local_icm_arb_write [1][2];
      logic local_icm_arb_burstcount [1][2];
      logic [7:0] local_icm_arb_address [1][2];
      logic [7:0] local_icm_arb_writedata [1][2];
      logic local_icm_arb_byteenable [1][2];
      logic local_icm_arb_stall [1][2];
      logic local_icm_wrp_ack [1][2];
      logic local_icm_rrp_datavalid [1][2];
      logic [7:0] local_icm_rrp_data [1][2];

      for( __f = 0; __f < 1; __f = __f + 1 )
      begin:local_mem_group
         for( __g = 0; __g < 2; __g = __g + 1 )
         begin:master
            // INST avm_to_ic of acl_avm_to_ic
            acl_avm_to_ic
            #(
               .DATA_W(8),
               .WRITEDATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(32),
               .BYTEENA_W(1)
            )
            avm_to_ic
            (
               // AVM avm
               .avm_enable(local_avm_aspace31_enable[__f][__g]),
               .avm_read(local_avm_aspace31_read[__f][__g]),
               .avm_write(local_avm_aspace31_write[__f][__g]),
               .avm_burstcount(local_avm_aspace31_burstcount[__f][__g]),
               .avm_address(local_avm_aspace31_address[__f][__g]),
               .avm_writedata(local_avm_aspace31_writedata[__f][__g]),
               .avm_byteenable(local_avm_aspace31_byteenable[__f][__g]),
               .avm_waitrequest(local_avm_aspace31_waitrequest[__f][__g]),
               .avm_readdata(local_avm_aspace31_readdata[__f][__g]),
               .avm_readdatavalid(local_avm_aspace31_readdatavalid[__f][__g]),
               .avm_writeack(local_avm_aspace31_writeack[__f][__g]),
               // ICM ic
               .ic_arb_request(local_icm_arb_request[__f][__g]),
               .ic_arb_enable(local_icm_arb_enable[__f][__g]),
               .ic_arb_read(local_icm_arb_read[__f][__g]),
               .ic_arb_write(local_icm_arb_write[__f][__g]),
               .ic_arb_burstcount(local_icm_arb_burstcount[__f][__g]),
               .ic_arb_address(local_icm_arb_address[__f][__g]),
               .ic_arb_writedata(local_icm_arb_writedata[__f][__g]),
               .ic_arb_byteenable(local_icm_arb_byteenable[__f][__g]),
               .ic_arb_stall(local_icm_arb_stall[__f][__g]),
               .ic_wrp_ack(local_icm_wrp_ack[__f][__g]),
               .ic_rrp_datavalid(local_icm_rrp_datavalid[__f][__g]),
               .ic_rrp_data(local_icm_rrp_data[__f][__g])
            );

         end

         for( __g = 0; __g < 1; __g = __g + 1 )
         begin:bank
            logic port_enable [1:2];
            logic port_read [1:2];
            logic port_write [1:2];
            logic [7:0] port_address [1:2];
            logic [7:0] port_writedata [1:2];
            logic port_byteenable [1:2];
            logic port_waitrequest [1:2];
            logic [7:0] port_readdata [1:2];
            logic port_readdatavalid [1:2];

            // INST mem0 of acl_mem1x
            acl_mem1x
            #(
               .INTENDED_DEVICE_FAMILY("Cyclone V"),
               .DEPTH_WORDS(256),
               .WIDTH(8),
               .MEM_LATENCY(3),
               .ENABLED(0),
               .RDW_MODE("OLD_DATA"),
               .RAM_OPERATION_MODE("BIDIR_DUAL_PORT"),
               .PREFERRED_WIDTH(160),
               .RAM_BLOCK_TYPE("M10K")
            )
            mem0
            (
               .clk(clock),
               .resetn(resetn),
               // AVS avs_port1
               .avs_port1_enable(port_enable[1]),
               .avs_port1_read(port_read[1]),
               .avs_port1_write(port_write[1]),
               .avs_port1_address(port_address[1]),
               .avs_port1_writedata(port_writedata[1]),
               .avs_port1_byteenable(port_byteenable[1]),
               .avs_port1_waitrequest(port_waitrequest[1]),
               .avs_port1_readdata(port_readdata[1]),
               .avs_port1_readdatavalid(port_readdatavalid[1]),
               // AVS avs_port2
               .avs_port2_enable(port_enable[2]),
               .avs_port2_read(port_read[2]),
               .avs_port2_write(port_write[2]),
               .avs_port2_address(port_address[2]),
               .avs_port2_writedata(port_writedata[2]),
               .avs_port2_byteenable(port_byteenable[2]),
               .avs_port2_waitrequest(port_waitrequest[2]),
               .avs_port2_readdata(port_readdata[2]),
               .avs_port2_readdatavalid(port_readdatavalid[2])
            );

         end

         for( __g = 0; __g < 2; __g = __g + 1 )
         begin:router
            logic b_arb_request [1];
            logic b_arb_enable [1];
            logic b_arb_read [1];
            logic b_arb_write [1];
            logic b_arb_burstcount [1];
            logic [7:0] b_arb_address [1];
            logic [7:0] b_arb_writedata [1];
            logic b_arb_byteenable [1];
            logic b_arb_stall [1];
            logic b_wrp_ack [1];
            logic b_rrp_datavalid [1];
            logic [7:0] b_rrp_data [1];
            logic bank_select;

            // INST router of acl_ic_local_mem_router
            acl_ic_local_mem_router
            #(
               .DATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(8),
               .BYTEENA_W(1),
               .NUM_BANKS(1)
            )
            router
            (
               .clock(clock),
               .resetn(resetn),
               .bank_select(bank_select),
               // ICM m
               .m_arb_request(local_icm_arb_request[__f][__g]),
               .m_arb_enable(local_icm_arb_enable[__f][__g]),
               .m_arb_read(local_icm_arb_read[__f][__g]),
               .m_arb_write(local_icm_arb_write[__f][__g]),
               .m_arb_burstcount(local_icm_arb_burstcount[__f][__g]),
               .m_arb_address(local_icm_arb_address[__f][__g]),
               .m_arb_writedata(local_icm_arb_writedata[__f][__g]),
               .m_arb_byteenable(local_icm_arb_byteenable[__f][__g]),
               .m_arb_stall(local_icm_arb_stall[__f][__g]),
               .m_wrp_ack(local_icm_wrp_ack[__f][__g]),
               .m_rrp_datavalid(local_icm_rrp_datavalid[__f][__g]),
               .m_rrp_data(local_icm_rrp_data[__f][__g]),
               // ICM b
               .b_arb_request(b_arb_request),
               .b_arb_enable(b_arb_enable),
               .b_arb_read(b_arb_read),
               .b_arb_write(b_arb_write),
               .b_arb_burstcount(b_arb_burstcount),
               .b_arb_address(b_arb_address),
               .b_arb_writedata(b_arb_writedata),
               .b_arb_byteenable(b_arb_byteenable),
               .b_arb_stall(b_arb_stall),
               .b_wrp_ack(b_wrp_ack),
               .b_rrp_datavalid(b_rrp_datavalid),
               .b_rrp_data(b_rrp_data)
            );

            assign bank_select = 1'b1;
         end

         for( __g = 0; __g < 1; __g = __g + 1 )
         begin:port1bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[0].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[0].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[0].b_arb_read[0];
            assign icm_in_arb_write[0] = router[0].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[0].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[0].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[0].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[0].b_arb_byteenable[0];
            assign router[0].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[0].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[0].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[0].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_6
            conv_pipe_system_interconnect_6 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[1] = icm_out_arb_enable;
            assign bank[0].port_read[1] = icm_out_arb_read;
            assign bank[0].port_write[1] = icm_out_arb_write;
            assign bank[0].port_address[1] = icm_out_arb_address;
            assign bank[0].port_writedata[1] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[1] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[1];
            assign icm_out_rrp_data = bank[0].port_readdata[1];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[1];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __g = 0; __g < 1; __g = __g + 1 )
         begin:port2bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[1].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[1].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[1].b_arb_read[0];
            assign icm_in_arb_write[0] = router[1].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[1].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[1].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[1].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[1].b_arb_byteenable[0];
            assign router[1].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[1].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[1].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[1].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_7
            conv_pipe_system_interconnect_7 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[2] = icm_out_arb_enable;
            assign bank[0].port_read[2] = icm_out_arb_read;
            assign bank[0].port_write[2] = icm_out_arb_write;
            assign bank[0].port_address[2] = icm_out_arb_address;
            assign bank[0].port_writedata[2] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[2] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[2];
            assign icm_out_rrp_data = bank[0].port_readdata[2];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[2];
            assign icm_out_wrp_ack = 'b0;
         end

      end

   end
   endgenerate

   generate
   begin:local_mem_system_aspace32
      logic local_icm_arb_request [1][2];
      logic local_icm_arb_enable [1][2];
      logic local_icm_arb_read [1][2];
      logic local_icm_arb_write [1][2];
      logic local_icm_arb_burstcount [1][2];
      logic [7:0] local_icm_arb_address [1][2];
      logic [7:0] local_icm_arb_writedata [1][2];
      logic local_icm_arb_byteenable [1][2];
      logic local_icm_arb_stall [1][2];
      logic local_icm_wrp_ack [1][2];
      logic local_icm_rrp_datavalid [1][2];
      logic [7:0] local_icm_rrp_data [1][2];

      for( __g = 0; __g < 1; __g = __g + 1 )
      begin:local_mem_group
         for( __h = 0; __h < 2; __h = __h + 1 )
         begin:master
            // INST avm_to_ic of acl_avm_to_ic
            acl_avm_to_ic
            #(
               .DATA_W(8),
               .WRITEDATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(32),
               .BYTEENA_W(1)
            )
            avm_to_ic
            (
               // AVM avm
               .avm_enable(local_avm_aspace32_enable[__g][__h]),
               .avm_read(local_avm_aspace32_read[__g][__h]),
               .avm_write(local_avm_aspace32_write[__g][__h]),
               .avm_burstcount(local_avm_aspace32_burstcount[__g][__h]),
               .avm_address(local_avm_aspace32_address[__g][__h]),
               .avm_writedata(local_avm_aspace32_writedata[__g][__h]),
               .avm_byteenable(local_avm_aspace32_byteenable[__g][__h]),
               .avm_waitrequest(local_avm_aspace32_waitrequest[__g][__h]),
               .avm_readdata(local_avm_aspace32_readdata[__g][__h]),
               .avm_readdatavalid(local_avm_aspace32_readdatavalid[__g][__h]),
               .avm_writeack(local_avm_aspace32_writeack[__g][__h]),
               // ICM ic
               .ic_arb_request(local_icm_arb_request[__g][__h]),
               .ic_arb_enable(local_icm_arb_enable[__g][__h]),
               .ic_arb_read(local_icm_arb_read[__g][__h]),
               .ic_arb_write(local_icm_arb_write[__g][__h]),
               .ic_arb_burstcount(local_icm_arb_burstcount[__g][__h]),
               .ic_arb_address(local_icm_arb_address[__g][__h]),
               .ic_arb_writedata(local_icm_arb_writedata[__g][__h]),
               .ic_arb_byteenable(local_icm_arb_byteenable[__g][__h]),
               .ic_arb_stall(local_icm_arb_stall[__g][__h]),
               .ic_wrp_ack(local_icm_wrp_ack[__g][__h]),
               .ic_rrp_datavalid(local_icm_rrp_datavalid[__g][__h]),
               .ic_rrp_data(local_icm_rrp_data[__g][__h])
            );

         end

         for( __h = 0; __h < 1; __h = __h + 1 )
         begin:bank
            logic port_enable [1:2];
            logic port_read [1:2];
            logic port_write [1:2];
            logic [7:0] port_address [1:2];
            logic [7:0] port_writedata [1:2];
            logic port_byteenable [1:2];
            logic port_waitrequest [1:2];
            logic [7:0] port_readdata [1:2];
            logic port_readdatavalid [1:2];

            // INST mem0 of acl_mem1x
            acl_mem1x
            #(
               .INTENDED_DEVICE_FAMILY("Cyclone V"),
               .DEPTH_WORDS(256),
               .WIDTH(8),
               .MEM_LATENCY(3),
               .ENABLED(0),
               .RDW_MODE("OLD_DATA"),
               .RAM_OPERATION_MODE("BIDIR_DUAL_PORT"),
               .PREFERRED_WIDTH(160),
               .RAM_BLOCK_TYPE("M10K")
            )
            mem0
            (
               .clk(clock),
               .resetn(resetn),
               // AVS avs_port1
               .avs_port1_enable(port_enable[1]),
               .avs_port1_read(port_read[1]),
               .avs_port1_write(port_write[1]),
               .avs_port1_address(port_address[1]),
               .avs_port1_writedata(port_writedata[1]),
               .avs_port1_byteenable(port_byteenable[1]),
               .avs_port1_waitrequest(port_waitrequest[1]),
               .avs_port1_readdata(port_readdata[1]),
               .avs_port1_readdatavalid(port_readdatavalid[1]),
               // AVS avs_port2
               .avs_port2_enable(port_enable[2]),
               .avs_port2_read(port_read[2]),
               .avs_port2_write(port_write[2]),
               .avs_port2_address(port_address[2]),
               .avs_port2_writedata(port_writedata[2]),
               .avs_port2_byteenable(port_byteenable[2]),
               .avs_port2_waitrequest(port_waitrequest[2]),
               .avs_port2_readdata(port_readdata[2]),
               .avs_port2_readdatavalid(port_readdatavalid[2])
            );

         end

         for( __h = 0; __h < 2; __h = __h + 1 )
         begin:router
            logic b_arb_request [1];
            logic b_arb_enable [1];
            logic b_arb_read [1];
            logic b_arb_write [1];
            logic b_arb_burstcount [1];
            logic [7:0] b_arb_address [1];
            logic [7:0] b_arb_writedata [1];
            logic b_arb_byteenable [1];
            logic b_arb_stall [1];
            logic b_wrp_ack [1];
            logic b_rrp_datavalid [1];
            logic [7:0] b_rrp_data [1];
            logic bank_select;

            // INST router of acl_ic_local_mem_router
            acl_ic_local_mem_router
            #(
               .DATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(8),
               .BYTEENA_W(1),
               .NUM_BANKS(1)
            )
            router
            (
               .clock(clock),
               .resetn(resetn),
               .bank_select(bank_select),
               // ICM m
               .m_arb_request(local_icm_arb_request[__g][__h]),
               .m_arb_enable(local_icm_arb_enable[__g][__h]),
               .m_arb_read(local_icm_arb_read[__g][__h]),
               .m_arb_write(local_icm_arb_write[__g][__h]),
               .m_arb_burstcount(local_icm_arb_burstcount[__g][__h]),
               .m_arb_address(local_icm_arb_address[__g][__h]),
               .m_arb_writedata(local_icm_arb_writedata[__g][__h]),
               .m_arb_byteenable(local_icm_arb_byteenable[__g][__h]),
               .m_arb_stall(local_icm_arb_stall[__g][__h]),
               .m_wrp_ack(local_icm_wrp_ack[__g][__h]),
               .m_rrp_datavalid(local_icm_rrp_datavalid[__g][__h]),
               .m_rrp_data(local_icm_rrp_data[__g][__h]),
               // ICM b
               .b_arb_request(b_arb_request),
               .b_arb_enable(b_arb_enable),
               .b_arb_read(b_arb_read),
               .b_arb_write(b_arb_write),
               .b_arb_burstcount(b_arb_burstcount),
               .b_arb_address(b_arb_address),
               .b_arb_writedata(b_arb_writedata),
               .b_arb_byteenable(b_arb_byteenable),
               .b_arb_stall(b_arb_stall),
               .b_wrp_ack(b_wrp_ack),
               .b_rrp_datavalid(b_rrp_datavalid),
               .b_rrp_data(b_rrp_data)
            );

            assign bank_select = 1'b1;
         end

         for( __h = 0; __h < 1; __h = __h + 1 )
         begin:port1bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[0].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[0].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[0].b_arb_read[0];
            assign icm_in_arb_write[0] = router[0].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[0].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[0].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[0].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[0].b_arb_byteenable[0];
            assign router[0].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[0].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[0].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[0].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_6
            conv_pipe_system_interconnect_6 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[1] = icm_out_arb_enable;
            assign bank[0].port_read[1] = icm_out_arb_read;
            assign bank[0].port_write[1] = icm_out_arb_write;
            assign bank[0].port_address[1] = icm_out_arb_address;
            assign bank[0].port_writedata[1] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[1] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[1];
            assign icm_out_rrp_data = bank[0].port_readdata[1];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[1];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __h = 0; __h < 1; __h = __h + 1 )
         begin:port2bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[1].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[1].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[1].b_arb_read[0];
            assign icm_in_arb_write[0] = router[1].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[1].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[1].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[1].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[1].b_arb_byteenable[0];
            assign router[1].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[1].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[1].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[1].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_7
            conv_pipe_system_interconnect_7 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[2] = icm_out_arb_enable;
            assign bank[0].port_read[2] = icm_out_arb_read;
            assign bank[0].port_write[2] = icm_out_arb_write;
            assign bank[0].port_address[2] = icm_out_arb_address;
            assign bank[0].port_writedata[2] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[2] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[2];
            assign icm_out_rrp_data = bank[0].port_readdata[2];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[2];
            assign icm_out_wrp_ack = 'b0;
         end

      end

   end
   endgenerate

   generate
   begin:local_mem_system_aspace33
      logic local_icm_arb_request [1][2];
      logic local_icm_arb_enable [1][2];
      logic local_icm_arb_read [1][2];
      logic local_icm_arb_write [1][2];
      logic local_icm_arb_burstcount [1][2];
      logic [7:0] local_icm_arb_address [1][2];
      logic [7:0] local_icm_arb_writedata [1][2];
      logic local_icm_arb_byteenable [1][2];
      logic local_icm_arb_stall [1][2];
      logic local_icm_wrp_ack [1][2];
      logic local_icm_rrp_datavalid [1][2];
      logic [7:0] local_icm_rrp_data [1][2];

      for( __h = 0; __h < 1; __h = __h + 1 )
      begin:local_mem_group
         for( __i1 = 0; __i1 < 2; __i1 = __i1 + 1 )
         begin:master
            // INST avm_to_ic of acl_avm_to_ic
            acl_avm_to_ic
            #(
               .DATA_W(8),
               .WRITEDATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(32),
               .BYTEENA_W(1)
            )
            avm_to_ic
            (
               // AVM avm
               .avm_enable(local_avm_aspace33_enable[__h][__i1]),
               .avm_read(local_avm_aspace33_read[__h][__i1]),
               .avm_write(local_avm_aspace33_write[__h][__i1]),
               .avm_burstcount(local_avm_aspace33_burstcount[__h][__i1]),
               .avm_address(local_avm_aspace33_address[__h][__i1]),
               .avm_writedata(local_avm_aspace33_writedata[__h][__i1]),
               .avm_byteenable(local_avm_aspace33_byteenable[__h][__i1]),
               .avm_waitrequest(local_avm_aspace33_waitrequest[__h][__i1]),
               .avm_readdata(local_avm_aspace33_readdata[__h][__i1]),
               .avm_readdatavalid(local_avm_aspace33_readdatavalid[__h][__i1]),
               .avm_writeack(local_avm_aspace33_writeack[__h][__i1]),
               // ICM ic
               .ic_arb_request(local_icm_arb_request[__h][__i1]),
               .ic_arb_enable(local_icm_arb_enable[__h][__i1]),
               .ic_arb_read(local_icm_arb_read[__h][__i1]),
               .ic_arb_write(local_icm_arb_write[__h][__i1]),
               .ic_arb_burstcount(local_icm_arb_burstcount[__h][__i1]),
               .ic_arb_address(local_icm_arb_address[__h][__i1]),
               .ic_arb_writedata(local_icm_arb_writedata[__h][__i1]),
               .ic_arb_byteenable(local_icm_arb_byteenable[__h][__i1]),
               .ic_arb_stall(local_icm_arb_stall[__h][__i1]),
               .ic_wrp_ack(local_icm_wrp_ack[__h][__i1]),
               .ic_rrp_datavalid(local_icm_rrp_datavalid[__h][__i1]),
               .ic_rrp_data(local_icm_rrp_data[__h][__i1])
            );

         end

         for( __i1 = 0; __i1 < 1; __i1 = __i1 + 1 )
         begin:bank
            logic port_enable [1:2];
            logic port_read [1:2];
            logic port_write [1:2];
            logic [7:0] port_address [1:2];
            logic [7:0] port_writedata [1:2];
            logic port_byteenable [1:2];
            logic port_waitrequest [1:2];
            logic [7:0] port_readdata [1:2];
            logic port_readdatavalid [1:2];

            // INST mem0 of acl_mem1x
            acl_mem1x
            #(
               .INTENDED_DEVICE_FAMILY("Cyclone V"),
               .DEPTH_WORDS(256),
               .WIDTH(8),
               .MEM_LATENCY(3),
               .ENABLED(0),
               .RDW_MODE("OLD_DATA"),
               .RAM_OPERATION_MODE("BIDIR_DUAL_PORT"),
               .PREFERRED_WIDTH(160),
               .RAM_BLOCK_TYPE("M10K")
            )
            mem0
            (
               .clk(clock),
               .resetn(resetn),
               // AVS avs_port1
               .avs_port1_enable(port_enable[1]),
               .avs_port1_read(port_read[1]),
               .avs_port1_write(port_write[1]),
               .avs_port1_address(port_address[1]),
               .avs_port1_writedata(port_writedata[1]),
               .avs_port1_byteenable(port_byteenable[1]),
               .avs_port1_waitrequest(port_waitrequest[1]),
               .avs_port1_readdata(port_readdata[1]),
               .avs_port1_readdatavalid(port_readdatavalid[1]),
               // AVS avs_port2
               .avs_port2_enable(port_enable[2]),
               .avs_port2_read(port_read[2]),
               .avs_port2_write(port_write[2]),
               .avs_port2_address(port_address[2]),
               .avs_port2_writedata(port_writedata[2]),
               .avs_port2_byteenable(port_byteenable[2]),
               .avs_port2_waitrequest(port_waitrequest[2]),
               .avs_port2_readdata(port_readdata[2]),
               .avs_port2_readdatavalid(port_readdatavalid[2])
            );

         end

         for( __i1 = 0; __i1 < 2; __i1 = __i1 + 1 )
         begin:router
            logic b_arb_request [1];
            logic b_arb_enable [1];
            logic b_arb_read [1];
            logic b_arb_write [1];
            logic b_arb_burstcount [1];
            logic [7:0] b_arb_address [1];
            logic [7:0] b_arb_writedata [1];
            logic b_arb_byteenable [1];
            logic b_arb_stall [1];
            logic b_wrp_ack [1];
            logic b_rrp_datavalid [1];
            logic [7:0] b_rrp_data [1];
            logic bank_select;

            // INST router of acl_ic_local_mem_router
            acl_ic_local_mem_router
            #(
               .DATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(8),
               .BYTEENA_W(1),
               .NUM_BANKS(1)
            )
            router
            (
               .clock(clock),
               .resetn(resetn),
               .bank_select(bank_select),
               // ICM m
               .m_arb_request(local_icm_arb_request[__h][__i1]),
               .m_arb_enable(local_icm_arb_enable[__h][__i1]),
               .m_arb_read(local_icm_arb_read[__h][__i1]),
               .m_arb_write(local_icm_arb_write[__h][__i1]),
               .m_arb_burstcount(local_icm_arb_burstcount[__h][__i1]),
               .m_arb_address(local_icm_arb_address[__h][__i1]),
               .m_arb_writedata(local_icm_arb_writedata[__h][__i1]),
               .m_arb_byteenable(local_icm_arb_byteenable[__h][__i1]),
               .m_arb_stall(local_icm_arb_stall[__h][__i1]),
               .m_wrp_ack(local_icm_wrp_ack[__h][__i1]),
               .m_rrp_datavalid(local_icm_rrp_datavalid[__h][__i1]),
               .m_rrp_data(local_icm_rrp_data[__h][__i1]),
               // ICM b
               .b_arb_request(b_arb_request),
               .b_arb_enable(b_arb_enable),
               .b_arb_read(b_arb_read),
               .b_arb_write(b_arb_write),
               .b_arb_burstcount(b_arb_burstcount),
               .b_arb_address(b_arb_address),
               .b_arb_writedata(b_arb_writedata),
               .b_arb_byteenable(b_arb_byteenable),
               .b_arb_stall(b_arb_stall),
               .b_wrp_ack(b_wrp_ack),
               .b_rrp_datavalid(b_rrp_datavalid),
               .b_rrp_data(b_rrp_data)
            );

            assign bank_select = 1'b1;
         end

         for( __i1 = 0; __i1 < 1; __i1 = __i1 + 1 )
         begin:port1bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[0].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[0].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[0].b_arb_read[0];
            assign icm_in_arb_write[0] = router[0].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[0].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[0].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[0].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[0].b_arb_byteenable[0];
            assign router[0].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[0].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[0].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[0].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_6
            conv_pipe_system_interconnect_6 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[1] = icm_out_arb_enable;
            assign bank[0].port_read[1] = icm_out_arb_read;
            assign bank[0].port_write[1] = icm_out_arb_write;
            assign bank[0].port_address[1] = icm_out_arb_address;
            assign bank[0].port_writedata[1] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[1] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[1];
            assign icm_out_rrp_data = bank[0].port_readdata[1];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[1];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __i1 = 0; __i1 < 1; __i1 = __i1 + 1 )
         begin:port2bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[1].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[1].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[1].b_arb_read[0];
            assign icm_in_arb_write[0] = router[1].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[1].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[1].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[1].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[1].b_arb_byteenable[0];
            assign router[1].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[1].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[1].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[1].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_7
            conv_pipe_system_interconnect_7 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[2] = icm_out_arb_enable;
            assign bank[0].port_read[2] = icm_out_arb_read;
            assign bank[0].port_write[2] = icm_out_arb_write;
            assign bank[0].port_address[2] = icm_out_arb_address;
            assign bank[0].port_writedata[2] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[2] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[2];
            assign icm_out_rrp_data = bank[0].port_readdata[2];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[2];
            assign icm_out_wrp_ack = 'b0;
         end

      end

   end
   endgenerate

   generate
   begin:local_mem_system_aspace34
      logic local_icm_arb_request [1][2];
      logic local_icm_arb_enable [1][2];
      logic local_icm_arb_read [1][2];
      logic local_icm_arb_write [1][2];
      logic local_icm_arb_burstcount [1][2];
      logic [7:0] local_icm_arb_address [1][2];
      logic [7:0] local_icm_arb_writedata [1][2];
      logic local_icm_arb_byteenable [1][2];
      logic local_icm_arb_stall [1][2];
      logic local_icm_wrp_ack [1][2];
      logic local_icm_rrp_datavalid [1][2];
      logic [7:0] local_icm_rrp_data [1][2];

      for( __i1 = 0; __i1 < 1; __i1 = __i1 + 1 )
      begin:local_mem_group
         for( __j1 = 0; __j1 < 2; __j1 = __j1 + 1 )
         begin:master
            // INST avm_to_ic of acl_avm_to_ic
            acl_avm_to_ic
            #(
               .DATA_W(8),
               .WRITEDATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(32),
               .BYTEENA_W(1)
            )
            avm_to_ic
            (
               // AVM avm
               .avm_enable(local_avm_aspace34_enable[__i1][__j1]),
               .avm_read(local_avm_aspace34_read[__i1][__j1]),
               .avm_write(local_avm_aspace34_write[__i1][__j1]),
               .avm_burstcount(local_avm_aspace34_burstcount[__i1][__j1]),
               .avm_address(local_avm_aspace34_address[__i1][__j1]),
               .avm_writedata(local_avm_aspace34_writedata[__i1][__j1]),
               .avm_byteenable(local_avm_aspace34_byteenable[__i1][__j1]),
               .avm_waitrequest(local_avm_aspace34_waitrequest[__i1][__j1]),
               .avm_readdata(local_avm_aspace34_readdata[__i1][__j1]),
               .avm_readdatavalid(local_avm_aspace34_readdatavalid[__i1][__j1]),
               .avm_writeack(local_avm_aspace34_writeack[__i1][__j1]),
               // ICM ic
               .ic_arb_request(local_icm_arb_request[__i1][__j1]),
               .ic_arb_enable(local_icm_arb_enable[__i1][__j1]),
               .ic_arb_read(local_icm_arb_read[__i1][__j1]),
               .ic_arb_write(local_icm_arb_write[__i1][__j1]),
               .ic_arb_burstcount(local_icm_arb_burstcount[__i1][__j1]),
               .ic_arb_address(local_icm_arb_address[__i1][__j1]),
               .ic_arb_writedata(local_icm_arb_writedata[__i1][__j1]),
               .ic_arb_byteenable(local_icm_arb_byteenable[__i1][__j1]),
               .ic_arb_stall(local_icm_arb_stall[__i1][__j1]),
               .ic_wrp_ack(local_icm_wrp_ack[__i1][__j1]),
               .ic_rrp_datavalid(local_icm_rrp_datavalid[__i1][__j1]),
               .ic_rrp_data(local_icm_rrp_data[__i1][__j1])
            );

         end

         for( __j1 = 0; __j1 < 1; __j1 = __j1 + 1 )
         begin:bank
            logic port_enable [1:2];
            logic port_read [1:2];
            logic port_write [1:2];
            logic [7:0] port_address [1:2];
            logic [7:0] port_writedata [1:2];
            logic port_byteenable [1:2];
            logic port_waitrequest [1:2];
            logic [7:0] port_readdata [1:2];
            logic port_readdatavalid [1:2];

            // INST mem0 of acl_mem1x
            acl_mem1x
            #(
               .INTENDED_DEVICE_FAMILY("Cyclone V"),
               .DEPTH_WORDS(256),
               .WIDTH(8),
               .MEM_LATENCY(3),
               .ENABLED(0),
               .RDW_MODE("OLD_DATA"),
               .RAM_OPERATION_MODE("BIDIR_DUAL_PORT"),
               .PREFERRED_WIDTH(160),
               .RAM_BLOCK_TYPE("M10K")
            )
            mem0
            (
               .clk(clock),
               .resetn(resetn),
               // AVS avs_port1
               .avs_port1_enable(port_enable[1]),
               .avs_port1_read(port_read[1]),
               .avs_port1_write(port_write[1]),
               .avs_port1_address(port_address[1]),
               .avs_port1_writedata(port_writedata[1]),
               .avs_port1_byteenable(port_byteenable[1]),
               .avs_port1_waitrequest(port_waitrequest[1]),
               .avs_port1_readdata(port_readdata[1]),
               .avs_port1_readdatavalid(port_readdatavalid[1]),
               // AVS avs_port2
               .avs_port2_enable(port_enable[2]),
               .avs_port2_read(port_read[2]),
               .avs_port2_write(port_write[2]),
               .avs_port2_address(port_address[2]),
               .avs_port2_writedata(port_writedata[2]),
               .avs_port2_byteenable(port_byteenable[2]),
               .avs_port2_waitrequest(port_waitrequest[2]),
               .avs_port2_readdata(port_readdata[2]),
               .avs_port2_readdatavalid(port_readdatavalid[2])
            );

         end

         for( __j1 = 0; __j1 < 2; __j1 = __j1 + 1 )
         begin:router
            logic b_arb_request [1];
            logic b_arb_enable [1];
            logic b_arb_read [1];
            logic b_arb_write [1];
            logic b_arb_burstcount [1];
            logic [7:0] b_arb_address [1];
            logic [7:0] b_arb_writedata [1];
            logic b_arb_byteenable [1];
            logic b_arb_stall [1];
            logic b_wrp_ack [1];
            logic b_rrp_datavalid [1];
            logic [7:0] b_rrp_data [1];
            logic bank_select;

            // INST router of acl_ic_local_mem_router
            acl_ic_local_mem_router
            #(
               .DATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(8),
               .BYTEENA_W(1),
               .NUM_BANKS(1)
            )
            router
            (
               .clock(clock),
               .resetn(resetn),
               .bank_select(bank_select),
               // ICM m
               .m_arb_request(local_icm_arb_request[__i1][__j1]),
               .m_arb_enable(local_icm_arb_enable[__i1][__j1]),
               .m_arb_read(local_icm_arb_read[__i1][__j1]),
               .m_arb_write(local_icm_arb_write[__i1][__j1]),
               .m_arb_burstcount(local_icm_arb_burstcount[__i1][__j1]),
               .m_arb_address(local_icm_arb_address[__i1][__j1]),
               .m_arb_writedata(local_icm_arb_writedata[__i1][__j1]),
               .m_arb_byteenable(local_icm_arb_byteenable[__i1][__j1]),
               .m_arb_stall(local_icm_arb_stall[__i1][__j1]),
               .m_wrp_ack(local_icm_wrp_ack[__i1][__j1]),
               .m_rrp_datavalid(local_icm_rrp_datavalid[__i1][__j1]),
               .m_rrp_data(local_icm_rrp_data[__i1][__j1]),
               // ICM b
               .b_arb_request(b_arb_request),
               .b_arb_enable(b_arb_enable),
               .b_arb_read(b_arb_read),
               .b_arb_write(b_arb_write),
               .b_arb_burstcount(b_arb_burstcount),
               .b_arb_address(b_arb_address),
               .b_arb_writedata(b_arb_writedata),
               .b_arb_byteenable(b_arb_byteenable),
               .b_arb_stall(b_arb_stall),
               .b_wrp_ack(b_wrp_ack),
               .b_rrp_datavalid(b_rrp_datavalid),
               .b_rrp_data(b_rrp_data)
            );

            assign bank_select = 1'b1;
         end

         for( __j1 = 0; __j1 < 1; __j1 = __j1 + 1 )
         begin:port1bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[0].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[0].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[0].b_arb_read[0];
            assign icm_in_arb_write[0] = router[0].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[0].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[0].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[0].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[0].b_arb_byteenable[0];
            assign router[0].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[0].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[0].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[0].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_6
            conv_pipe_system_interconnect_6 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[1] = icm_out_arb_enable;
            assign bank[0].port_read[1] = icm_out_arb_read;
            assign bank[0].port_write[1] = icm_out_arb_write;
            assign bank[0].port_address[1] = icm_out_arb_address;
            assign bank[0].port_writedata[1] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[1] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[1];
            assign icm_out_rrp_data = bank[0].port_readdata[1];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[1];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __j1 = 0; __j1 < 1; __j1 = __j1 + 1 )
         begin:port2bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[1].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[1].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[1].b_arb_read[0];
            assign icm_in_arb_write[0] = router[1].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[1].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[1].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[1].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[1].b_arb_byteenable[0];
            assign router[1].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[1].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[1].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[1].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_7
            conv_pipe_system_interconnect_7 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[2] = icm_out_arb_enable;
            assign bank[0].port_read[2] = icm_out_arb_read;
            assign bank[0].port_write[2] = icm_out_arb_write;
            assign bank[0].port_address[2] = icm_out_arb_address;
            assign bank[0].port_writedata[2] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[2] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[2];
            assign icm_out_rrp_data = bank[0].port_readdata[2];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[2];
            assign icm_out_wrp_ack = 'b0;
         end

      end

   end
   endgenerate

   generate
   begin:local_mem_system_aspace35
      logic local_icm_arb_request [1][2];
      logic local_icm_arb_enable [1][2];
      logic local_icm_arb_read [1][2];
      logic local_icm_arb_write [1][2];
      logic local_icm_arb_burstcount [1][2];
      logic [7:0] local_icm_arb_address [1][2];
      logic [7:0] local_icm_arb_writedata [1][2];
      logic local_icm_arb_byteenable [1][2];
      logic local_icm_arb_stall [1][2];
      logic local_icm_wrp_ack [1][2];
      logic local_icm_rrp_datavalid [1][2];
      logic [7:0] local_icm_rrp_data [1][2];

      for( __j1 = 0; __j1 < 1; __j1 = __j1 + 1 )
      begin:local_mem_group
         for( __k1 = 0; __k1 < 2; __k1 = __k1 + 1 )
         begin:master
            // INST avm_to_ic of acl_avm_to_ic
            acl_avm_to_ic
            #(
               .DATA_W(8),
               .WRITEDATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(32),
               .BYTEENA_W(1)
            )
            avm_to_ic
            (
               // AVM avm
               .avm_enable(local_avm_aspace35_enable[__j1][__k1]),
               .avm_read(local_avm_aspace35_read[__j1][__k1]),
               .avm_write(local_avm_aspace35_write[__j1][__k1]),
               .avm_burstcount(local_avm_aspace35_burstcount[__j1][__k1]),
               .avm_address(local_avm_aspace35_address[__j1][__k1]),
               .avm_writedata(local_avm_aspace35_writedata[__j1][__k1]),
               .avm_byteenable(local_avm_aspace35_byteenable[__j1][__k1]),
               .avm_waitrequest(local_avm_aspace35_waitrequest[__j1][__k1]),
               .avm_readdata(local_avm_aspace35_readdata[__j1][__k1]),
               .avm_readdatavalid(local_avm_aspace35_readdatavalid[__j1][__k1]),
               .avm_writeack(local_avm_aspace35_writeack[__j1][__k1]),
               // ICM ic
               .ic_arb_request(local_icm_arb_request[__j1][__k1]),
               .ic_arb_enable(local_icm_arb_enable[__j1][__k1]),
               .ic_arb_read(local_icm_arb_read[__j1][__k1]),
               .ic_arb_write(local_icm_arb_write[__j1][__k1]),
               .ic_arb_burstcount(local_icm_arb_burstcount[__j1][__k1]),
               .ic_arb_address(local_icm_arb_address[__j1][__k1]),
               .ic_arb_writedata(local_icm_arb_writedata[__j1][__k1]),
               .ic_arb_byteenable(local_icm_arb_byteenable[__j1][__k1]),
               .ic_arb_stall(local_icm_arb_stall[__j1][__k1]),
               .ic_wrp_ack(local_icm_wrp_ack[__j1][__k1]),
               .ic_rrp_datavalid(local_icm_rrp_datavalid[__j1][__k1]),
               .ic_rrp_data(local_icm_rrp_data[__j1][__k1])
            );

         end

         for( __k1 = 0; __k1 < 1; __k1 = __k1 + 1 )
         begin:bank
            logic port_enable [1:2];
            logic port_read [1:2];
            logic port_write [1:2];
            logic [7:0] port_address [1:2];
            logic [7:0] port_writedata [1:2];
            logic port_byteenable [1:2];
            logic port_waitrequest [1:2];
            logic [7:0] port_readdata [1:2];
            logic port_readdatavalid [1:2];

            // INST mem0 of acl_mem1x
            acl_mem1x
            #(
               .INTENDED_DEVICE_FAMILY("Cyclone V"),
               .DEPTH_WORDS(256),
               .WIDTH(8),
               .MEM_LATENCY(3),
               .ENABLED(0),
               .RDW_MODE("OLD_DATA"),
               .RAM_OPERATION_MODE("BIDIR_DUAL_PORT"),
               .PREFERRED_WIDTH(160),
               .RAM_BLOCK_TYPE("M10K")
            )
            mem0
            (
               .clk(clock),
               .resetn(resetn),
               // AVS avs_port1
               .avs_port1_enable(port_enable[1]),
               .avs_port1_read(port_read[1]),
               .avs_port1_write(port_write[1]),
               .avs_port1_address(port_address[1]),
               .avs_port1_writedata(port_writedata[1]),
               .avs_port1_byteenable(port_byteenable[1]),
               .avs_port1_waitrequest(port_waitrequest[1]),
               .avs_port1_readdata(port_readdata[1]),
               .avs_port1_readdatavalid(port_readdatavalid[1]),
               // AVS avs_port2
               .avs_port2_enable(port_enable[2]),
               .avs_port2_read(port_read[2]),
               .avs_port2_write(port_write[2]),
               .avs_port2_address(port_address[2]),
               .avs_port2_writedata(port_writedata[2]),
               .avs_port2_byteenable(port_byteenable[2]),
               .avs_port2_waitrequest(port_waitrequest[2]),
               .avs_port2_readdata(port_readdata[2]),
               .avs_port2_readdatavalid(port_readdatavalid[2])
            );

         end

         for( __k1 = 0; __k1 < 2; __k1 = __k1 + 1 )
         begin:router
            logic b_arb_request [1];
            logic b_arb_enable [1];
            logic b_arb_read [1];
            logic b_arb_write [1];
            logic b_arb_burstcount [1];
            logic [7:0] b_arb_address [1];
            logic [7:0] b_arb_writedata [1];
            logic b_arb_byteenable [1];
            logic b_arb_stall [1];
            logic b_wrp_ack [1];
            logic b_rrp_datavalid [1];
            logic [7:0] b_rrp_data [1];
            logic bank_select;

            // INST router of acl_ic_local_mem_router
            acl_ic_local_mem_router
            #(
               .DATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(8),
               .BYTEENA_W(1),
               .NUM_BANKS(1)
            )
            router
            (
               .clock(clock),
               .resetn(resetn),
               .bank_select(bank_select),
               // ICM m
               .m_arb_request(local_icm_arb_request[__j1][__k1]),
               .m_arb_enable(local_icm_arb_enable[__j1][__k1]),
               .m_arb_read(local_icm_arb_read[__j1][__k1]),
               .m_arb_write(local_icm_arb_write[__j1][__k1]),
               .m_arb_burstcount(local_icm_arb_burstcount[__j1][__k1]),
               .m_arb_address(local_icm_arb_address[__j1][__k1]),
               .m_arb_writedata(local_icm_arb_writedata[__j1][__k1]),
               .m_arb_byteenable(local_icm_arb_byteenable[__j1][__k1]),
               .m_arb_stall(local_icm_arb_stall[__j1][__k1]),
               .m_wrp_ack(local_icm_wrp_ack[__j1][__k1]),
               .m_rrp_datavalid(local_icm_rrp_datavalid[__j1][__k1]),
               .m_rrp_data(local_icm_rrp_data[__j1][__k1]),
               // ICM b
               .b_arb_request(b_arb_request),
               .b_arb_enable(b_arb_enable),
               .b_arb_read(b_arb_read),
               .b_arb_write(b_arb_write),
               .b_arb_burstcount(b_arb_burstcount),
               .b_arb_address(b_arb_address),
               .b_arb_writedata(b_arb_writedata),
               .b_arb_byteenable(b_arb_byteenable),
               .b_arb_stall(b_arb_stall),
               .b_wrp_ack(b_wrp_ack),
               .b_rrp_datavalid(b_rrp_datavalid),
               .b_rrp_data(b_rrp_data)
            );

            assign bank_select = 1'b1;
         end

         for( __k1 = 0; __k1 < 1; __k1 = __k1 + 1 )
         begin:port1bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[0].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[0].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[0].b_arb_read[0];
            assign icm_in_arb_write[0] = router[0].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[0].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[0].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[0].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[0].b_arb_byteenable[0];
            assign router[0].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[0].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[0].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[0].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_6
            conv_pipe_system_interconnect_6 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[1] = icm_out_arb_enable;
            assign bank[0].port_read[1] = icm_out_arb_read;
            assign bank[0].port_write[1] = icm_out_arb_write;
            assign bank[0].port_address[1] = icm_out_arb_address;
            assign bank[0].port_writedata[1] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[1] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[1];
            assign icm_out_rrp_data = bank[0].port_readdata[1];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[1];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __k1 = 0; __k1 < 1; __k1 = __k1 + 1 )
         begin:port2bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[1].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[1].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[1].b_arb_read[0];
            assign icm_in_arb_write[0] = router[1].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[1].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[1].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[1].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[1].b_arb_byteenable[0];
            assign router[1].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[1].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[1].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[1].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_7
            conv_pipe_system_interconnect_7 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[2] = icm_out_arb_enable;
            assign bank[0].port_read[2] = icm_out_arb_read;
            assign bank[0].port_write[2] = icm_out_arb_write;
            assign bank[0].port_address[2] = icm_out_arb_address;
            assign bank[0].port_writedata[2] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[2] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[2];
            assign icm_out_rrp_data = bank[0].port_readdata[2];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[2];
            assign icm_out_wrp_ack = 'b0;
         end

      end

   end
   endgenerate

   generate
   begin:local_mem_system_aspace36
      logic local_icm_arb_request [1][2];
      logic local_icm_arb_enable [1][2];
      logic local_icm_arb_read [1][2];
      logic local_icm_arb_write [1][2];
      logic local_icm_arb_burstcount [1][2];
      logic [7:0] local_icm_arb_address [1][2];
      logic [7:0] local_icm_arb_writedata [1][2];
      logic local_icm_arb_byteenable [1][2];
      logic local_icm_arb_stall [1][2];
      logic local_icm_wrp_ack [1][2];
      logic local_icm_rrp_datavalid [1][2];
      logic [7:0] local_icm_rrp_data [1][2];

      for( __k1 = 0; __k1 < 1; __k1 = __k1 + 1 )
      begin:local_mem_group
         for( __l1 = 0; __l1 < 2; __l1 = __l1 + 1 )
         begin:master
            // INST avm_to_ic of acl_avm_to_ic
            acl_avm_to_ic
            #(
               .DATA_W(8),
               .WRITEDATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(32),
               .BYTEENA_W(1)
            )
            avm_to_ic
            (
               // AVM avm
               .avm_enable(local_avm_aspace36_enable[__k1][__l1]),
               .avm_read(local_avm_aspace36_read[__k1][__l1]),
               .avm_write(local_avm_aspace36_write[__k1][__l1]),
               .avm_burstcount(local_avm_aspace36_burstcount[__k1][__l1]),
               .avm_address(local_avm_aspace36_address[__k1][__l1]),
               .avm_writedata(local_avm_aspace36_writedata[__k1][__l1]),
               .avm_byteenable(local_avm_aspace36_byteenable[__k1][__l1]),
               .avm_waitrequest(local_avm_aspace36_waitrequest[__k1][__l1]),
               .avm_readdata(local_avm_aspace36_readdata[__k1][__l1]),
               .avm_readdatavalid(local_avm_aspace36_readdatavalid[__k1][__l1]),
               .avm_writeack(local_avm_aspace36_writeack[__k1][__l1]),
               // ICM ic
               .ic_arb_request(local_icm_arb_request[__k1][__l1]),
               .ic_arb_enable(local_icm_arb_enable[__k1][__l1]),
               .ic_arb_read(local_icm_arb_read[__k1][__l1]),
               .ic_arb_write(local_icm_arb_write[__k1][__l1]),
               .ic_arb_burstcount(local_icm_arb_burstcount[__k1][__l1]),
               .ic_arb_address(local_icm_arb_address[__k1][__l1]),
               .ic_arb_writedata(local_icm_arb_writedata[__k1][__l1]),
               .ic_arb_byteenable(local_icm_arb_byteenable[__k1][__l1]),
               .ic_arb_stall(local_icm_arb_stall[__k1][__l1]),
               .ic_wrp_ack(local_icm_wrp_ack[__k1][__l1]),
               .ic_rrp_datavalid(local_icm_rrp_datavalid[__k1][__l1]),
               .ic_rrp_data(local_icm_rrp_data[__k1][__l1])
            );

         end

         for( __l1 = 0; __l1 < 1; __l1 = __l1 + 1 )
         begin:bank
            logic port_enable [1:2];
            logic port_read [1:2];
            logic port_write [1:2];
            logic [7:0] port_address [1:2];
            logic [7:0] port_writedata [1:2];
            logic port_byteenable [1:2];
            logic port_waitrequest [1:2];
            logic [7:0] port_readdata [1:2];
            logic port_readdatavalid [1:2];

            // INST mem0 of acl_mem1x
            acl_mem1x
            #(
               .INTENDED_DEVICE_FAMILY("Cyclone V"),
               .DEPTH_WORDS(256),
               .WIDTH(8),
               .MEM_LATENCY(3),
               .ENABLED(0),
               .RDW_MODE("OLD_DATA"),
               .RAM_OPERATION_MODE("BIDIR_DUAL_PORT"),
               .PREFERRED_WIDTH(160),
               .RAM_BLOCK_TYPE("M10K")
            )
            mem0
            (
               .clk(clock),
               .resetn(resetn),
               // AVS avs_port1
               .avs_port1_enable(port_enable[1]),
               .avs_port1_read(port_read[1]),
               .avs_port1_write(port_write[1]),
               .avs_port1_address(port_address[1]),
               .avs_port1_writedata(port_writedata[1]),
               .avs_port1_byteenable(port_byteenable[1]),
               .avs_port1_waitrequest(port_waitrequest[1]),
               .avs_port1_readdata(port_readdata[1]),
               .avs_port1_readdatavalid(port_readdatavalid[1]),
               // AVS avs_port2
               .avs_port2_enable(port_enable[2]),
               .avs_port2_read(port_read[2]),
               .avs_port2_write(port_write[2]),
               .avs_port2_address(port_address[2]),
               .avs_port2_writedata(port_writedata[2]),
               .avs_port2_byteenable(port_byteenable[2]),
               .avs_port2_waitrequest(port_waitrequest[2]),
               .avs_port2_readdata(port_readdata[2]),
               .avs_port2_readdatavalid(port_readdatavalid[2])
            );

         end

         for( __l1 = 0; __l1 < 2; __l1 = __l1 + 1 )
         begin:router
            logic b_arb_request [1];
            logic b_arb_enable [1];
            logic b_arb_read [1];
            logic b_arb_write [1];
            logic b_arb_burstcount [1];
            logic [7:0] b_arb_address [1];
            logic [7:0] b_arb_writedata [1];
            logic b_arb_byteenable [1];
            logic b_arb_stall [1];
            logic b_wrp_ack [1];
            logic b_rrp_datavalid [1];
            logic [7:0] b_rrp_data [1];
            logic bank_select;

            // INST router of acl_ic_local_mem_router
            acl_ic_local_mem_router
            #(
               .DATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(8),
               .BYTEENA_W(1),
               .NUM_BANKS(1)
            )
            router
            (
               .clock(clock),
               .resetn(resetn),
               .bank_select(bank_select),
               // ICM m
               .m_arb_request(local_icm_arb_request[__k1][__l1]),
               .m_arb_enable(local_icm_arb_enable[__k1][__l1]),
               .m_arb_read(local_icm_arb_read[__k1][__l1]),
               .m_arb_write(local_icm_arb_write[__k1][__l1]),
               .m_arb_burstcount(local_icm_arb_burstcount[__k1][__l1]),
               .m_arb_address(local_icm_arb_address[__k1][__l1]),
               .m_arb_writedata(local_icm_arb_writedata[__k1][__l1]),
               .m_arb_byteenable(local_icm_arb_byteenable[__k1][__l1]),
               .m_arb_stall(local_icm_arb_stall[__k1][__l1]),
               .m_wrp_ack(local_icm_wrp_ack[__k1][__l1]),
               .m_rrp_datavalid(local_icm_rrp_datavalid[__k1][__l1]),
               .m_rrp_data(local_icm_rrp_data[__k1][__l1]),
               // ICM b
               .b_arb_request(b_arb_request),
               .b_arb_enable(b_arb_enable),
               .b_arb_read(b_arb_read),
               .b_arb_write(b_arb_write),
               .b_arb_burstcount(b_arb_burstcount),
               .b_arb_address(b_arb_address),
               .b_arb_writedata(b_arb_writedata),
               .b_arb_byteenable(b_arb_byteenable),
               .b_arb_stall(b_arb_stall),
               .b_wrp_ack(b_wrp_ack),
               .b_rrp_datavalid(b_rrp_datavalid),
               .b_rrp_data(b_rrp_data)
            );

            assign bank_select = 1'b1;
         end

         for( __l1 = 0; __l1 < 1; __l1 = __l1 + 1 )
         begin:port1bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[0].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[0].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[0].b_arb_read[0];
            assign icm_in_arb_write[0] = router[0].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[0].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[0].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[0].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[0].b_arb_byteenable[0];
            assign router[0].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[0].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[0].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[0].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_6
            conv_pipe_system_interconnect_6 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[1] = icm_out_arb_enable;
            assign bank[0].port_read[1] = icm_out_arb_read;
            assign bank[0].port_write[1] = icm_out_arb_write;
            assign bank[0].port_address[1] = icm_out_arb_address;
            assign bank[0].port_writedata[1] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[1] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[1];
            assign icm_out_rrp_data = bank[0].port_readdata[1];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[1];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __l1 = 0; __l1 < 1; __l1 = __l1 + 1 )
         begin:port2bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[1].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[1].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[1].b_arb_read[0];
            assign icm_in_arb_write[0] = router[1].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[1].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[1].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[1].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[1].b_arb_byteenable[0];
            assign router[1].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[1].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[1].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[1].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_7
            conv_pipe_system_interconnect_7 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[2] = icm_out_arb_enable;
            assign bank[0].port_read[2] = icm_out_arb_read;
            assign bank[0].port_write[2] = icm_out_arb_write;
            assign bank[0].port_address[2] = icm_out_arb_address;
            assign bank[0].port_writedata[2] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[2] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[2];
            assign icm_out_rrp_data = bank[0].port_readdata[2];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[2];
            assign icm_out_wrp_ack = 'b0;
         end

      end

   end
   endgenerate

   generate
   begin:local_mem_system_aspace37
      logic local_icm_arb_request [1][2];
      logic local_icm_arb_enable [1][2];
      logic local_icm_arb_read [1][2];
      logic local_icm_arb_write [1][2];
      logic local_icm_arb_burstcount [1][2];
      logic [7:0] local_icm_arb_address [1][2];
      logic [7:0] local_icm_arb_writedata [1][2];
      logic local_icm_arb_byteenable [1][2];
      logic local_icm_arb_stall [1][2];
      logic local_icm_wrp_ack [1][2];
      logic local_icm_rrp_datavalid [1][2];
      logic [7:0] local_icm_rrp_data [1][2];

      for( __l1 = 0; __l1 < 1; __l1 = __l1 + 1 )
      begin:local_mem_group
         for( __m1 = 0; __m1 < 2; __m1 = __m1 + 1 )
         begin:master
            // INST avm_to_ic of acl_avm_to_ic
            acl_avm_to_ic
            #(
               .DATA_W(8),
               .WRITEDATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(32),
               .BYTEENA_W(1)
            )
            avm_to_ic
            (
               // AVM avm
               .avm_enable(local_avm_aspace37_enable[__l1][__m1]),
               .avm_read(local_avm_aspace37_read[__l1][__m1]),
               .avm_write(local_avm_aspace37_write[__l1][__m1]),
               .avm_burstcount(local_avm_aspace37_burstcount[__l1][__m1]),
               .avm_address(local_avm_aspace37_address[__l1][__m1]),
               .avm_writedata(local_avm_aspace37_writedata[__l1][__m1]),
               .avm_byteenable(local_avm_aspace37_byteenable[__l1][__m1]),
               .avm_waitrequest(local_avm_aspace37_waitrequest[__l1][__m1]),
               .avm_readdata(local_avm_aspace37_readdata[__l1][__m1]),
               .avm_readdatavalid(local_avm_aspace37_readdatavalid[__l1][__m1]),
               .avm_writeack(local_avm_aspace37_writeack[__l1][__m1]),
               // ICM ic
               .ic_arb_request(local_icm_arb_request[__l1][__m1]),
               .ic_arb_enable(local_icm_arb_enable[__l1][__m1]),
               .ic_arb_read(local_icm_arb_read[__l1][__m1]),
               .ic_arb_write(local_icm_arb_write[__l1][__m1]),
               .ic_arb_burstcount(local_icm_arb_burstcount[__l1][__m1]),
               .ic_arb_address(local_icm_arb_address[__l1][__m1]),
               .ic_arb_writedata(local_icm_arb_writedata[__l1][__m1]),
               .ic_arb_byteenable(local_icm_arb_byteenable[__l1][__m1]),
               .ic_arb_stall(local_icm_arb_stall[__l1][__m1]),
               .ic_wrp_ack(local_icm_wrp_ack[__l1][__m1]),
               .ic_rrp_datavalid(local_icm_rrp_datavalid[__l1][__m1]),
               .ic_rrp_data(local_icm_rrp_data[__l1][__m1])
            );

         end

         for( __m1 = 0; __m1 < 1; __m1 = __m1 + 1 )
         begin:bank
            logic port_enable [1:2];
            logic port_read [1:2];
            logic port_write [1:2];
            logic [7:0] port_address [1:2];
            logic [7:0] port_writedata [1:2];
            logic port_byteenable [1:2];
            logic port_waitrequest [1:2];
            logic [7:0] port_readdata [1:2];
            logic port_readdatavalid [1:2];

            // INST mem0 of acl_mem1x
            acl_mem1x
            #(
               .INTENDED_DEVICE_FAMILY("Cyclone V"),
               .DEPTH_WORDS(256),
               .WIDTH(8),
               .MEM_LATENCY(3),
               .ENABLED(0),
               .RDW_MODE("OLD_DATA"),
               .RAM_OPERATION_MODE("BIDIR_DUAL_PORT"),
               .PREFERRED_WIDTH(160),
               .RAM_BLOCK_TYPE("M10K")
            )
            mem0
            (
               .clk(clock),
               .resetn(resetn),
               // AVS avs_port1
               .avs_port1_enable(port_enable[1]),
               .avs_port1_read(port_read[1]),
               .avs_port1_write(port_write[1]),
               .avs_port1_address(port_address[1]),
               .avs_port1_writedata(port_writedata[1]),
               .avs_port1_byteenable(port_byteenable[1]),
               .avs_port1_waitrequest(port_waitrequest[1]),
               .avs_port1_readdata(port_readdata[1]),
               .avs_port1_readdatavalid(port_readdatavalid[1]),
               // AVS avs_port2
               .avs_port2_enable(port_enable[2]),
               .avs_port2_read(port_read[2]),
               .avs_port2_write(port_write[2]),
               .avs_port2_address(port_address[2]),
               .avs_port2_writedata(port_writedata[2]),
               .avs_port2_byteenable(port_byteenable[2]),
               .avs_port2_waitrequest(port_waitrequest[2]),
               .avs_port2_readdata(port_readdata[2]),
               .avs_port2_readdatavalid(port_readdatavalid[2])
            );

         end

         for( __m1 = 0; __m1 < 2; __m1 = __m1 + 1 )
         begin:router
            logic b_arb_request [1];
            logic b_arb_enable [1];
            logic b_arb_read [1];
            logic b_arb_write [1];
            logic b_arb_burstcount [1];
            logic [7:0] b_arb_address [1];
            logic [7:0] b_arb_writedata [1];
            logic b_arb_byteenable [1];
            logic b_arb_stall [1];
            logic b_wrp_ack [1];
            logic b_rrp_datavalid [1];
            logic [7:0] b_rrp_data [1];
            logic bank_select;

            // INST router of acl_ic_local_mem_router
            acl_ic_local_mem_router
            #(
               .DATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(8),
               .BYTEENA_W(1),
               .NUM_BANKS(1)
            )
            router
            (
               .clock(clock),
               .resetn(resetn),
               .bank_select(bank_select),
               // ICM m
               .m_arb_request(local_icm_arb_request[__l1][__m1]),
               .m_arb_enable(local_icm_arb_enable[__l1][__m1]),
               .m_arb_read(local_icm_arb_read[__l1][__m1]),
               .m_arb_write(local_icm_arb_write[__l1][__m1]),
               .m_arb_burstcount(local_icm_arb_burstcount[__l1][__m1]),
               .m_arb_address(local_icm_arb_address[__l1][__m1]),
               .m_arb_writedata(local_icm_arb_writedata[__l1][__m1]),
               .m_arb_byteenable(local_icm_arb_byteenable[__l1][__m1]),
               .m_arb_stall(local_icm_arb_stall[__l1][__m1]),
               .m_wrp_ack(local_icm_wrp_ack[__l1][__m1]),
               .m_rrp_datavalid(local_icm_rrp_datavalid[__l1][__m1]),
               .m_rrp_data(local_icm_rrp_data[__l1][__m1]),
               // ICM b
               .b_arb_request(b_arb_request),
               .b_arb_enable(b_arb_enable),
               .b_arb_read(b_arb_read),
               .b_arb_write(b_arb_write),
               .b_arb_burstcount(b_arb_burstcount),
               .b_arb_address(b_arb_address),
               .b_arb_writedata(b_arb_writedata),
               .b_arb_byteenable(b_arb_byteenable),
               .b_arb_stall(b_arb_stall),
               .b_wrp_ack(b_wrp_ack),
               .b_rrp_datavalid(b_rrp_datavalid),
               .b_rrp_data(b_rrp_data)
            );

            assign bank_select = 1'b1;
         end

         for( __m1 = 0; __m1 < 1; __m1 = __m1 + 1 )
         begin:port1bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[0].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[0].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[0].b_arb_read[0];
            assign icm_in_arb_write[0] = router[0].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[0].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[0].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[0].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[0].b_arb_byteenable[0];
            assign router[0].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[0].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[0].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[0].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_6
            conv_pipe_system_interconnect_6 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[1] = icm_out_arb_enable;
            assign bank[0].port_read[1] = icm_out_arb_read;
            assign bank[0].port_write[1] = icm_out_arb_write;
            assign bank[0].port_address[1] = icm_out_arb_address;
            assign bank[0].port_writedata[1] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[1] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[1];
            assign icm_out_rrp_data = bank[0].port_readdata[1];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[1];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __m1 = 0; __m1 < 1; __m1 = __m1 + 1 )
         begin:port2bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[1].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[1].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[1].b_arb_read[0];
            assign icm_in_arb_write[0] = router[1].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[1].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[1].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[1].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[1].b_arb_byteenable[0];
            assign router[1].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[1].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[1].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[1].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_7
            conv_pipe_system_interconnect_7 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[2] = icm_out_arb_enable;
            assign bank[0].port_read[2] = icm_out_arb_read;
            assign bank[0].port_write[2] = icm_out_arb_write;
            assign bank[0].port_address[2] = icm_out_arb_address;
            assign bank[0].port_writedata[2] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[2] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[2];
            assign icm_out_rrp_data = bank[0].port_readdata[2];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[2];
            assign icm_out_wrp_ack = 'b0;
         end

      end

   end
   endgenerate

   generate
   begin:local_mem_system_aspace38
      logic local_icm_arb_request [1][2];
      logic local_icm_arb_enable [1][2];
      logic local_icm_arb_read [1][2];
      logic local_icm_arb_write [1][2];
      logic local_icm_arb_burstcount [1][2];
      logic [7:0] local_icm_arb_address [1][2];
      logic [7:0] local_icm_arb_writedata [1][2];
      logic local_icm_arb_byteenable [1][2];
      logic local_icm_arb_stall [1][2];
      logic local_icm_wrp_ack [1][2];
      logic local_icm_rrp_datavalid [1][2];
      logic [7:0] local_icm_rrp_data [1][2];

      for( __m1 = 0; __m1 < 1; __m1 = __m1 + 1 )
      begin:local_mem_group
         for( __n1 = 0; __n1 < 2; __n1 = __n1 + 1 )
         begin:master
            // INST avm_to_ic of acl_avm_to_ic
            acl_avm_to_ic
            #(
               .DATA_W(8),
               .WRITEDATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(32),
               .BYTEENA_W(1)
            )
            avm_to_ic
            (
               // AVM avm
               .avm_enable(local_avm_aspace38_enable[__m1][__n1]),
               .avm_read(local_avm_aspace38_read[__m1][__n1]),
               .avm_write(local_avm_aspace38_write[__m1][__n1]),
               .avm_burstcount(local_avm_aspace38_burstcount[__m1][__n1]),
               .avm_address(local_avm_aspace38_address[__m1][__n1]),
               .avm_writedata(local_avm_aspace38_writedata[__m1][__n1]),
               .avm_byteenable(local_avm_aspace38_byteenable[__m1][__n1]),
               .avm_waitrequest(local_avm_aspace38_waitrequest[__m1][__n1]),
               .avm_readdata(local_avm_aspace38_readdata[__m1][__n1]),
               .avm_readdatavalid(local_avm_aspace38_readdatavalid[__m1][__n1]),
               .avm_writeack(local_avm_aspace38_writeack[__m1][__n1]),
               // ICM ic
               .ic_arb_request(local_icm_arb_request[__m1][__n1]),
               .ic_arb_enable(local_icm_arb_enable[__m1][__n1]),
               .ic_arb_read(local_icm_arb_read[__m1][__n1]),
               .ic_arb_write(local_icm_arb_write[__m1][__n1]),
               .ic_arb_burstcount(local_icm_arb_burstcount[__m1][__n1]),
               .ic_arb_address(local_icm_arb_address[__m1][__n1]),
               .ic_arb_writedata(local_icm_arb_writedata[__m1][__n1]),
               .ic_arb_byteenable(local_icm_arb_byteenable[__m1][__n1]),
               .ic_arb_stall(local_icm_arb_stall[__m1][__n1]),
               .ic_wrp_ack(local_icm_wrp_ack[__m1][__n1]),
               .ic_rrp_datavalid(local_icm_rrp_datavalid[__m1][__n1]),
               .ic_rrp_data(local_icm_rrp_data[__m1][__n1])
            );

         end

         for( __n1 = 0; __n1 < 1; __n1 = __n1 + 1 )
         begin:bank
            logic port_enable [1:2];
            logic port_read [1:2];
            logic port_write [1:2];
            logic [7:0] port_address [1:2];
            logic [7:0] port_writedata [1:2];
            logic port_byteenable [1:2];
            logic port_waitrequest [1:2];
            logic [7:0] port_readdata [1:2];
            logic port_readdatavalid [1:2];

            // INST mem0 of acl_mem1x
            acl_mem1x
            #(
               .INTENDED_DEVICE_FAMILY("Cyclone V"),
               .DEPTH_WORDS(256),
               .WIDTH(8),
               .MEM_LATENCY(3),
               .ENABLED(0),
               .RDW_MODE("OLD_DATA"),
               .RAM_OPERATION_MODE("BIDIR_DUAL_PORT"),
               .PREFERRED_WIDTH(160),
               .RAM_BLOCK_TYPE("M10K")
            )
            mem0
            (
               .clk(clock),
               .resetn(resetn),
               // AVS avs_port1
               .avs_port1_enable(port_enable[1]),
               .avs_port1_read(port_read[1]),
               .avs_port1_write(port_write[1]),
               .avs_port1_address(port_address[1]),
               .avs_port1_writedata(port_writedata[1]),
               .avs_port1_byteenable(port_byteenable[1]),
               .avs_port1_waitrequest(port_waitrequest[1]),
               .avs_port1_readdata(port_readdata[1]),
               .avs_port1_readdatavalid(port_readdatavalid[1]),
               // AVS avs_port2
               .avs_port2_enable(port_enable[2]),
               .avs_port2_read(port_read[2]),
               .avs_port2_write(port_write[2]),
               .avs_port2_address(port_address[2]),
               .avs_port2_writedata(port_writedata[2]),
               .avs_port2_byteenable(port_byteenable[2]),
               .avs_port2_waitrequest(port_waitrequest[2]),
               .avs_port2_readdata(port_readdata[2]),
               .avs_port2_readdatavalid(port_readdatavalid[2])
            );

         end

         for( __n1 = 0; __n1 < 2; __n1 = __n1 + 1 )
         begin:router
            logic b_arb_request [1];
            logic b_arb_enable [1];
            logic b_arb_read [1];
            logic b_arb_write [1];
            logic b_arb_burstcount [1];
            logic [7:0] b_arb_address [1];
            logic [7:0] b_arb_writedata [1];
            logic b_arb_byteenable [1];
            logic b_arb_stall [1];
            logic b_wrp_ack [1];
            logic b_rrp_datavalid [1];
            logic [7:0] b_rrp_data [1];
            logic bank_select;

            // INST router of acl_ic_local_mem_router
            acl_ic_local_mem_router
            #(
               .DATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(8),
               .BYTEENA_W(1),
               .NUM_BANKS(1)
            )
            router
            (
               .clock(clock),
               .resetn(resetn),
               .bank_select(bank_select),
               // ICM m
               .m_arb_request(local_icm_arb_request[__m1][__n1]),
               .m_arb_enable(local_icm_arb_enable[__m1][__n1]),
               .m_arb_read(local_icm_arb_read[__m1][__n1]),
               .m_arb_write(local_icm_arb_write[__m1][__n1]),
               .m_arb_burstcount(local_icm_arb_burstcount[__m1][__n1]),
               .m_arb_address(local_icm_arb_address[__m1][__n1]),
               .m_arb_writedata(local_icm_arb_writedata[__m1][__n1]),
               .m_arb_byteenable(local_icm_arb_byteenable[__m1][__n1]),
               .m_arb_stall(local_icm_arb_stall[__m1][__n1]),
               .m_wrp_ack(local_icm_wrp_ack[__m1][__n1]),
               .m_rrp_datavalid(local_icm_rrp_datavalid[__m1][__n1]),
               .m_rrp_data(local_icm_rrp_data[__m1][__n1]),
               // ICM b
               .b_arb_request(b_arb_request),
               .b_arb_enable(b_arb_enable),
               .b_arb_read(b_arb_read),
               .b_arb_write(b_arb_write),
               .b_arb_burstcount(b_arb_burstcount),
               .b_arb_address(b_arb_address),
               .b_arb_writedata(b_arb_writedata),
               .b_arb_byteenable(b_arb_byteenable),
               .b_arb_stall(b_arb_stall),
               .b_wrp_ack(b_wrp_ack),
               .b_rrp_datavalid(b_rrp_datavalid),
               .b_rrp_data(b_rrp_data)
            );

            assign bank_select = 1'b1;
         end

         for( __n1 = 0; __n1 < 1; __n1 = __n1 + 1 )
         begin:port1bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[0].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[0].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[0].b_arb_read[0];
            assign icm_in_arb_write[0] = router[0].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[0].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[0].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[0].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[0].b_arb_byteenable[0];
            assign router[0].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[0].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[0].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[0].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_6
            conv_pipe_system_interconnect_6 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[1] = icm_out_arb_enable;
            assign bank[0].port_read[1] = icm_out_arb_read;
            assign bank[0].port_write[1] = icm_out_arb_write;
            assign bank[0].port_address[1] = icm_out_arb_address;
            assign bank[0].port_writedata[1] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[1] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[1];
            assign icm_out_rrp_data = bank[0].port_readdata[1];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[1];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __n1 = 0; __n1 < 1; __n1 = __n1 + 1 )
         begin:port2bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[1].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[1].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[1].b_arb_read[0];
            assign icm_in_arb_write[0] = router[1].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[1].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[1].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[1].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[1].b_arb_byteenable[0];
            assign router[1].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[1].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[1].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[1].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_7
            conv_pipe_system_interconnect_7 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[2] = icm_out_arb_enable;
            assign bank[0].port_read[2] = icm_out_arb_read;
            assign bank[0].port_write[2] = icm_out_arb_write;
            assign bank[0].port_address[2] = icm_out_arb_address;
            assign bank[0].port_writedata[2] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[2] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[2];
            assign icm_out_rrp_data = bank[0].port_readdata[2];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[2];
            assign icm_out_wrp_ack = 'b0;
         end

      end

   end
   endgenerate

   generate
   begin:local_mem_system_aspace39
      logic local_icm_arb_request [1][2];
      logic local_icm_arb_enable [1][2];
      logic local_icm_arb_read [1][2];
      logic local_icm_arb_write [1][2];
      logic local_icm_arb_burstcount [1][2];
      logic [7:0] local_icm_arb_address [1][2];
      logic [7:0] local_icm_arb_writedata [1][2];
      logic local_icm_arb_byteenable [1][2];
      logic local_icm_arb_stall [1][2];
      logic local_icm_wrp_ack [1][2];
      logic local_icm_rrp_datavalid [1][2];
      logic [7:0] local_icm_rrp_data [1][2];

      for( __n1 = 0; __n1 < 1; __n1 = __n1 + 1 )
      begin:local_mem_group
         for( __o1 = 0; __o1 < 2; __o1 = __o1 + 1 )
         begin:master
            // INST avm_to_ic of acl_avm_to_ic
            acl_avm_to_ic
            #(
               .DATA_W(8),
               .WRITEDATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(32),
               .BYTEENA_W(1)
            )
            avm_to_ic
            (
               // AVM avm
               .avm_enable(local_avm_aspace39_enable[__n1][__o1]),
               .avm_read(local_avm_aspace39_read[__n1][__o1]),
               .avm_write(local_avm_aspace39_write[__n1][__o1]),
               .avm_burstcount(local_avm_aspace39_burstcount[__n1][__o1]),
               .avm_address(local_avm_aspace39_address[__n1][__o1]),
               .avm_writedata(local_avm_aspace39_writedata[__n1][__o1]),
               .avm_byteenable(local_avm_aspace39_byteenable[__n1][__o1]),
               .avm_waitrequest(local_avm_aspace39_waitrequest[__n1][__o1]),
               .avm_readdata(local_avm_aspace39_readdata[__n1][__o1]),
               .avm_readdatavalid(local_avm_aspace39_readdatavalid[__n1][__o1]),
               .avm_writeack(local_avm_aspace39_writeack[__n1][__o1]),
               // ICM ic
               .ic_arb_request(local_icm_arb_request[__n1][__o1]),
               .ic_arb_enable(local_icm_arb_enable[__n1][__o1]),
               .ic_arb_read(local_icm_arb_read[__n1][__o1]),
               .ic_arb_write(local_icm_arb_write[__n1][__o1]),
               .ic_arb_burstcount(local_icm_arb_burstcount[__n1][__o1]),
               .ic_arb_address(local_icm_arb_address[__n1][__o1]),
               .ic_arb_writedata(local_icm_arb_writedata[__n1][__o1]),
               .ic_arb_byteenable(local_icm_arb_byteenable[__n1][__o1]),
               .ic_arb_stall(local_icm_arb_stall[__n1][__o1]),
               .ic_wrp_ack(local_icm_wrp_ack[__n1][__o1]),
               .ic_rrp_datavalid(local_icm_rrp_datavalid[__n1][__o1]),
               .ic_rrp_data(local_icm_rrp_data[__n1][__o1])
            );

         end

         for( __o1 = 0; __o1 < 1; __o1 = __o1 + 1 )
         begin:bank
            logic port_enable [1:2];
            logic port_read [1:2];
            logic port_write [1:2];
            logic [7:0] port_address [1:2];
            logic [7:0] port_writedata [1:2];
            logic port_byteenable [1:2];
            logic port_waitrequest [1:2];
            logic [7:0] port_readdata [1:2];
            logic port_readdatavalid [1:2];

            // INST mem0 of acl_mem1x
            acl_mem1x
            #(
               .INTENDED_DEVICE_FAMILY("Cyclone V"),
               .DEPTH_WORDS(256),
               .WIDTH(8),
               .MEM_LATENCY(3),
               .ENABLED(0),
               .RDW_MODE("OLD_DATA"),
               .RAM_OPERATION_MODE("BIDIR_DUAL_PORT"),
               .PREFERRED_WIDTH(160),
               .RAM_BLOCK_TYPE("M10K")
            )
            mem0
            (
               .clk(clock),
               .resetn(resetn),
               // AVS avs_port1
               .avs_port1_enable(port_enable[1]),
               .avs_port1_read(port_read[1]),
               .avs_port1_write(port_write[1]),
               .avs_port1_address(port_address[1]),
               .avs_port1_writedata(port_writedata[1]),
               .avs_port1_byteenable(port_byteenable[1]),
               .avs_port1_waitrequest(port_waitrequest[1]),
               .avs_port1_readdata(port_readdata[1]),
               .avs_port1_readdatavalid(port_readdatavalid[1]),
               // AVS avs_port2
               .avs_port2_enable(port_enable[2]),
               .avs_port2_read(port_read[2]),
               .avs_port2_write(port_write[2]),
               .avs_port2_address(port_address[2]),
               .avs_port2_writedata(port_writedata[2]),
               .avs_port2_byteenable(port_byteenable[2]),
               .avs_port2_waitrequest(port_waitrequest[2]),
               .avs_port2_readdata(port_readdata[2]),
               .avs_port2_readdatavalid(port_readdatavalid[2])
            );

         end

         for( __o1 = 0; __o1 < 2; __o1 = __o1 + 1 )
         begin:router
            logic b_arb_request [1];
            logic b_arb_enable [1];
            logic b_arb_read [1];
            logic b_arb_write [1];
            logic b_arb_burstcount [1];
            logic [7:0] b_arb_address [1];
            logic [7:0] b_arb_writedata [1];
            logic b_arb_byteenable [1];
            logic b_arb_stall [1];
            logic b_wrp_ack [1];
            logic b_rrp_datavalid [1];
            logic [7:0] b_rrp_data [1];
            logic bank_select;

            // INST router of acl_ic_local_mem_router
            acl_ic_local_mem_router
            #(
               .DATA_W(8),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(8),
               .BYTEENA_W(1),
               .NUM_BANKS(1)
            )
            router
            (
               .clock(clock),
               .resetn(resetn),
               .bank_select(bank_select),
               // ICM m
               .m_arb_request(local_icm_arb_request[__n1][__o1]),
               .m_arb_enable(local_icm_arb_enable[__n1][__o1]),
               .m_arb_read(local_icm_arb_read[__n1][__o1]),
               .m_arb_write(local_icm_arb_write[__n1][__o1]),
               .m_arb_burstcount(local_icm_arb_burstcount[__n1][__o1]),
               .m_arb_address(local_icm_arb_address[__n1][__o1]),
               .m_arb_writedata(local_icm_arb_writedata[__n1][__o1]),
               .m_arb_byteenable(local_icm_arb_byteenable[__n1][__o1]),
               .m_arb_stall(local_icm_arb_stall[__n1][__o1]),
               .m_wrp_ack(local_icm_wrp_ack[__n1][__o1]),
               .m_rrp_datavalid(local_icm_rrp_datavalid[__n1][__o1]),
               .m_rrp_data(local_icm_rrp_data[__n1][__o1]),
               // ICM b
               .b_arb_request(b_arb_request),
               .b_arb_enable(b_arb_enable),
               .b_arb_read(b_arb_read),
               .b_arb_write(b_arb_write),
               .b_arb_burstcount(b_arb_burstcount),
               .b_arb_address(b_arb_address),
               .b_arb_writedata(b_arb_writedata),
               .b_arb_byteenable(b_arb_byteenable),
               .b_arb_stall(b_arb_stall),
               .b_wrp_ack(b_wrp_ack),
               .b_rrp_datavalid(b_rrp_datavalid),
               .b_rrp_data(b_rrp_data)
            );

            assign bank_select = 1'b1;
         end

         for( __o1 = 0; __o1 < 1; __o1 = __o1 + 1 )
         begin:port1bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[0].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[0].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[0].b_arb_read[0];
            assign icm_in_arb_write[0] = router[0].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[0].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[0].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[0].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[0].b_arb_byteenable[0];
            assign router[0].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[0].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[0].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[0].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_6
            conv_pipe_system_interconnect_6 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[1] = icm_out_arb_enable;
            assign bank[0].port_read[1] = icm_out_arb_read;
            assign bank[0].port_write[1] = icm_out_arb_write;
            assign bank[0].port_address[1] = icm_out_arb_address;
            assign bank[0].port_writedata[1] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[1] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[1];
            assign icm_out_rrp_data = bank[0].port_readdata[1];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[1];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __o1 = 0; __o1 < 1; __o1 = __o1 + 1 )
         begin:port2bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [7:0] icm_in_arb_address [1];
            logic [7:0] icm_in_arb_writedata [1];
            logic icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [7:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [7:0] icm_out_arb_address;
            logic [7:0] icm_out_arb_writedata;
            logic icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [7:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[1].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[1].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[1].b_arb_read[0];
            assign icm_in_arb_write[0] = router[1].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[1].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[1].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[1].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[1].b_arb_byteenable[0];
            assign router[1].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[1].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[1].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[1].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_7
            conv_pipe_system_interconnect_7 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[2] = icm_out_arb_enable;
            assign bank[0].port_read[2] = icm_out_arb_read;
            assign bank[0].port_write[2] = icm_out_arb_write;
            assign bank[0].port_address[2] = icm_out_arb_address;
            assign bank[0].port_writedata[2] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[2] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[2];
            assign icm_out_rrp_data = bank[0].port_readdata[2];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[2];
            assign icm_out_wrp_ack = 'b0;
         end

      end

   end
   endgenerate

endmodule

/////////////////////////////////////////////////////////////////
// MODULE memRead_top_wrapper_0
/////////////////////////////////////////////////////////////////
module memRead_top_wrapper_0
(
   input logic start,
   input logic [447:0] kernel_arguments,
   output logic kernel_valid_in,
   output logic kernel_valid_out,
   output logic has_a_write_pending,
   output logic has_a_lsu_active,
   input logic clock,
   input logic resetn,
   input logic clock2x,
   // AVM avm_local_bb10_ld__inst0
   output logic avm_local_bb10_ld__inst0_enable,
   output logic avm_local_bb10_ld__inst0_read,
   output logic avm_local_bb10_ld__inst0_write,
   output logic [4:0] avm_local_bb10_ld__inst0_burstcount,
   output logic [29:0] avm_local_bb10_ld__inst0_address,
   output logic [255:0] avm_local_bb10_ld__inst0_writedata,
   output logic [31:0] avm_local_bb10_ld__inst0_byteenable,
   input logic avm_local_bb10_ld__inst0_waitrequest,
   input logic [255:0] avm_local_bb10_ld__inst0_readdata,
   input logic avm_local_bb10_ld__inst0_readdatavalid,
   input logic avm_local_bb10_ld__inst0_writeack,
   // AVM avm_local_bb10_ld_memcoalesce_bias_load_0_inst0
   output logic avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_enable,
   output logic avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_read,
   output logic avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_write,
   output logic [4:0] avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_burstcount,
   output logic [29:0] avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_address,
   output logic [255:0] avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_writedata,
   output logic [31:0] avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_byteenable,
   input logic avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_waitrequest,
   input logic [255:0] avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_readdata,
   input logic avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_readdatavalid,
   input logic avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_writeack,
   // AVM avm_local_bb10_ld_memcoalesce_weights_load_0_inst0
   output logic avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_enable,
   output logic avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_read,
   output logic avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_write,
   output logic [4:0] avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_burstcount,
   output logic [29:0] avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_address,
   output logic [255:0] avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_writedata,
   output logic [31:0] avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_byteenable,
   input logic avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_waitrequest,
   input logic [255:0] avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_readdata,
   input logic avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_readdatavalid,
   input logic avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_writeack,
   // AVM avm_local_bb4_ld__inst0
   output logic avm_local_bb4_ld__inst0_enable,
   output logic avm_local_bb4_ld__inst0_read,
   output logic avm_local_bb4_ld__inst0_write,
   output logic [4:0] avm_local_bb4_ld__inst0_burstcount,
   output logic [29:0] avm_local_bb4_ld__inst0_address,
   output logic [255:0] avm_local_bb4_ld__inst0_writedata,
   output logic [31:0] avm_local_bb4_ld__inst0_byteenable,
   input logic avm_local_bb4_ld__inst0_waitrequest,
   input logic [255:0] avm_local_bb4_ld__inst0_readdata,
   input logic avm_local_bb4_ld__inst0_readdatavalid,
   input logic avm_local_bb4_ld__inst0_writeack,
   // AVST avm_channel_id_bias_ch_write
   output logic avm_channel_id_bias_ch_write_valid,
   input logic avm_channel_id_bias_ch_write_ready,
   output logic [127:0] avm_channel_id_bias_ch_write_data,
   input logic avm_channel_id_bias_ch_write_almostfull,
   // AVST avm_channel_id_data_ch_write
   output logic avm_channel_id_data_ch_write_valid,
   input logic avm_channel_id_data_ch_write_ready,
   output logic [511:0] avm_channel_id_data_ch_write_data,
   input logic avm_channel_id_data_ch_write_almostfull,
   // AVST avm_channel_id_weight_ch_write
   output logic avm_channel_id_weight_ch_write_valid,
   input logic avm_channel_id_weight_ch_write_ready,
   output logic [511:0] avm_channel_id_weight_ch_write_data,
   input logic avm_channel_id_weight_ch_write_almostfull
);
   genvar __i;
   genvar __j;
   genvar __k;
   logic lmem_invalid_single_bit;
   logic [1:0] lmem_invalid_aspaces;
   logic local_avm_aspace5_enable [1][3];
   logic local_avm_aspace5_read [1][3];
   logic local_avm_aspace5_write [1][3];
   logic local_avm_aspace5_burstcount [1][3];
   logic [31:0] local_avm_aspace5_address [1][3];
   logic [31:0] local_avm_aspace5_writedata [1][3];
   logic [3:0] local_avm_aspace5_byteenable [1][3];
   logic local_avm_aspace5_waitrequest [1][3];
   logic [31:0] local_avm_aspace5_readdata [1][3];
   logic local_avm_aspace5_readdatavalid [1][3];
   logic local_avm_aspace5_writeack [1][3];
   logic local_avm_aspace6_enable [1][2];
   logic local_avm_aspace6_read [1][2];
   logic local_avm_aspace6_write [1][2];
   logic local_avm_aspace6_burstcount [1][2];
   logic [31:0] local_avm_aspace6_address [1][2];
   logic [511:0] local_avm_aspace6_writedata [1][2];
   logic [63:0] local_avm_aspace6_byteenable [1][2];
   logic local_avm_aspace6_waitrequest [1][2];
   logic [511:0] local_avm_aspace6_readdata [1][2];
   logic local_avm_aspace6_readdatavalid [1][2];
   logic local_avm_aspace6_writeack [1][2];

   // INST kernel of memRead_function_wrapper
   memRead_function_wrapper kernel
   (
      .local_router_hang(lmem_invalid_single_bit),
      .start(start),
      .kernel_arguments(kernel_arguments),
      .kernel_valid_in(kernel_valid_in),
      .kernel_valid_out(kernel_valid_out),
      .has_a_write_pending(has_a_write_pending),
      .has_a_lsu_active(has_a_lsu_active),
      .clock(clock),
      .resetn(resetn),
      .clock2x(clock2x),
      // AVM avm_local_bb10_ld__inst0
      .avm_local_bb10_ld__inst0_enable(avm_local_bb10_ld__inst0_enable),
      .avm_local_bb10_ld__inst0_read(avm_local_bb10_ld__inst0_read),
      .avm_local_bb10_ld__inst0_write(avm_local_bb10_ld__inst0_write),
      .avm_local_bb10_ld__inst0_burstcount(avm_local_bb10_ld__inst0_burstcount),
      .avm_local_bb10_ld__inst0_address(avm_local_bb10_ld__inst0_address),
      .avm_local_bb10_ld__inst0_writedata(avm_local_bb10_ld__inst0_writedata),
      .avm_local_bb10_ld__inst0_byteenable(avm_local_bb10_ld__inst0_byteenable),
      .avm_local_bb10_ld__inst0_waitrequest(avm_local_bb10_ld__inst0_waitrequest),
      .avm_local_bb10_ld__inst0_readdata(avm_local_bb10_ld__inst0_readdata),
      .avm_local_bb10_ld__inst0_readdatavalid(avm_local_bb10_ld__inst0_readdatavalid),
      .avm_local_bb10_ld__inst0_writeack(avm_local_bb10_ld__inst0_writeack),
      // AVM avm_local_bb10_ld_memcoalesce_bias_load_0_inst0
      .avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_enable(avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_enable),
      .avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_read(avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_read),
      .avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_write(avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_write),
      .avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_burstcount(avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_burstcount),
      .avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_address(avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_address),
      .avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_writedata(avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_writedata),
      .avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_byteenable(avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_byteenable),
      .avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_waitrequest(avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_waitrequest),
      .avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_readdata(avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_readdata),
      .avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_readdatavalid(avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_readdatavalid),
      .avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_writeack(avm_local_bb10_ld_memcoalesce_bias_load_0_inst0_writeack),
      // AVM avm_local_bb10_ld_memcoalesce_weights_load_0_inst0
      .avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_enable(avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_enable),
      .avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_read(avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_read),
      .avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_write(avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_write),
      .avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_burstcount(avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_burstcount),
      .avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_address(avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_address),
      .avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_writedata(avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_writedata),
      .avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_byteenable(avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_byteenable),
      .avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_waitrequest(avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_waitrequest),
      .avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_readdata(avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_readdata),
      .avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_readdatavalid(avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_readdatavalid),
      .avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_writeack(avm_local_bb10_ld_memcoalesce_weights_load_0_inst0_writeack),
      // AVM avm_local_bb4_ld__inst0
      .avm_local_bb4_ld__inst0_enable(avm_local_bb4_ld__inst0_enable),
      .avm_local_bb4_ld__inst0_read(avm_local_bb4_ld__inst0_read),
      .avm_local_bb4_ld__inst0_write(avm_local_bb4_ld__inst0_write),
      .avm_local_bb4_ld__inst0_burstcount(avm_local_bb4_ld__inst0_burstcount),
      .avm_local_bb4_ld__inst0_address(avm_local_bb4_ld__inst0_address),
      .avm_local_bb4_ld__inst0_writedata(avm_local_bb4_ld__inst0_writedata),
      .avm_local_bb4_ld__inst0_byteenable(avm_local_bb4_ld__inst0_byteenable),
      .avm_local_bb4_ld__inst0_waitrequest(avm_local_bb4_ld__inst0_waitrequest),
      .avm_local_bb4_ld__inst0_readdata(avm_local_bb4_ld__inst0_readdata),
      .avm_local_bb4_ld__inst0_readdatavalid(avm_local_bb4_ld__inst0_readdatavalid),
      .avm_local_bb4_ld__inst0_writeack(avm_local_bb4_ld__inst0_writeack),
      // AVM avm_local_bb10_ld_memcoalesce_null_load_0_inst0
      .avm_local_bb10_ld_memcoalesce_null_load_0_inst0_enable(local_avm_aspace5_enable[0][0]),
      .avm_local_bb10_ld_memcoalesce_null_load_0_inst0_read(local_avm_aspace5_read[0][0]),
      .avm_local_bb10_ld_memcoalesce_null_load_0_inst0_write(local_avm_aspace5_write[0][0]),
      .avm_local_bb10_ld_memcoalesce_null_load_0_inst0_burstcount(local_avm_aspace5_burstcount[0][0]),
      .avm_local_bb10_ld_memcoalesce_null_load_0_inst0_address(local_avm_aspace5_address[0][0]),
      .avm_local_bb10_ld_memcoalesce_null_load_0_inst0_writedata(local_avm_aspace5_writedata[0][0]),
      .avm_local_bb10_ld_memcoalesce_null_load_0_inst0_byteenable(local_avm_aspace5_byteenable[0][0]),
      .avm_local_bb10_ld_memcoalesce_null_load_0_inst0_waitrequest(local_avm_aspace5_waitrequest[0][0]),
      .avm_local_bb10_ld_memcoalesce_null_load_0_inst0_readdata(local_avm_aspace5_readdata[0][0]),
      .avm_local_bb10_ld_memcoalesce_null_load_0_inst0_readdatavalid(local_avm_aspace5_readdatavalid[0][0]),
      .avm_local_bb10_ld_memcoalesce_null_load_0_inst0_writeack(local_avm_aspace5_writeack[0][0]),
      // AVM avm_local_bb10_st_memcoalesce_null_insertValue_34_inst0
      .avm_local_bb10_st_memcoalesce_null_insertValue_34_inst0_enable(local_avm_aspace5_enable[0][1]),
      .avm_local_bb10_st_memcoalesce_null_insertValue_34_inst0_read(local_avm_aspace5_read[0][1]),
      .avm_local_bb10_st_memcoalesce_null_insertValue_34_inst0_write(local_avm_aspace5_write[0][1]),
      .avm_local_bb10_st_memcoalesce_null_insertValue_34_inst0_burstcount(local_avm_aspace5_burstcount[0][1]),
      .avm_local_bb10_st_memcoalesce_null_insertValue_34_inst0_address(local_avm_aspace5_address[0][1]),
      .avm_local_bb10_st_memcoalesce_null_insertValue_34_inst0_writedata(local_avm_aspace5_writedata[0][1]),
      .avm_local_bb10_st_memcoalesce_null_insertValue_34_inst0_byteenable(local_avm_aspace5_byteenable[0][1]),
      .avm_local_bb10_st_memcoalesce_null_insertValue_34_inst0_waitrequest(local_avm_aspace5_waitrequest[0][1]),
      .avm_local_bb10_st_memcoalesce_null_insertValue_34_inst0_readdata(local_avm_aspace5_readdata[0][1]),
      .avm_local_bb10_st_memcoalesce_null_insertValue_34_inst0_readdatavalid(local_avm_aspace5_readdatavalid[0][1]),
      .avm_local_bb10_st_memcoalesce_null_insertValue_34_inst0_writeack(local_avm_aspace5_writeack[0][1]),
      // AVM avm_local_bb4_st_memcoalesce_null_insertValue_3_inst0
      .avm_local_bb4_st_memcoalesce_null_insertValue_3_inst0_enable(local_avm_aspace5_enable[0][2]),
      .avm_local_bb4_st_memcoalesce_null_insertValue_3_inst0_read(local_avm_aspace5_read[0][2]),
      .avm_local_bb4_st_memcoalesce_null_insertValue_3_inst0_write(local_avm_aspace5_write[0][2]),
      .avm_local_bb4_st_memcoalesce_null_insertValue_3_inst0_burstcount(local_avm_aspace5_burstcount[0][2]),
      .avm_local_bb4_st_memcoalesce_null_insertValue_3_inst0_address(local_avm_aspace5_address[0][2]),
      .avm_local_bb4_st_memcoalesce_null_insertValue_3_inst0_writedata(local_avm_aspace5_writedata[0][2]),
      .avm_local_bb4_st_memcoalesce_null_insertValue_3_inst0_byteenable(local_avm_aspace5_byteenable[0][2]),
      .avm_local_bb4_st_memcoalesce_null_insertValue_3_inst0_waitrequest(local_avm_aspace5_waitrequest[0][2]),
      .avm_local_bb4_st_memcoalesce_null_insertValue_3_inst0_readdata(local_avm_aspace5_readdata[0][2]),
      .avm_local_bb4_st_memcoalesce_null_insertValue_3_inst0_readdatavalid(local_avm_aspace5_readdatavalid[0][2]),
      .avm_local_bb4_st_memcoalesce_null_insertValue_3_inst0_writeack(local_avm_aspace5_writeack[0][2]),
      // AVM avm_local_bb10_ld_memcoalesce_null_load_019_inst0
      .avm_local_bb10_ld_memcoalesce_null_load_019_inst0_enable(local_avm_aspace6_enable[0][0]),
      .avm_local_bb10_ld_memcoalesce_null_load_019_inst0_read(local_avm_aspace6_read[0][0]),
      .avm_local_bb10_ld_memcoalesce_null_load_019_inst0_write(local_avm_aspace6_write[0][0]),
      .avm_local_bb10_ld_memcoalesce_null_load_019_inst0_burstcount(local_avm_aspace6_burstcount[0][0]),
      .avm_local_bb10_ld_memcoalesce_null_load_019_inst0_address(local_avm_aspace6_address[0][0]),
      .avm_local_bb10_ld_memcoalesce_null_load_019_inst0_writedata(local_avm_aspace6_writedata[0][0]),
      .avm_local_bb10_ld_memcoalesce_null_load_019_inst0_byteenable(local_avm_aspace6_byteenable[0][0]),
      .avm_local_bb10_ld_memcoalesce_null_load_019_inst0_waitrequest(local_avm_aspace6_waitrequest[0][0]),
      .avm_local_bb10_ld_memcoalesce_null_load_019_inst0_readdata(local_avm_aspace6_readdata[0][0]),
      .avm_local_bb10_ld_memcoalesce_null_load_019_inst0_readdatavalid(local_avm_aspace6_readdatavalid[0][0]),
      .avm_local_bb10_ld_memcoalesce_null_load_019_inst0_writeack(local_avm_aspace6_writeack[0][0]),
      // AVM avm_local_bb10_st_memcoalesce_null_insertValue_63_inst0
      .avm_local_bb10_st_memcoalesce_null_insertValue_63_inst0_enable(local_avm_aspace6_enable[0][1]),
      .avm_local_bb10_st_memcoalesce_null_insertValue_63_inst0_read(local_avm_aspace6_read[0][1]),
      .avm_local_bb10_st_memcoalesce_null_insertValue_63_inst0_write(local_avm_aspace6_write[0][1]),
      .avm_local_bb10_st_memcoalesce_null_insertValue_63_inst0_burstcount(local_avm_aspace6_burstcount[0][1]),
      .avm_local_bb10_st_memcoalesce_null_insertValue_63_inst0_address(local_avm_aspace6_address[0][1]),
      .avm_local_bb10_st_memcoalesce_null_insertValue_63_inst0_writedata(local_avm_aspace6_writedata[0][1]),
      .avm_local_bb10_st_memcoalesce_null_insertValue_63_inst0_byteenable(local_avm_aspace6_byteenable[0][1]),
      .avm_local_bb10_st_memcoalesce_null_insertValue_63_inst0_waitrequest(local_avm_aspace6_waitrequest[0][1]),
      .avm_local_bb10_st_memcoalesce_null_insertValue_63_inst0_readdata(local_avm_aspace6_readdata[0][1]),
      .avm_local_bb10_st_memcoalesce_null_insertValue_63_inst0_readdatavalid(local_avm_aspace6_readdatavalid[0][1]),
      .avm_local_bb10_st_memcoalesce_null_insertValue_63_inst0_writeack(local_avm_aspace6_writeack[0][1]),
      // AVST avst_local_bb10__bias_ch_inst0
      .avst_local_bb10__bias_ch_inst0_valid(avm_channel_id_bias_ch_write_valid),
      .avst_local_bb10__bias_ch_inst0_ready(avm_channel_id_bias_ch_write_ready),
      .avst_local_bb10__bias_ch_inst0_data(avm_channel_id_bias_ch_write_data),
      .avst_local_bb10__bias_ch_inst0_almostfull(avm_channel_id_bias_ch_write_almostfull),
      // AVST avst_local_bb10__data_ch_inst0
      .avst_local_bb10__data_ch_inst0_valid(avm_channel_id_data_ch_write_valid),
      .avst_local_bb10__data_ch_inst0_ready(avm_channel_id_data_ch_write_ready),
      .avst_local_bb10__data_ch_inst0_data(avm_channel_id_data_ch_write_data),
      .avst_local_bb10__data_ch_inst0_almostfull(avm_channel_id_data_ch_write_almostfull),
      // AVST avst_local_bb10__weight_ch_inst0
      .avst_local_bb10__weight_ch_inst0_valid(avm_channel_id_weight_ch_write_valid),
      .avst_local_bb10__weight_ch_inst0_ready(avm_channel_id_weight_ch_write_ready),
      .avst_local_bb10__weight_ch_inst0_data(avm_channel_id_weight_ch_write_data),
      .avst_local_bb10__weight_ch_inst0_almostfull(avm_channel_id_weight_ch_write_almostfull)
   );

   assign lmem_invalid_single_bit = |lmem_invalid_aspaces;
   generate
   begin:local_mem_system_aspace5
      logic local_icm_arb_request [1][3];
      logic local_icm_arb_enable [1][3];
      logic local_icm_arb_read [1][3];
      logic local_icm_arb_write [1][3];
      logic local_icm_arb_burstcount [1][3];
      logic [12:0] local_icm_arb_address [1][3];
      logic [31:0] local_icm_arb_writedata [1][3];
      logic [3:0] local_icm_arb_byteenable [1][3];
      logic local_icm_arb_stall [1][3];
      logic local_icm_wrp_ack [1][3];
      logic local_icm_rrp_datavalid [1][3];
      logic [31:0] local_icm_rrp_data [1][3];

      for( __i = 0; __i < 1; __i = __i + 1 )
      begin:local_mem_group
         for( __j = 0; __j < 3; __j = __j + 1 )
         begin:master
            // INST avm_to_ic of acl_avm_to_ic
            acl_avm_to_ic
            #(
               .DATA_W(32),
               .WRITEDATA_W(32),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(32),
               .BYTEENA_W(4)
            )
            avm_to_ic
            (
               // AVM avm
               .avm_enable(local_avm_aspace5_enable[__i][__j]),
               .avm_read(local_avm_aspace5_read[__i][__j]),
               .avm_write(local_avm_aspace5_write[__i][__j]),
               .avm_burstcount(local_avm_aspace5_burstcount[__i][__j]),
               .avm_address(local_avm_aspace5_address[__i][__j]),
               .avm_writedata(local_avm_aspace5_writedata[__i][__j]),
               .avm_byteenable(local_avm_aspace5_byteenable[__i][__j]),
               .avm_waitrequest(local_avm_aspace5_waitrequest[__i][__j]),
               .avm_readdata(local_avm_aspace5_readdata[__i][__j]),
               .avm_readdatavalid(local_avm_aspace5_readdatavalid[__i][__j]),
               .avm_writeack(local_avm_aspace5_writeack[__i][__j]),
               // ICM ic
               .ic_arb_request(local_icm_arb_request[__i][__j]),
               .ic_arb_enable(local_icm_arb_enable[__i][__j]),
               .ic_arb_read(local_icm_arb_read[__i][__j]),
               .ic_arb_write(local_icm_arb_write[__i][__j]),
               .ic_arb_burstcount(local_icm_arb_burstcount[__i][__j]),
               .ic_arb_address(local_icm_arb_address[__i][__j]),
               .ic_arb_writedata(local_icm_arb_writedata[__i][__j]),
               .ic_arb_byteenable(local_icm_arb_byteenable[__i][__j]),
               .ic_arb_stall(local_icm_arb_stall[__i][__j]),
               .ic_wrp_ack(local_icm_wrp_ack[__i][__j]),
               .ic_rrp_datavalid(local_icm_rrp_datavalid[__i][__j]),
               .ic_rrp_data(local_icm_rrp_data[__i][__j])
            );

         end

         for( __j = 0; __j < 1; __j = __j + 1 )
         begin:bank
            logic port_enable [1:2];
            logic port_read [1:2];
            logic port_write [1:2];
            logic [12:0] port_address [1:2];
            logic [31:0] port_writedata [1:2];
            logic [3:0] port_byteenable [1:2];
            logic port_waitrequest [1:2];
            logic [31:0] port_readdata [1:2];
            logic port_readdatavalid [1:2];

            // INST mem0 of acl_mem1x
            acl_mem1x
            #(
               .INTENDED_DEVICE_FAMILY("Cyclone V"),
               .DEPTH_WORDS(8192),
               .WIDTH(32),
               .MEM_LATENCY(3),
               .ENABLED(0),
               .RDW_MODE("OLD_DATA"),
               .RAM_OPERATION_MODE("DUAL_PORT"),
               .PREFERRED_WIDTH(320),
               .RAM_BLOCK_TYPE("M10K")
            )
            mem0
            (
               .clk(clock),
               .resetn(resetn),
               // AVS avs_port1
               .avs_port1_enable(port_enable[1]),
               .avs_port1_read(port_read[1]),
               .avs_port1_write(port_write[1]),
               .avs_port1_address(port_address[1]),
               .avs_port1_writedata(port_writedata[1]),
               .avs_port1_byteenable(port_byteenable[1]),
               .avs_port1_waitrequest(port_waitrequest[1]),
               .avs_port1_readdata(port_readdata[1]),
               .avs_port1_readdatavalid(port_readdatavalid[1]),
               // AVS avs_port2
               .avs_port2_enable(port_enable[2]),
               .avs_port2_read(port_read[2]),
               .avs_port2_write(port_write[2]),
               .avs_port2_address(port_address[2]),
               .avs_port2_writedata(port_writedata[2]),
               .avs_port2_byteenable(port_byteenable[2]),
               .avs_port2_waitrequest(port_waitrequest[2]),
               .avs_port2_readdata(port_readdata[2]),
               .avs_port2_readdatavalid(port_readdatavalid[2])
            );

         end

         for( __j = 0; __j < 3; __j = __j + 1 )
         begin:router
            logic b_arb_request [1];
            logic b_arb_enable [1];
            logic b_arb_read [1];
            logic b_arb_write [1];
            logic b_arb_burstcount [1];
            logic [12:0] b_arb_address [1];
            logic [31:0] b_arb_writedata [1];
            logic [3:0] b_arb_byteenable [1];
            logic b_arb_stall [1];
            logic b_wrp_ack [1];
            logic b_rrp_datavalid [1];
            logic [31:0] b_rrp_data [1];
            logic bank_select;

            // INST router of acl_ic_local_mem_router
            acl_ic_local_mem_router
            #(
               .DATA_W(32),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(13),
               .BYTEENA_W(4),
               .NUM_BANKS(1)
            )
            router
            (
               .clock(clock),
               .resetn(resetn),
               .bank_select(bank_select),
               // ICM m
               .m_arb_request(local_icm_arb_request[__i][__j]),
               .m_arb_enable(local_icm_arb_enable[__i][__j]),
               .m_arb_read(local_icm_arb_read[__i][__j]),
               .m_arb_write(local_icm_arb_write[__i][__j]),
               .m_arb_burstcount(local_icm_arb_burstcount[__i][__j]),
               .m_arb_address(local_icm_arb_address[__i][__j]),
               .m_arb_writedata(local_icm_arb_writedata[__i][__j]),
               .m_arb_byteenable(local_icm_arb_byteenable[__i][__j]),
               .m_arb_stall(local_icm_arb_stall[__i][__j]),
               .m_wrp_ack(local_icm_wrp_ack[__i][__j]),
               .m_rrp_datavalid(local_icm_rrp_datavalid[__i][__j]),
               .m_rrp_data(local_icm_rrp_data[__i][__j]),
               // ICM b
               .b_arb_request(b_arb_request),
               .b_arb_enable(b_arb_enable),
               .b_arb_read(b_arb_read),
               .b_arb_write(b_arb_write),
               .b_arb_burstcount(b_arb_burstcount),
               .b_arb_address(b_arb_address),
               .b_arb_writedata(b_arb_writedata),
               .b_arb_byteenable(b_arb_byteenable),
               .b_arb_stall(b_arb_stall),
               .b_wrp_ack(b_wrp_ack),
               .b_rrp_datavalid(b_rrp_datavalid),
               .b_rrp_data(b_rrp_data)
            );

            assign bank_select = 1'b1;
         end

         for( __j = 0; __j < 1; __j = __j + 1 )
         begin:port1bank0
            logic icm_in_arb_request [2];
            logic icm_in_arb_enable [2];
            logic icm_in_arb_read [2];
            logic icm_in_arb_write [2];
            logic icm_in_arb_burstcount [2];
            logic [12:0] icm_in_arb_address [2];
            logic [31:0] icm_in_arb_writedata [2];
            logic [3:0] icm_in_arb_byteenable [2];
            logic icm_in_arb_stall [2];
            logic icm_in_wrp_ack [2];
            logic icm_in_rrp_datavalid [2];
            logic [31:0] icm_in_rrp_data [2];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [12:0] icm_out_arb_address;
            logic [31:0] icm_out_arb_writedata;
            logic [3:0] icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [31:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[1].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[1].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[1].b_arb_read[0];
            assign icm_in_arb_write[0] = router[1].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[1].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[1].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[1].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[1].b_arb_byteenable[0];
            assign router[1].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[1].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[1].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[1].b_rrp_data[0] = icm_in_rrp_data[0];
            assign icm_in_arb_request[1] = router[2].b_arb_request[0];
            assign icm_in_arb_enable[1] = router[2].b_arb_enable[0];
            assign icm_in_arb_read[1] = router[2].b_arb_read[0];
            assign icm_in_arb_write[1] = router[2].b_arb_write[0];
            assign icm_in_arb_burstcount[1] = router[2].b_arb_burstcount[0];
            assign icm_in_arb_address[1] = router[2].b_arb_address[0];
            assign icm_in_arb_writedata[1] = router[2].b_arb_writedata[0];
            assign icm_in_arb_byteenable[1] = router[2].b_arb_byteenable[0];
            assign router[2].b_arb_stall[0] = icm_in_arb_stall[1];
            assign router[2].b_wrp_ack[0] = icm_in_wrp_ack[1];
            assign router[2].b_rrp_datavalid[0] = icm_in_rrp_datavalid[1];
            assign router[2].b_rrp_data[0] = icm_in_rrp_data[1];
            // INST data_ic of conv_pipe_system_interconnect_8
            conv_pipe_system_interconnect_8 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[1] = icm_out_arb_enable;
            assign bank[0].port_read[1] = icm_out_arb_read;
            assign bank[0].port_write[1] = icm_out_arb_write;
            assign bank[0].port_address[1] = icm_out_arb_address;
            assign bank[0].port_writedata[1] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[1] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[1];
            assign icm_out_rrp_data = bank[0].port_readdata[1];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[1];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __j = 0; __j < 1; __j = __j + 1 )
         begin:port2bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [12:0] icm_in_arb_address [1];
            logic [31:0] icm_in_arb_writedata [1];
            logic [3:0] icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [31:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [12:0] icm_out_arb_address;
            logic [31:0] icm_out_arb_writedata;
            logic [3:0] icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [31:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[0].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[0].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[0].b_arb_read[0];
            assign icm_in_arb_write[0] = router[0].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[0].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[0].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[0].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[0].b_arb_byteenable[0];
            assign router[0].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[0].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[0].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[0].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_9
            conv_pipe_system_interconnect_9 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[2] = icm_out_arb_enable;
            assign bank[0].port_read[2] = icm_out_arb_read;
            assign bank[0].port_write[2] = icm_out_arb_write;
            assign bank[0].port_address[2] = icm_out_arb_address;
            assign bank[0].port_writedata[2] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[2] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[2];
            assign icm_out_rrp_data = bank[0].port_readdata[2];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[2];
            assign icm_out_wrp_ack = 'b0;
         end

      end

   end
   endgenerate

   generate
   begin:local_mem_system_aspace6
      logic local_icm_arb_request [1][2];
      logic local_icm_arb_enable [1][2];
      logic local_icm_arb_read [1][2];
      logic local_icm_arb_write [1][2];
      logic local_icm_arb_burstcount [1][2];
      logic [11:0] local_icm_arb_address [1][2];
      logic [511:0] local_icm_arb_writedata [1][2];
      logic [63:0] local_icm_arb_byteenable [1][2];
      logic local_icm_arb_stall [1][2];
      logic local_icm_wrp_ack [1][2];
      logic local_icm_rrp_datavalid [1][2];
      logic [511:0] local_icm_rrp_data [1][2];

      for( __j = 0; __j < 1; __j = __j + 1 )
      begin:local_mem_group
         for( __k = 0; __k < 2; __k = __k + 1 )
         begin:master
            // INST avm_to_ic of acl_avm_to_ic
            acl_avm_to_ic
            #(
               .DATA_W(512),
               .WRITEDATA_W(512),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(32),
               .BYTEENA_W(64)
            )
            avm_to_ic
            (
               // AVM avm
               .avm_enable(local_avm_aspace6_enable[__j][__k]),
               .avm_read(local_avm_aspace6_read[__j][__k]),
               .avm_write(local_avm_aspace6_write[__j][__k]),
               .avm_burstcount(local_avm_aspace6_burstcount[__j][__k]),
               .avm_address(local_avm_aspace6_address[__j][__k]),
               .avm_writedata(local_avm_aspace6_writedata[__j][__k]),
               .avm_byteenable(local_avm_aspace6_byteenable[__j][__k]),
               .avm_waitrequest(local_avm_aspace6_waitrequest[__j][__k]),
               .avm_readdata(local_avm_aspace6_readdata[__j][__k]),
               .avm_readdatavalid(local_avm_aspace6_readdatavalid[__j][__k]),
               .avm_writeack(local_avm_aspace6_writeack[__j][__k]),
               // ICM ic
               .ic_arb_request(local_icm_arb_request[__j][__k]),
               .ic_arb_enable(local_icm_arb_enable[__j][__k]),
               .ic_arb_read(local_icm_arb_read[__j][__k]),
               .ic_arb_write(local_icm_arb_write[__j][__k]),
               .ic_arb_burstcount(local_icm_arb_burstcount[__j][__k]),
               .ic_arb_address(local_icm_arb_address[__j][__k]),
               .ic_arb_writedata(local_icm_arb_writedata[__j][__k]),
               .ic_arb_byteenable(local_icm_arb_byteenable[__j][__k]),
               .ic_arb_stall(local_icm_arb_stall[__j][__k]),
               .ic_wrp_ack(local_icm_wrp_ack[__j][__k]),
               .ic_rrp_datavalid(local_icm_rrp_datavalid[__j][__k]),
               .ic_rrp_data(local_icm_rrp_data[__j][__k])
            );

         end

         for( __k = 0; __k < 1; __k = __k + 1 )
         begin:bank
            logic port_enable [1:2];
            logic port_read [1:2];
            logic port_write [1:2];
            logic [11:0] port_address [1:2];
            logic [511:0] port_writedata [1:2];
            logic [63:0] port_byteenable [1:2];
            logic port_waitrequest [1:2];
            logic [511:0] port_readdata [1:2];
            logic port_readdatavalid [1:2];

            // INST mem0 of acl_mem1x
            acl_mem1x
            #(
               .INTENDED_DEVICE_FAMILY("Cyclone V"),
               .DEPTH_WORDS(4096),
               .WIDTH(512),
               .MEM_LATENCY(1),
               .ENABLED(0),
               .RDW_MODE("OLD_DATA"),
               .RAM_OPERATION_MODE("DUAL_PORT"),
               .PREFERRED_WIDTH(320),
               .RAM_BLOCK_TYPE("M10K")
            )
            mem0
            (
               .clk(clock),
               .resetn(resetn),
               // AVS avs_port1
               .avs_port1_enable(port_enable[1]),
               .avs_port1_read(port_read[1]),
               .avs_port1_write(port_write[1]),
               .avs_port1_address(port_address[1]),
               .avs_port1_writedata(port_writedata[1]),
               .avs_port1_byteenable(port_byteenable[1]),
               .avs_port1_waitrequest(port_waitrequest[1]),
               .avs_port1_readdata(port_readdata[1]),
               .avs_port1_readdatavalid(port_readdatavalid[1]),
               // AVS avs_port2
               .avs_port2_enable(port_enable[2]),
               .avs_port2_read(port_read[2]),
               .avs_port2_write(port_write[2]),
               .avs_port2_address(port_address[2]),
               .avs_port2_writedata(port_writedata[2]),
               .avs_port2_byteenable(port_byteenable[2]),
               .avs_port2_waitrequest(port_waitrequest[2]),
               .avs_port2_readdata(port_readdata[2]),
               .avs_port2_readdatavalid(port_readdatavalid[2])
            );

         end

         for( __k = 0; __k < 2; __k = __k + 1 )
         begin:router
            logic b_arb_request [1];
            logic b_arb_enable [1];
            logic b_arb_read [1];
            logic b_arb_write [1];
            logic b_arb_burstcount [1];
            logic [11:0] b_arb_address [1];
            logic [511:0] b_arb_writedata [1];
            logic [63:0] b_arb_byteenable [1];
            logic b_arb_stall [1];
            logic b_wrp_ack [1];
            logic b_rrp_datavalid [1];
            logic [511:0] b_rrp_data [1];
            logic bank_select;

            // INST router of acl_ic_local_mem_router
            acl_ic_local_mem_router
            #(
               .DATA_W(512),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(12),
               .BYTEENA_W(64),
               .NUM_BANKS(1)
            )
            router
            (
               .clock(clock),
               .resetn(resetn),
               .bank_select(bank_select),
               // ICM m
               .m_arb_request(local_icm_arb_request[__j][__k]),
               .m_arb_enable(local_icm_arb_enable[__j][__k]),
               .m_arb_read(local_icm_arb_read[__j][__k]),
               .m_arb_write(local_icm_arb_write[__j][__k]),
               .m_arb_burstcount(local_icm_arb_burstcount[__j][__k]),
               .m_arb_address(local_icm_arb_address[__j][__k]),
               .m_arb_writedata(local_icm_arb_writedata[__j][__k]),
               .m_arb_byteenable(local_icm_arb_byteenable[__j][__k]),
               .m_arb_stall(local_icm_arb_stall[__j][__k]),
               .m_wrp_ack(local_icm_wrp_ack[__j][__k]),
               .m_rrp_datavalid(local_icm_rrp_datavalid[__j][__k]),
               .m_rrp_data(local_icm_rrp_data[__j][__k]),
               // ICM b
               .b_arb_request(b_arb_request),
               .b_arb_enable(b_arb_enable),
               .b_arb_read(b_arb_read),
               .b_arb_write(b_arb_write),
               .b_arb_burstcount(b_arb_burstcount),
               .b_arb_address(b_arb_address),
               .b_arb_writedata(b_arb_writedata),
               .b_arb_byteenable(b_arb_byteenable),
               .b_arb_stall(b_arb_stall),
               .b_wrp_ack(b_wrp_ack),
               .b_rrp_datavalid(b_rrp_datavalid),
               .b_rrp_data(b_rrp_data)
            );

            assign bank_select = 1'b1;
         end

         for( __k = 0; __k < 1; __k = __k + 1 )
         begin:port1bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [11:0] icm_in_arb_address [1];
            logic [511:0] icm_in_arb_writedata [1];
            logic [63:0] icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [511:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [11:0] icm_out_arb_address;
            logic [511:0] icm_out_arb_writedata;
            logic [63:0] icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [511:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[1].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[1].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[1].b_arb_read[0];
            assign icm_in_arb_write[0] = router[1].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[1].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[1].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[1].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[1].b_arb_byteenable[0];
            assign router[1].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[1].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[1].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[1].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_10
            conv_pipe_system_interconnect_10 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[1] = icm_out_arb_enable;
            assign bank[0].port_read[1] = icm_out_arb_read;
            assign bank[0].port_write[1] = icm_out_arb_write;
            assign bank[0].port_address[1] = icm_out_arb_address;
            assign bank[0].port_writedata[1] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[1] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[1];
            assign icm_out_rrp_data = bank[0].port_readdata[1];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[1];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __k = 0; __k < 1; __k = __k + 1 )
         begin:port2bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [11:0] icm_in_arb_address [1];
            logic [511:0] icm_in_arb_writedata [1];
            logic [63:0] icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [511:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [11:0] icm_out_arb_address;
            logic [511:0] icm_out_arb_writedata;
            logic [63:0] icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [511:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[0].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[0].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[0].b_arb_read[0];
            assign icm_in_arb_write[0] = router[0].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[0].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[0].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[0].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[0].b_arb_byteenable[0];
            assign router[0].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[0].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[0].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[0].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_11
            conv_pipe_system_interconnect_11 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[2] = icm_out_arb_enable;
            assign bank[0].port_read[2] = icm_out_arb_read;
            assign bank[0].port_write[2] = icm_out_arb_write;
            assign bank[0].port_address[2] = icm_out_arb_address;
            assign bank[0].port_writedata[2] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[2] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[2];
            assign icm_out_rrp_data = bank[0].port_readdata[2];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[2];
            assign icm_out_wrp_ack = 'b0;
         end

      end

   end
   endgenerate

endmodule

/////////////////////////////////////////////////////////////////
// MODULE memWrite_top_wrapper_0
/////////////////////////////////////////////////////////////////
module memWrite_top_wrapper_0
(
   input logic start,
   input logic [175:0] kernel_arguments,
   input logic [31:0] work_dim,
   input logic [31:0] global_offset [2:0],
   output logic kernel_valid_out,
   output logic has_a_write_pending,
   output logic has_a_lsu_active,
   input logic [31:0] global_id [2:0],
   input logic [31:0] local_id [2:0],
   input logic [31:0] group_id [2:0],
   input logic [31:0] global_size [2:0],
   input logic [31:0] local_size [2:0],
   input logic [31:0] num_groups [2:0],
   input logic [31:0] workgroup_size,
   output logic kernel_stall_out,
   input logic kernel_valid_in,
   input logic clock,
   input logic resetn,
   input logic clock2x,
   // AVM avm_local_bb1_st_c0_exe2_inst0
   output logic avm_local_bb1_st_c0_exe2_inst0_enable,
   output logic avm_local_bb1_st_c0_exe2_inst0_read,
   output logic avm_local_bb1_st_c0_exe2_inst0_write,
   output logic [4:0] avm_local_bb1_st_c0_exe2_inst0_burstcount,
   output logic [29:0] avm_local_bb1_st_c0_exe2_inst0_address,
   output logic [255:0] avm_local_bb1_st_c0_exe2_inst0_writedata,
   output logic [31:0] avm_local_bb1_st_c0_exe2_inst0_byteenable,
   input logic avm_local_bb1_st_c0_exe2_inst0_waitrequest,
   input logic [255:0] avm_local_bb1_st_c0_exe2_inst0_readdata,
   input logic avm_local_bb1_st_c0_exe2_inst0_readdatavalid,
   input logic avm_local_bb1_st_c0_exe2_inst0_writeack,
   // AVST avm_channel_id_bypass_ch_read
   input logic avm_channel_id_bypass_ch_read_valid,
   output logic avm_channel_id_bypass_ch_read_ready,
   input logic [127:0] avm_channel_id_bypass_ch_read_data,
   // AVST avm_channel_id_pool_ch_read
   input logic avm_channel_id_pool_ch_read_valid,
   output logic avm_channel_id_pool_ch_read_ready,
   input logic [127:0] avm_channel_id_pool_ch_read_data
);
   genvar __i;
   genvar __j;
   logic lmem_invalid_single_bit;
   logic lmem_invalid_aspaces;
   logic local_avm_aspace41_enable [1][2];
   logic local_avm_aspace41_read [1][2];
   logic local_avm_aspace41_write [1][2];
   logic local_avm_aspace41_burstcount [1][2];
   logic [31:0] local_avm_aspace41_address [1][2];
   logic [127:0] local_avm_aspace41_writedata [1][2];
   logic [15:0] local_avm_aspace41_byteenable [1][2];
   logic local_avm_aspace41_waitrequest [1][2];
   logic [127:0] local_avm_aspace41_readdata [1][2];
   logic local_avm_aspace41_readdatavalid [1][2];
   logic local_avm_aspace41_writeack [1][2];

   // INST kernel of memWrite_function_wrapper
   memWrite_function_wrapper kernel
   (
      .local_router_hang(lmem_invalid_single_bit),
      .start(start),
      .kernel_arguments(kernel_arguments),
      .work_dim(work_dim),
      .global_offset_0(global_offset[0]),
      .global_offset_1(global_offset[1]),
      .global_offset_2(global_offset[2]),
      .kernel_valid_out(kernel_valid_out),
      .has_a_write_pending(has_a_write_pending),
      .has_a_lsu_active(has_a_lsu_active),
      .global_id_0(global_id[0]),
      .global_id_1(global_id[1]),
      .global_id_2(global_id[2]),
      .local_id_0(local_id[0]),
      .local_id_1(local_id[1]),
      .local_id_2(local_id[2]),
      .group_id_0(group_id[0]),
      .group_id_1(group_id[1]),
      .group_id_2(group_id[2]),
      .global_size_0(global_size[0]),
      .global_size_1(global_size[1]),
      .global_size_2(global_size[2]),
      .local_size_0(local_size[0]),
      .local_size_1(local_size[1]),
      .local_size_2(local_size[2]),
      .num_groups_0(num_groups[0]),
      .num_groups_1(num_groups[1]),
      .num_groups_2(num_groups[2]),
      .workgroup_size(workgroup_size),
      .kernel_stall_out(kernel_stall_out),
      .kernel_valid_in(kernel_valid_in),
      .clock(clock),
      .resetn(resetn),
      .clock2x(clock2x),
      // AVM avm_local_bb1_st_c0_exe2_inst0
      .avm_local_bb1_st_c0_exe2_inst0_enable(avm_local_bb1_st_c0_exe2_inst0_enable),
      .avm_local_bb1_st_c0_exe2_inst0_read(avm_local_bb1_st_c0_exe2_inst0_read),
      .avm_local_bb1_st_c0_exe2_inst0_write(avm_local_bb1_st_c0_exe2_inst0_write),
      .avm_local_bb1_st_c0_exe2_inst0_burstcount(avm_local_bb1_st_c0_exe2_inst0_burstcount),
      .avm_local_bb1_st_c0_exe2_inst0_address(avm_local_bb1_st_c0_exe2_inst0_address),
      .avm_local_bb1_st_c0_exe2_inst0_writedata(avm_local_bb1_st_c0_exe2_inst0_writedata),
      .avm_local_bb1_st_c0_exe2_inst0_byteenable(avm_local_bb1_st_c0_exe2_inst0_byteenable),
      .avm_local_bb1_st_c0_exe2_inst0_waitrequest(avm_local_bb1_st_c0_exe2_inst0_waitrequest),
      .avm_local_bb1_st_c0_exe2_inst0_readdata(avm_local_bb1_st_c0_exe2_inst0_readdata),
      .avm_local_bb1_st_c0_exe2_inst0_readdatavalid(avm_local_bb1_st_c0_exe2_inst0_readdatavalid),
      .avm_local_bb1_st_c0_exe2_inst0_writeack(avm_local_bb1_st_c0_exe2_inst0_writeack),
      // AVM avm_local_bb1_ld__inst0
      .avm_local_bb1_ld__inst0_enable(local_avm_aspace41_enable[0][0]),
      .avm_local_bb1_ld__inst0_read(local_avm_aspace41_read[0][0]),
      .avm_local_bb1_ld__inst0_write(local_avm_aspace41_write[0][0]),
      .avm_local_bb1_ld__inst0_burstcount(local_avm_aspace41_burstcount[0][0]),
      .avm_local_bb1_ld__inst0_address(local_avm_aspace41_address[0][0]),
      .avm_local_bb1_ld__inst0_writedata(local_avm_aspace41_writedata[0][0]),
      .avm_local_bb1_ld__inst0_byteenable(local_avm_aspace41_byteenable[0][0]),
      .avm_local_bb1_ld__inst0_waitrequest(local_avm_aspace41_waitrequest[0][0]),
      .avm_local_bb1_ld__inst0_readdata(local_avm_aspace41_readdata[0][0]),
      .avm_local_bb1_ld__inst0_readdatavalid(local_avm_aspace41_readdatavalid[0][0]),
      .avm_local_bb1_ld__inst0_writeack(local_avm_aspace41_writeack[0][0]),
      // AVM avm_local_bb1_st_memcoalesce_null_insertValue_15_inst0
      .avm_local_bb1_st_memcoalesce_null_insertValue_15_inst0_enable(local_avm_aspace41_enable[0][1]),
      .avm_local_bb1_st_memcoalesce_null_insertValue_15_inst0_read(local_avm_aspace41_read[0][1]),
      .avm_local_bb1_st_memcoalesce_null_insertValue_15_inst0_write(local_avm_aspace41_write[0][1]),
      .avm_local_bb1_st_memcoalesce_null_insertValue_15_inst0_burstcount(local_avm_aspace41_burstcount[0][1]),
      .avm_local_bb1_st_memcoalesce_null_insertValue_15_inst0_address(local_avm_aspace41_address[0][1]),
      .avm_local_bb1_st_memcoalesce_null_insertValue_15_inst0_writedata(local_avm_aspace41_writedata[0][1]),
      .avm_local_bb1_st_memcoalesce_null_insertValue_15_inst0_byteenable(local_avm_aspace41_byteenable[0][1]),
      .avm_local_bb1_st_memcoalesce_null_insertValue_15_inst0_waitrequest(local_avm_aspace41_waitrequest[0][1]),
      .avm_local_bb1_st_memcoalesce_null_insertValue_15_inst0_readdata(local_avm_aspace41_readdata[0][1]),
      .avm_local_bb1_st_memcoalesce_null_insertValue_15_inst0_readdatavalid(local_avm_aspace41_readdatavalid[0][1]),
      .avm_local_bb1_st_memcoalesce_null_insertValue_15_inst0_writeack(local_avm_aspace41_writeack[0][1]),
      // AVST avst_local_bb1__bypass_ch_inst0
      .avst_local_bb1__bypass_ch_inst0_valid(avm_channel_id_bypass_ch_read_valid),
      .avst_local_bb1__bypass_ch_inst0_ready(avm_channel_id_bypass_ch_read_ready),
      .avst_local_bb1__bypass_ch_inst0_data(avm_channel_id_bypass_ch_read_data),
      // AVST avst_local_bb1__pool_ch_inst0
      .avst_local_bb1__pool_ch_inst0_valid(avm_channel_id_pool_ch_read_valid),
      .avst_local_bb1__pool_ch_inst0_ready(avm_channel_id_pool_ch_read_ready),
      .avst_local_bb1__pool_ch_inst0_data(avm_channel_id_pool_ch_read_data)
   );

   assign lmem_invalid_single_bit = |lmem_invalid_aspaces;
   generate
   begin:local_mem_system_aspace41
      logic local_icm_arb_request [1][2];
      logic local_icm_arb_enable [1][2];
      logic local_icm_arb_read [1][2];
      logic local_icm_arb_write [1][2];
      logic local_icm_arb_burstcount [1][2];
      logic [1:0] local_icm_arb_address [1][2];
      logic [127:0] local_icm_arb_writedata [1][2];
      logic [15:0] local_icm_arb_byteenable [1][2];
      logic local_icm_arb_stall [1][2];
      logic local_icm_wrp_ack [1][2];
      logic local_icm_rrp_datavalid [1][2];
      logic [127:0] local_icm_rrp_data [1][2];

      for( __i = 0; __i < 1; __i = __i + 1 )
      begin:local_mem_group
         for( __j = 0; __j < 2; __j = __j + 1 )
         begin:master
            // INST avm_to_ic of acl_avm_to_ic
            acl_avm_to_ic
            #(
               .DATA_W(128),
               .WRITEDATA_W(128),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(32),
               .BYTEENA_W(16)
            )
            avm_to_ic
            (
               // AVM avm
               .avm_enable(local_avm_aspace41_enable[__i][__j]),
               .avm_read(local_avm_aspace41_read[__i][__j]),
               .avm_write(local_avm_aspace41_write[__i][__j]),
               .avm_burstcount(local_avm_aspace41_burstcount[__i][__j]),
               .avm_address(local_avm_aspace41_address[__i][__j]),
               .avm_writedata(local_avm_aspace41_writedata[__i][__j]),
               .avm_byteenable(local_avm_aspace41_byteenable[__i][__j]),
               .avm_waitrequest(local_avm_aspace41_waitrequest[__i][__j]),
               .avm_readdata(local_avm_aspace41_readdata[__i][__j]),
               .avm_readdatavalid(local_avm_aspace41_readdatavalid[__i][__j]),
               .avm_writeack(local_avm_aspace41_writeack[__i][__j]),
               // ICM ic
               .ic_arb_request(local_icm_arb_request[__i][__j]),
               .ic_arb_enable(local_icm_arb_enable[__i][__j]),
               .ic_arb_read(local_icm_arb_read[__i][__j]),
               .ic_arb_write(local_icm_arb_write[__i][__j]),
               .ic_arb_burstcount(local_icm_arb_burstcount[__i][__j]),
               .ic_arb_address(local_icm_arb_address[__i][__j]),
               .ic_arb_writedata(local_icm_arb_writedata[__i][__j]),
               .ic_arb_byteenable(local_icm_arb_byteenable[__i][__j]),
               .ic_arb_stall(local_icm_arb_stall[__i][__j]),
               .ic_wrp_ack(local_icm_wrp_ack[__i][__j]),
               .ic_rrp_datavalid(local_icm_rrp_datavalid[__i][__j]),
               .ic_rrp_data(local_icm_rrp_data[__i][__j])
            );

         end

         for( __j = 0; __j < 1; __j = __j + 1 )
         begin:bank
            logic port_enable [1:2];
            logic port_read [1:2];
            logic port_write [1:2];
            logic [1:0] port_address [1:2];
            logic [127:0] port_writedata [1:2];
            logic [15:0] port_byteenable [1:2];
            logic port_waitrequest [1:2];
            logic [127:0] port_readdata [1:2];
            logic port_readdatavalid [1:2];

            // INST mem0 of acl_mem1x
            acl_mem1x
            #(
               .INTENDED_DEVICE_FAMILY("Cyclone V"),
               .DEPTH_WORDS(3),
               .WIDTH(128),
               .MEM_LATENCY(3),
               .ENABLED(0),
               .RDW_MODE("DONT_CARE"),
               .RAM_OPERATION_MODE("DUAL_PORT"),
               .PREFERRED_WIDTH(320),
               .RAM_BLOCK_TYPE("M10K")
            )
            mem0
            (
               .clk(clock),
               .resetn(resetn),
               // AVS avs_port1
               .avs_port1_enable(port_enable[1]),
               .avs_port1_read(port_read[1]),
               .avs_port1_write(port_write[1]),
               .avs_port1_address(port_address[1]),
               .avs_port1_writedata(port_writedata[1]),
               .avs_port1_byteenable(port_byteenable[1]),
               .avs_port1_waitrequest(port_waitrequest[1]),
               .avs_port1_readdata(port_readdata[1]),
               .avs_port1_readdatavalid(port_readdatavalid[1]),
               // AVS avs_port2
               .avs_port2_enable(port_enable[2]),
               .avs_port2_read(port_read[2]),
               .avs_port2_write(port_write[2]),
               .avs_port2_address(port_address[2]),
               .avs_port2_writedata(port_writedata[2]),
               .avs_port2_byteenable(port_byteenable[2]),
               .avs_port2_waitrequest(port_waitrequest[2]),
               .avs_port2_readdata(port_readdata[2]),
               .avs_port2_readdatavalid(port_readdatavalid[2])
            );

         end

         for( __j = 0; __j < 2; __j = __j + 1 )
         begin:router
            logic b_arb_request [1];
            logic b_arb_enable [1];
            logic b_arb_read [1];
            logic b_arb_write [1];
            logic b_arb_burstcount [1];
            logic [1:0] b_arb_address [1];
            logic [127:0] b_arb_writedata [1];
            logic [15:0] b_arb_byteenable [1];
            logic b_arb_stall [1];
            logic b_wrp_ack [1];
            logic b_rrp_datavalid [1];
            logic [127:0] b_rrp_data [1];
            logic bank_select;

            // INST router of acl_ic_local_mem_router
            acl_ic_local_mem_router
            #(
               .DATA_W(128),
               .BURSTCOUNT_W(1),
               .ADDRESS_W(2),
               .BYTEENA_W(16),
               .NUM_BANKS(1)
            )
            router
            (
               .clock(clock),
               .resetn(resetn),
               .bank_select(bank_select),
               // ICM m
               .m_arb_request(local_icm_arb_request[__i][__j]),
               .m_arb_enable(local_icm_arb_enable[__i][__j]),
               .m_arb_read(local_icm_arb_read[__i][__j]),
               .m_arb_write(local_icm_arb_write[__i][__j]),
               .m_arb_burstcount(local_icm_arb_burstcount[__i][__j]),
               .m_arb_address(local_icm_arb_address[__i][__j]),
               .m_arb_writedata(local_icm_arb_writedata[__i][__j]),
               .m_arb_byteenable(local_icm_arb_byteenable[__i][__j]),
               .m_arb_stall(local_icm_arb_stall[__i][__j]),
               .m_wrp_ack(local_icm_wrp_ack[__i][__j]),
               .m_rrp_datavalid(local_icm_rrp_datavalid[__i][__j]),
               .m_rrp_data(local_icm_rrp_data[__i][__j]),
               // ICM b
               .b_arb_request(b_arb_request),
               .b_arb_enable(b_arb_enable),
               .b_arb_read(b_arb_read),
               .b_arb_write(b_arb_write),
               .b_arb_burstcount(b_arb_burstcount),
               .b_arb_address(b_arb_address),
               .b_arb_writedata(b_arb_writedata),
               .b_arb_byteenable(b_arb_byteenable),
               .b_arb_stall(b_arb_stall),
               .b_wrp_ack(b_wrp_ack),
               .b_rrp_datavalid(b_rrp_datavalid),
               .b_rrp_data(b_rrp_data)
            );

            assign bank_select = 1'b1;
         end

         for( __j = 0; __j < 1; __j = __j + 1 )
         begin:port1bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [1:0] icm_in_arb_address [1];
            logic [127:0] icm_in_arb_writedata [1];
            logic [15:0] icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [127:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [1:0] icm_out_arb_address;
            logic [127:0] icm_out_arb_writedata;
            logic [15:0] icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [127:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[1].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[1].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[1].b_arb_read[0];
            assign icm_in_arb_write[0] = router[1].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[1].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[1].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[1].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[1].b_arb_byteenable[0];
            assign router[1].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[1].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[1].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[1].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_12
            conv_pipe_system_interconnect_12 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[1] = icm_out_arb_enable;
            assign bank[0].port_read[1] = icm_out_arb_read;
            assign bank[0].port_write[1] = icm_out_arb_write;
            assign bank[0].port_address[1] = icm_out_arb_address;
            assign bank[0].port_writedata[1] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[1] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[1];
            assign icm_out_rrp_data = bank[0].port_readdata[1];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[1];
            assign icm_out_wrp_ack = 'b0;
         end

         for( __j = 0; __j < 1; __j = __j + 1 )
         begin:port2bank0
            logic icm_in_arb_request [1];
            logic icm_in_arb_enable [1];
            logic icm_in_arb_read [1];
            logic icm_in_arb_write [1];
            logic icm_in_arb_burstcount [1];
            logic [1:0] icm_in_arb_address [1];
            logic [127:0] icm_in_arb_writedata [1];
            logic [15:0] icm_in_arb_byteenable [1];
            logic icm_in_arb_stall [1];
            logic icm_in_wrp_ack [1];
            logic icm_in_rrp_datavalid [1];
            logic [127:0] icm_in_rrp_data [1];
            logic icm_out_arb_request;
            logic icm_out_arb_enable;
            logic icm_out_arb_read;
            logic icm_out_arb_write;
            logic icm_out_arb_burstcount;
            logic [1:0] icm_out_arb_address;
            logic [127:0] icm_out_arb_writedata;
            logic [15:0] icm_out_arb_byteenable;
            logic icm_out_arb_stall;
            logic icm_out_wrp_ack;
            logic icm_out_rrp_datavalid;
            logic [127:0] icm_out_rrp_data;

            assign icm_in_arb_request[0] = router[0].b_arb_request[0];
            assign icm_in_arb_enable[0] = router[0].b_arb_enable[0];
            assign icm_in_arb_read[0] = router[0].b_arb_read[0];
            assign icm_in_arb_write[0] = router[0].b_arb_write[0];
            assign icm_in_arb_burstcount[0] = router[0].b_arb_burstcount[0];
            assign icm_in_arb_address[0] = router[0].b_arb_address[0];
            assign icm_in_arb_writedata[0] = router[0].b_arb_writedata[0];
            assign icm_in_arb_byteenable[0] = router[0].b_arb_byteenable[0];
            assign router[0].b_arb_stall[0] = icm_in_arb_stall[0];
            assign router[0].b_wrp_ack[0] = icm_in_wrp_ack[0];
            assign router[0].b_rrp_datavalid[0] = icm_in_rrp_datavalid[0];
            assign router[0].b_rrp_data[0] = icm_in_rrp_data[0];
            // INST data_ic of conv_pipe_system_interconnect_13
            conv_pipe_system_interconnect_13 data_ic
            (
               .clock(clock),
               .resetn(resetn),
               // ICM m
               .m_arb_request(icm_in_arb_request),
               .m_arb_enable(icm_in_arb_enable),
               .m_arb_read(icm_in_arb_read),
               .m_arb_write(icm_in_arb_write),
               .m_arb_burstcount(icm_in_arb_burstcount),
               .m_arb_address(icm_in_arb_address),
               .m_arb_writedata(icm_in_arb_writedata),
               .m_arb_byteenable(icm_in_arb_byteenable),
               .m_arb_stall(icm_in_arb_stall),
               .m_wrp_ack(icm_in_wrp_ack),
               .m_rrp_datavalid(icm_in_rrp_datavalid),
               .m_rrp_data(icm_in_rrp_data),
               // ICM mout
               .mout_arb_request(icm_out_arb_request),
               .mout_arb_enable(icm_out_arb_enable),
               .mout_arb_read(icm_out_arb_read),
               .mout_arb_write(icm_out_arb_write),
               .mout_arb_burstcount(icm_out_arb_burstcount),
               .mout_arb_address(icm_out_arb_address),
               .mout_arb_writedata(icm_out_arb_writedata),
               .mout_arb_byteenable(icm_out_arb_byteenable),
               .mout_arb_id(),
               .mout_arb_stall(icm_out_arb_stall),
               .mout_wrp_ack(icm_out_wrp_ack),
               .mout_rrp_datavalid(icm_out_rrp_datavalid),
               .mout_rrp_data(icm_out_rrp_data)
            );

            assign bank[0].port_enable[2] = icm_out_arb_enable;
            assign bank[0].port_read[2] = icm_out_arb_read;
            assign bank[0].port_write[2] = icm_out_arb_write;
            assign bank[0].port_address[2] = icm_out_arb_address;
            assign bank[0].port_writedata[2] = icm_out_arb_writedata;
            assign bank[0].port_byteenable[2] = icm_out_arb_byteenable;
            assign icm_out_arb_stall = bank[0].port_waitrequest[2];
            assign icm_out_rrp_data = bank[0].port_readdata[2];
            assign icm_out_rrp_datavalid = bank[0].port_readdatavalid[2];
            assign icm_out_wrp_ack = 'b0;
         end

      end

   end
   endgenerate

endmodule

/////////////////////////////////////////////////////////////////
// MODULE conv_pipe_system_interconnect_0
/////////////////////////////////////////////////////////////////
module conv_pipe_system_interconnect_0
(
   input logic clock,
   input logic resetn,
   // ICM m
   input logic m_arb_request [2],
   input logic m_arb_enable [2],
   input logic m_arb_read [2],
   input logic m_arb_write [2],
   input logic m_arb_burstcount [2],
   input logic [7:0] m_arb_address [2],
   input logic [15:0] m_arb_writedata [2],
   input logic [1:0] m_arb_byteenable [2],
   output logic m_arb_stall [2],
   output logic m_wrp_ack [2],
   output logic m_rrp_datavalid [2],
   output logic [15:0] m_rrp_data [2],
   // ICM mout
   output logic mout_arb_request,
   output logic mout_arb_enable,
   output logic mout_arb_read,
   output logic mout_arb_write,
   output logic mout_arb_burstcount,
   output logic [7:0] mout_arb_address,
   output logic [15:0] mout_arb_writedata,
   output logic [1:0] mout_arb_byteenable,
   output logic mout_arb_id,
   input logic mout_arb_stall,
   input logic mout_wrp_ack,
   input logic mout_rrp_datavalid,
   input logic [15:0] mout_rrp_data
);
   genvar __i;
   generate
      for( __i = 0; __i < 2; __i = __i + 1 )
      begin:m
         logic id;
         acl_ic_master_intf
         #(
            .DATA_W(16),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(8),
            .BYTEENA_W(2),
            .ID_W(1)
         ) m_intf();
         acl_arb_intf
         #(
            .DATA_W(16),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(8),
            .BYTEENA_W(2),
            .ID_W(1)
         ) arb_intf();
         acl_ic_wrp_intf
         #(
            .ID_W(1)
         ) wrp_intf();
         acl_ic_rrp_intf
         #(
            .DATA_W(16),
            .ID_W(1)
         ) rrp_intf();

         assign id = __i;
         // INST m_endp of acl_ic_master_endpoint
         acl_ic_master_endpoint
         #(
            .DATA_W(16),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(8),
            .BYTEENA_W(2),
            .ID_W(1),
            .TOTAL_NUM_MASTERS(2),
            .ID(__i)
         )
         m_endp
         (
            .clock(clock),
            .resetn(resetn),
            .m_intf(m_intf),
            .arb_intf(arb_intf),
            .wrp_intf(wrp_intf),
            .rrp_intf(rrp_intf)
         );

         assign m_intf.arb.req.request = m_arb_request[__i];
         assign m_intf.arb.req.enable = m_arb_enable[__i];
         assign m_intf.arb.req.read = m_arb_read[__i];
         assign m_intf.arb.req.write = m_arb_write[__i];
         assign m_intf.arb.req.burstcount = m_arb_burstcount[__i];
         assign m_intf.arb.req.address = m_arb_address[__i];
         assign m_intf.arb.req.writedata = m_arb_writedata[__i];
         assign m_intf.arb.req.byteenable = m_arb_byteenable[__i];
         assign m_arb_stall[__i] = m_intf.arb.stall;
         assign m_wrp_ack[__i] = m_intf.wrp.ack;
         assign m_rrp_datavalid[__i] = m_intf.rrp.datavalid;
         assign m_rrp_data[__i] = m_intf.rrp.data;
         assign m_intf.arb.req.id = id;
      end

   endgenerate

   generate
   begin:s
      acl_arb_intf
      #(
         .DATA_W(16),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(8),
         .BYTEENA_W(2),
         .ID_W(1)
      ) in_arb_intf();
      acl_arb_intf
      #(
         .DATA_W(16),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(8),
         .BYTEENA_W(2),
         .ID_W(1)
      ) out_arb_intf();
      acl_ic_wrp_intf
      #(
         .ID_W(1)
      ) wrp_intf();
      acl_ic_rrp_intf
      #(
         .DATA_W(16),
         .ID_W(1)
      ) rrp_intf();

      // INST s_endp of acl_ic_slave_endpoint
      acl_ic_slave_endpoint
      #(
         .DATA_W(16),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(8),
         .BYTEENA_W(2),
         .ID_W(1),
         .NUM_MASTERS(2),
         .PIPELINE_RETURN_PATHS(0),
         .WRP_FIFO_DEPTH(0),
         .RRP_FIFO_DEPTH(0),
         .RRP_USE_LL_FIFO(1),
         .SLAVE_FIXED_LATENCY(4),
         .SEPARATE_READ_WRITE_STALLS(0)
      )
      s_endp
      (
         .clock(clock),
         .resetn(resetn),
         .m_intf(in_arb_intf),
         .s_intf(out_arb_intf),
         .s_readdatavalid(mout_rrp_datavalid),
         .s_readdata(mout_rrp_data),
         .s_writeack(mout_wrp_ack),
         .wrp_intf(wrp_intf),
         .rrp_intf(rrp_intf)
      );

   end
   endgenerate

   generate
   begin:wrp
      assign m[0].wrp_intf.ack = s.wrp_intf.ack;
      assign m[0].wrp_intf.id = s.wrp_intf.id;
   end
   endgenerate

   generate
   begin:rrp
      assign m[1].rrp_intf.datavalid = s.rrp_intf.datavalid;
      assign m[1].rrp_intf.data = s.rrp_intf.data;
      assign m[1].rrp_intf.id = s.rrp_intf.id;
   end
   endgenerate

   generate
      for( __i = 0; __i < 1; __i = __i + 1 )
      begin:a
         acl_arb_intf
         #(
            .DATA_W(16),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(8),
            .BYTEENA_W(2),
            .ID_W(1)
         ) m0_intf();
         acl_arb_intf
         #(
            .DATA_W(16),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(8),
            .BYTEENA_W(2),
            .ID_W(1)
         ) m1_intf();
         acl_arb_intf
         #(
            .DATA_W(16),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(8),
            .BYTEENA_W(2),
            .ID_W(1)
         ) mout_intf();

         // INST a of acl_arb2
         acl_arb2
         #(
            .DATA_W(16),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(8),
            .BYTEENA_W(2),
            .ID_W(1),
            .PIPELINE("none"),
            .KEEP_LAST_GRANT(0),
            .NO_STALL_NETWORK(0)
         )
         a
         (
            .clock(clock),
            .resetn(resetn),
            .m0_intf(m0_intf),
            .m1_intf(m1_intf),
            .mout_intf(mout_intf)
         );

      end

   endgenerate

   generate
      for( __i = 0; __i < 1; __i = __i + 1 )
      begin:dp
         acl_arb_intf
         #(
            .DATA_W(16),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(8),
            .BYTEENA_W(2),
            .ID_W(1)
         ) in_intf();
         acl_arb_intf
         #(
            .DATA_W(16),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(8),
            .BYTEENA_W(2),
            .ID_W(1)
         ) out_intf();

         // INST dp of acl_arb_pipeline_reg
         acl_arb_pipeline_reg
         #(
            .DATA_W(16),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(8),
            .BYTEENA_W(2),
            .ID_W(1)
         )
         dp
         (
            .clock(clock),
            .resetn(resetn),
            .in_intf(in_intf),
            .out_intf(out_intf)
         );

      end

   endgenerate

   assign mout_arb_request = s.out_arb_intf.req.request;
   assign mout_arb_enable = s.out_arb_intf.req.enable;
   assign mout_arb_read = s.out_arb_intf.req.read;
   assign mout_arb_write = s.out_arb_intf.req.write;
   assign mout_arb_burstcount = s.out_arb_intf.req.burstcount;
   assign mout_arb_address = s.out_arb_intf.req.address;
   assign mout_arb_writedata = s.out_arb_intf.req.writedata;
   assign mout_arb_byteenable = s.out_arb_intf.req.byteenable;
   assign mout_arb_id = s.out_arb_intf.req.id;
   assign s.out_arb_intf.stall = mout_arb_stall;
   assign s.in_arb_intf.req = dp[0].out_intf.req;
   assign dp[0].out_intf.stall = s.in_arb_intf.stall;
   assign dp[0].in_intf.req = a[0].mout_intf.req;
   assign a[0].mout_intf.stall = dp[0].in_intf.stall;
   assign a[0].m0_intf.req = m[1].arb_intf.req;
   assign m[1].arb_intf.stall = a[0].m0_intf.stall;
   assign a[0].m1_intf.req = m[0].arb_intf.req;
   assign m[0].arb_intf.stall = a[0].m1_intf.stall;
endmodule

/////////////////////////////////////////////////////////////////
// MODULE conv_pipe_system_interconnect_1
/////////////////////////////////////////////////////////////////
module conv_pipe_system_interconnect_1
(
   input logic clock,
   input logic resetn,
   // ICM m
   input logic m_arb_request [2],
   input logic m_arb_enable [2],
   input logic m_arb_read [2],
   input logic m_arb_write [2],
   input logic m_arb_burstcount [2],
   input logic [7:0] m_arb_address [2],
   input logic [15:0] m_arb_writedata [2],
   input logic [1:0] m_arb_byteenable [2],
   output logic m_arb_stall [2],
   output logic m_wrp_ack [2],
   output logic m_rrp_datavalid [2],
   output logic [15:0] m_rrp_data [2],
   // ICM mout
   output logic mout_arb_request,
   output logic mout_arb_enable,
   output logic mout_arb_read,
   output logic mout_arb_write,
   output logic mout_arb_burstcount,
   output logic [7:0] mout_arb_address,
   output logic [15:0] mout_arb_writedata,
   output logic [1:0] mout_arb_byteenable,
   output logic mout_arb_id,
   input logic mout_arb_stall,
   input logic mout_wrp_ack,
   input logic mout_rrp_datavalid,
   input logic [15:0] mout_rrp_data
);
   genvar __i;
   generate
      for( __i = 0; __i < 2; __i = __i + 1 )
      begin:m
         logic id;
         acl_ic_master_intf
         #(
            .DATA_W(16),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(8),
            .BYTEENA_W(2),
            .ID_W(1)
         ) m_intf();
         acl_arb_intf
         #(
            .DATA_W(16),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(8),
            .BYTEENA_W(2),
            .ID_W(1)
         ) arb_intf();
         acl_ic_wrp_intf
         #(
            .ID_W(1)
         ) wrp_intf();
         acl_ic_rrp_intf
         #(
            .DATA_W(16),
            .ID_W(1)
         ) rrp_intf();

         assign id = __i;
         // INST m_endp of acl_ic_master_endpoint
         acl_ic_master_endpoint
         #(
            .DATA_W(16),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(8),
            .BYTEENA_W(2),
            .ID_W(1),
            .TOTAL_NUM_MASTERS(2),
            .ID(__i)
         )
         m_endp
         (
            .clock(clock),
            .resetn(resetn),
            .m_intf(m_intf),
            .arb_intf(arb_intf),
            .wrp_intf(wrp_intf),
            .rrp_intf(rrp_intf)
         );

         assign m_intf.arb.req.request = m_arb_request[__i];
         assign m_intf.arb.req.enable = m_arb_enable[__i];
         assign m_intf.arb.req.read = m_arb_read[__i];
         assign m_intf.arb.req.write = m_arb_write[__i];
         assign m_intf.arb.req.burstcount = m_arb_burstcount[__i];
         assign m_intf.arb.req.address = m_arb_address[__i];
         assign m_intf.arb.req.writedata = m_arb_writedata[__i];
         assign m_intf.arb.req.byteenable = m_arb_byteenable[__i];
         assign m_arb_stall[__i] = m_intf.arb.stall;
         assign m_wrp_ack[__i] = m_intf.wrp.ack;
         assign m_rrp_datavalid[__i] = m_intf.rrp.datavalid;
         assign m_rrp_data[__i] = m_intf.rrp.data;
         assign m_intf.arb.req.id = id;
      end

   endgenerate

   generate
   begin:s
      acl_arb_intf
      #(
         .DATA_W(16),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(8),
         .BYTEENA_W(2),
         .ID_W(1)
      ) in_arb_intf();
      acl_arb_intf
      #(
         .DATA_W(16),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(8),
         .BYTEENA_W(2),
         .ID_W(1)
      ) out_arb_intf();
      acl_ic_wrp_intf
      #(
         .ID_W(1)
      ) wrp_intf();
      acl_ic_rrp_intf
      #(
         .DATA_W(16),
         .ID_W(1)
      ) rrp_intf();

      // INST s_endp of acl_ic_slave_endpoint
      acl_ic_slave_endpoint
      #(
         .DATA_W(16),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(8),
         .BYTEENA_W(2),
         .ID_W(1),
         .NUM_MASTERS(2),
         .PIPELINE_RETURN_PATHS(0),
         .WRP_FIFO_DEPTH(0),
         .RRP_FIFO_DEPTH(0),
         .RRP_USE_LL_FIFO(1),
         .SLAVE_FIXED_LATENCY(4),
         .SEPARATE_READ_WRITE_STALLS(0)
      )
      s_endp
      (
         .clock(clock),
         .resetn(resetn),
         .m_intf(in_arb_intf),
         .s_intf(out_arb_intf),
         .s_readdatavalid(mout_rrp_datavalid),
         .s_readdata(mout_rrp_data),
         .s_writeack(mout_wrp_ack),
         .wrp_intf(wrp_intf),
         .rrp_intf(rrp_intf)
      );

   end
   endgenerate

   generate
   begin:wrp
   end
   endgenerate

   generate
   begin:rrp
      assign m[0].rrp_intf.datavalid = s.rrp_intf.datavalid;
      assign m[0].rrp_intf.data = s.rrp_intf.data;
      assign m[0].rrp_intf.id = s.rrp_intf.id;
      assign m[1].rrp_intf.datavalid = s.rrp_intf.datavalid;
      assign m[1].rrp_intf.data = s.rrp_intf.data;
      assign m[1].rrp_intf.id = s.rrp_intf.id;
   end
   endgenerate

   generate
      for( __i = 0; __i < 1; __i = __i + 1 )
      begin:a
         acl_arb_intf
         #(
            .DATA_W(16),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(8),
            .BYTEENA_W(2),
            .ID_W(1)
         ) m0_intf();
         acl_arb_intf
         #(
            .DATA_W(16),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(8),
            .BYTEENA_W(2),
            .ID_W(1)
         ) m1_intf();
         acl_arb_intf
         #(
            .DATA_W(16),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(8),
            .BYTEENA_W(2),
            .ID_W(1)
         ) mout_intf();

         // INST a of acl_arb2
         acl_arb2
         #(
            .DATA_W(16),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(8),
            .BYTEENA_W(2),
            .ID_W(1),
            .PIPELINE("none"),
            .KEEP_LAST_GRANT(0),
            .NO_STALL_NETWORK(0)
         )
         a
         (
            .clock(clock),
            .resetn(resetn),
            .m0_intf(m0_intf),
            .m1_intf(m1_intf),
            .mout_intf(mout_intf)
         );

      end

   endgenerate

   generate
      for( __i = 0; __i < 1; __i = __i + 1 )
      begin:dp
         acl_arb_intf
         #(
            .DATA_W(16),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(8),
            .BYTEENA_W(2),
            .ID_W(1)
         ) in_intf();
         acl_arb_intf
         #(
            .DATA_W(16),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(8),
            .BYTEENA_W(2),
            .ID_W(1)
         ) out_intf();

         // INST dp of acl_arb_pipeline_reg
         acl_arb_pipeline_reg
         #(
            .DATA_W(16),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(8),
            .BYTEENA_W(2),
            .ID_W(1)
         )
         dp
         (
            .clock(clock),
            .resetn(resetn),
            .in_intf(in_intf),
            .out_intf(out_intf)
         );

      end

   endgenerate

   assign mout_arb_request = s.out_arb_intf.req.request;
   assign mout_arb_enable = s.out_arb_intf.req.enable;
   assign mout_arb_read = s.out_arb_intf.req.read;
   assign mout_arb_write = s.out_arb_intf.req.write;
   assign mout_arb_burstcount = s.out_arb_intf.req.burstcount;
   assign mout_arb_address = s.out_arb_intf.req.address;
   assign mout_arb_writedata = s.out_arb_intf.req.writedata;
   assign mout_arb_byteenable = s.out_arb_intf.req.byteenable;
   assign mout_arb_id = s.out_arb_intf.req.id;
   assign s.out_arb_intf.stall = mout_arb_stall;
   assign s.in_arb_intf.req = dp[0].out_intf.req;
   assign dp[0].out_intf.stall = s.in_arb_intf.stall;
   assign dp[0].in_intf.req = a[0].mout_intf.req;
   assign a[0].mout_intf.stall = dp[0].in_intf.stall;
   assign a[0].m0_intf.req = m[0].arb_intf.req;
   assign m[0].arb_intf.stall = a[0].m0_intf.stall;
   assign a[0].m1_intf.req = m[1].arb_intf.req;
   assign m[1].arb_intf.stall = a[0].m1_intf.stall;
endmodule

/////////////////////////////////////////////////////////////////
// MODULE conv_pipe_system_interconnect_2
/////////////////////////////////////////////////////////////////
module conv_pipe_system_interconnect_2
(
   input logic clock,
   input logic resetn,
   // ICM m
   input logic m_arb_request [1],
   input logic m_arb_enable [1],
   input logic m_arb_read [1],
   input logic m_arb_write [1],
   input logic m_arb_burstcount [1],
   input logic [7:0] m_arb_address [1],
   input logic [15:0] m_arb_writedata [1],
   input logic [1:0] m_arb_byteenable [1],
   output logic m_arb_stall [1],
   output logic m_wrp_ack [1],
   output logic m_rrp_datavalid [1],
   output logic [15:0] m_rrp_data [1],
   // ICM mout
   output logic mout_arb_request,
   output logic mout_arb_enable,
   output logic mout_arb_read,
   output logic mout_arb_write,
   output logic mout_arb_burstcount,
   output logic [7:0] mout_arb_address,
   output logic [15:0] mout_arb_writedata,
   output logic [1:0] mout_arb_byteenable,
   output logic mout_arb_id,
   input logic mout_arb_stall,
   input logic mout_wrp_ack,
   input logic mout_rrp_datavalid,
   input logic [15:0] mout_rrp_data
);
   genvar __i;
   generate
      for( __i = 0; __i < 1; __i = __i + 1 )
      begin:m
         logic id;
         acl_ic_master_intf
         #(
            .DATA_W(16),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(8),
            .BYTEENA_W(2),
            .ID_W(1)
         ) m_intf();
         acl_arb_intf
         #(
            .DATA_W(16),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(8),
            .BYTEENA_W(2),
            .ID_W(1)
         ) arb_intf();
         acl_ic_wrp_intf
         #(
            .ID_W(1)
         ) wrp_intf();
         acl_ic_rrp_intf
         #(
            .DATA_W(16),
            .ID_W(1)
         ) rrp_intf();

         assign id = __i;
         // INST m_endp of acl_ic_master_endpoint
         acl_ic_master_endpoint
         #(
            .DATA_W(16),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(8),
            .BYTEENA_W(2),
            .ID_W(1),
            .TOTAL_NUM_MASTERS(1),
            .ID(__i)
         )
         m_endp
         (
            .clock(clock),
            .resetn(resetn),
            .m_intf(m_intf),
            .arb_intf(arb_intf),
            .wrp_intf(wrp_intf),
            .rrp_intf(rrp_intf)
         );

         assign m_intf.arb.req.request = m_arb_request[__i];
         assign m_intf.arb.req.enable = m_arb_enable[__i];
         assign m_intf.arb.req.read = m_arb_read[__i];
         assign m_intf.arb.req.write = m_arb_write[__i];
         assign m_intf.arb.req.burstcount = m_arb_burstcount[__i];
         assign m_intf.arb.req.address = m_arb_address[__i];
         assign m_intf.arb.req.writedata = m_arb_writedata[__i];
         assign m_intf.arb.req.byteenable = m_arb_byteenable[__i];
         assign m_arb_stall[__i] = m_intf.arb.stall;
         assign m_wrp_ack[__i] = m_intf.wrp.ack;
         assign m_rrp_datavalid[__i] = m_intf.rrp.datavalid;
         assign m_rrp_data[__i] = m_intf.rrp.data;
         assign m_intf.arb.req.id = id;
      end

   endgenerate

   generate
   begin:s
      acl_arb_intf
      #(
         .DATA_W(16),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(8),
         .BYTEENA_W(2),
         .ID_W(1)
      ) in_arb_intf();
      acl_arb_intf
      #(
         .DATA_W(16),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(8),
         .BYTEENA_W(2),
         .ID_W(1)
      ) out_arb_intf();
      acl_ic_wrp_intf
      #(
         .ID_W(1)
      ) wrp_intf();
      acl_ic_rrp_intf
      #(
         .DATA_W(16),
         .ID_W(1)
      ) rrp_intf();

      // INST s_endp of acl_ic_slave_endpoint
      acl_ic_slave_endpoint
      #(
         .DATA_W(16),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(8),
         .BYTEENA_W(2),
         .ID_W(1),
         .NUM_MASTERS(1),
         .PIPELINE_RETURN_PATHS(0),
         .WRP_FIFO_DEPTH(0),
         .RRP_FIFO_DEPTH(0),
         .RRP_USE_LL_FIFO(1),
         .SLAVE_FIXED_LATENCY(4),
         .SEPARATE_READ_WRITE_STALLS(0)
      )
      s_endp
      (
         .clock(clock),
         .resetn(resetn),
         .m_intf(in_arb_intf),
         .s_intf(out_arb_intf),
         .s_readdatavalid(mout_rrp_datavalid),
         .s_readdata(mout_rrp_data),
         .s_writeack(mout_wrp_ack),
         .wrp_intf(wrp_intf),
         .rrp_intf(rrp_intf)
      );

   end
   endgenerate

   generate
   begin:wrp
   end
   endgenerate

   generate
   begin:rrp
      assign m[0].rrp_intf.datavalid = s.rrp_intf.datavalid;
      assign m[0].rrp_intf.data = s.rrp_intf.data;
      assign m[0].rrp_intf.id = s.rrp_intf.id;
   end
   endgenerate

   assign mout_arb_request = s.out_arb_intf.req.request;
   assign mout_arb_enable = s.out_arb_intf.req.enable;
   assign mout_arb_read = s.out_arb_intf.req.read;
   assign mout_arb_write = s.out_arb_intf.req.write;
   assign mout_arb_burstcount = s.out_arb_intf.req.burstcount;
   assign mout_arb_address = s.out_arb_intf.req.address;
   assign mout_arb_writedata = s.out_arb_intf.req.writedata;
   assign mout_arb_byteenable = s.out_arb_intf.req.byteenable;
   assign mout_arb_id = s.out_arb_intf.req.id;
   assign s.out_arb_intf.stall = mout_arb_stall;
   assign s.in_arb_intf.req = m[0].arb_intf.req;
   assign m[0].arb_intf.stall = s.in_arb_intf.stall;
endmodule

/////////////////////////////////////////////////////////////////
// MODULE conv_pipe_system_interconnect_3
/////////////////////////////////////////////////////////////////
module conv_pipe_system_interconnect_3
(
   input logic clock,
   input logic resetn,
   // ICM m
   input logic m_arb_request [1],
   input logic m_arb_enable [1],
   input logic m_arb_read [1],
   input logic m_arb_write [1],
   input logic m_arb_burstcount [1],
   input logic [7:0] m_arb_address [1],
   input logic [15:0] m_arb_writedata [1],
   input logic [1:0] m_arb_byteenable [1],
   output logic m_arb_stall [1],
   output logic m_wrp_ack [1],
   output logic m_rrp_datavalid [1],
   output logic [15:0] m_rrp_data [1],
   // ICM mout
   output logic mout_arb_request,
   output logic mout_arb_enable,
   output logic mout_arb_read,
   output logic mout_arb_write,
   output logic mout_arb_burstcount,
   output logic [7:0] mout_arb_address,
   output logic [15:0] mout_arb_writedata,
   output logic [1:0] mout_arb_byteenable,
   output logic mout_arb_id,
   input logic mout_arb_stall,
   input logic mout_wrp_ack,
   input logic mout_rrp_datavalid,
   input logic [15:0] mout_rrp_data
);
   genvar __i;
   generate
      for( __i = 0; __i < 1; __i = __i + 1 )
      begin:m
         logic id;
         acl_ic_master_intf
         #(
            .DATA_W(16),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(8),
            .BYTEENA_W(2),
            .ID_W(1)
         ) m_intf();
         acl_arb_intf
         #(
            .DATA_W(16),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(8),
            .BYTEENA_W(2),
            .ID_W(1)
         ) arb_intf();
         acl_ic_wrp_intf
         #(
            .ID_W(1)
         ) wrp_intf();
         acl_ic_rrp_intf
         #(
            .DATA_W(16),
            .ID_W(1)
         ) rrp_intf();

         assign id = __i;
         // INST m_endp of acl_ic_master_endpoint
         acl_ic_master_endpoint
         #(
            .DATA_W(16),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(8),
            .BYTEENA_W(2),
            .ID_W(1),
            .TOTAL_NUM_MASTERS(1),
            .ID(__i)
         )
         m_endp
         (
            .clock(clock),
            .resetn(resetn),
            .m_intf(m_intf),
            .arb_intf(arb_intf),
            .wrp_intf(wrp_intf),
            .rrp_intf(rrp_intf)
         );

         assign m_intf.arb.req.request = m_arb_request[__i];
         assign m_intf.arb.req.enable = m_arb_enable[__i];
         assign m_intf.arb.req.read = m_arb_read[__i];
         assign m_intf.arb.req.write = m_arb_write[__i];
         assign m_intf.arb.req.burstcount = m_arb_burstcount[__i];
         assign m_intf.arb.req.address = m_arb_address[__i];
         assign m_intf.arb.req.writedata = m_arb_writedata[__i];
         assign m_intf.arb.req.byteenable = m_arb_byteenable[__i];
         assign m_arb_stall[__i] = m_intf.arb.stall;
         assign m_wrp_ack[__i] = m_intf.wrp.ack;
         assign m_rrp_datavalid[__i] = m_intf.rrp.datavalid;
         assign m_rrp_data[__i] = m_intf.rrp.data;
         assign m_intf.arb.req.id = id;
      end

   endgenerate

   generate
   begin:s
      acl_arb_intf
      #(
         .DATA_W(16),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(8),
         .BYTEENA_W(2),
         .ID_W(1)
      ) in_arb_intf();
      acl_arb_intf
      #(
         .DATA_W(16),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(8),
         .BYTEENA_W(2),
         .ID_W(1)
      ) out_arb_intf();
      acl_ic_wrp_intf
      #(
         .ID_W(1)
      ) wrp_intf();
      acl_ic_rrp_intf
      #(
         .DATA_W(16),
         .ID_W(1)
      ) rrp_intf();

      // INST s_endp of acl_ic_slave_endpoint
      acl_ic_slave_endpoint
      #(
         .DATA_W(16),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(8),
         .BYTEENA_W(2),
         .ID_W(1),
         .NUM_MASTERS(1),
         .PIPELINE_RETURN_PATHS(0),
         .WRP_FIFO_DEPTH(0),
         .RRP_FIFO_DEPTH(0),
         .RRP_USE_LL_FIFO(1),
         .SLAVE_FIXED_LATENCY(4),
         .SEPARATE_READ_WRITE_STALLS(0)
      )
      s_endp
      (
         .clock(clock),
         .resetn(resetn),
         .m_intf(in_arb_intf),
         .s_intf(out_arb_intf),
         .s_readdatavalid(mout_rrp_datavalid),
         .s_readdata(mout_rrp_data),
         .s_writeack(mout_wrp_ack),
         .wrp_intf(wrp_intf),
         .rrp_intf(rrp_intf)
      );

   end
   endgenerate

   generate
   begin:wrp
   end
   endgenerate

   generate
   begin:rrp
      assign m[0].rrp_intf.datavalid = s.rrp_intf.datavalid;
      assign m[0].rrp_intf.data = s.rrp_intf.data;
      assign m[0].rrp_intf.id = s.rrp_intf.id;
   end
   endgenerate

   generate
      for( __i = 0; __i < 1; __i = __i + 1 )
      begin:dp
         acl_arb_intf
         #(
            .DATA_W(16),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(8),
            .BYTEENA_W(2),
            .ID_W(1)
         ) in_intf();
         acl_arb_intf
         #(
            .DATA_W(16),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(8),
            .BYTEENA_W(2),
            .ID_W(1)
         ) out_intf();

         // INST dp of acl_arb_pipeline_reg
         acl_arb_pipeline_reg
         #(
            .DATA_W(16),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(8),
            .BYTEENA_W(2),
            .ID_W(1)
         )
         dp
         (
            .clock(clock),
            .resetn(resetn),
            .in_intf(in_intf),
            .out_intf(out_intf)
         );

      end

   endgenerate

   assign mout_arb_request = s.out_arb_intf.req.request;
   assign mout_arb_enable = s.out_arb_intf.req.enable;
   assign mout_arb_read = s.out_arb_intf.req.read;
   assign mout_arb_write = s.out_arb_intf.req.write;
   assign mout_arb_burstcount = s.out_arb_intf.req.burstcount;
   assign mout_arb_address = s.out_arb_intf.req.address;
   assign mout_arb_writedata = s.out_arb_intf.req.writedata;
   assign mout_arb_byteenable = s.out_arb_intf.req.byteenable;
   assign mout_arb_id = s.out_arb_intf.req.id;
   assign s.out_arb_intf.stall = mout_arb_stall;
   assign s.in_arb_intf.req = dp[0].out_intf.req;
   assign dp[0].out_intf.stall = s.in_arb_intf.stall;
   assign dp[0].in_intf.req = m[0].arb_intf.req;
   assign m[0].arb_intf.stall = dp[0].in_intf.stall;
endmodule

/////////////////////////////////////////////////////////////////
// MODULE conv_pipe_system_interconnect_4
/////////////////////////////////////////////////////////////////
module conv_pipe_system_interconnect_4
(
   input logic clock,
   input logic resetn,
   // ICM m
   input logic m_arb_request [1],
   input logic m_arb_enable [1],
   input logic m_arb_read [1],
   input logic m_arb_write [1],
   input logic m_arb_burstcount [1],
   input logic [7:0] m_arb_address [1],
   input logic [31:0] m_arb_writedata [1],
   input logic [3:0] m_arb_byteenable [1],
   output logic m_arb_stall [1],
   output logic m_wrp_ack [1],
   output logic m_rrp_datavalid [1],
   output logic [31:0] m_rrp_data [1],
   // ICM mout
   output logic mout_arb_request,
   output logic mout_arb_enable,
   output logic mout_arb_read,
   output logic mout_arb_write,
   output logic mout_arb_burstcount,
   output logic [7:0] mout_arb_address,
   output logic [31:0] mout_arb_writedata,
   output logic [3:0] mout_arb_byteenable,
   output logic mout_arb_id,
   input logic mout_arb_stall,
   input logic mout_wrp_ack,
   input logic mout_rrp_datavalid,
   input logic [31:0] mout_rrp_data
);
   genvar __i;
   generate
      for( __i = 0; __i < 1; __i = __i + 1 )
      begin:m
         logic id;
         acl_ic_master_intf
         #(
            .DATA_W(32),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(8),
            .BYTEENA_W(4),
            .ID_W(1)
         ) m_intf();
         acl_arb_intf
         #(
            .DATA_W(32),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(8),
            .BYTEENA_W(4),
            .ID_W(1)
         ) arb_intf();
         acl_ic_wrp_intf
         #(
            .ID_W(1)
         ) wrp_intf();
         acl_ic_rrp_intf
         #(
            .DATA_W(32),
            .ID_W(1)
         ) rrp_intf();

         assign id = __i;
         // INST m_endp of acl_ic_master_endpoint
         acl_ic_master_endpoint
         #(
            .DATA_W(32),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(8),
            .BYTEENA_W(4),
            .ID_W(1),
            .TOTAL_NUM_MASTERS(1),
            .ID(__i)
         )
         m_endp
         (
            .clock(clock),
            .resetn(resetn),
            .m_intf(m_intf),
            .arb_intf(arb_intf),
            .wrp_intf(wrp_intf),
            .rrp_intf(rrp_intf)
         );

         assign m_intf.arb.req.request = m_arb_request[__i];
         assign m_intf.arb.req.enable = m_arb_enable[__i];
         assign m_intf.arb.req.read = m_arb_read[__i];
         assign m_intf.arb.req.write = m_arb_write[__i];
         assign m_intf.arb.req.burstcount = m_arb_burstcount[__i];
         assign m_intf.arb.req.address = m_arb_address[__i];
         assign m_intf.arb.req.writedata = m_arb_writedata[__i];
         assign m_intf.arb.req.byteenable = m_arb_byteenable[__i];
         assign m_arb_stall[__i] = m_intf.arb.stall;
         assign m_wrp_ack[__i] = m_intf.wrp.ack;
         assign m_rrp_datavalid[__i] = m_intf.rrp.datavalid;
         assign m_rrp_data[__i] = m_intf.rrp.data;
         assign m_intf.arb.req.id = id;
      end

   endgenerate

   generate
   begin:s
      acl_arb_intf
      #(
         .DATA_W(32),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(8),
         .BYTEENA_W(4),
         .ID_W(1)
      ) in_arb_intf();
      acl_arb_intf
      #(
         .DATA_W(32),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(8),
         .BYTEENA_W(4),
         .ID_W(1)
      ) out_arb_intf();
      acl_ic_wrp_intf
      #(
         .ID_W(1)
      ) wrp_intf();
      acl_ic_rrp_intf
      #(
         .DATA_W(32),
         .ID_W(1)
      ) rrp_intf();

      // INST s_endp of acl_ic_slave_endpoint
      acl_ic_slave_endpoint
      #(
         .DATA_W(32),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(8),
         .BYTEENA_W(4),
         .ID_W(1),
         .NUM_MASTERS(1),
         .PIPELINE_RETURN_PATHS(0),
         .WRP_FIFO_DEPTH(0),
         .RRP_FIFO_DEPTH(0),
         .RRP_USE_LL_FIFO(1),
         .SLAVE_FIXED_LATENCY(3),
         .SEPARATE_READ_WRITE_STALLS(0)
      )
      s_endp
      (
         .clock(clock),
         .resetn(resetn),
         .m_intf(in_arb_intf),
         .s_intf(out_arb_intf),
         .s_readdatavalid(mout_rrp_datavalid),
         .s_readdata(mout_rrp_data),
         .s_writeack(mout_wrp_ack),
         .wrp_intf(wrp_intf),
         .rrp_intf(rrp_intf)
      );

   end
   endgenerate

   generate
   begin:wrp
      assign m[0].wrp_intf.ack = s.wrp_intf.ack;
      assign m[0].wrp_intf.id = s.wrp_intf.id;
   end
   endgenerate

   generate
   begin:rrp
   end
   endgenerate

   assign mout_arb_request = s.out_arb_intf.req.request;
   assign mout_arb_enable = s.out_arb_intf.req.enable;
   assign mout_arb_read = s.out_arb_intf.req.read;
   assign mout_arb_write = s.out_arb_intf.req.write;
   assign mout_arb_burstcount = s.out_arb_intf.req.burstcount;
   assign mout_arb_address = s.out_arb_intf.req.address;
   assign mout_arb_writedata = s.out_arb_intf.req.writedata;
   assign mout_arb_byteenable = s.out_arb_intf.req.byteenable;
   assign mout_arb_id = s.out_arb_intf.req.id;
   assign s.out_arb_intf.stall = mout_arb_stall;
   assign s.in_arb_intf.req = m[0].arb_intf.req;
   assign m[0].arb_intf.stall = s.in_arb_intf.stall;
endmodule

/////////////////////////////////////////////////////////////////
// MODULE conv_pipe_system_interconnect_5
/////////////////////////////////////////////////////////////////
module conv_pipe_system_interconnect_5
(
   input logic clock,
   input logic resetn,
   // ICM m
   input logic m_arb_request [1],
   input logic m_arb_enable [1],
   input logic m_arb_read [1],
   input logic m_arb_write [1],
   input logic m_arb_burstcount [1],
   input logic [7:0] m_arb_address [1],
   input logic [31:0] m_arb_writedata [1],
   input logic [3:0] m_arb_byteenable [1],
   output logic m_arb_stall [1],
   output logic m_wrp_ack [1],
   output logic m_rrp_datavalid [1],
   output logic [31:0] m_rrp_data [1],
   // ICM mout
   output logic mout_arb_request,
   output logic mout_arb_enable,
   output logic mout_arb_read,
   output logic mout_arb_write,
   output logic mout_arb_burstcount,
   output logic [7:0] mout_arb_address,
   output logic [31:0] mout_arb_writedata,
   output logic [3:0] mout_arb_byteenable,
   output logic mout_arb_id,
   input logic mout_arb_stall,
   input logic mout_wrp_ack,
   input logic mout_rrp_datavalid,
   input logic [31:0] mout_rrp_data
);
   genvar __i;
   generate
      for( __i = 0; __i < 1; __i = __i + 1 )
      begin:m
         logic id;
         acl_ic_master_intf
         #(
            .DATA_W(32),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(8),
            .BYTEENA_W(4),
            .ID_W(1)
         ) m_intf();
         acl_arb_intf
         #(
            .DATA_W(32),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(8),
            .BYTEENA_W(4),
            .ID_W(1)
         ) arb_intf();
         acl_ic_wrp_intf
         #(
            .ID_W(1)
         ) wrp_intf();
         acl_ic_rrp_intf
         #(
            .DATA_W(32),
            .ID_W(1)
         ) rrp_intf();

         assign id = __i;
         // INST m_endp of acl_ic_master_endpoint
         acl_ic_master_endpoint
         #(
            .DATA_W(32),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(8),
            .BYTEENA_W(4),
            .ID_W(1),
            .TOTAL_NUM_MASTERS(1),
            .ID(__i)
         )
         m_endp
         (
            .clock(clock),
            .resetn(resetn),
            .m_intf(m_intf),
            .arb_intf(arb_intf),
            .wrp_intf(wrp_intf),
            .rrp_intf(rrp_intf)
         );

         assign m_intf.arb.req.request = m_arb_request[__i];
         assign m_intf.arb.req.enable = m_arb_enable[__i];
         assign m_intf.arb.req.read = m_arb_read[__i];
         assign m_intf.arb.req.write = m_arb_write[__i];
         assign m_intf.arb.req.burstcount = m_arb_burstcount[__i];
         assign m_intf.arb.req.address = m_arb_address[__i];
         assign m_intf.arb.req.writedata = m_arb_writedata[__i];
         assign m_intf.arb.req.byteenable = m_arb_byteenable[__i];
         assign m_arb_stall[__i] = m_intf.arb.stall;
         assign m_wrp_ack[__i] = m_intf.wrp.ack;
         assign m_rrp_datavalid[__i] = m_intf.rrp.datavalid;
         assign m_rrp_data[__i] = m_intf.rrp.data;
         assign m_intf.arb.req.id = id;
      end

   endgenerate

   generate
   begin:s
      acl_arb_intf
      #(
         .DATA_W(32),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(8),
         .BYTEENA_W(4),
         .ID_W(1)
      ) in_arb_intf();
      acl_arb_intf
      #(
         .DATA_W(32),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(8),
         .BYTEENA_W(4),
         .ID_W(1)
      ) out_arb_intf();
      acl_ic_wrp_intf
      #(
         .ID_W(1)
      ) wrp_intf();
      acl_ic_rrp_intf
      #(
         .DATA_W(32),
         .ID_W(1)
      ) rrp_intf();

      // INST s_endp of acl_ic_slave_endpoint
      acl_ic_slave_endpoint
      #(
         .DATA_W(32),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(8),
         .BYTEENA_W(4),
         .ID_W(1),
         .NUM_MASTERS(1),
         .PIPELINE_RETURN_PATHS(0),
         .WRP_FIFO_DEPTH(0),
         .RRP_FIFO_DEPTH(0),
         .RRP_USE_LL_FIFO(1),
         .SLAVE_FIXED_LATENCY(3),
         .SEPARATE_READ_WRITE_STALLS(0)
      )
      s_endp
      (
         .clock(clock),
         .resetn(resetn),
         .m_intf(in_arb_intf),
         .s_intf(out_arb_intf),
         .s_readdatavalid(mout_rrp_datavalid),
         .s_readdata(mout_rrp_data),
         .s_writeack(mout_wrp_ack),
         .wrp_intf(wrp_intf),
         .rrp_intf(rrp_intf)
      );

   end
   endgenerate

   generate
   begin:wrp
   end
   endgenerate

   generate
   begin:rrp
      assign m[0].rrp_intf.datavalid = s.rrp_intf.datavalid;
      assign m[0].rrp_intf.data = s.rrp_intf.data;
      assign m[0].rrp_intf.id = s.rrp_intf.id;
   end
   endgenerate

   assign mout_arb_request = s.out_arb_intf.req.request;
   assign mout_arb_enable = s.out_arb_intf.req.enable;
   assign mout_arb_read = s.out_arb_intf.req.read;
   assign mout_arb_write = s.out_arb_intf.req.write;
   assign mout_arb_burstcount = s.out_arb_intf.req.burstcount;
   assign mout_arb_address = s.out_arb_intf.req.address;
   assign mout_arb_writedata = s.out_arb_intf.req.writedata;
   assign mout_arb_byteenable = s.out_arb_intf.req.byteenable;
   assign mout_arb_id = s.out_arb_intf.req.id;
   assign s.out_arb_intf.stall = mout_arb_stall;
   assign s.in_arb_intf.req = m[0].arb_intf.req;
   assign m[0].arb_intf.stall = s.in_arb_intf.stall;
endmodule

/////////////////////////////////////////////////////////////////
// MODULE conv_pipe_system_interconnect_6
/////////////////////////////////////////////////////////////////
module conv_pipe_system_interconnect_6
(
   input logic clock,
   input logic resetn,
   // ICM m
   input logic m_arb_request [1],
   input logic m_arb_enable [1],
   input logic m_arb_read [1],
   input logic m_arb_write [1],
   input logic m_arb_burstcount [1],
   input logic [7:0] m_arb_address [1],
   input logic [7:0] m_arb_writedata [1],
   input logic m_arb_byteenable [1],
   output logic m_arb_stall [1],
   output logic m_wrp_ack [1],
   output logic m_rrp_datavalid [1],
   output logic [7:0] m_rrp_data [1],
   // ICM mout
   output logic mout_arb_request,
   output logic mout_arb_enable,
   output logic mout_arb_read,
   output logic mout_arb_write,
   output logic mout_arb_burstcount,
   output logic [7:0] mout_arb_address,
   output logic [7:0] mout_arb_writedata,
   output logic mout_arb_byteenable,
   output logic mout_arb_id,
   input logic mout_arb_stall,
   input logic mout_wrp_ack,
   input logic mout_rrp_datavalid,
   input logic [7:0] mout_rrp_data
);
   genvar __i;
   generate
      for( __i = 0; __i < 1; __i = __i + 1 )
      begin:m
         logic id;
         acl_ic_master_intf
         #(
            .DATA_W(8),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(8),
            .BYTEENA_W(1),
            .ID_W(1)
         ) m_intf();
         acl_arb_intf
         #(
            .DATA_W(8),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(8),
            .BYTEENA_W(1),
            .ID_W(1)
         ) arb_intf();
         acl_ic_wrp_intf
         #(
            .ID_W(1)
         ) wrp_intf();
         acl_ic_rrp_intf
         #(
            .DATA_W(8),
            .ID_W(1)
         ) rrp_intf();

         assign id = __i;
         // INST m_endp of acl_ic_master_endpoint
         acl_ic_master_endpoint
         #(
            .DATA_W(8),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(8),
            .BYTEENA_W(1),
            .ID_W(1),
            .TOTAL_NUM_MASTERS(1),
            .ID(__i)
         )
         m_endp
         (
            .clock(clock),
            .resetn(resetn),
            .m_intf(m_intf),
            .arb_intf(arb_intf),
            .wrp_intf(wrp_intf),
            .rrp_intf(rrp_intf)
         );

         assign m_intf.arb.req.request = m_arb_request[__i];
         assign m_intf.arb.req.enable = m_arb_enable[__i];
         assign m_intf.arb.req.read = m_arb_read[__i];
         assign m_intf.arb.req.write = m_arb_write[__i];
         assign m_intf.arb.req.burstcount = m_arb_burstcount[__i];
         assign m_intf.arb.req.address = m_arb_address[__i];
         assign m_intf.arb.req.writedata = m_arb_writedata[__i];
         assign m_intf.arb.req.byteenable = m_arb_byteenable[__i];
         assign m_arb_stall[__i] = m_intf.arb.stall;
         assign m_wrp_ack[__i] = m_intf.wrp.ack;
         assign m_rrp_datavalid[__i] = m_intf.rrp.datavalid;
         assign m_rrp_data[__i] = m_intf.rrp.data;
         assign m_intf.arb.req.id = id;
      end

   endgenerate

   generate
   begin:s
      acl_arb_intf
      #(
         .DATA_W(8),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(8),
         .BYTEENA_W(1),
         .ID_W(1)
      ) in_arb_intf();
      acl_arb_intf
      #(
         .DATA_W(8),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(8),
         .BYTEENA_W(1),
         .ID_W(1)
      ) out_arb_intf();
      acl_ic_wrp_intf
      #(
         .ID_W(1)
      ) wrp_intf();
      acl_ic_rrp_intf
      #(
         .DATA_W(8),
         .ID_W(1)
      ) rrp_intf();

      // INST s_endp of acl_ic_slave_endpoint
      acl_ic_slave_endpoint
      #(
         .DATA_W(8),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(8),
         .BYTEENA_W(1),
         .ID_W(1),
         .NUM_MASTERS(1),
         .PIPELINE_RETURN_PATHS(0),
         .WRP_FIFO_DEPTH(0),
         .RRP_FIFO_DEPTH(0),
         .RRP_USE_LL_FIFO(1),
         .SLAVE_FIXED_LATENCY(3),
         .SEPARATE_READ_WRITE_STALLS(0)
      )
      s_endp
      (
         .clock(clock),
         .resetn(resetn),
         .m_intf(in_arb_intf),
         .s_intf(out_arb_intf),
         .s_readdatavalid(mout_rrp_datavalid),
         .s_readdata(mout_rrp_data),
         .s_writeack(mout_wrp_ack),
         .wrp_intf(wrp_intf),
         .rrp_intf(rrp_intf)
      );

   end
   endgenerate

   generate
   begin:wrp
   end
   endgenerate

   generate
   begin:rrp
      assign m[0].rrp_intf.datavalid = s.rrp_intf.datavalid;
      assign m[0].rrp_intf.data = s.rrp_intf.data;
      assign m[0].rrp_intf.id = s.rrp_intf.id;
   end
   endgenerate

   assign mout_arb_request = s.out_arb_intf.req.request;
   assign mout_arb_enable = s.out_arb_intf.req.enable;
   assign mout_arb_read = s.out_arb_intf.req.read;
   assign mout_arb_write = s.out_arb_intf.req.write;
   assign mout_arb_burstcount = s.out_arb_intf.req.burstcount;
   assign mout_arb_address = s.out_arb_intf.req.address;
   assign mout_arb_writedata = s.out_arb_intf.req.writedata;
   assign mout_arb_byteenable = s.out_arb_intf.req.byteenable;
   assign mout_arb_id = s.out_arb_intf.req.id;
   assign s.out_arb_intf.stall = mout_arb_stall;
   assign s.in_arb_intf.req = m[0].arb_intf.req;
   assign m[0].arb_intf.stall = s.in_arb_intf.stall;
endmodule

/////////////////////////////////////////////////////////////////
// MODULE conv_pipe_system_interconnect_7
/////////////////////////////////////////////////////////////////
module conv_pipe_system_interconnect_7
(
   input logic clock,
   input logic resetn,
   // ICM m
   input logic m_arb_request [1],
   input logic m_arb_enable [1],
   input logic m_arb_read [1],
   input logic m_arb_write [1],
   input logic m_arb_burstcount [1],
   input logic [7:0] m_arb_address [1],
   input logic [7:0] m_arb_writedata [1],
   input logic m_arb_byteenable [1],
   output logic m_arb_stall [1],
   output logic m_wrp_ack [1],
   output logic m_rrp_datavalid [1],
   output logic [7:0] m_rrp_data [1],
   // ICM mout
   output logic mout_arb_request,
   output logic mout_arb_enable,
   output logic mout_arb_read,
   output logic mout_arb_write,
   output logic mout_arb_burstcount,
   output logic [7:0] mout_arb_address,
   output logic [7:0] mout_arb_writedata,
   output logic mout_arb_byteenable,
   output logic mout_arb_id,
   input logic mout_arb_stall,
   input logic mout_wrp_ack,
   input logic mout_rrp_datavalid,
   input logic [7:0] mout_rrp_data
);
   genvar __i;
   generate
      for( __i = 0; __i < 1; __i = __i + 1 )
      begin:m
         logic id;
         acl_ic_master_intf
         #(
            .DATA_W(8),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(8),
            .BYTEENA_W(1),
            .ID_W(1)
         ) m_intf();
         acl_arb_intf
         #(
            .DATA_W(8),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(8),
            .BYTEENA_W(1),
            .ID_W(1)
         ) arb_intf();
         acl_ic_wrp_intf
         #(
            .ID_W(1)
         ) wrp_intf();
         acl_ic_rrp_intf
         #(
            .DATA_W(8),
            .ID_W(1)
         ) rrp_intf();

         assign id = __i;
         // INST m_endp of acl_ic_master_endpoint
         acl_ic_master_endpoint
         #(
            .DATA_W(8),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(8),
            .BYTEENA_W(1),
            .ID_W(1),
            .TOTAL_NUM_MASTERS(1),
            .ID(__i)
         )
         m_endp
         (
            .clock(clock),
            .resetn(resetn),
            .m_intf(m_intf),
            .arb_intf(arb_intf),
            .wrp_intf(wrp_intf),
            .rrp_intf(rrp_intf)
         );

         assign m_intf.arb.req.request = m_arb_request[__i];
         assign m_intf.arb.req.enable = m_arb_enable[__i];
         assign m_intf.arb.req.read = m_arb_read[__i];
         assign m_intf.arb.req.write = m_arb_write[__i];
         assign m_intf.arb.req.burstcount = m_arb_burstcount[__i];
         assign m_intf.arb.req.address = m_arb_address[__i];
         assign m_intf.arb.req.writedata = m_arb_writedata[__i];
         assign m_intf.arb.req.byteenable = m_arb_byteenable[__i];
         assign m_arb_stall[__i] = m_intf.arb.stall;
         assign m_wrp_ack[__i] = m_intf.wrp.ack;
         assign m_rrp_datavalid[__i] = m_intf.rrp.datavalid;
         assign m_rrp_data[__i] = m_intf.rrp.data;
         assign m_intf.arb.req.id = id;
      end

   endgenerate

   generate
   begin:s
      acl_arb_intf
      #(
         .DATA_W(8),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(8),
         .BYTEENA_W(1),
         .ID_W(1)
      ) in_arb_intf();
      acl_arb_intf
      #(
         .DATA_W(8),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(8),
         .BYTEENA_W(1),
         .ID_W(1)
      ) out_arb_intf();
      acl_ic_wrp_intf
      #(
         .ID_W(1)
      ) wrp_intf();
      acl_ic_rrp_intf
      #(
         .DATA_W(8),
         .ID_W(1)
      ) rrp_intf();

      // INST s_endp of acl_ic_slave_endpoint
      acl_ic_slave_endpoint
      #(
         .DATA_W(8),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(8),
         .BYTEENA_W(1),
         .ID_W(1),
         .NUM_MASTERS(1),
         .PIPELINE_RETURN_PATHS(0),
         .WRP_FIFO_DEPTH(0),
         .RRP_FIFO_DEPTH(0),
         .RRP_USE_LL_FIFO(1),
         .SLAVE_FIXED_LATENCY(3),
         .SEPARATE_READ_WRITE_STALLS(0)
      )
      s_endp
      (
         .clock(clock),
         .resetn(resetn),
         .m_intf(in_arb_intf),
         .s_intf(out_arb_intf),
         .s_readdatavalid(mout_rrp_datavalid),
         .s_readdata(mout_rrp_data),
         .s_writeack(mout_wrp_ack),
         .wrp_intf(wrp_intf),
         .rrp_intf(rrp_intf)
      );

   end
   endgenerate

   generate
   begin:wrp
      assign m[0].wrp_intf.ack = s.wrp_intf.ack;
      assign m[0].wrp_intf.id = s.wrp_intf.id;
   end
   endgenerate

   generate
   begin:rrp
   end
   endgenerate

   assign mout_arb_request = s.out_arb_intf.req.request;
   assign mout_arb_enable = s.out_arb_intf.req.enable;
   assign mout_arb_read = s.out_arb_intf.req.read;
   assign mout_arb_write = s.out_arb_intf.req.write;
   assign mout_arb_burstcount = s.out_arb_intf.req.burstcount;
   assign mout_arb_address = s.out_arb_intf.req.address;
   assign mout_arb_writedata = s.out_arb_intf.req.writedata;
   assign mout_arb_byteenable = s.out_arb_intf.req.byteenable;
   assign mout_arb_id = s.out_arb_intf.req.id;
   assign s.out_arb_intf.stall = mout_arb_stall;
   assign s.in_arb_intf.req = m[0].arb_intf.req;
   assign m[0].arb_intf.stall = s.in_arb_intf.stall;
endmodule

/////////////////////////////////////////////////////////////////
// MODULE conv_pipe_system_interconnect_8
/////////////////////////////////////////////////////////////////
module conv_pipe_system_interconnect_8
(
   input logic clock,
   input logic resetn,
   // ICM m
   input logic m_arb_request [2],
   input logic m_arb_enable [2],
   input logic m_arb_read [2],
   input logic m_arb_write [2],
   input logic m_arb_burstcount [2],
   input logic [12:0] m_arb_address [2],
   input logic [31:0] m_arb_writedata [2],
   input logic [3:0] m_arb_byteenable [2],
   output logic m_arb_stall [2],
   output logic m_wrp_ack [2],
   output logic m_rrp_datavalid [2],
   output logic [31:0] m_rrp_data [2],
   // ICM mout
   output logic mout_arb_request,
   output logic mout_arb_enable,
   output logic mout_arb_read,
   output logic mout_arb_write,
   output logic mout_arb_burstcount,
   output logic [12:0] mout_arb_address,
   output logic [31:0] mout_arb_writedata,
   output logic [3:0] mout_arb_byteenable,
   output logic mout_arb_id,
   input logic mout_arb_stall,
   input logic mout_wrp_ack,
   input logic mout_rrp_datavalid,
   input logic [31:0] mout_rrp_data
);
   genvar __i;
   generate
      for( __i = 0; __i < 2; __i = __i + 1 )
      begin:m
         logic id;
         acl_ic_master_intf
         #(
            .DATA_W(32),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(13),
            .BYTEENA_W(4),
            .ID_W(1)
         ) m_intf();
         acl_arb_intf
         #(
            .DATA_W(32),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(13),
            .BYTEENA_W(4),
            .ID_W(1)
         ) arb_intf();
         acl_ic_wrp_intf
         #(
            .ID_W(1)
         ) wrp_intf();
         acl_ic_rrp_intf
         #(
            .DATA_W(32),
            .ID_W(1)
         ) rrp_intf();

         assign id = __i;
         // INST m_endp of acl_ic_master_endpoint
         acl_ic_master_endpoint
         #(
            .DATA_W(32),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(13),
            .BYTEENA_W(4),
            .ID_W(1),
            .TOTAL_NUM_MASTERS(2),
            .ID(__i)
         )
         m_endp
         (
            .clock(clock),
            .resetn(resetn),
            .m_intf(m_intf),
            .arb_intf(arb_intf),
            .wrp_intf(wrp_intf),
            .rrp_intf(rrp_intf)
         );

         assign m_intf.arb.req.request = m_arb_request[__i];
         assign m_intf.arb.req.enable = m_arb_enable[__i];
         assign m_intf.arb.req.read = m_arb_read[__i];
         assign m_intf.arb.req.write = m_arb_write[__i];
         assign m_intf.arb.req.burstcount = m_arb_burstcount[__i];
         assign m_intf.arb.req.address = m_arb_address[__i];
         assign m_intf.arb.req.writedata = m_arb_writedata[__i];
         assign m_intf.arb.req.byteenable = m_arb_byteenable[__i];
         assign m_arb_stall[__i] = m_intf.arb.stall;
         assign m_wrp_ack[__i] = m_intf.wrp.ack;
         assign m_rrp_datavalid[__i] = m_intf.rrp.datavalid;
         assign m_rrp_data[__i] = m_intf.rrp.data;
         assign m_intf.arb.req.id = id;
      end

   endgenerate

   generate
   begin:s
      acl_arb_intf
      #(
         .DATA_W(32),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(13),
         .BYTEENA_W(4),
         .ID_W(1)
      ) in_arb_intf();
      acl_arb_intf
      #(
         .DATA_W(32),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(13),
         .BYTEENA_W(4),
         .ID_W(1)
      ) out_arb_intf();
      acl_ic_wrp_intf
      #(
         .ID_W(1)
      ) wrp_intf();
      acl_ic_rrp_intf
      #(
         .DATA_W(32),
         .ID_W(1)
      ) rrp_intf();

      // INST s_endp of acl_ic_slave_endpoint
      acl_ic_slave_endpoint
      #(
         .DATA_W(32),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(13),
         .BYTEENA_W(4),
         .ID_W(1),
         .NUM_MASTERS(2),
         .PIPELINE_RETURN_PATHS(0),
         .WRP_FIFO_DEPTH(0),
         .RRP_FIFO_DEPTH(0),
         .RRP_USE_LL_FIFO(1),
         .SLAVE_FIXED_LATENCY(3),
         .SEPARATE_READ_WRITE_STALLS(0)
      )
      s_endp
      (
         .clock(clock),
         .resetn(resetn),
         .m_intf(in_arb_intf),
         .s_intf(out_arb_intf),
         .s_readdatavalid(mout_rrp_datavalid),
         .s_readdata(mout_rrp_data),
         .s_writeack(mout_wrp_ack),
         .wrp_intf(wrp_intf),
         .rrp_intf(rrp_intf)
      );

   end
   endgenerate

   generate
   begin:wrp
      assign m[0].wrp_intf.ack = s.wrp_intf.ack;
      assign m[0].wrp_intf.id = s.wrp_intf.id;
      assign m[1].wrp_intf.ack = s.wrp_intf.ack;
      assign m[1].wrp_intf.id = s.wrp_intf.id;
   end
   endgenerate

   generate
   begin:rrp
   end
   endgenerate

   generate
      for( __i = 0; __i < 1; __i = __i + 1 )
      begin:a
         acl_arb_intf
         #(
            .DATA_W(32),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(13),
            .BYTEENA_W(4),
            .ID_W(1)
         ) m0_intf();
         acl_arb_intf
         #(
            .DATA_W(32),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(13),
            .BYTEENA_W(4),
            .ID_W(1)
         ) m1_intf();
         acl_arb_intf
         #(
            .DATA_W(32),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(13),
            .BYTEENA_W(4),
            .ID_W(1)
         ) mout_intf();

         // INST a of acl_arb2
         acl_arb2
         #(
            .DATA_W(32),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(13),
            .BYTEENA_W(4),
            .ID_W(1),
            .PIPELINE("none"),
            .KEEP_LAST_GRANT(0),
            .NO_STALL_NETWORK(0)
         )
         a
         (
            .clock(clock),
            .resetn(resetn),
            .m0_intf(m0_intf),
            .m1_intf(m1_intf),
            .mout_intf(mout_intf)
         );

      end

   endgenerate

   generate
      for( __i = 0; __i < 1; __i = __i + 1 )
      begin:dp
         acl_arb_intf
         #(
            .DATA_W(32),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(13),
            .BYTEENA_W(4),
            .ID_W(1)
         ) in_intf();
         acl_arb_intf
         #(
            .DATA_W(32),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(13),
            .BYTEENA_W(4),
            .ID_W(1)
         ) out_intf();

         // INST dp of acl_arb_pipeline_reg
         acl_arb_pipeline_reg
         #(
            .DATA_W(32),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(13),
            .BYTEENA_W(4),
            .ID_W(1)
         )
         dp
         (
            .clock(clock),
            .resetn(resetn),
            .in_intf(in_intf),
            .out_intf(out_intf)
         );

      end

   endgenerate

   assign mout_arb_request = s.out_arb_intf.req.request;
   assign mout_arb_enable = s.out_arb_intf.req.enable;
   assign mout_arb_read = s.out_arb_intf.req.read;
   assign mout_arb_write = s.out_arb_intf.req.write;
   assign mout_arb_burstcount = s.out_arb_intf.req.burstcount;
   assign mout_arb_address = s.out_arb_intf.req.address;
   assign mout_arb_writedata = s.out_arb_intf.req.writedata;
   assign mout_arb_byteenable = s.out_arb_intf.req.byteenable;
   assign mout_arb_id = s.out_arb_intf.req.id;
   assign s.out_arb_intf.stall = mout_arb_stall;
   assign s.in_arb_intf.req = dp[0].out_intf.req;
   assign dp[0].out_intf.stall = s.in_arb_intf.stall;
   assign dp[0].in_intf.req = a[0].mout_intf.req;
   assign a[0].mout_intf.stall = dp[0].in_intf.stall;
   assign a[0].m0_intf.req = m[0].arb_intf.req;
   assign m[0].arb_intf.stall = a[0].m0_intf.stall;
   assign a[0].m1_intf.req = m[1].arb_intf.req;
   assign m[1].arb_intf.stall = a[0].m1_intf.stall;
endmodule

/////////////////////////////////////////////////////////////////
// MODULE conv_pipe_system_interconnect_9
/////////////////////////////////////////////////////////////////
module conv_pipe_system_interconnect_9
(
   input logic clock,
   input logic resetn,
   // ICM m
   input logic m_arb_request [1],
   input logic m_arb_enable [1],
   input logic m_arb_read [1],
   input logic m_arb_write [1],
   input logic m_arb_burstcount [1],
   input logic [12:0] m_arb_address [1],
   input logic [31:0] m_arb_writedata [1],
   input logic [3:0] m_arb_byteenable [1],
   output logic m_arb_stall [1],
   output logic m_wrp_ack [1],
   output logic m_rrp_datavalid [1],
   output logic [31:0] m_rrp_data [1],
   // ICM mout
   output logic mout_arb_request,
   output logic mout_arb_enable,
   output logic mout_arb_read,
   output logic mout_arb_write,
   output logic mout_arb_burstcount,
   output logic [12:0] mout_arb_address,
   output logic [31:0] mout_arb_writedata,
   output logic [3:0] mout_arb_byteenable,
   output logic mout_arb_id,
   input logic mout_arb_stall,
   input logic mout_wrp_ack,
   input logic mout_rrp_datavalid,
   input logic [31:0] mout_rrp_data
);
   genvar __i;
   generate
      for( __i = 0; __i < 1; __i = __i + 1 )
      begin:m
         logic id;
         acl_ic_master_intf
         #(
            .DATA_W(32),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(13),
            .BYTEENA_W(4),
            .ID_W(1)
         ) m_intf();
         acl_arb_intf
         #(
            .DATA_W(32),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(13),
            .BYTEENA_W(4),
            .ID_W(1)
         ) arb_intf();
         acl_ic_wrp_intf
         #(
            .ID_W(1)
         ) wrp_intf();
         acl_ic_rrp_intf
         #(
            .DATA_W(32),
            .ID_W(1)
         ) rrp_intf();

         assign id = __i;
         // INST m_endp of acl_ic_master_endpoint
         acl_ic_master_endpoint
         #(
            .DATA_W(32),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(13),
            .BYTEENA_W(4),
            .ID_W(1),
            .TOTAL_NUM_MASTERS(1),
            .ID(__i)
         )
         m_endp
         (
            .clock(clock),
            .resetn(resetn),
            .m_intf(m_intf),
            .arb_intf(arb_intf),
            .wrp_intf(wrp_intf),
            .rrp_intf(rrp_intf)
         );

         assign m_intf.arb.req.request = m_arb_request[__i];
         assign m_intf.arb.req.enable = m_arb_enable[__i];
         assign m_intf.arb.req.read = m_arb_read[__i];
         assign m_intf.arb.req.write = m_arb_write[__i];
         assign m_intf.arb.req.burstcount = m_arb_burstcount[__i];
         assign m_intf.arb.req.address = m_arb_address[__i];
         assign m_intf.arb.req.writedata = m_arb_writedata[__i];
         assign m_intf.arb.req.byteenable = m_arb_byteenable[__i];
         assign m_arb_stall[__i] = m_intf.arb.stall;
         assign m_wrp_ack[__i] = m_intf.wrp.ack;
         assign m_rrp_datavalid[__i] = m_intf.rrp.datavalid;
         assign m_rrp_data[__i] = m_intf.rrp.data;
         assign m_intf.arb.req.id = id;
      end

   endgenerate

   generate
   begin:s
      acl_arb_intf
      #(
         .DATA_W(32),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(13),
         .BYTEENA_W(4),
         .ID_W(1)
      ) in_arb_intf();
      acl_arb_intf
      #(
         .DATA_W(32),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(13),
         .BYTEENA_W(4),
         .ID_W(1)
      ) out_arb_intf();
      acl_ic_wrp_intf
      #(
         .ID_W(1)
      ) wrp_intf();
      acl_ic_rrp_intf
      #(
         .DATA_W(32),
         .ID_W(1)
      ) rrp_intf();

      // INST s_endp of acl_ic_slave_endpoint
      acl_ic_slave_endpoint
      #(
         .DATA_W(32),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(13),
         .BYTEENA_W(4),
         .ID_W(1),
         .NUM_MASTERS(1),
         .PIPELINE_RETURN_PATHS(0),
         .WRP_FIFO_DEPTH(0),
         .RRP_FIFO_DEPTH(0),
         .RRP_USE_LL_FIFO(1),
         .SLAVE_FIXED_LATENCY(3),
         .SEPARATE_READ_WRITE_STALLS(0)
      )
      s_endp
      (
         .clock(clock),
         .resetn(resetn),
         .m_intf(in_arb_intf),
         .s_intf(out_arb_intf),
         .s_readdatavalid(mout_rrp_datavalid),
         .s_readdata(mout_rrp_data),
         .s_writeack(mout_wrp_ack),
         .wrp_intf(wrp_intf),
         .rrp_intf(rrp_intf)
      );

   end
   endgenerate

   generate
   begin:wrp
   end
   endgenerate

   generate
   begin:rrp
      assign m[0].rrp_intf.datavalid = s.rrp_intf.datavalid;
      assign m[0].rrp_intf.data = s.rrp_intf.data;
      assign m[0].rrp_intf.id = s.rrp_intf.id;
   end
   endgenerate

   generate
      for( __i = 0; __i < 1; __i = __i + 1 )
      begin:dp
         acl_arb_intf
         #(
            .DATA_W(32),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(13),
            .BYTEENA_W(4),
            .ID_W(1)
         ) in_intf();
         acl_arb_intf
         #(
            .DATA_W(32),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(13),
            .BYTEENA_W(4),
            .ID_W(1)
         ) out_intf();

         // INST dp of acl_arb_pipeline_reg
         acl_arb_pipeline_reg
         #(
            .DATA_W(32),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(13),
            .BYTEENA_W(4),
            .ID_W(1)
         )
         dp
         (
            .clock(clock),
            .resetn(resetn),
            .in_intf(in_intf),
            .out_intf(out_intf)
         );

      end

   endgenerate

   assign mout_arb_request = s.out_arb_intf.req.request;
   assign mout_arb_enable = s.out_arb_intf.req.enable;
   assign mout_arb_read = s.out_arb_intf.req.read;
   assign mout_arb_write = s.out_arb_intf.req.write;
   assign mout_arb_burstcount = s.out_arb_intf.req.burstcount;
   assign mout_arb_address = s.out_arb_intf.req.address;
   assign mout_arb_writedata = s.out_arb_intf.req.writedata;
   assign mout_arb_byteenable = s.out_arb_intf.req.byteenable;
   assign mout_arb_id = s.out_arb_intf.req.id;
   assign s.out_arb_intf.stall = mout_arb_stall;
   assign s.in_arb_intf.req = dp[0].out_intf.req;
   assign dp[0].out_intf.stall = s.in_arb_intf.stall;
   assign dp[0].in_intf.req = m[0].arb_intf.req;
   assign m[0].arb_intf.stall = dp[0].in_intf.stall;
endmodule

/////////////////////////////////////////////////////////////////
// MODULE conv_pipe_system_interconnect_10
/////////////////////////////////////////////////////////////////
module conv_pipe_system_interconnect_10
(
   input logic clock,
   input logic resetn,
   // ICM m
   input logic m_arb_request [1],
   input logic m_arb_enable [1],
   input logic m_arb_read [1],
   input logic m_arb_write [1],
   input logic m_arb_burstcount [1],
   input logic [11:0] m_arb_address [1],
   input logic [511:0] m_arb_writedata [1],
   input logic [63:0] m_arb_byteenable [1],
   output logic m_arb_stall [1],
   output logic m_wrp_ack [1],
   output logic m_rrp_datavalid [1],
   output logic [511:0] m_rrp_data [1],
   // ICM mout
   output logic mout_arb_request,
   output logic mout_arb_enable,
   output logic mout_arb_read,
   output logic mout_arb_write,
   output logic mout_arb_burstcount,
   output logic [11:0] mout_arb_address,
   output logic [511:0] mout_arb_writedata,
   output logic [63:0] mout_arb_byteenable,
   output logic mout_arb_id,
   input logic mout_arb_stall,
   input logic mout_wrp_ack,
   input logic mout_rrp_datavalid,
   input logic [511:0] mout_rrp_data
);
   genvar __i;
   generate
      for( __i = 0; __i < 1; __i = __i + 1 )
      begin:m
         logic id;
         acl_ic_master_intf
         #(
            .DATA_W(512),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(12),
            .BYTEENA_W(64),
            .ID_W(1)
         ) m_intf();
         acl_arb_intf
         #(
            .DATA_W(512),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(12),
            .BYTEENA_W(64),
            .ID_W(1)
         ) arb_intf();
         acl_ic_wrp_intf
         #(
            .ID_W(1)
         ) wrp_intf();
         acl_ic_rrp_intf
         #(
            .DATA_W(512),
            .ID_W(1)
         ) rrp_intf();

         assign id = __i;
         // INST m_endp of acl_ic_master_endpoint
         acl_ic_master_endpoint
         #(
            .DATA_W(512),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(12),
            .BYTEENA_W(64),
            .ID_W(1),
            .TOTAL_NUM_MASTERS(1),
            .ID(__i)
         )
         m_endp
         (
            .clock(clock),
            .resetn(resetn),
            .m_intf(m_intf),
            .arb_intf(arb_intf),
            .wrp_intf(wrp_intf),
            .rrp_intf(rrp_intf)
         );

         assign m_intf.arb.req.request = m_arb_request[__i];
         assign m_intf.arb.req.enable = m_arb_enable[__i];
         assign m_intf.arb.req.read = m_arb_read[__i];
         assign m_intf.arb.req.write = m_arb_write[__i];
         assign m_intf.arb.req.burstcount = m_arb_burstcount[__i];
         assign m_intf.arb.req.address = m_arb_address[__i];
         assign m_intf.arb.req.writedata = m_arb_writedata[__i];
         assign m_intf.arb.req.byteenable = m_arb_byteenable[__i];
         assign m_arb_stall[__i] = m_intf.arb.stall;
         assign m_wrp_ack[__i] = m_intf.wrp.ack;
         assign m_rrp_datavalid[__i] = m_intf.rrp.datavalid;
         assign m_rrp_data[__i] = m_intf.rrp.data;
         assign m_intf.arb.req.id = id;
      end

   endgenerate

   generate
   begin:s
      acl_arb_intf
      #(
         .DATA_W(512),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(12),
         .BYTEENA_W(64),
         .ID_W(1)
      ) in_arb_intf();
      acl_arb_intf
      #(
         .DATA_W(512),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(12),
         .BYTEENA_W(64),
         .ID_W(1)
      ) out_arb_intf();
      acl_ic_wrp_intf
      #(
         .ID_W(1)
      ) wrp_intf();
      acl_ic_rrp_intf
      #(
         .DATA_W(512),
         .ID_W(1)
      ) rrp_intf();

      // INST s_endp of acl_ic_slave_endpoint
      acl_ic_slave_endpoint
      #(
         .DATA_W(512),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(12),
         .BYTEENA_W(64),
         .ID_W(1),
         .NUM_MASTERS(1),
         .PIPELINE_RETURN_PATHS(0),
         .WRP_FIFO_DEPTH(0),
         .RRP_FIFO_DEPTH(0),
         .RRP_USE_LL_FIFO(1),
         .SLAVE_FIXED_LATENCY(1),
         .SEPARATE_READ_WRITE_STALLS(0)
      )
      s_endp
      (
         .clock(clock),
         .resetn(resetn),
         .m_intf(in_arb_intf),
         .s_intf(out_arb_intf),
         .s_readdatavalid(mout_rrp_datavalid),
         .s_readdata(mout_rrp_data),
         .s_writeack(mout_wrp_ack),
         .wrp_intf(wrp_intf),
         .rrp_intf(rrp_intf)
      );

   end
   endgenerate

   generate
   begin:wrp
      assign m[0].wrp_intf.ack = s.wrp_intf.ack;
      assign m[0].wrp_intf.id = s.wrp_intf.id;
   end
   endgenerate

   generate
   begin:rrp
   end
   endgenerate

   assign mout_arb_request = s.out_arb_intf.req.request;
   assign mout_arb_enable = s.out_arb_intf.req.enable;
   assign mout_arb_read = s.out_arb_intf.req.read;
   assign mout_arb_write = s.out_arb_intf.req.write;
   assign mout_arb_burstcount = s.out_arb_intf.req.burstcount;
   assign mout_arb_address = s.out_arb_intf.req.address;
   assign mout_arb_writedata = s.out_arb_intf.req.writedata;
   assign mout_arb_byteenable = s.out_arb_intf.req.byteenable;
   assign mout_arb_id = s.out_arb_intf.req.id;
   assign s.out_arb_intf.stall = mout_arb_stall;
   assign s.in_arb_intf.req = m[0].arb_intf.req;
   assign m[0].arb_intf.stall = s.in_arb_intf.stall;
endmodule

/////////////////////////////////////////////////////////////////
// MODULE conv_pipe_system_interconnect_11
/////////////////////////////////////////////////////////////////
module conv_pipe_system_interconnect_11
(
   input logic clock,
   input logic resetn,
   // ICM m
   input logic m_arb_request [1],
   input logic m_arb_enable [1],
   input logic m_arb_read [1],
   input logic m_arb_write [1],
   input logic m_arb_burstcount [1],
   input logic [11:0] m_arb_address [1],
   input logic [511:0] m_arb_writedata [1],
   input logic [63:0] m_arb_byteenable [1],
   output logic m_arb_stall [1],
   output logic m_wrp_ack [1],
   output logic m_rrp_datavalid [1],
   output logic [511:0] m_rrp_data [1],
   // ICM mout
   output logic mout_arb_request,
   output logic mout_arb_enable,
   output logic mout_arb_read,
   output logic mout_arb_write,
   output logic mout_arb_burstcount,
   output logic [11:0] mout_arb_address,
   output logic [511:0] mout_arb_writedata,
   output logic [63:0] mout_arb_byteenable,
   output logic mout_arb_id,
   input logic mout_arb_stall,
   input logic mout_wrp_ack,
   input logic mout_rrp_datavalid,
   input logic [511:0] mout_rrp_data
);
   genvar __i;
   generate
      for( __i = 0; __i < 1; __i = __i + 1 )
      begin:m
         logic id;
         acl_ic_master_intf
         #(
            .DATA_W(512),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(12),
            .BYTEENA_W(64),
            .ID_W(1)
         ) m_intf();
         acl_arb_intf
         #(
            .DATA_W(512),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(12),
            .BYTEENA_W(64),
            .ID_W(1)
         ) arb_intf();
         acl_ic_wrp_intf
         #(
            .ID_W(1)
         ) wrp_intf();
         acl_ic_rrp_intf
         #(
            .DATA_W(512),
            .ID_W(1)
         ) rrp_intf();

         assign id = __i;
         // INST m_endp of acl_ic_master_endpoint
         acl_ic_master_endpoint
         #(
            .DATA_W(512),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(12),
            .BYTEENA_W(64),
            .ID_W(1),
            .TOTAL_NUM_MASTERS(1),
            .ID(__i)
         )
         m_endp
         (
            .clock(clock),
            .resetn(resetn),
            .m_intf(m_intf),
            .arb_intf(arb_intf),
            .wrp_intf(wrp_intf),
            .rrp_intf(rrp_intf)
         );

         assign m_intf.arb.req.request = m_arb_request[__i];
         assign m_intf.arb.req.enable = m_arb_enable[__i];
         assign m_intf.arb.req.read = m_arb_read[__i];
         assign m_intf.arb.req.write = m_arb_write[__i];
         assign m_intf.arb.req.burstcount = m_arb_burstcount[__i];
         assign m_intf.arb.req.address = m_arb_address[__i];
         assign m_intf.arb.req.writedata = m_arb_writedata[__i];
         assign m_intf.arb.req.byteenable = m_arb_byteenable[__i];
         assign m_arb_stall[__i] = m_intf.arb.stall;
         assign m_wrp_ack[__i] = m_intf.wrp.ack;
         assign m_rrp_datavalid[__i] = m_intf.rrp.datavalid;
         assign m_rrp_data[__i] = m_intf.rrp.data;
         assign m_intf.arb.req.id = id;
      end

   endgenerate

   generate
   begin:s
      acl_arb_intf
      #(
         .DATA_W(512),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(12),
         .BYTEENA_W(64),
         .ID_W(1)
      ) in_arb_intf();
      acl_arb_intf
      #(
         .DATA_W(512),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(12),
         .BYTEENA_W(64),
         .ID_W(1)
      ) out_arb_intf();
      acl_ic_wrp_intf
      #(
         .ID_W(1)
      ) wrp_intf();
      acl_ic_rrp_intf
      #(
         .DATA_W(512),
         .ID_W(1)
      ) rrp_intf();

      // INST s_endp of acl_ic_slave_endpoint
      acl_ic_slave_endpoint
      #(
         .DATA_W(512),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(12),
         .BYTEENA_W(64),
         .ID_W(1),
         .NUM_MASTERS(1),
         .PIPELINE_RETURN_PATHS(0),
         .WRP_FIFO_DEPTH(0),
         .RRP_FIFO_DEPTH(0),
         .RRP_USE_LL_FIFO(1),
         .SLAVE_FIXED_LATENCY(1),
         .SEPARATE_READ_WRITE_STALLS(0)
      )
      s_endp
      (
         .clock(clock),
         .resetn(resetn),
         .m_intf(in_arb_intf),
         .s_intf(out_arb_intf),
         .s_readdatavalid(mout_rrp_datavalid),
         .s_readdata(mout_rrp_data),
         .s_writeack(mout_wrp_ack),
         .wrp_intf(wrp_intf),
         .rrp_intf(rrp_intf)
      );

   end
   endgenerate

   generate
   begin:wrp
   end
   endgenerate

   generate
   begin:rrp
      assign m[0].rrp_intf.datavalid = s.rrp_intf.datavalid;
      assign m[0].rrp_intf.data = s.rrp_intf.data;
      assign m[0].rrp_intf.id = s.rrp_intf.id;
   end
   endgenerate

   assign mout_arb_request = s.out_arb_intf.req.request;
   assign mout_arb_enable = s.out_arb_intf.req.enable;
   assign mout_arb_read = s.out_arb_intf.req.read;
   assign mout_arb_write = s.out_arb_intf.req.write;
   assign mout_arb_burstcount = s.out_arb_intf.req.burstcount;
   assign mout_arb_address = s.out_arb_intf.req.address;
   assign mout_arb_writedata = s.out_arb_intf.req.writedata;
   assign mout_arb_byteenable = s.out_arb_intf.req.byteenable;
   assign mout_arb_id = s.out_arb_intf.req.id;
   assign s.out_arb_intf.stall = mout_arb_stall;
   assign s.in_arb_intf.req = m[0].arb_intf.req;
   assign m[0].arb_intf.stall = s.in_arb_intf.stall;
endmodule

/////////////////////////////////////////////////////////////////
// MODULE conv_pipe_system_interconnect_12
/////////////////////////////////////////////////////////////////
module conv_pipe_system_interconnect_12
(
   input logic clock,
   input logic resetn,
   // ICM m
   input logic m_arb_request [1],
   input logic m_arb_enable [1],
   input logic m_arb_read [1],
   input logic m_arb_write [1],
   input logic m_arb_burstcount [1],
   input logic [1:0] m_arb_address [1],
   input logic [127:0] m_arb_writedata [1],
   input logic [15:0] m_arb_byteenable [1],
   output logic m_arb_stall [1],
   output logic m_wrp_ack [1],
   output logic m_rrp_datavalid [1],
   output logic [127:0] m_rrp_data [1],
   // ICM mout
   output logic mout_arb_request,
   output logic mout_arb_enable,
   output logic mout_arb_read,
   output logic mout_arb_write,
   output logic mout_arb_burstcount,
   output logic [1:0] mout_arb_address,
   output logic [127:0] mout_arb_writedata,
   output logic [15:0] mout_arb_byteenable,
   output logic mout_arb_id,
   input logic mout_arb_stall,
   input logic mout_wrp_ack,
   input logic mout_rrp_datavalid,
   input logic [127:0] mout_rrp_data
);
   genvar __i;
   generate
      for( __i = 0; __i < 1; __i = __i + 1 )
      begin:m
         logic id;
         acl_ic_master_intf
         #(
            .DATA_W(128),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(2),
            .BYTEENA_W(16),
            .ID_W(1)
         ) m_intf();
         acl_arb_intf
         #(
            .DATA_W(128),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(2),
            .BYTEENA_W(16),
            .ID_W(1)
         ) arb_intf();
         acl_ic_wrp_intf
         #(
            .ID_W(1)
         ) wrp_intf();
         acl_ic_rrp_intf
         #(
            .DATA_W(128),
            .ID_W(1)
         ) rrp_intf();

         assign id = __i;
         // INST m_endp of acl_ic_master_endpoint
         acl_ic_master_endpoint
         #(
            .DATA_W(128),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(2),
            .BYTEENA_W(16),
            .ID_W(1),
            .TOTAL_NUM_MASTERS(1),
            .ID(__i)
         )
         m_endp
         (
            .clock(clock),
            .resetn(resetn),
            .m_intf(m_intf),
            .arb_intf(arb_intf),
            .wrp_intf(wrp_intf),
            .rrp_intf(rrp_intf)
         );

         assign m_intf.arb.req.request = m_arb_request[__i];
         assign m_intf.arb.req.enable = m_arb_enable[__i];
         assign m_intf.arb.req.read = m_arb_read[__i];
         assign m_intf.arb.req.write = m_arb_write[__i];
         assign m_intf.arb.req.burstcount = m_arb_burstcount[__i];
         assign m_intf.arb.req.address = m_arb_address[__i];
         assign m_intf.arb.req.writedata = m_arb_writedata[__i];
         assign m_intf.arb.req.byteenable = m_arb_byteenable[__i];
         assign m_arb_stall[__i] = m_intf.arb.stall;
         assign m_wrp_ack[__i] = m_intf.wrp.ack;
         assign m_rrp_datavalid[__i] = m_intf.rrp.datavalid;
         assign m_rrp_data[__i] = m_intf.rrp.data;
         assign m_intf.arb.req.id = id;
      end

   endgenerate

   generate
   begin:s
      acl_arb_intf
      #(
         .DATA_W(128),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(2),
         .BYTEENA_W(16),
         .ID_W(1)
      ) in_arb_intf();
      acl_arb_intf
      #(
         .DATA_W(128),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(2),
         .BYTEENA_W(16),
         .ID_W(1)
      ) out_arb_intf();
      acl_ic_wrp_intf
      #(
         .ID_W(1)
      ) wrp_intf();
      acl_ic_rrp_intf
      #(
         .DATA_W(128),
         .ID_W(1)
      ) rrp_intf();

      // INST s_endp of acl_ic_slave_endpoint
      acl_ic_slave_endpoint
      #(
         .DATA_W(128),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(2),
         .BYTEENA_W(16),
         .ID_W(1),
         .NUM_MASTERS(1),
         .PIPELINE_RETURN_PATHS(0),
         .WRP_FIFO_DEPTH(0),
         .RRP_FIFO_DEPTH(0),
         .RRP_USE_LL_FIFO(1),
         .SLAVE_FIXED_LATENCY(3),
         .SEPARATE_READ_WRITE_STALLS(0)
      )
      s_endp
      (
         .clock(clock),
         .resetn(resetn),
         .m_intf(in_arb_intf),
         .s_intf(out_arb_intf),
         .s_readdatavalid(mout_rrp_datavalid),
         .s_readdata(mout_rrp_data),
         .s_writeack(mout_wrp_ack),
         .wrp_intf(wrp_intf),
         .rrp_intf(rrp_intf)
      );

   end
   endgenerate

   generate
   begin:wrp
      assign m[0].wrp_intf.ack = s.wrp_intf.ack;
      assign m[0].wrp_intf.id = s.wrp_intf.id;
   end
   endgenerate

   generate
   begin:rrp
   end
   endgenerate

   assign mout_arb_request = s.out_arb_intf.req.request;
   assign mout_arb_enable = s.out_arb_intf.req.enable;
   assign mout_arb_read = s.out_arb_intf.req.read;
   assign mout_arb_write = s.out_arb_intf.req.write;
   assign mout_arb_burstcount = s.out_arb_intf.req.burstcount;
   assign mout_arb_address = s.out_arb_intf.req.address;
   assign mout_arb_writedata = s.out_arb_intf.req.writedata;
   assign mout_arb_byteenable = s.out_arb_intf.req.byteenable;
   assign mout_arb_id = s.out_arb_intf.req.id;
   assign s.out_arb_intf.stall = mout_arb_stall;
   assign s.in_arb_intf.req = m[0].arb_intf.req;
   assign m[0].arb_intf.stall = s.in_arb_intf.stall;
endmodule

/////////////////////////////////////////////////////////////////
// MODULE conv_pipe_system_interconnect_13
/////////////////////////////////////////////////////////////////
module conv_pipe_system_interconnect_13
(
   input logic clock,
   input logic resetn,
   // ICM m
   input logic m_arb_request [1],
   input logic m_arb_enable [1],
   input logic m_arb_read [1],
   input logic m_arb_write [1],
   input logic m_arb_burstcount [1],
   input logic [1:0] m_arb_address [1],
   input logic [127:0] m_arb_writedata [1],
   input logic [15:0] m_arb_byteenable [1],
   output logic m_arb_stall [1],
   output logic m_wrp_ack [1],
   output logic m_rrp_datavalid [1],
   output logic [127:0] m_rrp_data [1],
   // ICM mout
   output logic mout_arb_request,
   output logic mout_arb_enable,
   output logic mout_arb_read,
   output logic mout_arb_write,
   output logic mout_arb_burstcount,
   output logic [1:0] mout_arb_address,
   output logic [127:0] mout_arb_writedata,
   output logic [15:0] mout_arb_byteenable,
   output logic mout_arb_id,
   input logic mout_arb_stall,
   input logic mout_wrp_ack,
   input logic mout_rrp_datavalid,
   input logic [127:0] mout_rrp_data
);
   genvar __i;
   generate
      for( __i = 0; __i < 1; __i = __i + 1 )
      begin:m
         logic id;
         acl_ic_master_intf
         #(
            .DATA_W(128),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(2),
            .BYTEENA_W(16),
            .ID_W(1)
         ) m_intf();
         acl_arb_intf
         #(
            .DATA_W(128),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(2),
            .BYTEENA_W(16),
            .ID_W(1)
         ) arb_intf();
         acl_ic_wrp_intf
         #(
            .ID_W(1)
         ) wrp_intf();
         acl_ic_rrp_intf
         #(
            .DATA_W(128),
            .ID_W(1)
         ) rrp_intf();

         assign id = __i;
         // INST m_endp of acl_ic_master_endpoint
         acl_ic_master_endpoint
         #(
            .DATA_W(128),
            .BURSTCOUNT_W(1),
            .ADDRESS_W(2),
            .BYTEENA_W(16),
            .ID_W(1),
            .TOTAL_NUM_MASTERS(1),
            .ID(__i)
         )
         m_endp
         (
            .clock(clock),
            .resetn(resetn),
            .m_intf(m_intf),
            .arb_intf(arb_intf),
            .wrp_intf(wrp_intf),
            .rrp_intf(rrp_intf)
         );

         assign m_intf.arb.req.request = m_arb_request[__i];
         assign m_intf.arb.req.enable = m_arb_enable[__i];
         assign m_intf.arb.req.read = m_arb_read[__i];
         assign m_intf.arb.req.write = m_arb_write[__i];
         assign m_intf.arb.req.burstcount = m_arb_burstcount[__i];
         assign m_intf.arb.req.address = m_arb_address[__i];
         assign m_intf.arb.req.writedata = m_arb_writedata[__i];
         assign m_intf.arb.req.byteenable = m_arb_byteenable[__i];
         assign m_arb_stall[__i] = m_intf.arb.stall;
         assign m_wrp_ack[__i] = m_intf.wrp.ack;
         assign m_rrp_datavalid[__i] = m_intf.rrp.datavalid;
         assign m_rrp_data[__i] = m_intf.rrp.data;
         assign m_intf.arb.req.id = id;
      end

   endgenerate

   generate
   begin:s
      acl_arb_intf
      #(
         .DATA_W(128),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(2),
         .BYTEENA_W(16),
         .ID_W(1)
      ) in_arb_intf();
      acl_arb_intf
      #(
         .DATA_W(128),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(2),
         .BYTEENA_W(16),
         .ID_W(1)
      ) out_arb_intf();
      acl_ic_wrp_intf
      #(
         .ID_W(1)
      ) wrp_intf();
      acl_ic_rrp_intf
      #(
         .DATA_W(128),
         .ID_W(1)
      ) rrp_intf();

      // INST s_endp of acl_ic_slave_endpoint
      acl_ic_slave_endpoint
      #(
         .DATA_W(128),
         .BURSTCOUNT_W(1),
         .ADDRESS_W(2),
         .BYTEENA_W(16),
         .ID_W(1),
         .NUM_MASTERS(1),
         .PIPELINE_RETURN_PATHS(0),
         .WRP_FIFO_DEPTH(0),
         .RRP_FIFO_DEPTH(0),
         .RRP_USE_LL_FIFO(1),
         .SLAVE_FIXED_LATENCY(3),
         .SEPARATE_READ_WRITE_STALLS(0)
      )
      s_endp
      (
         .clock(clock),
         .resetn(resetn),
         .m_intf(in_arb_intf),
         .s_intf(out_arb_intf),
         .s_readdatavalid(mout_rrp_datavalid),
         .s_readdata(mout_rrp_data),
         .s_writeack(mout_wrp_ack),
         .wrp_intf(wrp_intf),
         .rrp_intf(rrp_intf)
      );

   end
   endgenerate

   generate
   begin:wrp
   end
   endgenerate

   generate
   begin:rrp
      assign m[0].rrp_intf.datavalid = s.rrp_intf.datavalid;
      assign m[0].rrp_intf.data = s.rrp_intf.data;
      assign m[0].rrp_intf.id = s.rrp_intf.id;
   end
   endgenerate

   assign mout_arb_request = s.out_arb_intf.req.request;
   assign mout_arb_enable = s.out_arb_intf.req.enable;
   assign mout_arb_read = s.out_arb_intf.req.read;
   assign mout_arb_write = s.out_arb_intf.req.write;
   assign mout_arb_burstcount = s.out_arb_intf.req.burstcount;
   assign mout_arb_address = s.out_arb_intf.req.address;
   assign mout_arb_writedata = s.out_arb_intf.req.writedata;
   assign mout_arb_byteenable = s.out_arb_intf.req.byteenable;
   assign mout_arb_id = s.out_arb_intf.req.id;
   assign s.out_arb_intf.stall = mout_arb_stall;
   assign s.in_arb_intf.req = m[0].arb_intf.req;
   assign m[0].arb_intf.stall = s.in_arb_intf.stall;
endmodule

