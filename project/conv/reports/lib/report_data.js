var loopsJSON={
  "columns":["Pipelined", "II", "Bottleneck"]
  , "functions":
  [
    {
      "name":"Block2"
      , "data":
      ["Yes", "2", "n/a"]
      , "debug":
      [
        [
          {
            "filename":"conv_pipe.cl"
            , "line":168
            , "level":0
          }
        ]
      ]
    }
    , {
      "name":"Block3"
      , "data":
      ["Yes", "1", "n/a"]
      , "debug":
      [
        [
          {
            "filename":"conv_pipe.cl"
            , "line":169
            , "level":1
          }
        ]
      ]
    }
    , {
      "name":"Block4"
      , "data":
      ["Yes", "1", "n/a"]
      , "debug":
      [
        [
          {
            "filename":"conv_pipe.cl"
            , "line":170
            , "level":2
          }
        ]
      ]
    }
    , {
      "name":"Fully unrolled loop"
      , "data":
      ["n/a", "n/a", "n/a"]
      , "debug":
      [
        [
          {
            "filename":"conv_pipe.cl"
            , "line":182
            , "level":3
          }
        ]
      ]
      , "details":
      [
        "Unrolled by #pragma unroll"
      ]
    }
    , {
      "name":"Block8"
      , "data":
      ["Yes", "1", "n/a"]
      , "debug":
      [
        [
          {
            "filename":"conv_pipe.cl"
            , "line":205
            , "level":0
          }
        ]
      ]
    }
    , {
      "name":"Block10"
      , "data":
      ["Yes", "1", "n/a"]
      , "debug":
      [
        [
          {
            "filename":"conv_pipe.cl"
            , "line":238
            , "level":1
          }
        ]
      ]
    }
    , {
      "name":"Fully unrolled loop"
      , "data":
      ["n/a", "n/a", "n/a"]
      , "debug":
      [
        [
          {
            "filename":"conv_pipe.cl"
            , "line":253
            , "level":2
          }
        ]
      ]
      , "details":
      [
        "Unrolled by #pragma unroll"
      ]
    }
    , {
      "name":"Fully unrolled loop"
      , "data":
      ["n/a", "n/a", "n/a"]
      , "debug":
      [
        [
          {
            "filename":"conv_pipe.cl"
            , "line":300
            , "level":2
          }
        ]
      ]
      , "details":
      [
        "Unrolled by #pragma unroll"
      ]
    }
    , {
      "name":"Block14"
      , "data":
      ["Yes", "1", "n/a"]
      , "debug":
      [
        [
          {
            "filename":"conv_pipe.cl"
            , "line":404
            , "level":0
          }
        ]
      ]
    }
    , {
      "name":"Fully unrolled loop"
      , "data":
      ["n/a", "n/a", "n/a"]
      , "debug":
      [
        [
          {
            "filename":"conv_pipe.cl"
            , "line":409
            , "level":1
          }
        ]
      ]
      , "details":
      [
        "Unrolled by #pragma unroll"
      ]
    }
    , {
      "name":"Fully unrolled loop"
      , "data":
      ["n/a", "n/a", "n/a"]
      , "debug":
      [
        [
          {
            "filename":"conv_pipe.cl"
            , "line":415
            , "level":2
          }
        ]
      ]
      , "details":
      [
        "Unrolled by #pragma unroll"
      ]
    }
    , {
      "name":"Block15"
      , "data":
      ["Yes", "1", "n/a"]
      , "debug":
      [
        [
          {
            "filename":"conv_pipe.cl"
            , "line":420
            , "level":1
          }
        ]
      ]
    }
    , {
      "name":"Fully unrolled loop"
      , "data":
      ["n/a", "n/a", "n/a"]
      , "debug":
      [
        [
          {
            "filename":"conv_pipe.cl"
            , "line":426
            , "level":2
          }
        ]
      ]
      , "details":
      [
        "Unrolled by #pragma unroll"
      ]
    }
    , {
      "name":"1X Fully unrolled loop"
      , "data":
      ["n/a", "n/a", "n/a"]
      , "debug":
      [
        [
          {
            "filename":"conv_pipe.cl"
            , "line":83
            , "level":3
          }
        ]
      ]
      , "details":
      [
        "Loop has trip count of 1"
      ]
    }
    , {
      "name":"Fully unrolled loop"
      , "data":
      ["n/a", "n/a", "n/a"]
      , "debug":
      [
        [
          {
            "filename":"conv_pipe.cl"
            , "line":431
            , "level":3
          }
        ]
      ]
      , "details":
      [
        "Unrolled by #pragma unroll"
      ]
    }
    , {
      "name":"Fully unrolled loop"
      , "data":
      ["n/a", "n/a", "n/a"]
      , "debug":
      [
        [
          {
            "filename":"conv_pipe.cl"
            , "line":446
            , "level":1
          }
        ]
      ]
      , "details":
      [
        "Unrolled by #pragma unroll"
      ]
    }
    , {
      "name":"Fully unrolled loop"
      , "data":
      ["n/a", "n/a", "n/a"]
      , "debug":
      [
        [
          {
            "filename":"conv_pipe.cl"
            , "line":449
            , "level":2
          }
        ]
      ]
      , "details":
      [
        "Unrolled by #pragma unroll"
      ]
    }
    , {
      "name":"Block20"
      , "data":
      ["Yes", "1", "n/a"]
      , "debug":
      [
        [
          {
            "filename":"conv_pipe.cl"
            , "line":528
            , "level":0
          }
        ]
      ]
    }
    , {
      "name":"Fully unrolled loop"
      , "data":
      ["n/a", "n/a", "n/a"]
      , "debug":
      [
        [
          {
            "filename":"conv_pipe.cl"
            , "line":534
            , "level":1
          }
        ]
      ]
      , "details":
      [
        "Unrolled by #pragma unroll"
      ]
    }
    , {
      "name":"Fully unrolled loop"
      , "data":
      ["n/a", "n/a", "n/a"]
      , "debug":
      [
        [
          {
            "filename":"conv_pipe.cl"
            , "line":554
            , "level":2
          }
        ]
      ]
      , "details":
      [
        "Unrolled by #pragma unroll"
      ]
    }
    , {
      "name":"Fully unrolled loop"
      , "data":
      ["n/a", "n/a", "n/a"]
      , "debug":
      [
        [
          {
            "filename":"conv_pipe.cl"
            , "line":645
            , "level":0
          }
        ]
      ]
      , "details":
      [
        "Unrolled by #pragma unroll"
      ]
    }
    , {
      "name":"Fully unrolled loop"
      , "data":
      ["n/a", "n/a", "n/a"]
      , "debug":
      [
        [
          {
            "filename":"conv_pipe.cl"
            , "line":720
            , "level":0
          }
        ]
      ]
      , "details":
      [
        "Unrolled by #pragma unroll"
      ]
    }
    , {
      "name":"Fully unrolled loop"
      , "data":
      ["n/a", "n/a", "n/a"]
      , "debug":
      [
        [
          {
            "filename":"conv_pipe.cl"
            , "line":727
            , "level":0
          }
        ]
      ]
      , "details":
      [
        "Unrolled by #pragma unroll"
      ]
    }
    , {
      "name":"Fully unrolled loop"
      , "data":
      ["n/a", "n/a", "n/a"]
      , "debug":
      [
        [
          {
            "filename":"conv_pipe.cl"
            , "line":735
            , "level":0
          }
        ]
      ]
      , "details":
      [
        "Unrolled by #pragma unroll"
      ]
    }
    , {
      "name":"Block26"
      , "data":
      ["No", "n/a", "n/a"]
      , "debug":
      [
        [
          {
            "filename":"conv_pipe.cl"
            , "line":747
            , "level":0
          }
        ]
      ]
    }
    , {
      "name":"Fully unrolled loop"
      , "data":
      ["n/a", "n/a", "n/a"]
      , "debug":
      [
        [
          {
            "filename":"conv_pipe.cl"
            , "line":752
            , "level":1
          }
        ]
      ]
      , "details":
      [
        "Unrolled by #pragma unroll"
      ]
    }
    , {
      "name":"Fully unrolled loop"
      , "data":
      ["n/a", "n/a", "n/a"]
      , "debug":
      [
        [
          {
            "filename":"conv_pipe.cl"
            , "line":790
            , "level":0
          }
        ]
      ]
      , "details":
      [
        "Unrolled by #pragma unroll"
      ]
    }
  ]
}
;var mavJSON={
  "nodes":
  [
    {
      "type":"kernel"
      , "id":35
      , "name":"memRead"
      , "file":""
      , "line":"0"
      , "children":[
        {
          "type":"bb"
          , "id":3
          , "name":"Block0.wii_blk"
          , "file":""
          , "line":"0"
          , "details":
          {
            "Latency":"10"
          }
        }
        , {
          "type":"bb"
          , "id":4
          , "name":"Block1"
          , "file":""
          , "line":"0"
          , "details":
          {
            "Latency":"2"
          }
        }
        , {
          "type":"bb"
          , "id":5
          , "name":"Block2"
          , "file":""
          , "line":"0"
          , "II":2
          , "LoopInfo":"Entry to loop. "
          , "hasFmaxBottlenecks":"No"
          , "hasSubloops":"Yes"
          , "isPipelined":"Yes"
          , "loopTo":9
          , "details":
          {
            "Latency":"11"
          }
        }
        , {
          "type":"bb"
          , "id":6
          , "name":"Block3"
          , "file":""
          , "line":"0"
          , "II":1
          , "LoopInfo":"Entry to loop. "
          , "hasFmaxBottlenecks":"No"
          , "hasSubloops":"Yes"
          , "isPipelined":"Yes"
          , "loopTo":8
          , "details":
          {
            "Latency":"11"
          }
        }
        , {
          "type":"bb"
          , "id":7
          , "name":"Block4"
          , "file":""
          , "line":"0"
          , "II":1
          , "LoopInfo":""
          , "hasFmaxBottlenecks":"No"
          , "hasSubloops":"No"
          , "isPipelined":"Yes"
          , "children":[
            {
              "type":"inst"
              , "id":15
              , "name":"Load"
              , "file":"1"
              , "line":"178"
              , "details":
              {
                "Width":"32 bits"
                , "Type":"Burst-coalesced"
                , "Stall-free":"No"
                , "Start-Cycle":"10"
                , "Latency":"160"
              }
            }
            , {
              "type":"inst"
              , "id":16
              , "name":"Store"
              , "file":"1"
              , "line":"187"
              , "details":
              {
                "Width":"32 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"170"
                , "Latency":"3"
              }
            }
            , {
              "type":"inst"
              , "id":17
              , "name":"loop end"
              , "file":"1"
              , "line":"170"
              , "details":
              {
                "Start-Cycle":"173"
                , "Latency":"1"
                , "Additional Info":"Exit from a basic block. Control flow branches at this node to one or more merge nodes. There is no control branching between merge and branch node for the same basic block."
              }
            }
            , {
              "type":"inst"
              , "id":18
              , "name":"loop"
              , "file":""
              , "line":""
              , "loopTo":17
              , "details":
              {
                "Start-Cycle":"0"
                , "Latency":"1"
                , "Additional Info":"Entrance to a basic block. Control flow comes to this node from one or more branch nodes, unless it's the very first merge node in a kernel. There is no control branching between merge and branch node within the same basic block."
              }
            }
          ]
          , "details":
          {
            "Latency":"174"
          }
        }
        , {
          "type":"bb"
          , "id":8
          , "name":"Block5"
          , "file":""
          , "line":"0"
          , "II":1
          , "LoopInfo":"Exit which branches back to loop. "
          , "hasFmaxBottlenecks":"No"
          , "hasSubloops":"Yes"
          , "isPipelined":"Yes"
          , "details":
          {
            "Latency":"2"
          }
        }
        , {
          "type":"bb"
          , "id":9
          , "name":"Block6"
          , "file":""
          , "line":"0"
          , "II":2
          , "LoopInfo":"Exit which branches back to loop. "
          , "hasFmaxBottlenecks":"No"
          , "hasSubloops":"Yes"
          , "isPipelined":"Yes"
          , "details":
          {
            "Latency":"2"
          }
        }
        , {
          "type":"bb"
          , "id":10
          , "name":"Block7"
          , "file":""
          , "line":"0"
          , "details":
          {
            "Latency":"9"
          }
        }
        , {
          "type":"bb"
          , "id":11
          , "name":"Block8"
          , "file":""
          , "line":"0"
          , "II":1
          , "LoopInfo":"Entry to loop. "
          , "hasFmaxBottlenecks":"No"
          , "hasSubloops":"Yes"
          , "isPipelined":"Yes"
          , "loopTo":12
          , "details":
          {
            "Latency":"11"
          }
        }
        , {
          "type":"bb"
          , "id":12
          , "name":"Block9"
          , "file":""
          , "line":"0"
          , "II":1
          , "LoopInfo":"Exit which branches back to loop. "
          , "hasFmaxBottlenecks":"No"
          , "hasSubloops":"Yes"
          , "isPipelined":"Yes"
          , "details":
          {
            "Latency":"3"
          }
        }
        , {
          "type":"bb"
          , "id":13
          , "name":"Block10"
          , "file":""
          , "line":"0"
          , "II":1
          , "LoopInfo":""
          , "hasFmaxBottlenecks":"No"
          , "hasSubloops":"No"
          , "isPipelined":"Yes"
          , "children":[
            {
              "type":"inst"
              , "id":19
              , "name":"Load"
              , "file":"1"
              , "line":"249"
              , "details":
              {
                "Width":"32 bits"
                , "Type":"Burst-coalesced"
                , "Stall-free":"No"
                , "Start-Cycle":"15"
                , "Latency":"160"
              }
            }
            , {
              "type":"inst"
              , "id":20
              , "name":"Load"
              , "file":"1"
              , "line":"281"
              , "details":
              {
                "Width":"512 bits"
                , "Type":"Burst-coalesced"
                , "Stall-free":"No"
                , "Start-Cycle":"91"
                , "Latency":"84"
              }
            }
            , {
              "type":"inst"
              , "id":21
              , "name":"Load"
              , "file":"1"
              , "line":"290"
              , "details":
              {
                "Width":"128 bits"
                , "Type":"Burst-coalesced"
                , "Stall-free":"No"
                , "Start-Cycle":"15"
                , "Latency":"160"
              }
            }
            , {
              "type":"inst"
              , "id":22
              , "name":"Store"
              , "file":"1"
              , "line":"258"
              , "details":
              {
                "Width":"32 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"177"
                , "Latency":"3"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":23
              , "name":"Store"
              , "file":"1"
              , "line":"282"
              , "details":
              {
                "Width":"512 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"177"
                , "Latency":"1"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":24
              , "name":"Load"
              , "file":"1"
              , "line":"298"
              , "details":
              {
                "Width":"32 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"178"
                , "Latency":"5"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":25
              , "name":"Load"
              , "file":"1"
              , "line":"307"
              , "details":
              {
                "Width":"512 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"178"
                , "Latency":"1"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":26
              , "name":"Channel Write"
              , "file":"1"
              , "line":"303"
              , "details":
              {
                "Width":"512 bits"
                , "Depth":"0"
                , "Stall-free":"No"
                , "Start-Cycle":"189"
                , "Latency":"1"
              }
            }
            , {
              "type":"inst"
              , "id":28
              , "name":"Channel Write"
              , "file":"1"
              , "line":"308"
              , "details":
              {
                "Width":"512 bits"
                , "Depth":"0"
                , "Stall-free":"No"
                , "Start-Cycle":"189"
                , "Latency":"1"
              }
            }
            , {
              "type":"inst"
              , "id":30
              , "name":"Channel Write"
              , "file":"1"
              , "line":"291"
              , "details":
              {
                "Width":"128 bits"
                , "Depth":"10"
                , "Stall-free":"No"
                , "Start-Cycle":"175"
                , "Latency":"1"
              }
            }
            , {
              "type":"inst"
              , "id":32
              , "name":"loop end"
              , "file":"1"
              , "line":"238"
              , "details":
              {
                "Start-Cycle":"190"
                , "Latency":"1"
                , "Additional Info":"Exit from a basic block. Control flow branches at this node to one or more merge nodes. There is no control branching between merge and branch node for the same basic block."
              }
            }
            , {
              "type":"inst"
              , "id":33
              , "name":"loop"
              , "file":""
              , "line":""
              , "loopTo":32
              , "details":
              {
                "Start-Cycle":"0"
                , "Latency":"1"
                , "Additional Info":"Entrance to a basic block. Control flow comes to this node from one or more branch nodes, unless it's the very first merge node in a kernel. There is no control branching between merge and branch node within the same basic block."
              }
            }
          ]
          , "details":
          {
            "Latency":"191"
          }
        }
        , {
          "type":"bb"
          , "id":14
          , "name":"Block11"
          , "file":""
          , "line":"0"
          , "details":
          {
            "Latency":"2"
          }
        }
        , {
          "type":"memtype"
          , "id":36
          , "name":"Local Memory"
          , "file":""
          , "line":"0"
          , "children":[
            {
              "type":"memsys"
              , "id":37
              , "name":"win_buffer"
              , "file":""
              , "line":"0"
              , "replFactor":"1"
              , "banks":1
              , "pumping":1
              , "children":[
                {
                  "type":"bank"
                  , "id":38
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
            , {
              "type":"memsys"
              , "id":39
              , "name":"weight_buffer"
              , "file":""
              , "line":"0"
              , "replFactor":"1"
              , "banks":1
              , "pumping":1
              , "children":[
                {
                  "type":"bank"
                  , "id":40
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
          ]
        }
      ]
    }
    , {
      "type":"kernel"
      , "id":98
      , "name":"coreConv"
      , "file":""
      , "line":"0"
      , "children":[
        {
          "type":"bb"
          , "id":45
          , "name":"Block12.wii_blk"
          , "file":""
          , "line":"0"
          , "details":
          {
            "Latency":"6"
          }
        }
        , {
          "type":"bb"
          , "id":46
          , "name":"Block13"
          , "file":""
          , "line":"0"
          , "details":
          {
            "Latency":"3"
          }
        }
        , {
          "type":"bb"
          , "id":47
          , "name":"Block14"
          , "file":""
          , "line":"0"
          , "II":1
          , "LoopInfo":"Entry to loop. "
          , "hasFmaxBottlenecks":"No"
          , "hasSubloops":"Yes"
          , "isPipelined":"Yes"
          , "children":[
            {
              "type":"inst"
              , "id":51
              , "name":"Channel Read"
              , "file":"1"
              , "line":"406"
              , "details":
              {
                "Width":"128 bits"
                , "Depth":"10"
                , "Stall-free":"No"
                , "Start-Cycle":"8"
                , "Latency":"1"
              }
            }
            , {
              "type":"inst"
              , "id":52
              , "name":"end"
              , "file":"1"
              , "line":"420"
              , "details":
              {
                "Start-Cycle":"10"
                , "Latency":"1"
                , "Additional Info":"Exit from a basic block. Control flow branches at this node to one or more merge nodes. There is no control branching between merge and branch node for the same basic block."
              }
            }
            , {
              "type":"inst"
              , "id":53
              , "name":"loop"
              , "file":""
              , "line":""
              , "loopTo":95
              , "details":
              {
                "Start-Cycle":"0"
                , "Latency":"1"
                , "Additional Info":"Entrance to a basic block. Control flow comes to this node from one or more branch nodes, unless it's the very first merge node in a kernel. There is no control branching between merge and branch node within the same basic block."
              }
            }
          ]
          , "details":
          {
            "Latency":"11"
          }
        }
        , {
          "type":"bb"
          , "id":48
          , "name":"Block15"
          , "file":""
          , "line":"0"
          , "II":1
          , "LoopInfo":""
          , "hasFmaxBottlenecks":"No"
          , "hasSubloops":"No"
          , "isPipelined":"Yes"
          , "children":[
            {
              "type":"inst"
              , "id":54
              , "name":"Channel Read"
              , "file":"1"
              , "line":"422"
              , "details":
              {
                "Width":"512 bits"
                , "Depth":"0"
                , "Stall-free":"No"
                , "Start-Cycle":"9"
                , "Latency":"1"
              }
            }
            , {
              "type":"inst"
              , "id":55
              , "name":"Channel Read"
              , "file":"1"
              , "line":"423"
              , "details":
              {
                "Width":"512 bits"
                , "Depth":"0"
                , "Stall-free":"No"
                , "Start-Cycle":"9"
                , "Latency":"1"
              }
            }
            , {
              "type":"inst"
              , "id":56
              , "name":"'mult_add_fix8bx4' HDL Function Call"
              , "file":"1"
              , "line":"84"
              , "details":
              {
                "Start-Cycle":"12"
                , "Latency":"1"
                , "Additional Info":"The function is implemented in a library. Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":58
              , "name":"'mult_add_fix8bx4' HDL Function Call"
              , "file":"1"
              , "line":"84"
              , "details":
              {
                "Start-Cycle":"14"
                , "Latency":"1"
                , "Additional Info":"The function is implemented in a library. Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":59
              , "name":"'mult_add_fix8bx4' HDL Function Call"
              , "file":"1"
              , "line":"84"
              , "details":
              {
                "Start-Cycle":"12"
                , "Latency":"1"
                , "Additional Info":"The function is implemented in a library. Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":60
              , "name":"'mult_add_fix8bx4' HDL Function Call"
              , "file":"1"
              , "line":"84"
              , "details":
              {
                "Start-Cycle":"14"
                , "Latency":"1"
                , "Additional Info":"The function is implemented in a library. Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":61
              , "name":"'mult_add_fix8bx4' HDL Function Call"
              , "file":"1"
              , "line":"84"
              , "details":
              {
                "Start-Cycle":"12"
                , "Latency":"1"
                , "Additional Info":"The function is implemented in a library. Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":62
              , "name":"'mult_add_fix8bx4' HDL Function Call"
              , "file":"1"
              , "line":"84"
              , "details":
              {
                "Start-Cycle":"14"
                , "Latency":"1"
                , "Additional Info":"The function is implemented in a library. Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":63
              , "name":"'mult_add_fix8bx4' HDL Function Call"
              , "file":"1"
              , "line":"84"
              , "details":
              {
                "Start-Cycle":"12"
                , "Latency":"1"
                , "Additional Info":"The function is implemented in a library. Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":64
              , "name":"'mult_add_fix8bx4' HDL Function Call"
              , "file":"1"
              , "line":"84"
              , "details":
              {
                "Start-Cycle":"14"
                , "Latency":"1"
                , "Additional Info":"The function is implemented in a library. Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":65
              , "name":"'mult_add_fix8bx4' HDL Function Call"
              , "file":"1"
              , "line":"84"
              , "details":
              {
                "Start-Cycle":"12"
                , "Latency":"1"
                , "Additional Info":"The function is implemented in a library. Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":66
              , "name":"'mult_add_fix8bx4' HDL Function Call"
              , "file":"1"
              , "line":"84"
              , "details":
              {
                "Start-Cycle":"14"
                , "Latency":"1"
                , "Additional Info":"The function is implemented in a library. Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":67
              , "name":"'mult_add_fix8bx4' HDL Function Call"
              , "file":"1"
              , "line":"84"
              , "details":
              {
                "Start-Cycle":"12"
                , "Latency":"1"
                , "Additional Info":"The function is implemented in a library. Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":68
              , "name":"'mult_add_fix8bx4' HDL Function Call"
              , "file":"1"
              , "line":"84"
              , "details":
              {
                "Start-Cycle":"14"
                , "Latency":"1"
                , "Additional Info":"The function is implemented in a library. Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":69
              , "name":"'mult_add_fix8bx4' HDL Function Call"
              , "file":"1"
              , "line":"84"
              , "details":
              {
                "Start-Cycle":"12"
                , "Latency":"1"
                , "Additional Info":"The function is implemented in a library. Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":70
              , "name":"'mult_add_fix8bx4' HDL Function Call"
              , "file":"1"
              , "line":"84"
              , "details":
              {
                "Start-Cycle":"14"
                , "Latency":"1"
                , "Additional Info":"The function is implemented in a library. Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":71
              , "name":"'mult_add_fix8bx4' HDL Function Call"
              , "file":"1"
              , "line":"84"
              , "details":
              {
                "Start-Cycle":"12"
                , "Latency":"1"
                , "Additional Info":"The function is implemented in a library. Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":72
              , "name":"'mult_add_fix8bx4' HDL Function Call"
              , "file":"1"
              , "line":"84"
              , "details":
              {
                "Start-Cycle":"14"
                , "Latency":"1"
                , "Additional Info":"The function is implemented in a library. Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":73
              , "name":"'mult_add_fix8bx4' HDL Function Call"
              , "file":"1"
              , "line":"84"
              , "details":
              {
                "Start-Cycle":"12"
                , "Latency":"1"
                , "Additional Info":"The function is implemented in a library. Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":74
              , "name":"'mult_add_fix8bx4' HDL Function Call"
              , "file":"1"
              , "line":"84"
              , "details":
              {
                "Start-Cycle":"14"
                , "Latency":"1"
                , "Additional Info":"The function is implemented in a library. Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":75
              , "name":"'mult_add_fix8bx4' HDL Function Call"
              , "file":"1"
              , "line":"84"
              , "details":
              {
                "Start-Cycle":"12"
                , "Latency":"1"
                , "Additional Info":"The function is implemented in a library. Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":76
              , "name":"'mult_add_fix8bx4' HDL Function Call"
              , "file":"1"
              , "line":"84"
              , "details":
              {
                "Start-Cycle":"14"
                , "Latency":"1"
                , "Additional Info":"The function is implemented in a library. Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":77
              , "name":"'mult_add_fix8bx4' HDL Function Call"
              , "file":"1"
              , "line":"84"
              , "details":
              {
                "Start-Cycle":"12"
                , "Latency":"1"
                , "Additional Info":"The function is implemented in a library. Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":78
              , "name":"'mult_add_fix8bx4' HDL Function Call"
              , "file":"1"
              , "line":"84"
              , "details":
              {
                "Start-Cycle":"14"
                , "Latency":"1"
                , "Additional Info":"The function is implemented in a library. Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":79
              , "name":"'mult_add_fix8bx4' HDL Function Call"
              , "file":"1"
              , "line":"84"
              , "details":
              {
                "Start-Cycle":"12"
                , "Latency":"1"
                , "Additional Info":"The function is implemented in a library. Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":80
              , "name":"'mult_add_fix8bx4' HDL Function Call"
              , "file":"1"
              , "line":"84"
              , "details":
              {
                "Start-Cycle":"14"
                , "Latency":"1"
                , "Additional Info":"The function is implemented in a library. Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":81
              , "name":"'mult_add_fix8bx4' HDL Function Call"
              , "file":"1"
              , "line":"84"
              , "details":
              {
                "Start-Cycle":"12"
                , "Latency":"1"
                , "Additional Info":"The function is implemented in a library. Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":82
              , "name":"'mult_add_fix8bx4' HDL Function Call"
              , "file":"1"
              , "line":"84"
              , "details":
              {
                "Start-Cycle":"14"
                , "Latency":"1"
                , "Additional Info":"The function is implemented in a library. Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":83
              , "name":"'mult_add_fix8bx4' HDL Function Call"
              , "file":"1"
              , "line":"84"
              , "details":
              {
                "Start-Cycle":"12"
                , "Latency":"1"
                , "Additional Info":"The function is implemented in a library. Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":84
              , "name":"'mult_add_fix8bx4' HDL Function Call"
              , "file":"1"
              , "line":"84"
              , "details":
              {
                "Start-Cycle":"14"
                , "Latency":"1"
                , "Additional Info":"The function is implemented in a library. Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":85
              , "name":"'mult_add_fix8bx4' HDL Function Call"
              , "file":"1"
              , "line":"84"
              , "details":
              {
                "Start-Cycle":"12"
                , "Latency":"1"
                , "Additional Info":"The function is implemented in a library. Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":86
              , "name":"'mult_add_fix8bx4' HDL Function Call"
              , "file":"1"
              , "line":"84"
              , "details":
              {
                "Start-Cycle":"14"
                , "Latency":"1"
                , "Additional Info":"The function is implemented in a library. Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":87
              , "name":"'mult_add_fix8bx4' HDL Function Call"
              , "file":"1"
              , "line":"84"
              , "details":
              {
                "Start-Cycle":"12"
                , "Latency":"1"
                , "Additional Info":"The function is implemented in a library. Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":88
              , "name":"'mult_add_fix8bx4' HDL Function Call"
              , "file":"1"
              , "line":"84"
              , "details":
              {
                "Start-Cycle":"14"
                , "Latency":"1"
                , "Additional Info":"The function is implemented in a library. Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":89
              , "name":"loop end"
              , "file":"1"
              , "line":"420"
              , "details":
              {
                "Start-Cycle":"20"
                , "Latency":"1"
                , "Additional Info":"Exit from a basic block. Control flow branches at this node to one or more merge nodes. There is no control branching between merge and branch node for the same basic block."
              }
            }
            , {
              "type":"inst"
              , "id":90
              , "name":"loop"
              , "file":""
              , "line":""
              , "loopTo":89
              , "details":
              {
                "Start-Cycle":"0"
                , "Latency":"1"
                , "Additional Info":"Entrance to a basic block. Control flow comes to this node from one or more branch nodes, unless it's the very first merge node in a kernel. There is no control branching between merge and branch node within the same basic block."
              }
            }
          ]
          , "details":
          {
            "Latency":"21"
          }
        }
        , {
          "type":"bb"
          , "id":49
          , "name":"Block16"
          , "file":""
          , "line":"0"
          , "II":1
          , "LoopInfo":"Exit which branches back to loop. "
          , "hasFmaxBottlenecks":"No"
          , "hasSubloops":"Yes"
          , "isPipelined":"Yes"
          , "children":[
            {
              "type":"inst"
              , "id":91
              , "name":"Channel Write"
              , "file":"1"
              , "line":"491"
              , "details":
              {
                "Width":"128 bits"
                , "Depth":"0"
                , "Stall-free":"No"
                , "Start-Cycle":"11"
                , "Latency":"1"
              }
            }
            , {
              "type":"inst"
              , "id":93
              , "name":"Channel Write"
              , "file":"1"
              , "line":"489"
              , "details":
              {
                "Width":"128 bits"
                , "Depth":"15"
                , "Stall-free":"No"
                , "Start-Cycle":"11"
                , "Latency":"1"
              }
            }
            , {
              "type":"inst"
              , "id":95
              , "name":"loop end"
              , "file":"1"
              , "line":"404"
              , "details":
              {
                "Start-Cycle":"12"
                , "Latency":"1"
                , "Additional Info":"Exit from a basic block. Control flow branches at this node to one or more merge nodes. There is no control branching between merge and branch node for the same basic block."
              }
            }
            , {
              "type":"inst"
              , "id":96
              , "name":"begin"
              , "file":""
              , "line":""
              , "details":
              {
                "Start-Cycle":"0"
                , "Latency":"1"
                , "Additional Info":"Entrance to a basic block. Control flow comes to this node from one or more branch nodes, unless it's the very first merge node in a kernel. There is no control branching between merge and branch node within the same basic block."
              }
            }
          ]
          , "details":
          {
            "Latency":"13"
          }
        }
        , {
          "type":"bb"
          , "id":50
          , "name":"Block17"
          , "file":""
          , "line":"0"
          , "details":
          {
            "Latency":"2"
          }
        }
      ]
    }
    , {
      "type":"kernel"
      , "id":174
      , "name":"maxPool"
      , "file":""
      , "line":"0"
      , "children":[
        {
          "type":"bb"
          , "id":100
          , "name":"Block18.wii_blk"
          , "file":""
          , "line":"0"
          , "details":
          {
            "Latency":"6"
          }
        }
        , {
          "type":"bb"
          , "id":101
          , "name":"Block19"
          , "file":""
          , "line":"0"
          , "details":
          {
            "Latency":"3"
          }
        }
        , {
          "type":"bb"
          , "id":102
          , "name":"Block20"
          , "file":""
          , "line":"0"
          , "II":1
          , "LoopInfo":""
          , "hasFmaxBottlenecks":"No"
          , "hasSubloops":"No"
          , "isPipelined":"Yes"
          , "children":[
            {
              "type":"inst"
              , "id":104
              , "name":"Channel Read"
              , "file":"1"
              , "line":"530"
              , "details":
              {
                "Width":"128 bits"
                , "Depth":"0"
                , "Stall-free":"No"
                , "Start-Cycle":"10"
                , "Latency":"1"
              }
            }
            , {
              "type":"inst"
              , "id":105
              , "name":"Load"
              , "file":"1"
              , "line":"537"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"18"
                , "Latency":"4"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":106
              , "name":"Load"
              , "file":"1"
              , "line":"537"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"14"
                , "Latency":"4"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":107
              , "name":"Store"
              , "file":"1"
              , "line":"550"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"18"
                , "Latency":"2"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":108
              , "name":"Store"
              , "file":"1"
              , "line":"551"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"14"
                , "Latency":"2"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":109
              , "name":"Load"
              , "file":"1"
              , "line":"537"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"18"
                , "Latency":"4"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":110
              , "name":"Load"
              , "file":"1"
              , "line":"537"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"14"
                , "Latency":"4"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":111
              , "name":"Store"
              , "file":"1"
              , "line":"550"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"18"
                , "Latency":"2"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":112
              , "name":"Store"
              , "file":"1"
              , "line":"551"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"14"
                , "Latency":"2"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":113
              , "name":"Load"
              , "file":"1"
              , "line":"537"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"18"
                , "Latency":"4"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":114
              , "name":"Load"
              , "file":"1"
              , "line":"537"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"14"
                , "Latency":"4"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":115
              , "name":"Store"
              , "file":"1"
              , "line":"550"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"18"
                , "Latency":"2"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":116
              , "name":"Store"
              , "file":"1"
              , "line":"551"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"14"
                , "Latency":"2"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":117
              , "name":"Load"
              , "file":"1"
              , "line":"537"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"18"
                , "Latency":"4"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":118
              , "name":"Load"
              , "file":"1"
              , "line":"537"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"14"
                , "Latency":"4"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":119
              , "name":"Store"
              , "file":"1"
              , "line":"550"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"18"
                , "Latency":"2"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":120
              , "name":"Store"
              , "file":"1"
              , "line":"551"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"14"
                , "Latency":"2"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":121
              , "name":"Load"
              , "file":"1"
              , "line":"537"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"18"
                , "Latency":"4"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":122
              , "name":"Load"
              , "file":"1"
              , "line":"537"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"14"
                , "Latency":"4"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":123
              , "name":"Store"
              , "file":"1"
              , "line":"550"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"18"
                , "Latency":"2"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":124
              , "name":"Store"
              , "file":"1"
              , "line":"551"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"14"
                , "Latency":"2"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":125
              , "name":"Load"
              , "file":"1"
              , "line":"537"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"18"
                , "Latency":"4"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":126
              , "name":"Load"
              , "file":"1"
              , "line":"537"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"14"
                , "Latency":"4"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":127
              , "name":"Store"
              , "file":"1"
              , "line":"550"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"18"
                , "Latency":"2"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":128
              , "name":"Store"
              , "file":"1"
              , "line":"551"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"14"
                , "Latency":"2"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":129
              , "name":"Load"
              , "file":"1"
              , "line":"537"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"18"
                , "Latency":"4"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":130
              , "name":"Load"
              , "file":"1"
              , "line":"537"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"14"
                , "Latency":"4"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":131
              , "name":"Store"
              , "file":"1"
              , "line":"550"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"18"
                , "Latency":"2"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":132
              , "name":"Store"
              , "file":"1"
              , "line":"551"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"14"
                , "Latency":"2"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":133
              , "name":"Load"
              , "file":"1"
              , "line":"537"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"18"
                , "Latency":"4"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":134
              , "name":"Load"
              , "file":"1"
              , "line":"537"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"14"
                , "Latency":"4"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":135
              , "name":"Store"
              , "file":"1"
              , "line":"550"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"18"
                , "Latency":"2"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":136
              , "name":"Store"
              , "file":"1"
              , "line":"551"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"14"
                , "Latency":"2"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":137
              , "name":"Load"
              , "file":"1"
              , "line":"537"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"18"
                , "Latency":"4"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":138
              , "name":"Load"
              , "file":"1"
              , "line":"537"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"14"
                , "Latency":"4"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":139
              , "name":"Store"
              , "file":"1"
              , "line":"550"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"18"
                , "Latency":"2"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":140
              , "name":"Store"
              , "file":"1"
              , "line":"551"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"14"
                , "Latency":"2"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":141
              , "name":"Load"
              , "file":"1"
              , "line":"537"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"18"
                , "Latency":"4"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":142
              , "name":"Load"
              , "file":"1"
              , "line":"537"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"14"
                , "Latency":"4"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":143
              , "name":"Store"
              , "file":"1"
              , "line":"550"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"18"
                , "Latency":"2"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":144
              , "name":"Store"
              , "file":"1"
              , "line":"551"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"14"
                , "Latency":"2"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":145
              , "name":"Load"
              , "file":"1"
              , "line":"537"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"18"
                , "Latency":"4"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":146
              , "name":"Load"
              , "file":"1"
              , "line":"537"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"14"
                , "Latency":"4"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":147
              , "name":"Store"
              , "file":"1"
              , "line":"550"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"18"
                , "Latency":"2"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":148
              , "name":"Store"
              , "file":"1"
              , "line":"551"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"14"
                , "Latency":"2"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":149
              , "name":"Load"
              , "file":"1"
              , "line":"537"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"18"
                , "Latency":"4"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":150
              , "name":"Load"
              , "file":"1"
              , "line":"537"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"14"
                , "Latency":"4"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":151
              , "name":"Store"
              , "file":"1"
              , "line":"550"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"18"
                , "Latency":"2"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":152
              , "name":"Store"
              , "file":"1"
              , "line":"551"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"14"
                , "Latency":"2"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":153
              , "name":"Load"
              , "file":"1"
              , "line":"537"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"18"
                , "Latency":"4"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":154
              , "name":"Load"
              , "file":"1"
              , "line":"537"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"14"
                , "Latency":"4"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":155
              , "name":"Store"
              , "file":"1"
              , "line":"550"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"18"
                , "Latency":"2"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":156
              , "name":"Store"
              , "file":"1"
              , "line":"551"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"14"
                , "Latency":"2"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":157
              , "name":"Load"
              , "file":"1"
              , "line":"537"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"18"
                , "Latency":"4"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":158
              , "name":"Load"
              , "file":"1"
              , "line":"537"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"14"
                , "Latency":"4"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":159
              , "name":"Store"
              , "file":"1"
              , "line":"550"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"18"
                , "Latency":"2"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":160
              , "name":"Store"
              , "file":"1"
              , "line":"551"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"14"
                , "Latency":"2"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":161
              , "name":"Load"
              , "file":"1"
              , "line":"537"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"18"
                , "Latency":"4"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":162
              , "name":"Load"
              , "file":"1"
              , "line":"537"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"14"
                , "Latency":"4"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":163
              , "name":"Store"
              , "file":"1"
              , "line":"550"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"18"
                , "Latency":"2"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":164
              , "name":"Store"
              , "file":"1"
              , "line":"551"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"14"
                , "Latency":"2"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":165
              , "name":"Load"
              , "file":"1"
              , "line":"537"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"18"
                , "Latency":"4"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":166
              , "name":"Load"
              , "file":"1"
              , "line":"537"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"14"
                , "Latency":"4"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":167
              , "name":"Store"
              , "file":"1"
              , "line":"550"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"18"
                , "Latency":"2"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":168
              , "name":"Store"
              , "file":"1"
              , "line":"551"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"14"
                , "Latency":"2"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":169
              , "name":"Channel Write"
              , "file":"1"
              , "line":"568"
              , "details":
              {
                "Width":"128 bits"
                , "Depth":"0"
                , "Stall-free":"No"
                , "Start-Cycle":"29"
                , "Latency":"1"
              }
            }
            , {
              "type":"inst"
              , "id":171
              , "name":"loop end"
              , "file":"1"
              , "line":"528"
              , "details":
              {
                "Start-Cycle":"30"
                , "Latency":"1"
                , "Additional Info":"Exit from a basic block. Control flow branches at this node to one or more merge nodes. There is no control branching between merge and branch node for the same basic block."
              }
            }
            , {
              "type":"inst"
              , "id":172
              , "name":"loop"
              , "file":""
              , "line":""
              , "loopTo":171
              , "details":
              {
                "Start-Cycle":"0"
                , "Latency":"1"
                , "Additional Info":"Entrance to a basic block. Control flow comes to this node from one or more branch nodes, unless it's the very first merge node in a kernel. There is no control branching between merge and branch node within the same basic block."
              }
            }
          ]
          , "details":
          {
            "Latency":"31"
          }
        }
        , {
          "type":"bb"
          , "id":103
          , "name":"Block21"
          , "file":""
          , "line":"0"
          , "details":
          {
            "Latency":"2"
          }
        }
        , {
          "type":"memtype"
          , "id":175
          , "name":"Local Memory"
          , "file":""
          , "line":"0"
          , "children":[
            {
              "type":"memsys"
              , "id":176
              , "name":"line_buf_1"
              , "file":""
              , "line":"0"
              , "replFactor":"1"
              , "banks":1
              , "pumping":1
              , "children":[
                {
                  "type":"bank"
                  , "id":177
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
            , {
              "type":"memsys"
              , "id":178
              , "name":"line_buf_0"
              , "file":""
              , "line":"0"
              , "replFactor":"1"
              , "banks":1
              , "pumping":1
              , "children":[
                {
                  "type":"bank"
                  , "id":179
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
            , {
              "type":"memsys"
              , "id":180
              , "name":"line_buf_0"
              , "file":""
              , "line":"0"
              , "replFactor":"1"
              , "banks":1
              , "pumping":1
              , "children":[
                {
                  "type":"bank"
                  , "id":181
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
            , {
              "type":"memsys"
              , "id":182
              , "name":"line_buf_1"
              , "file":""
              , "line":"0"
              , "replFactor":"1"
              , "banks":1
              , "pumping":1
              , "children":[
                {
                  "type":"bank"
                  , "id":183
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
            , {
              "type":"memsys"
              , "id":184
              , "name":"line_buf_0"
              , "file":""
              , "line":"0"
              , "replFactor":"1"
              , "banks":1
              , "pumping":1
              , "children":[
                {
                  "type":"bank"
                  , "id":185
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
            , {
              "type":"memsys"
              , "id":186
              , "name":"line_buf_1"
              , "file":""
              , "line":"0"
              , "replFactor":"1"
              , "banks":1
              , "pumping":1
              , "children":[
                {
                  "type":"bank"
                  , "id":187
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
            , {
              "type":"memsys"
              , "id":188
              , "name":"line_buf_0"
              , "file":""
              , "line":"0"
              , "replFactor":"1"
              , "banks":1
              , "pumping":1
              , "children":[
                {
                  "type":"bank"
                  , "id":189
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
            , {
              "type":"memsys"
              , "id":190
              , "name":"line_buf_1"
              , "file":""
              , "line":"0"
              , "replFactor":"1"
              , "banks":1
              , "pumping":1
              , "children":[
                {
                  "type":"bank"
                  , "id":191
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
            , {
              "type":"memsys"
              , "id":192
              , "name":"line_buf_0"
              , "file":""
              , "line":"0"
              , "replFactor":"1"
              , "banks":1
              , "pumping":1
              , "children":[
                {
                  "type":"bank"
                  , "id":193
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
            , {
              "type":"memsys"
              , "id":194
              , "name":"line_buf_1"
              , "file":""
              , "line":"0"
              , "replFactor":"1"
              , "banks":1
              , "pumping":1
              , "children":[
                {
                  "type":"bank"
                  , "id":195
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
            , {
              "type":"memsys"
              , "id":196
              , "name":"line_buf_0"
              , "file":""
              , "line":"0"
              , "replFactor":"1"
              , "banks":1
              , "pumping":1
              , "children":[
                {
                  "type":"bank"
                  , "id":197
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
            , {
              "type":"memsys"
              , "id":198
              , "name":"line_buf_1"
              , "file":""
              , "line":"0"
              , "replFactor":"1"
              , "banks":1
              , "pumping":1
              , "children":[
                {
                  "type":"bank"
                  , "id":199
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
            , {
              "type":"memsys"
              , "id":200
              , "name":"line_buf_0"
              , "file":""
              , "line":"0"
              , "replFactor":"1"
              , "banks":1
              , "pumping":1
              , "children":[
                {
                  "type":"bank"
                  , "id":201
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
            , {
              "type":"memsys"
              , "id":202
              , "name":"line_buf_1"
              , "file":""
              , "line":"0"
              , "replFactor":"1"
              , "banks":1
              , "pumping":1
              , "children":[
                {
                  "type":"bank"
                  , "id":203
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
            , {
              "type":"memsys"
              , "id":204
              , "name":"line_buf_0"
              , "file":""
              , "line":"0"
              , "replFactor":"1"
              , "banks":1
              , "pumping":1
              , "children":[
                {
                  "type":"bank"
                  , "id":205
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
            , {
              "type":"memsys"
              , "id":206
              , "name":"line_buf_1"
              , "file":""
              , "line":"0"
              , "replFactor":"1"
              , "banks":1
              , "pumping":1
              , "children":[
                {
                  "type":"bank"
                  , "id":207
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
            , {
              "type":"memsys"
              , "id":208
              , "name":"line_buf_0"
              , "file":""
              , "line":"0"
              , "replFactor":"1"
              , "banks":1
              , "pumping":1
              , "children":[
                {
                  "type":"bank"
                  , "id":209
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
            , {
              "type":"memsys"
              , "id":210
              , "name":"line_buf_1"
              , "file":""
              , "line":"0"
              , "replFactor":"1"
              , "banks":1
              , "pumping":1
              , "children":[
                {
                  "type":"bank"
                  , "id":211
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
            , {
              "type":"memsys"
              , "id":212
              , "name":"line_buf_0"
              , "file":""
              , "line":"0"
              , "replFactor":"1"
              , "banks":1
              , "pumping":1
              , "children":[
                {
                  "type":"bank"
                  , "id":213
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
            , {
              "type":"memsys"
              , "id":214
              , "name":"line_buf_1"
              , "file":""
              , "line":"0"
              , "replFactor":"1"
              , "banks":1
              , "pumping":1
              , "children":[
                {
                  "type":"bank"
                  , "id":215
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
            , {
              "type":"memsys"
              , "id":216
              , "name":"line_buf_0"
              , "file":""
              , "line":"0"
              , "replFactor":"1"
              , "banks":1
              , "pumping":1
              , "children":[
                {
                  "type":"bank"
                  , "id":217
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
            , {
              "type":"memsys"
              , "id":218
              , "name":"line_buf_1"
              , "file":""
              , "line":"0"
              , "replFactor":"1"
              , "banks":1
              , "pumping":1
              , "children":[
                {
                  "type":"bank"
                  , "id":219
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
            , {
              "type":"memsys"
              , "id":220
              , "name":"line_buf_0"
              , "file":""
              , "line":"0"
              , "replFactor":"1"
              , "banks":1
              , "pumping":1
              , "children":[
                {
                  "type":"bank"
                  , "id":221
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
            , {
              "type":"memsys"
              , "id":222
              , "name":"line_buf_1"
              , "file":""
              , "line":"0"
              , "replFactor":"1"
              , "banks":1
              , "pumping":1
              , "children":[
                {
                  "type":"bank"
                  , "id":223
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
            , {
              "type":"memsys"
              , "id":224
              , "name":"line_buf_0"
              , "file":""
              , "line":"0"
              , "replFactor":"1"
              , "banks":1
              , "pumping":1
              , "children":[
                {
                  "type":"bank"
                  , "id":225
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
            , {
              "type":"memsys"
              , "id":226
              , "name":"line_buf_1"
              , "file":""
              , "line":"0"
              , "replFactor":"1"
              , "banks":1
              , "pumping":1
              , "children":[
                {
                  "type":"bank"
                  , "id":227
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
            , {
              "type":"memsys"
              , "id":228
              , "name":"line_buf_0"
              , "file":""
              , "line":"0"
              , "replFactor":"1"
              , "banks":1
              , "pumping":1
              , "children":[
                {
                  "type":"bank"
                  , "id":229
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
            , {
              "type":"memsys"
              , "id":230
              , "name":"line_buf_1"
              , "file":""
              , "line":"0"
              , "replFactor":"1"
              , "banks":1
              , "pumping":1
              , "children":[
                {
                  "type":"bank"
                  , "id":231
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
            , {
              "type":"memsys"
              , "id":232
              , "name":"line_buf_0"
              , "file":""
              , "line":"0"
              , "replFactor":"1"
              , "banks":1
              , "pumping":1
              , "children":[
                {
                  "type":"bank"
                  , "id":233
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
            , {
              "type":"memsys"
              , "id":234
              , "name":"line_buf_1"
              , "file":""
              , "line":"0"
              , "replFactor":"1"
              , "banks":1
              , "pumping":1
              , "children":[
                {
                  "type":"bank"
                  , "id":235
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
            , {
              "type":"memsys"
              , "id":236
              , "name":"line_buf_0"
              , "file":""
              , "line":"0"
              , "replFactor":"1"
              , "banks":1
              , "pumping":1
              , "children":[
                {
                  "type":"bank"
                  , "id":237
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
            , {
              "type":"memsys"
              , "id":238
              , "name":"line_buf_1"
              , "file":""
              , "line":"0"
              , "replFactor":"1"
              , "banks":1
              , "pumping":1
              , "children":[
                {
                  "type":"bank"
                  , "id":239
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
          ]
        }
      ]
    }
    , {
      "type":"kernel"
      , "id":251
      , "name":"memWrite"
      , "file":""
      , "line":"0"
      , "children":[
        {
          "type":"bb"
          , "id":241
          , "name":"Block22.wii_blk"
          , "file":""
          , "line":"0"
          , "details":
          {
            "Latency":"5"
          }
        }
        , {
          "type":"bb"
          , "id":242
          , "name":"Block23"
          , "file":""
          , "line":"0"
          , "children":[
            {
              "type":"inst"
              , "id":243
              , "name":"Channel Read"
              , "file":"1"
              , "line":"642"
              , "details":
              {
                "Width":"128 bits"
                , "Depth":"0"
                , "Stall-free":"No"
                , "Start-Cycle":"1"
                , "Latency":"1"
              }
            }
            , {
              "type":"inst"
              , "id":244
              , "name":"Channel Read"
              , "file":"1"
              , "line":"640"
              , "details":
              {
                "Width":"128 bits"
                , "Depth":"15"
                , "Stall-free":"No"
                , "Start-Cycle":"1"
                , "Latency":"1"
              }
            }
            , {
              "type":"inst"
              , "id":245
              , "name":"Store"
              , "file":"1"
              , "line":"646"
              , "details":
              {
                "Width":"128 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"3"
                , "Latency":"2"
              }
            }
            , {
              "type":"inst"
              , "id":246
              , "name":"Load"
              , "file":"1"
              , "line":"659"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"27"
                , "Latency":"4"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":247
              , "name":"Store"
              , "file":"1"
              , "line":"659"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Burst-coalesced"
                , "Stall-free":"No"
                , "Start-Cycle":"37"
                , "Latency":"4"
              }
            }
            , {
              "type":"inst"
              , "id":248
              , "name":"end"
              , "file":"1"
              , "line":"673"
              , "details":
              {
                "Start-Cycle":"56"
                , "Latency":"1"
                , "Additional Info":"Exit from a basic block. Control flow branches at this node to one or more merge nodes. There is no control branching between merge and branch node for the same basic block."
              }
            }
            , {
              "type":"inst"
              , "id":249
              , "name":"begin"
              , "file":""
              , "line":""
              , "details":
              {
                "Start-Cycle":"0"
                , "Latency":"1"
                , "Additional Info":"Entrance to a basic block. Control flow comes to this node from one or more branch nodes, unless it's the very first merge node in a kernel. There is no control branching between merge and branch node within the same basic block."
              }
            }
          ]
          , "details":
          {
            "Latency":"57"
          }
        }
        , {
          "type":"memtype"
          , "id":252
          , "name":"Local Memory"
          , "file":""
          , "line":"0"
          , "children":[
            {
              "type":"memsys"
              , "id":253
              , "name":"buffer"
              , "file":""
              , "line":"0"
              , "replFactor":"3"
              , "banks":1
              , "pumping":1
              , "children":[
                {
                  "type":"bank"
                  , "id":254
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
          ]
        }
      ]
    }
    , {
      "type":"kernel"
      , "id":280
      , "name":"lrn"
      , "file":""
      , "line":"0"
      , "children":[
        {
          "type":"bb"
          , "id":256
          , "name":"Block24.wii_blk"
          , "file":""
          , "line":"0"
          , "details":
          {
            "Latency":"4"
          }
        }
        , {
          "type":"bb"
          , "id":257
          , "name":"Block25"
          , "file":""
          , "line":"0"
          , "children":[
            {
              "type":"inst"
              , "id":260
              , "name":"Load"
              , "file":"1"
              , "line":"721"
              , "details":
              {
                "Width":"32 bits"
                , "Type":"Burst-coalesced"
                , "Stall-free":"No"
                , "Start-Cycle":"8"
                , "Latency":"160"
              }
            }
            , {
              "type":"inst"
              , "id":261
              , "name":"Store"
              , "file":"1"
              , "line":"721"
              , "details":
              {
                "Width":"16 bits"
                , "Type":"Pipelined"
                , "Stall-free":"No"
                , "Start-Cycle":"168"
                , "Latency":"31"
              }
            }
            , {
              "type":"inst"
              , "id":262
              , "name":"Store"
              , "file":"1"
              , "line":"721"
              , "details":
              {
                "Width":"16 bits"
                , "Type":"Pipelined"
                , "Stall-free":"No"
                , "Start-Cycle":"168"
                , "Latency":"31"
              }
            }
            , {
              "type":"inst"
              , "id":263
              , "name":"Store"
              , "file":"1"
              , "line":"728"
              , "details":
              {
                "Width":"16 bits"
                , "Type":"Pipelined"
                , "Stall-free":"No"
                , "Start-Cycle":"199"
                , "Latency":"31"
              }
            }
            , {
              "type":"inst"
              , "id":264
              , "name":"Store"
              , "file":"1"
              , "line":"736"
              , "details":
              {
                "Width":"16 bits"
                , "Type":"Pipelined"
                , "Stall-free":"No"
                , "Start-Cycle":"199"
                , "Latency":"31"
              }
            }
            , {
              "type":"inst"
              , "id":265
              , "name":"end"
              , "file":"1"
              , "line":"747"
              , "details":
              {
                "Start-Cycle":"297"
                , "Latency":"1"
                , "Additional Info":"Exit from a basic block. Control flow branches at this node to one or more merge nodes. There is no control branching between merge and branch node for the same basic block."
              }
            }
            , {
              "type":"inst"
              , "id":266
              , "name":"begin"
              , "file":""
              , "line":""
              , "details":
              {
                "Start-Cycle":"0"
                , "Latency":"1"
                , "Additional Info":"Entrance to a basic block. Control flow comes to this node from one or more branch nodes, unless it's the very first merge node in a kernel. There is no control branching between merge and branch node within the same basic block."
              }
            }
          ]
          , "details":
          {
            "Latency":"298"
          }
        }
        , {
          "type":"bb"
          , "id":258
          , "name":"Block26"
          , "file":""
          , "line":"0"
          , "II":1
          , "LoopInfo":"Loop is not pipelined. See Optimization Report for more information."
          , "hasFmaxBottlenecks":"No"
          , "hasSubloops":"No"
          , "isPipelined":"No"
          , "children":[
            {
              "type":"inst"
              , "id":267
              , "name":"Load"
              , "file":"1"
              , "line":"753"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"No"
                , "Start-Cycle":"8"
                , "Latency":"31"
              }
            }
            , {
              "type":"inst"
              , "id":268
              , "name":"Load"
              , "file":"1"
              , "line":"753"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"No"
                , "Start-Cycle":"8"
                , "Latency":"31"
              }
            }
            , {
              "type":"inst"
              , "id":269
              , "name":"Load"
              , "file":"1"
              , "line":"753"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"No"
                , "Start-Cycle":"8"
                , "Latency":"31"
              }
            }
            , {
              "type":"inst"
              , "id":270
              , "name":"Load"
              , "file":"1"
              , "line":"753"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"No"
                , "Start-Cycle":"8"
                , "Latency":"31"
              }
            }
            , {
              "type":"inst"
              , "id":271
              , "name":"Load"
              , "file":"1"
              , "line":"753"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"42"
                , "Latency":"5"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":272
              , "name":"Store"
              , "file":"1"
              , "line":"779"
              , "details":
              {
                "Width":"8 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"119"
                , "Latency":"2"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":273
              , "name":"loop end"
              , "file":"1"
              , "line":"747"
              , "details":
              {
                "Start-Cycle":"193"
                , "Latency":"1"
                , "Additional Info":"Exit from a basic block. Control flow branches at this node to one or more merge nodes. There is no control branching between merge and branch node for the same basic block."
              }
            }
            , {
              "type":"inst"
              , "id":274
              , "name":"loop"
              , "file":""
              , "line":""
              , "loopTo":273
              , "details":
              {
                "Start-Cycle":"0"
                , "Latency":"1"
                , "Additional Info":"Entrance to a basic block. Control flow comes to this node from one or more branch nodes, unless it's the very first merge node in a kernel. There is no control branching between merge and branch node within the same basic block."
              }
            }
          ]
          , "details":
          {
            "Latency":"194"
          }
        }
        , {
          "type":"bb"
          , "id":259
          , "name":"Block27"
          , "file":""
          , "line":"0"
          , "children":[
            {
              "type":"inst"
              , "id":275
              , "name":"Load"
              , "file":"1"
              , "line":"791"
              , "details":
              {
                "Width":"32 bits"
                , "Type":"Pipelined"
                , "Stall-free":"Yes"
                , "Start-Cycle":"2"
                , "Latency":"4"
                , "Additional Info":" Part of a stall-free cluster."
              }
            }
            , {
              "type":"inst"
              , "id":276
              , "name":"Store"
              , "file":"1"
              , "line":"793"
              , "details":
              {
                "Width":"32 bits"
                , "Type":"Burst-coalesced"
                , "Stall-free":"No"
                , "Start-Cycle":"12"
                , "Latency":"4"
              }
            }
            , {
              "type":"inst"
              , "id":277
              , "name":"end"
              , "file":"1"
              , "line":"800"
              , "details":
              {
                "Start-Cycle":"16"
                , "Latency":"1"
                , "Additional Info":"Exit from a basic block. Control flow branches at this node to one or more merge nodes. There is no control branching between merge and branch node for the same basic block."
              }
            }
            , {
              "type":"inst"
              , "id":278
              , "name":"begin"
              , "file":""
              , "line":""
              , "details":
              {
                "Start-Cycle":"0"
                , "Latency":"1"
                , "Additional Info":"Entrance to a basic block. Control flow comes to this node from one or more branch nodes, unless it's the very first merge node in a kernel. There is no control branching between merge and branch node within the same basic block."
              }
            }
          ]
          , "details":
          {
            "Latency":"17"
          }
        }
        , {
          "type":"memtype"
          , "id":281
          , "name":"Local Memory"
          , "file":""
          , "line":"0"
          , "children":[
            {
              "type":"memsys"
              , "id":282
              , "name":"z_buffer"
              , "file":""
              , "line":"0"
              , "replFactor":"3"
              , "banks":4
              , "pumping":2
              , "children":[
                {
                  "type":"bank"
                  , "id":283
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
                , {
                  "type":"bank"
                  , "id":284
                  , "name":"Bank 1"
                  , "file":""
                  , "line":"0"
                }
                , {
                  "type":"bank"
                  , "id":285
                  , "name":"Bank 2"
                  , "file":""
                  , "line":"0"
                }
                , {
                  "type":"bank"
                  , "id":286
                  , "name":"Bank 3"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
            , {
              "type":"memsys"
              , "id":287
              , "name":"lrn_buffer"
              , "file":""
              , "line":"0"
              , "replFactor":"3"
              , "banks":1
              , "pumping":1
              , "children":[
                {
                  "type":"bank"
                  , "id":288
                  , "name":"Bank 0"
                  , "file":""
                  , "line":"0"
                }
              ]
            }
          ]
        }
      ]
    }
    , {
      "type":"memtype"
      , "id":41
      , "name":"Global Memory"
      , "file":""
      , "line":"0"
      , "children":[
        {
          "type":"memsys"
          , "id":42
          , "name":""
          , "file":""
          , "line":"0"
          , "replFactor":"0"
          , "banks":1
          , "pumping":0
          , "children":[
            {
              "type":"bank"
              , "id":43
              , "name":"Bank 0"
              , "file":""
              , "line":"0"
            }
          ]
        }
      ]
    }
    , {
      "type":"channel"
      , "id":57
      , "name":"EFI_CHANNEL_0_mult_add_fix8bx4_0"
      , "file":""
      , "line":"0"
    }
    , {
      "type":"channel"
      , "id":31
      , "name":"bias_ch"
      , "file":""
      , "line":"0"
      , "details":
      {
        "Width":"128 bits"
        , "Depth":"10"
      }
    }
    , {
      "type":"channel"
      , "id":94
      , "name":"bypass_ch"
      , "file":""
      , "line":"0"
      , "details":
      {
        "Width":"128 bits"
        , "Depth":"15"
      }
    }
    , {
      "type":"channel"
      , "id":92
      , "name":"conv_ch"
      , "file":""
      , "line":"0"
      , "details":
      {
        "Width":"128 bits"
        , "Depth":"0"
      }
    }
    , {
      "type":"channel"
      , "id":27
      , "name":"data_ch"
      , "file":""
      , "line":"0"
      , "details":
      {
        "Width":"512 bits"
        , "Depth":"0"
      }
    }
    , {
      "type":"channel"
      , "id":170
      , "name":"pool_ch"
      , "file":""
      , "line":"0"
      , "details":
      {
        "Width":"128 bits"
        , "Depth":"0"
      }
    }
    , {
      "type":"channel"
      , "id":29
      , "name":"weight_ch"
      , "file":""
      , "line":"0"
      , "details":
      {
        "Width":"512 bits"
        , "Depth":"0"
      }
    }
  ]
  ,
  "links":
  [
    {
      "from":26
      , "to":27
    }
    ,
    {
      "from":28
      , "to":29
    }
    ,
    {
      "from":30
      , "to":31
    }
    ,
    {
      "from":38
      , "to":24
    }
    ,
    {
      "from":16
      , "to":38
    }
    ,
    {
      "from":22
      , "to":38
    }
    ,
    {
      "from":40
      , "to":25
    }
    ,
    {
      "from":23
      , "to":40
    }
    ,
    {
      "from":33
      , "to":19
    }
    ,
    {
      "from":15
      , "to":16
    }
    ,
    {
      "from":19
      , "to":28
    }
    ,
    {
      "from":20
      , "to":28
    }
    ,
    {
      "from":24
      , "to":28
    }
    ,
    {
      "from":25
      , "to":28
    }
    ,
    {
      "from":22
      , "to":28
    }
    ,
    {
      "from":23
      , "to":28
    }
    ,
    {
      "from":19
      , "to":22
    }
    ,
    {
      "from":20
      , "to":22
    }
    ,
    {
      "from":19
      , "to":26
    }
    ,
    {
      "from":20
      , "to":26
    }
    ,
    {
      "from":24
      , "to":26
    }
    ,
    {
      "from":25
      , "to":26
    }
    ,
    {
      "from":22
      , "to":26
    }
    ,
    {
      "from":23
      , "to":26
    }
    ,
    {
      "from":19
      , "to":25
    }
    ,
    {
      "from":20
      , "to":25
    }
    ,
    {
      "from":19
      , "to":23
    }
    ,
    {
      "from":20
      , "to":23
    }
    ,
    {
      "from":18
      , "to":15
    }
    ,
    {
      "from":21
      , "to":30
    }
    ,
    {
      "from":33
      , "to":21
    }
    ,
    {
      "from":33
      , "to":20
    }
    ,
    {
      "from":19
      , "to":24
    }
    ,
    {
      "from":20
      , "to":24
    }
    ,
    {
      "from":16
      , "to":17
    }
    ,
    {
      "from":17
      , "to":8
    }
    ,
    {
      "from":28
      , "to":32
    }
    ,
    {
      "from":26
      , "to":32
    }
    ,
    {
      "from":19
      , "to":32
    }
    ,
    {
      "from":20
      , "to":32
    }
    ,
    {
      "from":24
      , "to":32
    }
    ,
    {
      "from":25
      , "to":32
    }
    ,
    {
      "from":22
      , "to":32
    }
    ,
    {
      "from":23
      , "to":32
    }
    ,
    {
      "from":30
      , "to":32
    }
    ,
    {
      "from":9
      , "to":10
    }
    ,
    {
      "from":32
      , "to":12
    }
    ,
    {
      "from":8
      , "to":9
    }
    ,
    {
      "from":3
      , "to":4
    }
    ,
    {
      "from":12
      , "to":14
    }
    ,
    {
      "from":32
      , "to":33
    }
    ,
    {
      "from":11
      , "to":33
    }
    ,
    {
      "from":9
      , "to":5
    }
    ,
    {
      "from":4
      , "to":5
    }
    ,
    {
      "from":17
      , "to":18
    }
    ,
    {
      "from":6
      , "to":18
    }
    ,
    {
      "from":12
      , "to":11
    }
    ,
    {
      "from":10
      , "to":11
    }
    ,
    {
      "from":8
      , "to":6
    }
    ,
    {
      "from":5
      , "to":6
    }
    ,
    {
      "from":43
      , "to":19
    }
    ,
    {
      "from":43
      , "to":15
    }
    ,
    {
      "from":43
      , "to":21
    }
    ,
    {
      "from":43
      , "to":20
    }
    ,
    {
      "from":31
      , "to":51
    }
    ,
    {
      "from":27
      , "to":54
    }
    ,
    {
      "from":29
      , "to":55
    }
    ,
    {
      "from":56
      , "to":57
    }
    ,
    {
      "from":57
      , "to":58
    }
    ,
    {
      "from":59
      , "to":57
    }
    ,
    {
      "from":57
      , "to":60
    }
    ,
    {
      "from":61
      , "to":57
    }
    ,
    {
      "from":57
      , "to":62
    }
    ,
    {
      "from":63
      , "to":57
    }
    ,
    {
      "from":57
      , "to":64
    }
    ,
    {
      "from":65
      , "to":57
    }
    ,
    {
      "from":57
      , "to":66
    }
    ,
    {
      "from":67
      , "to":57
    }
    ,
    {
      "from":57
      , "to":68
    }
    ,
    {
      "from":69
      , "to":57
    }
    ,
    {
      "from":57
      , "to":70
    }
    ,
    {
      "from":71
      , "to":57
    }
    ,
    {
      "from":57
      , "to":72
    }
    ,
    {
      "from":73
      , "to":57
    }
    ,
    {
      "from":57
      , "to":74
    }
    ,
    {
      "from":75
      , "to":57
    }
    ,
    {
      "from":57
      , "to":76
    }
    ,
    {
      "from":77
      , "to":57
    }
    ,
    {
      "from":57
      , "to":78
    }
    ,
    {
      "from":79
      , "to":57
    }
    ,
    {
      "from":57
      , "to":80
    }
    ,
    {
      "from":81
      , "to":57
    }
    ,
    {
      "from":57
      , "to":82
    }
    ,
    {
      "from":83
      , "to":57
    }
    ,
    {
      "from":57
      , "to":84
    }
    ,
    {
      "from":85
      , "to":57
    }
    ,
    {
      "from":57
      , "to":86
    }
    ,
    {
      "from":87
      , "to":57
    }
    ,
    {
      "from":57
      , "to":88
    }
    ,
    {
      "from":91
      , "to":92
    }
    ,
    {
      "from":93
      , "to":94
    }
    ,
    {
      "from":90
      , "to":54
    }
    ,
    {
      "from":54
      , "to":70
    }
    ,
    {
      "from":55
      , "to":70
    }
    ,
    {
      "from":69
      , "to":70
    }
    ,
    {
      "from":53
      , "to":51
    }
    ,
    {
      "from":54
      , "to":88
    }
    ,
    {
      "from":55
      , "to":88
    }
    ,
    {
      "from":87
      , "to":88
    }
    ,
    {
      "from":54
      , "to":56
    }
    ,
    {
      "from":55
      , "to":56
    }
    ,
    {
      "from":54
      , "to":68
    }
    ,
    {
      "from":55
      , "to":68
    }
    ,
    {
      "from":67
      , "to":68
    }
    ,
    {
      "from":54
      , "to":72
    }
    ,
    {
      "from":55
      , "to":72
    }
    ,
    {
      "from":71
      , "to":72
    }
    ,
    {
      "from":96
      , "to":93
    }
    ,
    {
      "from":54
      , "to":73
    }
    ,
    {
      "from":55
      , "to":73
    }
    ,
    {
      "from":54
      , "to":69
    }
    ,
    {
      "from":55
      , "to":69
    }
    ,
    {
      "from":54
      , "to":74
    }
    ,
    {
      "from":55
      , "to":74
    }
    ,
    {
      "from":73
      , "to":74
    }
    ,
    {
      "from":54
      , "to":66
    }
    ,
    {
      "from":55
      , "to":66
    }
    ,
    {
      "from":65
      , "to":66
    }
    ,
    {
      "from":54
      , "to":86
    }
    ,
    {
      "from":55
      , "to":86
    }
    ,
    {
      "from":85
      , "to":86
    }
    ,
    {
      "from":54
      , "to":87
    }
    ,
    {
      "from":55
      , "to":87
    }
    ,
    {
      "from":54
      , "to":81
    }
    ,
    {
      "from":55
      , "to":81
    }
    ,
    {
      "from":54
      , "to":61
    }
    ,
    {
      "from":55
      , "to":61
    }
    ,
    {
      "from":54
      , "to":75
    }
    ,
    {
      "from":55
      , "to":75
    }
    ,
    {
      "from":54
      , "to":60
    }
    ,
    {
      "from":55
      , "to":60
    }
    ,
    {
      "from":59
      , "to":60
    }
    ,
    {
      "from":54
      , "to":80
    }
    ,
    {
      "from":55
      , "to":80
    }
    ,
    {
      "from":79
      , "to":80
    }
    ,
    {
      "from":54
      , "to":78
    }
    ,
    {
      "from":55
      , "to":78
    }
    ,
    {
      "from":77
      , "to":78
    }
    ,
    {
      "from":96
      , "to":91
    }
    ,
    {
      "from":54
      , "to":58
    }
    ,
    {
      "from":55
      , "to":58
    }
    ,
    {
      "from":56
      , "to":58
    }
    ,
    {
      "from":54
      , "to":62
    }
    ,
    {
      "from":55
      , "to":62
    }
    ,
    {
      "from":61
      , "to":62
    }
    ,
    {
      "from":54
      , "to":67
    }
    ,
    {
      "from":55
      , "to":67
    }
    ,
    {
      "from":54
      , "to":63
    }
    ,
    {
      "from":55
      , "to":63
    }
    ,
    {
      "from":54
      , "to":83
    }
    ,
    {
      "from":55
      , "to":83
    }
    ,
    {
      "from":54
      , "to":82
    }
    ,
    {
      "from":55
      , "to":82
    }
    ,
    {
      "from":81
      , "to":82
    }
    ,
    {
      "from":54
      , "to":84
    }
    ,
    {
      "from":55
      , "to":84
    }
    ,
    {
      "from":83
      , "to":84
    }
    ,
    {
      "from":54
      , "to":79
    }
    ,
    {
      "from":55
      , "to":79
    }
    ,
    {
      "from":90
      , "to":55
    }
    ,
    {
      "from":54
      , "to":76
    }
    ,
    {
      "from":55
      , "to":76
    }
    ,
    {
      "from":75
      , "to":76
    }
    ,
    {
      "from":54
      , "to":77
    }
    ,
    {
      "from":55
      , "to":77
    }
    ,
    {
      "from":54
      , "to":59
    }
    ,
    {
      "from":55
      , "to":59
    }
    ,
    {
      "from":54
      , "to":71
    }
    ,
    {
      "from":55
      , "to":71
    }
    ,
    {
      "from":54
      , "to":85
    }
    ,
    {
      "from":55
      , "to":85
    }
    ,
    {
      "from":54
      , "to":65
    }
    ,
    {
      "from":55
      , "to":65
    }
    ,
    {
      "from":54
      , "to":64
    }
    ,
    {
      "from":55
      , "to":64
    }
    ,
    {
      "from":63
      , "to":64
    }
    ,
    {
      "from":95
      , "to":50
    }
    ,
    {
      "from":51
      , "to":52
    }
    ,
    {
      "from":89
      , "to":96
    }
    ,
    {
      "from":45
      , "to":46
    }
    ,
    {
      "from":58
      , "to":89
    }
    ,
    {
      "from":54
      , "to":89
    }
    ,
    {
      "from":55
      , "to":89
    }
    ,
    {
      "from":60
      , "to":89
    }
    ,
    {
      "from":62
      , "to":89
    }
    ,
    {
      "from":64
      , "to":89
    }
    ,
    {
      "from":66
      , "to":89
    }
    ,
    {
      "from":68
      , "to":89
    }
    ,
    {
      "from":70
      , "to":89
    }
    ,
    {
      "from":72
      , "to":89
    }
    ,
    {
      "from":74
      , "to":89
    }
    ,
    {
      "from":76
      , "to":89
    }
    ,
    {
      "from":78
      , "to":89
    }
    ,
    {
      "from":80
      , "to":89
    }
    ,
    {
      "from":82
      , "to":89
    }
    ,
    {
      "from":84
      , "to":89
    }
    ,
    {
      "from":86
      , "to":89
    }
    ,
    {
      "from":88
      , "to":89
    }
    ,
    {
      "from":93
      , "to":95
    }
    ,
    {
      "from":91
      , "to":95
    }
    ,
    {
      "from":89
      , "to":90
    }
    ,
    {
      "from":52
      , "to":90
    }
    ,
    {
      "from":95
      , "to":53
    }
    ,
    {
      "from":46
      , "to":53
    }
    ,
    {
      "from":92
      , "to":104
    }
    ,
    {
      "from":169
      , "to":170
    }
    ,
    {
      "from":177
      , "to":105
    }
    ,
    {
      "from":107
      , "to":177
    }
    ,
    {
      "from":179
      , "to":106
    }
    ,
    {
      "from":108
      , "to":179
    }
    ,
    {
      "from":181
      , "to":110
    }
    ,
    {
      "from":112
      , "to":181
    }
    ,
    {
      "from":183
      , "to":109
    }
    ,
    {
      "from":111
      , "to":183
    }
    ,
    {
      "from":185
      , "to":114
    }
    ,
    {
      "from":116
      , "to":185
    }
    ,
    {
      "from":187
      , "to":113
    }
    ,
    {
      "from":115
      , "to":187
    }
    ,
    {
      "from":189
      , "to":118
    }
    ,
    {
      "from":120
      , "to":189
    }
    ,
    {
      "from":191
      , "to":117
    }
    ,
    {
      "from":119
      , "to":191
    }
    ,
    {
      "from":193
      , "to":122
    }
    ,
    {
      "from":124
      , "to":193
    }
    ,
    {
      "from":195
      , "to":121
    }
    ,
    {
      "from":123
      , "to":195
    }
    ,
    {
      "from":197
      , "to":126
    }
    ,
    {
      "from":128
      , "to":197
    }
    ,
    {
      "from":199
      , "to":125
    }
    ,
    {
      "from":127
      , "to":199
    }
    ,
    {
      "from":201
      , "to":130
    }
    ,
    {
      "from":132
      , "to":201
    }
    ,
    {
      "from":203
      , "to":129
    }
    ,
    {
      "from":131
      , "to":203
    }
    ,
    {
      "from":205
      , "to":134
    }
    ,
    {
      "from":136
      , "to":205
    }
    ,
    {
      "from":207
      , "to":133
    }
    ,
    {
      "from":135
      , "to":207
    }
    ,
    {
      "from":209
      , "to":138
    }
    ,
    {
      "from":140
      , "to":209
    }
    ,
    {
      "from":211
      , "to":137
    }
    ,
    {
      "from":139
      , "to":211
    }
    ,
    {
      "from":213
      , "to":142
    }
    ,
    {
      "from":144
      , "to":213
    }
    ,
    {
      "from":215
      , "to":141
    }
    ,
    {
      "from":143
      , "to":215
    }
    ,
    {
      "from":217
      , "to":146
    }
    ,
    {
      "from":148
      , "to":217
    }
    ,
    {
      "from":219
      , "to":145
    }
    ,
    {
      "from":147
      , "to":219
    }
    ,
    {
      "from":221
      , "to":150
    }
    ,
    {
      "from":152
      , "to":221
    }
    ,
    {
      "from":223
      , "to":149
    }
    ,
    {
      "from":151
      , "to":223
    }
    ,
    {
      "from":225
      , "to":154
    }
    ,
    {
      "from":156
      , "to":225
    }
    ,
    {
      "from":227
      , "to":153
    }
    ,
    {
      "from":155
      , "to":227
    }
    ,
    {
      "from":229
      , "to":158
    }
    ,
    {
      "from":160
      , "to":229
    }
    ,
    {
      "from":231
      , "to":157
    }
    ,
    {
      "from":159
      , "to":231
    }
    ,
    {
      "from":233
      , "to":162
    }
    ,
    {
      "from":164
      , "to":233
    }
    ,
    {
      "from":235
      , "to":161
    }
    ,
    {
      "from":163
      , "to":235
    }
    ,
    {
      "from":237
      , "to":166
    }
    ,
    {
      "from":168
      , "to":237
    }
    ,
    {
      "from":239
      , "to":165
    }
    ,
    {
      "from":167
      , "to":239
    }
    ,
    {
      "from":150
      , "to":151
    }
    ,
    {
      "from":104
      , "to":151
    }
    ,
    {
      "from":104
      , "to":108
    }
    ,
    {
      "from":104
      , "to":144
    }
    ,
    {
      "from":114
      , "to":115
    }
    ,
    {
      "from":104
      , "to":115
    }
    ,
    {
      "from":104
      , "to":165
    }
    ,
    {
      "from":104
      , "to":142
    }
    ,
    {
      "from":158
      , "to":159
    }
    ,
    {
      "from":104
      , "to":159
    }
    ,
    {
      "from":104
      , "to":169
    }
    ,
    {
      "from":105
      , "to":169
    }
    ,
    {
      "from":106
      , "to":169
    }
    ,
    {
      "from":109
      , "to":169
    }
    ,
    {
      "from":110
      , "to":169
    }
    ,
    {
      "from":113
      , "to":169
    }
    ,
    {
      "from":114
      , "to":169
    }
    ,
    {
      "from":117
      , "to":169
    }
    ,
    {
      "from":118
      , "to":169
    }
    ,
    {
      "from":121
      , "to":169
    }
    ,
    {
      "from":122
      , "to":169
    }
    ,
    {
      "from":125
      , "to":169
    }
    ,
    {
      "from":126
      , "to":169
    }
    ,
    {
      "from":129
      , "to":169
    }
    ,
    {
      "from":130
      , "to":169
    }
    ,
    {
      "from":133
      , "to":169
    }
    ,
    {
      "from":134
      , "to":169
    }
    ,
    {
      "from":137
      , "to":169
    }
    ,
    {
      "from":138
      , "to":169
    }
    ,
    {
      "from":141
      , "to":169
    }
    ,
    {
      "from":142
      , "to":169
    }
    ,
    {
      "from":145
      , "to":169
    }
    ,
    {
      "from":146
      , "to":169
    }
    ,
    {
      "from":149
      , "to":169
    }
    ,
    {
      "from":150
      , "to":169
    }
    ,
    {
      "from":153
      , "to":169
    }
    ,
    {
      "from":154
      , "to":169
    }
    ,
    {
      "from":157
      , "to":169
    }
    ,
    {
      "from":158
      , "to":169
    }
    ,
    {
      "from":161
      , "to":169
    }
    ,
    {
      "from":162
      , "to":169
    }
    ,
    {
      "from":165
      , "to":169
    }
    ,
    {
      "from":166
      , "to":169
    }
    ,
    {
      "from":132
      , "to":169
    }
    ,
    {
      "from":136
      , "to":169
    }
    ,
    {
      "from":140
      , "to":169
    }
    ,
    {
      "from":144
      , "to":169
    }
    ,
    {
      "from":148
      , "to":169
    }
    ,
    {
      "from":152
      , "to":169
    }
    ,
    {
      "from":156
      , "to":169
    }
    ,
    {
      "from":160
      , "to":169
    }
    ,
    {
      "from":164
      , "to":169
    }
    ,
    {
      "from":168
      , "to":169
    }
    ,
    {
      "from":108
      , "to":169
    }
    ,
    {
      "from":112
      , "to":169
    }
    ,
    {
      "from":116
      , "to":169
    }
    ,
    {
      "from":120
      , "to":169
    }
    ,
    {
      "from":124
      , "to":169
    }
    ,
    {
      "from":128
      , "to":169
    }
    ,
    {
      "from":135
      , "to":169
    }
    ,
    {
      "from":139
      , "to":169
    }
    ,
    {
      "from":143
      , "to":169
    }
    ,
    {
      "from":147
      , "to":169
    }
    ,
    {
      "from":151
      , "to":169
    }
    ,
    {
      "from":155
      , "to":169
    }
    ,
    {
      "from":159
      , "to":169
    }
    ,
    {
      "from":163
      , "to":169
    }
    ,
    {
      "from":167
      , "to":169
    }
    ,
    {
      "from":107
      , "to":169
    }
    ,
    {
      "from":111
      , "to":169
    }
    ,
    {
      "from":115
      , "to":169
    }
    ,
    {
      "from":119
      , "to":169
    }
    ,
    {
      "from":123
      , "to":169
    }
    ,
    {
      "from":131
      , "to":169
    }
    ,
    {
      "from":127
      , "to":169
    }
    ,
    {
      "from":166
      , "to":167
    }
    ,
    {
      "from":104
      , "to":167
    }
    ,
    {
      "from":104
      , "to":136
    }
    ,
    {
      "from":104
      , "to":121
    }
    ,
    {
      "from":104
      , "to":150
    }
    ,
    {
      "from":104
      , "to":129
    }
    ,
    {
      "from":104
      , "to":149
    }
    ,
    {
      "from":104
      , "to":134
    }
    ,
    {
      "from":104
      , "to":152
    }
    ,
    {
      "from":104
      , "to":157
    }
    ,
    {
      "from":104
      , "to":106
    }
    ,
    {
      "from":104
      , "to":114
    }
    ,
    {
      "from":104
      , "to":118
    }
    ,
    {
      "from":104
      , "to":141
    }
    ,
    {
      "from":104
      , "to":120
    }
    ,
    {
      "from":104
      , "to":137
    }
    ,
    {
      "from":142
      , "to":143
    }
    ,
    {
      "from":104
      , "to":143
    }
    ,
    {
      "from":104
      , "to":164
    }
    ,
    {
      "from":104
      , "to":160
    }
    ,
    {
      "from":104
      , "to":153
    }
    ,
    {
      "from":104
      , "to":130
    }
    ,
    {
      "from":104
      , "to":113
    }
    ,
    {
      "from":130
      , "to":131
    }
    ,
    {
      "from":104
      , "to":131
    }
    ,
    {
      "from":172
      , "to":104
    }
    ,
    {
      "from":104
      , "to":158
    }
    ,
    {
      "from":104
      , "to":148
    }
    ,
    {
      "from":104
      , "to":117
    }
    ,
    {
      "from":110
      , "to":111
    }
    ,
    {
      "from":104
      , "to":111
    }
    ,
    {
      "from":104
      , "to":161
    }
    ,
    {
      "from":104
      , "to":125
    }
    ,
    {
      "from":118
      , "to":119
    }
    ,
    {
      "from":104
      , "to":119
    }
    ,
    {
      "from":104
      , "to":140
    }
    ,
    {
      "from":104
      , "to":146
    }
    ,
    {
      "from":162
      , "to":163
    }
    ,
    {
      "from":104
      , "to":163
    }
    ,
    {
      "from":126
      , "to":127
    }
    ,
    {
      "from":104
      , "to":127
    }
    ,
    {
      "from":104
      , "to":154
    }
    ,
    {
      "from":104
      , "to":132
    }
    ,
    {
      "from":146
      , "to":147
    }
    ,
    {
      "from":104
      , "to":147
    }
    ,
    {
      "from":154
      , "to":155
    }
    ,
    {
      "from":104
      , "to":155
    }
    ,
    {
      "from":104
      , "to":124
    }
    ,
    {
      "from":104
      , "to":109
    }
    ,
    {
      "from":104
      , "to":105
    }
    ,
    {
      "from":104
      , "to":162
    }
    ,
    {
      "from":138
      , "to":139
    }
    ,
    {
      "from":104
      , "to":139
    }
    ,
    {
      "from":104
      , "to":168
    }
    ,
    {
      "from":104
      , "to":166
    }
    ,
    {
      "from":134
      , "to":135
    }
    ,
    {
      "from":104
      , "to":135
    }
    ,
    {
      "from":104
      , "to":116
    }
    ,
    {
      "from":104
      , "to":122
    }
    ,
    {
      "from":104
      , "to":145
    }
    ,
    {
      "from":104
      , "to":112
    }
    ,
    {
      "from":106
      , "to":107
    }
    ,
    {
      "from":104
      , "to":107
    }
    ,
    {
      "from":104
      , "to":126
    }
    ,
    {
      "from":104
      , "to":156
    }
    ,
    {
      "from":122
      , "to":123
    }
    ,
    {
      "from":104
      , "to":123
    }
    ,
    {
      "from":104
      , "to":110
    }
    ,
    {
      "from":104
      , "to":128
    }
    ,
    {
      "from":104
      , "to":133
    }
    ,
    {
      "from":104
      , "to":138
    }
    ,
    {
      "from":171
      , "to":172
    }
    ,
    {
      "from":101
      , "to":172
    }
    ,
    {
      "from":169
      , "to":171
    }
    ,
    {
      "from":171
      , "to":103
    }
    ,
    {
      "from":100
      , "to":101
    }
    ,
    {
      "from":170
      , "to":243
    }
    ,
    {
      "from":94
      , "to":244
    }
    ,
    {
      "from":254
      , "to":246
    }
    ,
    {
      "from":245
      , "to":254
    }
    ,
    {
      "from":249
      , "to":244
    }
    ,
    {
      "from":245
      , "to":246
    }
    ,
    {
      "from":249
      , "to":243
    }
    ,
    {
      "from":244
      , "to":245
    }
    ,
    {
      "from":243
      , "to":245
    }
    ,
    {
      "from":245
      , "to":247
    }
    ,
    {
      "from":246
      , "to":247
    }
    ,
    {
      "from":241
      , "to":249
    }
    ,
    {
      "from":245
      , "to":248
    }
    ,
    {
      "from":246
      , "to":248
    }
    ,
    {
      "from":247
      , "to":248
    }
    ,
    {
      "from":247
      , "to":43
    }
    ,
    {
      "from":283
      , "to":267
    }
    ,
    {
      "from":284
      , "to":267
    }
    ,
    {
      "from":285
      , "to":267
    }
    ,
    {
      "from":286
      , "to":267
    }
    ,
    {
      "from":283
      , "to":268
    }
    ,
    {
      "from":284
      , "to":268
    }
    ,
    {
      "from":285
      , "to":268
    }
    ,
    {
      "from":286
      , "to":268
    }
    ,
    {
      "from":283
      , "to":269
    }
    ,
    {
      "from":284
      , "to":269
    }
    ,
    {
      "from":285
      , "to":269
    }
    ,
    {
      "from":286
      , "to":269
    }
    ,
    {
      "from":283
      , "to":270
    }
    ,
    {
      "from":284
      , "to":270
    }
    ,
    {
      "from":285
      , "to":270
    }
    ,
    {
      "from":286
      , "to":270
    }
    ,
    {
      "from":283
      , "to":271
    }
    ,
    {
      "from":284
      , "to":271
    }
    ,
    {
      "from":285
      , "to":271
    }
    ,
    {
      "from":286
      , "to":271
    }
    ,
    {
      "from":261
      , "to":284
    }
    ,
    {
      "from":261
      , "to":286
    }
    ,
    {
      "from":262
      , "to":283
    }
    ,
    {
      "from":262
      , "to":285
    }
    ,
    {
      "from":263
      , "to":283
    }
    ,
    {
      "from":264
      , "to":284
    }
    ,
    {
      "from":264
      , "to":286
    }
    ,
    {
      "from":288
      , "to":275
    }
    ,
    {
      "from":272
      , "to":288
    }
    ,
    {
      "from":275
      , "to":276
    }
    ,
    {
      "from":261
      , "to":264
    }
    ,
    {
      "from":268
      , "to":271
    }
    ,
    {
      "from":267
      , "to":271
    }
    ,
    {
      "from":269
      , "to":271
    }
    ,
    {
      "from":270
      , "to":271
    }
    ,
    {
      "from":274
      , "to":268
    }
    ,
    {
      "from":260
      , "to":262
    }
    ,
    {
      "from":266
      , "to":260
    }
    ,
    {
      "from":274
      , "to":267
    }
    ,
    {
      "from":260
      , "to":261
    }
    ,
    {
      "from":274
      , "to":269
    }
    ,
    {
      "from":262
      , "to":263
    }
    ,
    {
      "from":274
      , "to":270
    }
    ,
    {
      "from":278
      , "to":275
    }
    ,
    {
      "from":268
      , "to":272
    }
    ,
    {
      "from":267
      , "to":272
    }
    ,
    {
      "from":269
      , "to":272
    }
    ,
    {
      "from":270
      , "to":272
    }
    ,
    {
      "from":271
      , "to":272
    }
    ,
    {
      "from":256
      , "to":266
    }
    ,
    {
      "from":273
      , "to":278
    }
    ,
    {
      "from":268
      , "to":273
    }
    ,
    {
      "from":267
      , "to":273
    }
    ,
    {
      "from":269
      , "to":273
    }
    ,
    {
      "from":270
      , "to":273
    }
    ,
    {
      "from":272
      , "to":273
    }
    ,
    {
      "from":276
      , "to":277
    }
    ,
    {
      "from":264
      , "to":265
    }
    ,
    {
      "from":263
      , "to":265
    }
    ,
    {
      "from":273
      , "to":274
    }
    ,
    {
      "from":265
      , "to":274
    }
    ,
    {
      "from":276
      , "to":43
    }
    ,
    {
      "from":43
      , "to":260
    }
  ]
  , "fileIndexMap":
  {
    "/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl":"1"
  }
}
;var areaJSON={
  "columns":["ALUTs", "FFs", "RAMs", "DSPs"]
  , "debug_enabled":1
  , "total_percent":
  [90.0789, 45.7343, 46.8824, 121.012, 32.1429]
  , "total":
  [50112, 102740, 622, 36]
  , "name":"Kernel System"
  , "max_resources":
  [109572, 219144, 514, 112]
  , "partitions":
  [
  ]
  , "resources":
  [
    {
      "name":"Board interface"
      , "data":
      [2160, 1908, 20, 0]
      , "details":
      [
        "Platform interface logic."
      ]
    }
    , {
      "name":"Global interconnect"
      , "data":
      [9588, 10682, 0, 0]
      , "details":
      [
        "Global interconnect for 5 global loads and 2 global stores. Reduce number of global loads and stores to simplify global interconnect."
      ]
    }
    , {
      "name":"conv_pipe.cl:69 (data_ch)"
      , "data":
      [512, 512, 0, 0]
      , "debug":
      [
        [
          {
            "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
            , "line":69
          }
        ]
      ]
      , "details":
      [
        "Channel is implemented 512 bits wide by 0 deep."
      ]
    }
    , {
      "name":"conv_pipe.cl:70 (weight_ch)"
      , "data":
      [512, 512, 0, 0]
      , "debug":
      [
        [
          {
            "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
            , "line":70
          }
        ]
      ]
      , "details":
      [
        "Channel is implemented 512 bits wide by 0 deep."
      ]
    }
    , {
      "name":"conv_pipe.cl:71 (bias_ch)"
      , "data":
      [55, 557, 4, 0]
      , "debug":
      [
        [
          {
            "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
            , "line":71
          }
        ]
      ]
      , "details":
      [
        "Channel is implemented 128 bits wide by 32 deep. Requested depth was 8.\nChannel depth was changed for the following reasons:\n- instruction scheduling requirements\n- nature of underlying FIFO implementation"
      ]
    }
    , {
      "name":"conv_pipe.cl:72 (conv_ch)"
      , "data":
      [128, 128, 0, 0]
      , "debug":
      [
        [
          {
            "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
            , "line":72
          }
        ]
      ]
      , "details":
      [
        "Channel is implemented 128 bits wide by 0 deep."
      ]
    }
    , {
      "name":"conv_pipe.cl:73 (pool_ch)"
      , "data":
      [128, 128, 0, 0]
      , "debug":
      [
        [
          {
            "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
            , "line":73
          }
        ]
      ]
      , "details":
      [
        "Channel is implemented 128 bits wide by 0 deep."
      ]
    }
    , {
      "name":"conv_pipe.cl:74 (bypass_ch)"
      , "data":
      [55, 557, 4, 0]
      , "debug":
      [
        [
          {
            "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
            , "line":74
          }
        ]
      ]
      , "details":
      [
        "Channel is implemented 128 bits wide by 32 deep. Requested depth was 0.\nChannel depth was changed for the following reasons:\n- instruction scheduling requirements\n- nature of underlying FIFO implementation"
      ]
    }
  ]
  , "functions":
  [
    {
      "name":"coreConv"
      , "compute_units":1
      , "details":
      [
        "Max global work dimension: 0"
        , "Number of compute units: 1"
      ]
      , "resources":
      [
        {
          "name":"Coalesced Private Variables: \n - 'lane_accum' (conv_pipe.cl:396)\n - 'accum_piped' (conv_pipe.cl:397)"
          , "data":
          [104, 3991, 0, 0]
          , "debug":
          [
            [
              {
                "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                , "line":396
              }
            ]
            , [
              {
                "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                , "line":397
              }
            ]
          ]
          , "details":
          [
            "Implemented using registers of the following size:\n- 61 registers of width 32 and depth 1"
          ]
        }
        , {
          "name":"Private Variable: \n - 'accum_piped' (conv_pipe.cl:397)"
          , "data":
          [480, 2958, 0, 0]
          , "debug":
          [
            [
              {
                "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                , "line":397
              }
            ]
          ]
          , "details":
          [
            "Implemented using registers of the following size:\n- 35 registers of width 32 and depth 1"
          ]
        }
      ]
      , "basicblocks":
      [
        {
          "name":"Block12.wii_blk"
          , "resources":
          [
            {
              "name":"State"
              , "data":
              [97, 97, 0, 0]
              , "details":
              [
                "Resources for live values and control logic. To reduce this area:\n- reduce size of local variables\n- reduce scope of local variables, localizing them as much as possible\n- reduce number of nested loops"
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"No Source Line"
                    , "data":
                    [0.75, 0.75, 0, 0]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:420"
                    , "data":
                    [0.25, 0.25, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":420
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:458"
                    , "data":
                    [64, 64, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":458
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:465"
                    , "data":
                    [32, 32, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":465
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
              ]
            }
          ]
          , "computation":
          [
            {
              "name":"conv_pipe.cl:456"
              , "data":
              [1, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":456
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Logical Right Shift"
                    , "data":
                    [1, 0, 0, 0]
                    , "details":
                    [
                      "This instruction does not depend on thread ID. Consider moving it, and all related instructions to the host to save area."
                      , "Work-Item Invariant instruction."
                    ]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:458"
              , "data":
              [32, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":458
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Add"
                    , "data":
                    [16, 0, 0, 0]
                    , "details":
                    [
                      "This instruction does not depend on thread ID. Consider moving it, and all related instructions to the host to save area."
                      , "Work-Item Invariant instruction."
                    ]
                  }
                  , "count":2
                }
                , {
                  "info":
                  {
                    "name":"Sub"
                    , "data":
                    [16, 0, 0, 0]
                    , "details":
                    [
                      "This instruction does not depend on thread ID. Consider moving it, and all related instructions to the host to save area."
                      , "Work-Item Invariant instruction."
                    ]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:465"
              , "data":
              [16, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":465
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Sub"
                    , "data":
                    [16, 0, 0, 0]
                    , "details":
                    [
                      "This instruction does not depend on thread ID. Consider moving it, and all related instructions to the host to save area."
                      , "Work-Item Invariant instruction."
                    ]
                  }
                  , "count":1
                }
              ]
            }
          ]
        }
        , {
          "name":"Block13"
          , "resources":
          [
            {
              "name":"State"
              , "data":
              [0, 169, 0, 0]
              , "details":
              [
                "Resources for live values and control logic. To reduce this area:\n- reduce size of local variables\n- reduce scope of local variables, localizing them as much as possible\n- reduce number of nested loops"
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Control flow logic"
                    , "data":
                    [0, 101, 0, 0]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:404"
                    , "data":
                    [0, 68, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":404
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
              ]
            }
          ]
          , "computation":
          [
          ]
        }
        , {
          "name":"Block14"
          , "resources":
          [
            {
              "name":"State"
              , "data":
              [16, 21, 0, 0]
              , "details":
              [
                "Resources for live values and control logic. To reduce this area:\n- reduce size of local variables\n- reduce scope of local variables, localizing them as much as possible\n- reduce number of nested loops"
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Control flow logic"
                    , "data":
                    [1, 1, 0, 0]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"No Source Line"
                    , "data":
                    [0, 6, 0, 0]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:406"
                    , "data":
                    [15, 14, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":406
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
              ]
            }
            , {
              "name":"Feedback"
              , "data":
              [15, 77, 0, 0]
              , "details":
              [
                "Resources for loop-carried dependencies. To reduce this area:\n- reduce number and size of loop-carried variables"
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"conv_pipe.cl:404"
                    , "data":
                    [15, 77, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":404
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
              ]
            }
            , {
              "name":"Cluster logic"
              , "data":
              [218, 502, 2, 0]
              , "details":
              [
                "Logic required to efficiently support sets of operations that do not stall. This area cannot be affected directly."
              ]
            }
          ]
          , "computation":
          [
          ]
        }
        , {
          "name":"Block15"
          , "resources":
          [
            {
              "name":"State"
              , "data":
              [730, 3360, 5, 0]
              , "details":
              [
                "Resources for live values and control logic. To reduce this area:\n- reduce size of local variables\n- reduce scope of local variables, localizing them as much as possible\n- reduce number of nested loops"
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Control flow logic"
                    , "data":
                    [130, 1891, 0, 0]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"No Source Line"
                    , "data":
                    [275, 690.875, 2, 0]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:416"
                    , "data":
                    [14.6667, 35.0474, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":416
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:420"
                    , "data":
                    [27, 182.5, 2, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":420
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:422"
                    , "data":
                    [17, 13.5, 0.5, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":422
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:423"
                    , "data":
                    [17, 13.5, 0.5, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":423
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:428"
                    , "data":
                    [222.667, 453.664, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":428
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:428 > conv_pipe.cl:84"
                    , "data":
                    [0, 16, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":428
                        }
                        , {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":84
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:432"
                    , "data":
                    [12, 28.8656, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":432
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:435"
                    , "data":
                    [14.6667, 35.0474, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":435
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
              ]
            }
            , {
              "name":"Feedback"
              , "data":
              [15, 411, 0, 0]
              , "details":
              [
                "Resources for loop-carried dependencies. To reduce this area:\n- reduce number and size of loop-carried variables"
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"conv_pipe.cl:404"
                    , "data":
                    [8, 405, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":404
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:420"
                    , "data":
                    [7, 6, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":420
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
              ]
            }
            , {
              "name":"Cluster logic"
              , "data":
              [469, 1030, 5, 0]
              , "details":
              [
                "Logic required to efficiently support sets of operations that do not stall. This area cannot be affected directly."
              ]
            }
          ]
          , "computation":
          [
            {
              "name":"conv_pipe.cl:428"
              , "data":
              [256, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":428
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Add"
                    , "data":
                    [256, 0, 0, 0]
                  }
                  , "count":16
                }
              ]
            }
          ]
        }
        , {
          "name":"Block16"
          , "resources":
          [
            {
              "name":"State"
              , "data":
              [4755, 7774, 3, 0]
              , "details":
              [
                "Resources for live values and control logic. To reduce this area:\n- reduce size of local variables\n- reduce scope of local variables, localizing them as much as possible\n- reduce number of nested loops"
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Control flow logic"
                    , "data":
                    [3264, 4896, 0, 0]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"No Source Line"
                    , "data":
                    [245.333, 490.667, 0, 0]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:404"
                    , "data":
                    [33, 26, 1, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":404
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:453"
                    , "data":
                    [128, 272, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":453
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:458"
                    , "data":
                    [384, 768, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":458
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:460"
                    , "data":
                    [5.33333, 10.6667, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":460
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:462"
                    , "data":
                    [120, 240, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":462
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:465"
                    , "data":
                    [445.333, 890.667, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":465
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:489"
                    , "data":
                    [97, 154, 1, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":489
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:491"
                    , "data":
                    [33, 26, 1, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":491
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
              ]
            }
            , {
              "name":"Cluster logic"
              , "data":
              [218, 502, 2, 0]
              , "details":
              [
                "Logic required to efficiently support sets of operations that do not stall. This area cannot be affected directly."
              ]
            }
          ]
          , "computation":
          [
            {
              "name":"No Source Line"
              , "data":
              [1280, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":""
                    , "line":0
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Add"
                    , "data":
                    [1280, 0, 0, 0]
                  }
                  , "count":95
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:458"
              , "data":
              [144, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":458
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Arithmetic Right Shift"
                    , "data":
                    [16, 0, 0, 0]
                  }
                  , "count":16
                }
                , {
                  "info":
                  {
                    "name":"Or"
                    , "data":
                    [128, 0, 0, 0]
                  }
                  , "count":16
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:465"
              , "data":
              [80, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":465
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Add"
                    , "data":
                    [64, 0, 0, 0]
                  }
                  , "count":16
                }
                , {
                  "info":
                  {
                    "name":"Arithmetic Right Shift"
                    , "data":
                    [16, 0, 0, 0]
                  }
                  , "count":16
                }
              ]
            }
          ]
        }
      ]
    }
    , {
      "name":"lrn"
      , "compute_units":1
      , "details":
      [
        "Number of compute units: 1"
      ]
      , "resources":
      [
        {
          "name":"Function overhead"
          , "data":
          [1608, 1756, 0, 0]
          , "details":
          [
            "Kernel dispatch logic."
          ]
        }
        , {
          "name":"conv_pipe.cl:701 (z_buffer)"
          , "data":
          [68, 512, 4, 0]
          , "debug":
          [
            [
              {
                "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                , "line":701
              }
            ]
          ]
          , "details":
          [
            "Local memory: Potentially inefficient configuration.\nRequested size 512 bytes (rounded up to nearest power of 2), implemented size 1536 bytes, replicated 3 times total, <b>stallable</b>, 5 reads and 4 writes. Additional information:\n- Reduce the number of write accesses or fix banking to make this memory system stall-free.\n- Replicated 3 times to efficiently support multiple simultaneous workgroups. This replication resulted in no increase in actual block RAM usage.\n- Banked on lowest dimension into 4 separate banks (this is a good thing)."
          ]
        }
        , {
          "name":"conv_pipe.cl:702 (lrn_buffer)"
          , "data":
          [0, 0, 1, 0]
          , "debug":
          [
            [
              {
                "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                , "line":702
              }
            ]
          ]
          , "details":
          [
            "Local memory: Optimal.\nRequested size 256 bytes (rounded up to nearest power of 2), implemented size 768 bytes, replicated 3 times total, stall-free, 1 read and 1 write. Additional information:\n- Replicated 3 times to efficiently support multiple simultaneous workgroups. This replication resulted in no increase in actual block RAM usage."
          ]
        }
      ]
      , "basicblocks":
      [
        {
          "name":"Block24.wii_blk"
          , "resources":
          [
            {
              "name":"State"
              , "data":
              [80, 80, 0, 0]
              , "details":
              [
                "Resources for live values and control logic. To reduce this area:\n- reduce size of local variables\n- reduce scope of local variables, localizing them as much as possible\n- reduce number of nested loops"
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Control flow logic"
                    , "data":
                    [80, 80, 0, 0]
                  }
                  , "count":0
                }
              ]
            }
          ]
          , "computation":
          [
          ]
        }
        , {
          "name":"Block25"
          , "resources":
          [
            {
              "name":"State"
              , "data":
              [676, 1360, 11, 0]
              , "details":
              [
                "Resources for live values and control logic. To reduce this area:\n- reduce size of local variables\n- reduce scope of local variables, localizing them as much as possible\n- reduce number of nested loops"
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Control flow logic"
                    , "data":
                    [80, 282, 0, 0]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"No Source Line"
                    , "data":
                    [163.333, 296.583, 3.75, 0]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:721"
                    , "data":
                    [165.667, 385.333, 0.333333, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":721
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:725"
                    , "data":
                    [15, 22.6667, 0.333333, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":725
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:728"
                    , "data":
                    [49.3333, 57.0833, 1.25, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":728
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:733"
                    , "data":
                    [15, 22.6667, 0.333333, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":733
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:736"
                    , "data":
                    [92.3333, 120.083, 2.25, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":736
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:747"
                    , "data":
                    [95.3333, 173.583, 2.75, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":747
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
              ]
            }
          ]
          , "computation":
          [
            {
              "name":"conv_pipe.cl:721"
              , "data":
              [649, 2409, 15, 2]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":721
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Add"
                    , "data":
                    [8, 0, 0, 0]
                  }
                  , "count":3
                }
                , {
                  "info":
                  {
                    "name":"Load"
                    , "data":
                    [453, 1985, 13, 0]
                  }
                  , "count":1
                }
                , {
                  "info":
                  {
                    "name":"Mul"
                    , "data":
                    [0, 56, 0, 2]
                  }
                  , "count":2
                }
                , {
                  "info":
                  {
                    "name":"Pointer Computation"
                    , "data":
                    [32, 0, 0, 0]
                  }
                  , "count":3
                }
                , {
                  "info":
                  {
                    "name":"Store"
                    , "data":
                    [156, 368, 2, 0]
                    , "details":
                    [
                      "Stallable write to memory declared on conv_pipe.cl:701."
                    ]
                  }
                  , "count":2
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:728"
              , "data":
              [78, 174, 1, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":728
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Store"
                    , "data":
                    [78, 174, 1, 0]
                    , "details":
                    [
                      "Stallable write to memory declared on conv_pipe.cl:701."
                    ]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:733"
              , "data":
              [8, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":733
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Integer Compare"
                    , "data":
                    [8, 0, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:736"
              , "data":
              [78, 184, 1, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":736
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Store"
                    , "data":
                    [78, 184, 1, 0]
                    , "details":
                    [
                      "Stallable write to memory declared on conv_pipe.cl:701."
                    ]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:744"
              , "data":
              [110, 73, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":744
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"'__acl_barrier' Function Call"
                    , "data":
                    [110, 73, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
          ]
        }
        , {
          "name":"Block26"
          , "resources":
          [
            {
              "name":"State"
              , "data":
              [2655.5, 6284.84, 14.5, 0]
              , "details":
              [
                "Resources for live values and control logic. To reduce this area:\n- reduce size of local variables\n- reduce scope of local variables, localizing them as much as possible\n- reduce number of nested loops"
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Control flow logic"
                    , "data":
                    [194, 538, 0, 0]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"No Source Line"
                    , "data":
                    [133.2, 245.4, 1, 0]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:721"
                    , "data":
                    [6.5, 114.5, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":721
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:747"
                    , "data":
                    [242, 932.333, 8.5, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":747
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:753"
                    , "data":
                    [193, 599, 3, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":753
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:755"
                    , "data":
                    [897.999, 1983, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":755
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:767"
                    , "data":
                    [16, 32, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":767
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:772"
                    , "data":
                    [719.8, 1535.6, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":772
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:775"
                    , "data":
                    [73, 148, 1, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":775
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:779"
                    , "data":
                    [32, 45, 1, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":779
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:785"
                    , "data":
                    [148, 112, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":785
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
              ]
            }
            , {
              "name":"Cluster logic"
              , "data":
              [390.5, 857.167, 5.5, 0]
              , "details":
              [
                "Logic required to efficiently support sets of operations that do not stall. This area cannot be affected directly."
              ]
            }
          ]
          , "computation":
          [
            {
              "name":"conv_pipe.cl:753"
              , "data":
              [504, 548, 8, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":753
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Add"
                    , "data":
                    [4, 0, 0, 0]
                  }
                  , "count":5
                }
                , {
                  "info":
                  {
                    "name":"Left Shift"
                    , "data":
                    [5, 0, 0, 0]
                  }
                  , "count":5
                }
                , {
                  "info":
                  {
                    "name":"Load"
                    , "data":
                    [495, 548, 8, 0]
                    , "details":
                    [
                      "Stall-free read from memory declared on conv_pipe.cl:701."
                      , "Stallable read from memory declared on conv_pipe.cl:701."
                    ]
                  }
                  , "count":5
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:754"
              , "data":
              [860, 1285, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":754
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Integer to Floating Point Conversion"
                    , "data":
                    [860, 1285, 0, 0]
                  }
                  , "count":5
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:755"
              , "data":
              [734, 240, 0, 5]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":755
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Fadd"
                    , "data":
                    [564, 0, 0, 0]
                  }
                  , "count":4
                }
                , {
                  "info":
                  {
                    "name":"Fmul"
                    , "data":
                    [170, 240, 0, 5]
                  }
                  , "count":5
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:772"
              , "data":
              [350, 96, 8, 2]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":772
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Fadd"
                    , "data":
                    [141, 0, 0, 0]
                  }
                  , "count":1
                }
                , {
                  "info":
                  {
                    "name":"Fmul"
                    , "data":
                    [68, 96, 0, 2]
                  }
                  , "count":2
                }
                , {
                  "info":
                  {
                    "name":"Fsub"
                    , "data":
                    [141, 0, 0, 0]
                  }
                  , "count":1
                }
                , {
                  "info":
                  {
                    "name":"On-chip Read-Only Memory Lookup"
                    , "data":
                    [0, 0, 8, 0]
                    , "details":
                    [
                      "Read from 1472 bit ROM. A copy of the ROM is created for each access."
                    ]
                  }
                  , "count":4
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:774"
              , "data":
              [4, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":774
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Add"
                    , "data":
                    [4, 0, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:775"
              , "data":
              [34, 48, 0, 1]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":775
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Fmul"
                    , "data":
                    [34, 48, 0, 1]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:779"
              , "data":
              [209, 211, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":779
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"'__acl__convert_FPtoSI_8' Function Call"
                    , "data":
                    [175, 187, 0, 0]
                  }
                  , "count":1
                }
                , {
                  "info":
                  {
                    "name":"Store"
                    , "data":
                    [34, 24, 0, 0]
                    , "details":
                    [
                      "Stall-free write to memory declared on conv_pipe.cl:702."
                    ]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:785"
              , "data":
              [110, 73, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":785
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"'__acl_barrier' Function Call"
                    , "data":
                    [110, 73, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
          ]
        }
        , {
          "name":"Block27"
          , "resources":
          [
            {
              "name":"State"
              , "data":
              [353, 762, 2, 0]
              , "details":
              [
                "Resources for live values and control logic. To reduce this area:\n- reduce size of local variables\n- reduce scope of local variables, localizing them as much as possible\n- reduce number of nested loops"
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Control flow logic"
                    , "data":
                    [240, 409, 0, 0]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"No Source Line"
                    , "data":
                    [64, 128, 0, 0]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:793"
                    , "data":
                    [49, 225, 2, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":793
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
              ]
            }
            , {
              "name":"Cluster logic"
              , "data":
              [218, 502, 2, 0]
              , "details":
              [
                "Logic required to efficiently support sets of operations that do not stall. This area cannot be affected directly."
              ]
            }
          ]
          , "computation":
          [
            {
              "name":"conv_pipe.cl:791"
              , "data":
              [9, 8, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":791
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Load"
                    , "data":
                    [9, 8, 0, 0]
                    , "details":
                    [
                      "Stall-free read from memory declared on conv_pipe.cl:702."
                    ]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:793"
              , "data":
              [398, 2128, 16, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":793
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Pointer Computation"
                    , "data":
                    [32, 0, 0, 0]
                  }
                  , "count":1
                }
                , {
                  "info":
                  {
                    "name":"Store"
                    , "data":
                    [366, 2128, 16, 0]
                  }
                  , "count":1
                }
              ]
            }
          ]
        }
      ]
    }
    , {
      "name":"maxPool"
      , "compute_units":1
      , "details":
      [
        "Number of compute units: 1"
      ]
      , "resources":
      [
        {
          "name":"Function overhead"
          , "data":
          [1570, 1685, 0, 0]
          , "details":
          [
            "Kernel dispatch logic."
          ]
        }
        , {
          "name":"Coalesced Private Variables: \n - 'line_buf_ptr' (conv_pipe.cl:515)\n - 'row_cnt' (conv_pipe.cl:518)"
          , "data":
          [8, 37, 0, 0]
          , "debug":
          [
            [
              {
                "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                , "line":515
              }
            ]
            , [
              {
                "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                , "line":518
              }
            ]
          ]
          , "details":
          [
            "Implemented using registers of the following size:\n- 2 registers of width 8 and depth 1"
          ]
        }
        , {
          "name":"Coalesced Private Variables: \n - 'pool_reg' (conv_pipe.cl:521)\n - 'max_value' (conv_pipe.cl:91)"
          , "data":
          [8, 517, 0, 0]
          , "debug":
          [
            [
              {
                "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                , "line":91
              }
            ]
            , [
              {
                "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                , "line":521
              }
            ]
          ]
          , "details":
          [
            "Implemented using registers of the following size:\n- 32 registers of width 8 and depth 1"
          ]
        }
        , {
          "name":"Private Variable: \n - 'row_pool_cnt' (conv_pipe.cl:517)"
          , "data":
          [0.8, 16.5, 0, 0]
          , "debug":
          [
            [
              {
                "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                , "line":517
              }
            ]
          ]
          , "details":
          [
            "Implemented using registers of the following size:\n- 1 register of width 8 and depth 1"
          ]
        }
        , {
          "name":"conv_pipe.cl:513 (line_buf_0)"
          , "data":
          [0, 0, 16, 0]
          , "debug":
          [
            [
              {
                "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                , "line":513
              }
            ]
          ]
          , "details":
          [
            "Private memory was split into multiple parts due to optimizations. See below for details on each part."
          ]
          , "subinfos":
          [
            {
              "info":
              {
                "name":"Part 1 (line_buf_0)"
                , "data":
                [0, 0, 1, 0]
                , "details":
                [
                  "Private memory implemented in on-chip block RAM."
                  , "Private memory: Optimal.\nRequested size 256 bytes (rounded up to nearest power of 2), implemented size 256 bytes, stall-free, 1 read and 1 write. Additional information:\n- No additional details."
                ]
              }
              , "count":0
            }
            , {
              "info":
              {
                "name":"Part 10 (line_buf_0)"
                , "data":
                [0, 0, 1, 0]
                , "details":
                [
                  "Private memory implemented in on-chip block RAM."
                  , "Private memory: Optimal.\nRequested size 256 bytes (rounded up to nearest power of 2), implemented size 256 bytes, stall-free, 1 read and 1 write. Additional information:\n- No additional details."
                ]
              }
              , "count":0
            }
            , {
              "info":
              {
                "name":"Part 11 (line_buf_0)"
                , "data":
                [0, 0, 1, 0]
                , "details":
                [
                  "Private memory implemented in on-chip block RAM."
                  , "Private memory: Optimal.\nRequested size 256 bytes (rounded up to nearest power of 2), implemented size 256 bytes, stall-free, 1 read and 1 write. Additional information:\n- No additional details."
                ]
              }
              , "count":0
            }
            , {
              "info":
              {
                "name":"Part 12 (line_buf_0)"
                , "data":
                [0, 0, 1, 0]
                , "details":
                [
                  "Private memory implemented in on-chip block RAM."
                  , "Private memory: Optimal.\nRequested size 256 bytes (rounded up to nearest power of 2), implemented size 256 bytes, stall-free, 1 read and 1 write. Additional information:\n- No additional details."
                ]
              }
              , "count":0
            }
            , {
              "info":
              {
                "name":"Part 13 (line_buf_0)"
                , "data":
                [0, 0, 1, 0]
                , "details":
                [
                  "Private memory implemented in on-chip block RAM."
                  , "Private memory: Optimal.\nRequested size 256 bytes (rounded up to nearest power of 2), implemented size 256 bytes, stall-free, 1 read and 1 write. Additional information:\n- No additional details."
                ]
              }
              , "count":0
            }
            , {
              "info":
              {
                "name":"Part 14 (line_buf_0)"
                , "data":
                [0, 0, 1, 0]
                , "details":
                [
                  "Private memory implemented in on-chip block RAM."
                  , "Private memory: Optimal.\nRequested size 256 bytes (rounded up to nearest power of 2), implemented size 256 bytes, stall-free, 1 read and 1 write. Additional information:\n- No additional details."
                ]
              }
              , "count":0
            }
            , {
              "info":
              {
                "name":"Part 15 (line_buf_0)"
                , "data":
                [0, 0, 1, 0]
                , "details":
                [
                  "Private memory implemented in on-chip block RAM."
                  , "Private memory: Optimal.\nRequested size 256 bytes (rounded up to nearest power of 2), implemented size 256 bytes, stall-free, 1 read and 1 write. Additional information:\n- No additional details."
                ]
              }
              , "count":0
            }
            , {
              "info":
              {
                "name":"Part 16 (line_buf_0)"
                , "data":
                [0, 0, 1, 0]
                , "details":
                [
                  "Private memory implemented in on-chip block RAM."
                  , "Private memory: Optimal.\nRequested size 256 bytes (rounded up to nearest power of 2), implemented size 256 bytes, stall-free, 1 read and 1 write. Additional information:\n- No additional details."
                ]
              }
              , "count":0
            }
            , {
              "info":
              {
                "name":"Part 2 (line_buf_0)"
                , "data":
                [0, 0, 1, 0]
                , "details":
                [
                  "Private memory implemented in on-chip block RAM."
                  , "Private memory: Optimal.\nRequested size 256 bytes (rounded up to nearest power of 2), implemented size 256 bytes, stall-free, 1 read and 1 write. Additional information:\n- No additional details."
                ]
              }
              , "count":0
            }
            , {
              "info":
              {
                "name":"Part 3 (line_buf_0)"
                , "data":
                [0, 0, 1, 0]
                , "details":
                [
                  "Private memory implemented in on-chip block RAM."
                  , "Private memory: Optimal.\nRequested size 256 bytes (rounded up to nearest power of 2), implemented size 256 bytes, stall-free, 1 read and 1 write. Additional information:\n- No additional details."
                ]
              }
              , "count":0
            }
            , {
              "info":
              {
                "name":"Part 4 (line_buf_0)"
                , "data":
                [0, 0, 1, 0]
                , "details":
                [
                  "Private memory implemented in on-chip block RAM."
                  , "Private memory: Optimal.\nRequested size 256 bytes (rounded up to nearest power of 2), implemented size 256 bytes, stall-free, 1 read and 1 write. Additional information:\n- No additional details."
                ]
              }
              , "count":0
            }
            , {
              "info":
              {
                "name":"Part 5 (line_buf_0)"
                , "data":
                [0, 0, 1, 0]
                , "details":
                [
                  "Private memory implemented in on-chip block RAM."
                  , "Private memory: Optimal.\nRequested size 256 bytes (rounded up to nearest power of 2), implemented size 256 bytes, stall-free, 1 read and 1 write. Additional information:\n- No additional details."
                ]
              }
              , "count":0
            }
            , {
              "info":
              {
                "name":"Part 6 (line_buf_0)"
                , "data":
                [0, 0, 1, 0]
                , "details":
                [
                  "Private memory implemented in on-chip block RAM."
                  , "Private memory: Optimal.\nRequested size 256 bytes (rounded up to nearest power of 2), implemented size 256 bytes, stall-free, 1 read and 1 write. Additional information:\n- No additional details."
                ]
              }
              , "count":0
            }
            , {
              "info":
              {
                "name":"Part 7 (line_buf_0)"
                , "data":
                [0, 0, 1, 0]
                , "details":
                [
                  "Private memory implemented in on-chip block RAM."
                  , "Private memory: Optimal.\nRequested size 256 bytes (rounded up to nearest power of 2), implemented size 256 bytes, stall-free, 1 read and 1 write. Additional information:\n- No additional details."
                ]
              }
              , "count":0
            }
            , {
              "info":
              {
                "name":"Part 8 (line_buf_0)"
                , "data":
                [0, 0, 1, 0]
                , "details":
                [
                  "Private memory implemented in on-chip block RAM."
                  , "Private memory: Optimal.\nRequested size 256 bytes (rounded up to nearest power of 2), implemented size 256 bytes, stall-free, 1 read and 1 write. Additional information:\n- No additional details."
                ]
              }
              , "count":0
            }
            , {
              "info":
              {
                "name":"Part 9 (line_buf_0)"
                , "data":
                [0, 0, 1, 0]
                , "details":
                [
                  "Private memory implemented in on-chip block RAM."
                  , "Private memory: Optimal.\nRequested size 256 bytes (rounded up to nearest power of 2), implemented size 256 bytes, stall-free, 1 read and 1 write. Additional information:\n- No additional details."
                ]
              }
              , "count":0
            }
          ]
        }
        , {
          "name":"conv_pipe.cl:514 (line_buf_1)"
          , "data":
          [0, 0, 16, 0]
          , "debug":
          [
            [
              {
                "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                , "line":514
              }
            ]
          ]
          , "details":
          [
            "Private memory was split into multiple parts due to optimizations. See below for details on each part."
          ]
          , "subinfos":
          [
            {
              "info":
              {
                "name":"Part 1 (line_buf_1)"
                , "data":
                [0, 0, 1, 0]
                , "details":
                [
                  "Private memory implemented in on-chip block RAM."
                  , "Private memory: Optimal.\nRequested size 256 bytes (rounded up to nearest power of 2), implemented size 256 bytes, stall-free, 1 read and 1 write. Additional information:\n- No additional details."
                ]
              }
              , "count":0
            }
            , {
              "info":
              {
                "name":"Part 10 (line_buf_1)"
                , "data":
                [0, 0, 1, 0]
                , "details":
                [
                  "Private memory implemented in on-chip block RAM."
                  , "Private memory: Optimal.\nRequested size 256 bytes (rounded up to nearest power of 2), implemented size 256 bytes, stall-free, 1 read and 1 write. Additional information:\n- No additional details."
                ]
              }
              , "count":0
            }
            , {
              "info":
              {
                "name":"Part 11 (line_buf_1)"
                , "data":
                [0, 0, 1, 0]
                , "details":
                [
                  "Private memory implemented in on-chip block RAM."
                  , "Private memory: Optimal.\nRequested size 256 bytes (rounded up to nearest power of 2), implemented size 256 bytes, stall-free, 1 read and 1 write. Additional information:\n- No additional details."
                ]
              }
              , "count":0
            }
            , {
              "info":
              {
                "name":"Part 12 (line_buf_1)"
                , "data":
                [0, 0, 1, 0]
                , "details":
                [
                  "Private memory implemented in on-chip block RAM."
                  , "Private memory: Optimal.\nRequested size 256 bytes (rounded up to nearest power of 2), implemented size 256 bytes, stall-free, 1 read and 1 write. Additional information:\n- No additional details."
                ]
              }
              , "count":0
            }
            , {
              "info":
              {
                "name":"Part 13 (line_buf_1)"
                , "data":
                [0, 0, 1, 0]
                , "details":
                [
                  "Private memory implemented in on-chip block RAM."
                  , "Private memory: Optimal.\nRequested size 256 bytes (rounded up to nearest power of 2), implemented size 256 bytes, stall-free, 1 read and 1 write. Additional information:\n- No additional details."
                ]
              }
              , "count":0
            }
            , {
              "info":
              {
                "name":"Part 14 (line_buf_1)"
                , "data":
                [0, 0, 1, 0]
                , "details":
                [
                  "Private memory implemented in on-chip block RAM."
                  , "Private memory: Optimal.\nRequested size 256 bytes (rounded up to nearest power of 2), implemented size 256 bytes, stall-free, 1 read and 1 write. Additional information:\n- No additional details."
                ]
              }
              , "count":0
            }
            , {
              "info":
              {
                "name":"Part 15 (line_buf_1)"
                , "data":
                [0, 0, 1, 0]
                , "details":
                [
                  "Private memory implemented in on-chip block RAM."
                  , "Private memory: Optimal.\nRequested size 256 bytes (rounded up to nearest power of 2), implemented size 256 bytes, stall-free, 1 read and 1 write. Additional information:\n- No additional details."
                ]
              }
              , "count":0
            }
            , {
              "info":
              {
                "name":"Part 16 (line_buf_1)"
                , "data":
                [0, 0, 1, 0]
                , "details":
                [
                  "Private memory implemented in on-chip block RAM."
                  , "Private memory: Optimal.\nRequested size 256 bytes (rounded up to nearest power of 2), implemented size 256 bytes, stall-free, 1 read and 1 write. Additional information:\n- No additional details."
                ]
              }
              , "count":0
            }
            , {
              "info":
              {
                "name":"Part 2 (line_buf_1)"
                , "data":
                [0, 0, 1, 0]
                , "details":
                [
                  "Private memory implemented in on-chip block RAM."
                  , "Private memory: Optimal.\nRequested size 256 bytes (rounded up to nearest power of 2), implemented size 256 bytes, stall-free, 1 read and 1 write. Additional information:\n- No additional details."
                ]
              }
              , "count":0
            }
            , {
              "info":
              {
                "name":"Part 3 (line_buf_1)"
                , "data":
                [0, 0, 1, 0]
                , "details":
                [
                  "Private memory implemented in on-chip block RAM."
                  , "Private memory: Optimal.\nRequested size 256 bytes (rounded up to nearest power of 2), implemented size 256 bytes, stall-free, 1 read and 1 write. Additional information:\n- No additional details."
                ]
              }
              , "count":0
            }
            , {
              "info":
              {
                "name":"Part 4 (line_buf_1)"
                , "data":
                [0, 0, 1, 0]
                , "details":
                [
                  "Private memory implemented in on-chip block RAM."
                  , "Private memory: Optimal.\nRequested size 256 bytes (rounded up to nearest power of 2), implemented size 256 bytes, stall-free, 1 read and 1 write. Additional information:\n- No additional details."
                ]
              }
              , "count":0
            }
            , {
              "info":
              {
                "name":"Part 5 (line_buf_1)"
                , "data":
                [0, 0, 1, 0]
                , "details":
                [
                  "Private memory implemented in on-chip block RAM."
                  , "Private memory: Optimal.\nRequested size 256 bytes (rounded up to nearest power of 2), implemented size 256 bytes, stall-free, 1 read and 1 write. Additional information:\n- No additional details."
                ]
              }
              , "count":0
            }
            , {
              "info":
              {
                "name":"Part 6 (line_buf_1)"
                , "data":
                [0, 0, 1, 0]
                , "details":
                [
                  "Private memory implemented in on-chip block RAM."
                  , "Private memory: Optimal.\nRequested size 256 bytes (rounded up to nearest power of 2), implemented size 256 bytes, stall-free, 1 read and 1 write. Additional information:\n- No additional details."
                ]
              }
              , "count":0
            }
            , {
              "info":
              {
                "name":"Part 7 (line_buf_1)"
                , "data":
                [0, 0, 1, 0]
                , "details":
                [
                  "Private memory implemented in on-chip block RAM."
                  , "Private memory: Optimal.\nRequested size 256 bytes (rounded up to nearest power of 2), implemented size 256 bytes, stall-free, 1 read and 1 write. Additional information:\n- No additional details."
                ]
              }
              , "count":0
            }
            , {
              "info":
              {
                "name":"Part 8 (line_buf_1)"
                , "data":
                [0, 0, 1, 0]
                , "details":
                [
                  "Private memory implemented in on-chip block RAM."
                  , "Private memory: Optimal.\nRequested size 256 bytes (rounded up to nearest power of 2), implemented size 256 bytes, stall-free, 1 read and 1 write. Additional information:\n- No additional details."
                ]
              }
              , "count":0
            }
            , {
              "info":
              {
                "name":"Part 9 (line_buf_1)"
                , "data":
                [0, 0, 1, 0]
                , "details":
                [
                  "Private memory implemented in on-chip block RAM."
                  , "Private memory: Optimal.\nRequested size 256 bytes (rounded up to nearest power of 2), implemented size 256 bytes, stall-free, 1 read and 1 write. Additional information:\n- No additional details."
                ]
              }
              , "count":0
            }
          ]
        }
      ]
      , "basicblocks":
      [
        {
          "name":"Block18.wii_blk"
          , "resources":
          [
            {
              "name":"State"
              , "data":
              [12, 12, 0, 0]
              , "details":
              [
                "Resources for live values and control logic. To reduce this area:\n- reduce size of local variables\n- reduce scope of local variables, localizing them as much as possible\n- reduce number of nested loops"
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"No Source Line"
                    , "data":
                    [2.83333, 2.83333, 0, 0]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:536"
                    , "data":
                    [1.16667, 1.16667, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":536
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:565"
                    , "data":
                    [4, 4, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":565
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:573"
                    , "data":
                    [4, 4, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":573
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
              ]
            }
          ]
          , "computation":
          [
            {
              "name":"conv_pipe.cl:573"
              , "data":
              [4, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":573
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Sub"
                    , "data":
                    [4, 0, 0, 0]
                    , "details":
                    [
                      "This instruction does not depend on thread ID. Consider moving it, and all related instructions to the host to save area."
                      , "Work-Item Invariant instruction."
                    ]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:595"
              , "data":
              [4, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":595
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Sub"
                    , "data":
                    [4, 0, 0, 0]
                    , "details":
                    [
                      "This instruction does not depend on thread ID. Consider moving it, and all related instructions to the host to save area."
                      , "Work-Item Invariant instruction."
                    ]
                  }
                  , "count":1
                }
              ]
            }
          ]
        }
        , {
          "name":"Block19"
          , "resources":
          [
            {
              "name":"State"
              , "data":
              [8, 148, 0, 0]
              , "details":
              [
                "Resources for live values and control logic. To reduce this area:\n- reduce size of local variables\n- reduce scope of local variables, localizing them as much as possible\n- reduce number of nested loops"
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Control flow logic"
                    , "data":
                    [0, 110, 0, 0]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:521"
                    , "data":
                    [8, 38, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":521
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
              ]
            }
          ]
          , "computation":
          [
          ]
        }
        , {
          "name":"Block20"
          , "resources":
          [
            {
              "name":"State"
              , "data":
              [754, 1410, 2, 0]
              , "details":
              [
                "Resources for live values and control logic. To reduce this area:\n- reduce size of local variables\n- reduce scope of local variables, localizing them as much as possible\n- reduce number of nested loops"
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Control flow logic"
                    , "data":
                    [2, 2, 0, 0]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"No Source Line"
                    , "data":
                    [46, 57.3845, 0, 0]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:518"
                    , "data":
                    [0, 0.107143, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":518
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:521"
                    , "data":
                    [0, 1.13452, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":521
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:525"
                    , "data":
                    [0, 0.133929, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":525
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:526"
                    , "data":
                    [0, 0.489405, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":526
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:527"
                    , "data":
                    [0, 0.366964, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":527
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:528"
                    , "data":
                    [33, 28.8531, 1, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":528
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:530"
                    , "data":
                    [64, 128, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":530
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:536"
                    , "data":
                    [0, 0.5, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":536
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:537"
                    , "data":
                    [0, 0.166667, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":537
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:537 > conv_pipe.cl:93"
                    , "data":
                    [64, 128, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":537
                        }
                        , {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":93
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:537 > conv_pipe.cl:94"
                    , "data":
                    [96, 192, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":537
                        }
                        , {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":94
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:539"
                    , "data":
                    [32, 64.1667, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":539
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:541 > conv_pipe.cl:93"
                    , "data":
                    [128, 256, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":541
                        }
                        , {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":93
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:541 > conv_pipe.cl:94"
                    , "data":
                    [128, 257.667, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":541
                        }
                        , {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":94
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:548 > conv_pipe.cl:93"
                    , "data":
                    [64, 128, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":548
                        }
                        , {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":93
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:548 > conv_pipe.cl:94"
                    , "data":
                    [64, 128, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":548
                        }
                        , {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":94
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:550"
                    , "data":
                    [0, 3.33333, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":550
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:551"
                    , "data":
                    [0, 0.333333, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":551
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:555"
                    , "data":
                    [0, 0.666667, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":555
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:565"
                    , "data":
                    [0, 0.333333, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":565
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:567"
                    , "data":
                    [0, 0.5, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":567
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:568"
                    , "data":
                    [33, 26, 1, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":568
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:573"
                    , "data":
                    [0, 0.908631, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":573
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:576"
                    , "data":
                    [0, 0.908631, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":576
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:579"
                    , "data":
                    [0, 0.366964, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":579
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:582"
                    , "data":
                    [0, 0.333333, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":582
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:583"
                    , "data":
                    [0, 0.133929, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":583
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:586"
                    , "data":
                    [0, 0.0625, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":586
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:587"
                    , "data":
                    [0, 0.107143, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":587
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:589"
                    , "data":
                    [0, 0.107143, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":589
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:592"
                    , "data":
                    [0, 0.333333, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":592
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:593"
                    , "data":
                    [0, 0.489405, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":593
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:595"
                    , "data":
                    [0, 1.4894, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":595
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:597"
                    , "data":
                    [0, 0.489405, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":597
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:600"
                    , "data":
                    [0, 0.133929, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":600
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
              ]
            }
            , {
              "name":"Feedback"
              , "data":
              [41.2, 244.5, 0, 0]
              , "details":
              [
                "Resources for loop-carried dependencies. To reduce this area:\n- reduce number and size of loop-carried variables"
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"conv_pipe.cl:526"
                    , "data":
                    [1.8, 37.125, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":526
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:528"
                    , "data":
                    [26, 27, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":528
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:573"
                    , "data":
                    [4, 34.5, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":573
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:576"
                    , "data":
                    [4, 34.5, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":576
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:593"
                    , "data":
                    [1.8, 37.125, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":593
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:595"
                    , "data":
                    [1.8, 37.125, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":595
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:597"
                    , "data":
                    [1.8, 37.125, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":597
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
              ]
            }
            , {
              "name":"Cluster logic"
              , "data":
              [461, 1035, 5, 0]
              , "details":
              [
                "Logic required to efficiently support sets of operations that do not stall. This area cannot be affected directly."
              ]
            }
          ]
          , "computation":
          [
            {
              "name":"conv_pipe.cl:537"
              , "data":
              [216, 192, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":537
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Load"
                    , "data":
                    [216, 192, 0, 0]
                    , "details":
                    [
                      "Stall-free read from memory declared on conv_pipe.cl:513."
                      , "Stall-free read from memory declared on conv_pipe.cl:514."
                    ]
                  }
                  , "count":32
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:537 > conv_pipe.cl:93"
              , "data":
              [64, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":537
                  }
                  , {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":93
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Integer Compare"
                    , "data":
                    [64, 0, 0, 0]
                  }
                  , "count":16
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:539"
              , "data":
              [72, 64, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":539
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Load"
                    , "data":
                    [72, 64, 0, 0]
                    , "details":
                    [
                      "Stall-free read from memory declared on conv_pipe.cl:513."
                    ]
                  }
                  , "count":16
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:541 > conv_pipe.cl:93"
              , "data":
              [64, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":541
                  }
                  , {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":93
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Integer Compare"
                    , "data":
                    [64, 0, 0, 0]
                  }
                  , "count":16
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:544 > conv_pipe.cl:93"
              , "data":
              [64, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":544
                  }
                  , {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":93
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Integer Compare"
                    , "data":
                    [64, 0, 0, 0]
                  }
                  , "count":16
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:548 > conv_pipe.cl:93"
              , "data":
              [64, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":548
                  }
                  , {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":93
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Integer Compare"
                    , "data":
                    [64, 0, 0, 0]
                  }
                  , "count":16
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:550"
              , "data":
              [544, 384, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":550
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Store"
                    , "data":
                    [544, 384, 0, 0]
                    , "details":
                    [
                      "Stall-free write to memory declared on conv_pipe.cl:514."
                    ]
                  }
                  , "count":16
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:551"
              , "data":
              [544, 384, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":551
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Store"
                    , "data":
                    [544, 384, 0, 0]
                    , "details":
                    [
                      "Stall-free write to memory declared on conv_pipe.cl:513."
                    ]
                  }
                  , "count":16
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:565"
              , "data":
              [4, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":565
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Integer Compare"
                    , "data":
                    [4, 0, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:567"
              , "data":
              [4, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":567
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Integer Compare"
                    , "data":
                    [4, 0, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:582"
              , "data":
              [4, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":582
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Integer Compare"
                    , "data":
                    [4, 0, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:586"
              , "data":
              [4, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":586
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Integer Compare"
                    , "data":
                    [4, 0, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
          ]
        }
      ]
    }
    , {
      "name":"memRead"
      , "compute_units":1
      , "details":
      [
        "Max global work dimension: 0"
        , "Number of compute units: 1"
      ]
      , "resources":
      [
        {
          "name":"Coalesced Private Variables: \n - 'feature_idx_dim3' (conv_pipe.cl:156)\n - 'win_itm_z' (conv_pipe.cl:168)"
          , "data":
          [9, 53, 0, 0]
          , "debug":
          [
            [
              {
                "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                , "line":156
              }
            ]
            , [
              {
                "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                , "line":168
              }
            ]
          ]
          , "details":
          [
            "Implemented using registers of the following size:\n- 1 register of width 16 and depth 2 (depth was increased by a factor of 2 due to a loop initiation interval of 2.)\nReducing the scope of the variable may reduce its depth (e.g. moving declaration inside a loop or using it as soon as possible)."
          ]
        }
        , {
          "name":"Coalesced Private Variables: \n - 'gp_num_x' (conv_pipe.cl:146)\n - 'gp_num_x_winbuf' (conv_pipe.cl:147)"
          , "data":
          [2.66667, 65.6667, 0, 0]
          , "debug":
          [
            [
              {
                "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                , "line":146
              }
            ]
            , [
              {
                "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                , "line":147
              }
            ]
          ]
          , "details":
          [
            "Implemented using registers of the following size:\n- 2 registers of width 16 and depth 1"
          ]
        }
        , {
          "name":"Coalesced Private Variables: \n - 'gp_num_y' (conv_pipe.cl:146)\n - 'gp_num_y_winbuf' (conv_pipe.cl:147)"
          , "data":
          [8, 69, 0, 0]
          , "debug":
          [
            [
              {
                "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                , "line":146
              }
            ]
            , [
              {
                "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                , "line":147
              }
            ]
          ]
          , "details":
          [
            "Implemented using registers of the following size:\n- 2 registers of width 16 and depth 1"
          ]
        }
        , {
          "name":"Coalesced Private Variables: \n - 'output_idx_dim1' (conv_pipe.cl:148)\n - 'output_idx_dim2' (conv_pipe.cl:148)\n - 'output_idx_dim3' (conv_pipe.cl:149)\n - 'gp_item_idx_x' (conv_pipe.cl:153)\n - 'win_itm_xyz' (conv_pipe.cl:238)"
          , "data":
          [40, 218.5, 0, 0]
          , "debug":
          [
            [
              {
                "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                , "line":148
              }
            ]
            , [
              {
                "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                , "line":149
              }
            ]
            , [
              {
                "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                , "line":153
              }
            ]
            , [
              {
                "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                , "line":238
              }
            ]
          ]
          , "details":
          [
            "Implemented using registers of the following sizes:\n- 3 registers of width 8 and depth 1\n- 1 register of width 16 and depth 1\n- 1 register of width 32 and depth 1"
          ]
        }
        , {
          "name":"Coalesced Private Variables: \n - 'win_itm_x' (conv_pipe.cl:150)\n - 'win_itm_y' (conv_pipe.cl:150)\n - 'win_itm_z' (conv_pipe.cl:151)\n - 'feature_idx_dim3' (conv_pipe.cl:156)"
          , "data":
          [24, 101, 0, 0]
          , "debug":
          [
            [
              {
                "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                , "line":150
              }
            ]
            , [
              {
                "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                , "line":151
              }
            ]
            , [
              {
                "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                , "line":156
              }
            ]
          ]
          , "details":
          [
            "Implemented using registers of the following sizes:\n- 2 registers of width 8 and depth 1\n- 1 register of width 16 and depth 1"
          ]
        }
        , {
          "name":"Private Variable: \n - 'out_idx_xyz' (conv_pipe.cl:205)"
          , "data":
          [8, 69, 0, 0]
          , "debug":
          [
            [
              {
                "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                , "line":205
              }
            ]
          ]
          , "details":
          [
            "Implemented using registers of the following size:\n- 1 register of width 32 and depth 1"
          ]
        }
        , {
          "name":"Private Variable: \n - 'out_idx_z' (conv_pipe.cl:146)"
          , "data":
          [16, 53, 0, 0]
          , "debug":
          [
            [
              {
                "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                , "line":146
              }
            ]
          ]
          , "details":
          [
            "Implemented using registers of the following size:\n- 1 register of width 16 and depth 1"
          ]
        }
        , {
          "name":"Private Variable: \n - 'out_idx_z_winbuf' (conv_pipe.cl:147)"
          , "data":
          [8, 37, 0, 0]
          , "debug":
          [
            [
              {
                "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                , "line":147
              }
            ]
          ]
          , "details":
          [
            "Implemented using registers of the following size:\n- 1 register of width 16 and depth 1"
          ]
        }
        , {
          "name":"Private Variable: \n - 'win_itm_x' (conv_pipe.cl:170)"
          , "data":
          [0.421053, 16.2632, 0, 0]
          , "debug":
          [
            [
              {
                "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                , "line":170
              }
            ]
          ]
          , "details":
          [
            "Implemented using registers of the following size:\n- 1 register of width 8 and depth 1"
          ]
        }
        , {
          "name":"Private Variable: \n - 'win_itm_y' (conv_pipe.cl:169)"
          , "data":
          [1.33333, 16.8333, 0, 0]
          , "debug":
          [
            [
              {
                "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                , "line":169
              }
            ]
          ]
          , "details":
          [
            "Implemented using registers of the following size:\n- 1 register of width 8 and depth 1"
          ]
        }
        , {
          "name":"conv_pipe.cl:163 (win_buffer)"
          , "data":
          [0, 0, 32, 0]
          , "debug":
          [
            [
              {
                "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                , "line":163
              }
            ]
          ]
          , "details":
          [
            "Local memory: Optimal.\nRequested size 32768 bytes (rounded up to nearest power of 2), implemented size 32768 bytes, stall-free, 1 read and 2 writes. Additional information:\n- Reducing accesses to exactly one read and one write for all on-chip memory systems may increase overall system performance."
          ]
        }
        , {
          "name":"conv_pipe.cl:165 (weight_buffer)"
          , "data":
          [0, 0, 256, 0]
          , "debug":
          [
            [
              {
                "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                , "line":165
              }
            ]
          ]
          , "details":
          [
            "Local memory: Optimal.\nRequested size 262144 bytes (rounded up to nearest power of 2), implemented size 262144 bytes, stall-free, 1 read and 1 write. Additional information:\n- No additional details."
          ]
        }
      ]
      , "basicblocks":
      [
        {
          "name":"Block0.wii_blk"
          , "resources":
          [
            {
              "name":"State"
              , "data":
              [506, 506, 0, 0]
              , "details":
              [
                "Resources for live values and control logic. To reduce this area:\n- reduce size of local variables\n- reduce scope of local variables, localizing them as much as possible\n- reduce number of nested loops"
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"No Source Line"
                    , "data":
                    [441.833, 441.833, 0, 0]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:168"
                    , "data":
                    [4.66667, 4.66667, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":168
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:169"
                    , "data":
                    [0.333333, 0.333333, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":169
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:170"
                    , "data":
                    [0.5, 0.5, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":170
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:176"
                    , "data":
                    [9.33333, 9.33333, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":176
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:205"
                    , "data":
                    [9.33333, 9.33333, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":205
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:210"
                    , "data":
                    [4, 4, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":210
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:230"
                    , "data":
                    [2.66667, 2.66667, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":230
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:243"
                    , "data":
                    [6, 6, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":243
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:261"
                    , "data":
                    [12.6667, 12.6667, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":261
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:318"
                    , "data":
                    [8, 8, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":318
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:340"
                    , "data":
                    [6.66667, 6.66667, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":340
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
              ]
            }
          ]
          , "computation":
          [
            {
              "name":"conv_pipe.cl:176"
              , "data":
              [8, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":176
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Add"
                    , "data":
                    [8, 0, 0, 0]
                    , "details":
                    [
                      "This instruction does not depend on thread ID. Consider moving it, and all related instructions to the host to save area."
                      , "Work-Item Invariant instruction."
                    ]
                  }
                  , "count":2
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:205"
              , "data":
              [0, 56, 0, 2]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":205
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Mul"
                    , "data":
                    [0, 56, 0, 2]
                    , "details":
                    [
                      "This instruction does not depend on thread ID. Consider moving it, and all related instructions to the host to save area."
                      , "Work-Item Invariant instruction."
                    ]
                  }
                  , "count":2
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:231"
              , "data":
              [4, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":231
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Integer Compare"
                    , "data":
                    [4, 0, 0, 0]
                    , "details":
                    [
                      "This instruction does not depend on thread ID. Consider moving it, and all related instructions to the host to save area."
                      , "Work-Item Invariant instruction."
                    ]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:233"
              , "data":
              [30, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":233
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Add"
                    , "data":
                    [30, 0, 0, 0]
                    , "details":
                    [
                      "This instruction does not depend on thread ID. Consider moving it, and all related instructions to the host to save area."
                      , "Work-Item Invariant instruction."
                    ]
                  }
                  , "count":2
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:243"
              , "data":
              [8, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":243
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Add"
                    , "data":
                    [8, 0, 0, 0]
                    , "details":
                    [
                      "This instruction does not depend on thread ID. Consider moving it, and all related instructions to the host to save area."
                      , "Work-Item Invariant instruction."
                    ]
                  }
                  , "count":2
                }
              ]
            }
          ]
        }
        , {
          "name":"Block1"
          , "resources":
          [
            {
              "name":"State"
              , "data":
              [0, 524, 0, 0]
              , "details":
              [
                "Resources for live values and control logic. To reduce this area:\n- reduce size of local variables\n- reduce scope of local variables, localizing them as much as possible\n- reduce number of nested loops"
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Control flow logic"
                    , "data":
                    [0, 506, 0, 0]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:156"
                    , "data":
                    [0, 18, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":156
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
              ]
            }
          ]
          , "computation":
          [
          ]
        }
        , {
          "name":"Block10"
          , "resources":
          [
            {
              "name":"State"
              , "data":
              [1968, 11041, 55, 0]
              , "details":
              [
                "Resources for live values and control logic. To reduce this area:\n- reduce size of local variables\n- reduce scope of local variables, localizing them as much as possible\n- reduce number of nested loops"
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Control flow logic"
                    , "data":
                    [215, 1275, 0, 0]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"No Source Line"
                    , "data":
                    [193.682, 5581.06, 41.7, 0]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:203"
                    , "data":
                    [2.66667, 5.37037, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":203
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:205"
                    , "data":
                    [16, 34.3241, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":205
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:215"
                    , "data":
                    [0, 3, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":215
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:218"
                    , "data":
                    [0.333333, 0.666667, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":218
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:219"
                    , "data":
                    [0.333333, 0.666667, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":219
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:220"
                    , "data":
                    [0.888889, 1.77778, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":220
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:222"
                    , "data":
                    [0.666667, 1.33333, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":222
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:225"
                    , "data":
                    [1.33333, 2.7037, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":225
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:226"
                    , "data":
                    [1, 2.22454, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":226
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:227"
                    , "data":
                    [2, 4.14815, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":227
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:230"
                    , "data":
                    [4, 8, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":230
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:231"
                    , "data":
                    [8, 16, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":231
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:233"
                    , "data":
                    [8, 16, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":233
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:238"
                    , "data":
                    [66.3182, 1405.47, 12.3, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":238
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:241"
                    , "data":
                    [0, 3.5, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":241
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:243"
                    , "data":
                    [16, 32.037, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":243
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:244"
                    , "data":
                    [16, 32.037, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":244
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:247"
                    , "data":
                    [32, 70, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":247
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:249"
                    , "data":
                    [64, 128, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":249
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:258"
                    , "data":
                    [56, 116, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":258
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:262"
                    , "data":
                    [2, 4.14815, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":262
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:263"
                    , "data":
                    [2, 4, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":263
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:264"
                    , "data":
                    [2, 4.14815, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":264
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:267"
                    , "data":
                    [1, 2.22454, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":267
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:268"
                    , "data":
                    [1, 2.1875, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":268
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:269"
                    , "data":
                    [1, 2.22454, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":269
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:272"
                    , "data":
                    [1.33333, 2.7037, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":272
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:274"
                    , "data":
                    [1.33333, 2.7037, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":274
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:279"
                    , "data":
                    [2.66667, 8.83333, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":279
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:281"
                    , "data":
                    [135, 260.037, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":281
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:282"
                    , "data":
                    [36, 74, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":282
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:287"
                    , "data":
                    [12, 24, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":287
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:289"
                    , "data":
                    [4.66667, 12.8333, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":289
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:290"
                    , "data":
                    [188.5, 188.5, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":290
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:291"
                    , "data":
                    [33.5, 26.5, 1, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":291
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:298"
                    , "data":
                    [49.3333, 99.6667, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":298
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:307"
                    , "data":
                    [774, 1550, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":307
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:318"
                    , "data":
                    [2.66667, 5.33333, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":318
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:319"
                    , "data":
                    [0.888889, 1.77778, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":319
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:320"
                    , "data":
                    [0.666667, 1.33333, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":320
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:322"
                    , "data":
                    [2.66667, 5.33333, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":322
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:323"
                    , "data":
                    [0.888889, 1.77778, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":323
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:326"
                    , "data":
                    [0.333333, 0.666667, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":326
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:327"
                    , "data":
                    [1, 2, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":327
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:328"
                    , "data":
                    [0.333333, 0.666667, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":328
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:331"
                    , "data":
                    [0.333333, 0.666667, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":331
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:333"
                    , "data":
                    [1.33333, 2.66667, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":333
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:357"
                    , "data":
                    [2.66667, 5.37037, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":357
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:359"
                    , "data":
                    [2.66667, 5.37037, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":359
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
              ]
            }
            , {
              "name":"Feedback"
              , "data":
              [55, 582.5, 0, 0]
              , "details":
              [
                "Resources for loop-carried dependencies. To reduce this area:\n- reduce number and size of loop-carried variables"
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"No Source Line"
                    , "data":
                    [40, 218.5, 0, 0]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:205"
                    , "data":
                    [8, 357, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":205
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:238"
                    , "data":
                    [7, 6, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":238
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:307"
                    , "data":
                    [0, 1, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":307
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
              ]
            }
            , {
              "name":"Cluster logic"
              , "data":
              [1005, 1572, 5, 0]
              , "details":
              [
                "Logic required to efficiently support sets of operations that do not stall. This area cannot be affected directly."
              ]
            }
          ]
          , "computation":
          [
            {
              "name":"No Source Line"
              , "data":
              [24, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":""
                    , "line":0
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Pointer Computation"
                    , "data":
                    [24, 0, 0, 0]
                  }
                  , "count":3
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:168"
              , "data":
              [3.5, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":168
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Integer Compare"
                    , "data":
                    [3.5, 0, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:238"
              , "data":
              [16, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":238
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Integer Compare"
                    , "data":
                    [16, 0, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:241"
              , "data":
              [3.5, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":241
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Integer Compare"
                    , "data":
                    [3.5, 0, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:243"
              , "data":
              [5.33333, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":243
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Add"
                    , "data":
                    [4, 0, 0, 0]
                  }
                  , "count":1
                }
                , {
                  "info":
                  {
                    "name":"Integer Compare"
                    , "data":
                    [1.33333, 0, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:244"
              , "data":
              [4, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":244
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Add"
                    , "data":
                    [4, 0, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:247"
              , "data":
              [16, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":247
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Integer Compare"
                    , "data":
                    [16, 0, 0, 0]
                  }
                  , "count":4
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:249"
              , "data":
              [532, 2049, 13, 2]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":249
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Add"
                    , "data":
                    [39, 0, 0, 0]
                  }
                  , "count":3
                }
                , {
                  "info":
                  {
                    "name":"Load"
                    , "data":
                    [453, 1985, 13, 0]
                  }
                  , "count":1
                }
                , {
                  "info":
                  {
                    "name":"Mul"
                    , "data":
                    [0, 64, 0, 2]
                  }
                  , "count":2
                }
                , {
                  "info":
                  {
                    "name":"Pointer Computation"
                    , "data":
                    [32, 0, 0, 0]
                  }
                  , "count":1
                }
                , {
                  "info":
                  {
                    "name":"Sub"
                    , "data":
                    [8, 0, 0, 0]
                  }
                  , "count":2
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:258"
              , "data":
              [42, 80, 0, 2]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":258
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Add"
                    , "data":
                    [8, 0, 0, 0]
                  }
                  , "count":2
                }
                , {
                  "info":
                  {
                    "name":"Mul"
                    , "data":
                    [0, 56, 0, 2]
                  }
                  , "count":2
                }
                , {
                  "info":
                  {
                    "name":"Store"
                    , "data":
                    [34, 24, 0, 0]
                    , "details":
                    [
                      "Stall-free write to memory declared on conv_pipe.cl:163."
                    ]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:261"
              , "data":
              [13.3333, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":261
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Integer Compare"
                    , "data":
                    [13.3333, 0, 0, 0]
                  }
                  , "count":3
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:263"
              , "data":
              [1.33333, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":263
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Integer Compare"
                    , "data":
                    [1.33333, 0, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:266"
              , "data":
              [4, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":266
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Integer Compare"
                    , "data":
                    [4, 0, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:281"
              , "data":
              [614.333, 3633, 21, 2]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":281
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Add"
                    , "data":
                    [24, 0, 0, 0]
                  }
                  , "count":3
                }
                , {
                  "info":
                  {
                    "name":"Integer Compare"
                    , "data":
                    [1.33333, 0, 0, 0]
                  }
                  , "count":1
                }
                , {
                  "info":
                  {
                    "name":"Load"
                    , "data":
                    [589, 3593, 21, 0]
                  }
                  , "count":1
                }
                , {
                  "info":
                  {
                    "name":"Mul"
                    , "data":
                    [0, 40, 0, 2]
                  }
                  , "count":2
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:282"
              , "data":
              [42, 80, 0, 2]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":282
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Add"
                    , "data":
                    [8, 0, 0, 0]
                  }
                  , "count":2
                }
                , {
                  "info":
                  {
                    "name":"Mul"
                    , "data":
                    [0, 56, 0, 2]
                  }
                  , "count":2
                }
                , {
                  "info":
                  {
                    "name":"Store"
                    , "data":
                    [34, 24, 0, 0]
                    , "details":
                    [
                      "Stall-free write to memory declared on conv_pipe.cl:165."
                    ]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:287"
              , "data":
              [8, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":287
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Add"
                    , "data":
                    [4, 0, 0, 0]
                  }
                  , "count":1
                }
                , {
                  "info":
                  {
                    "name":"Integer Compare"
                    , "data":
                    [4, 0, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:289"
              , "data":
              [2, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":289
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Or"
                    , "data":
                    [2, 0, 0, 0]
                  }
                  , "count":2
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:290"
              , "data":
              [429, 1961, 13, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":290
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Load"
                    , "data":
                    [429, 1961, 13, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:298"
              , "data":
              [25, 80, 0, 3]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":298
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Add"
                    , "data":
                    [16, 0, 0, 0]
                  }
                  , "count":3
                }
                , {
                  "info":
                  {
                    "name":"Load"
                    , "data":
                    [9, 8, 0, 0]
                    , "details":
                    [
                      "Stall-free read from memory declared on conv_pipe.cl:163."
                    ]
                  }
                  , "count":1
                }
                , {
                  "info":
                  {
                    "name":"Mul"
                    , "data":
                    [0, 72, 0, 3]
                  }
                  , "count":3
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:307"
              , "data":
              [9, 8, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":307
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Load"
                    , "data":
                    [9, 8, 0, 0]
                    , "details":
                    [
                      "Stall-free read from memory declared on conv_pipe.cl:165."
                    ]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:318"
              , "data":
              [13.3333, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":318
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Integer Compare"
                    , "data":
                    [13.3333, 0, 0, 0]
                  }
                  , "count":3
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:322"
              , "data":
              [1.33333, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":322
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Integer Compare"
                    , "data":
                    [1.33333, 0, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:325"
              , "data":
              [4, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":325
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Integer Compare"
                    , "data":
                    [4, 0, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
          ]
        }
        , {
          "name":"Block2"
          , "resources":
          [
            {
              "name":"State"
              , "data":
              [9, 25, 0, 0]
              , "details":
              [
                "Resources for live values and control logic. To reduce this area:\n- reduce size of local variables\n- reduce scope of local variables, localizing them as much as possible\n- reduce number of nested loops"
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Control flow logic"
                    , "data":
                    [1, 1, 0, 0]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"No Source Line"
                    , "data":
                    [0, 3.33333, 0, 0]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:168"
                    , "data":
                    [8, 20.6667, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":168
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
              ]
            }
            , {
              "name":"Feedback"
              , "data":
              [17, 60, 0, 0]
              , "details":
              [
                "Resources for loop-carried dependencies. To reduce this area:\n- reduce number and size of loop-carried variables"
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"conv_pipe.cl:168"
                    , "data":
                    [17, 60, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":168
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
              ]
            }
            , {
              "name":"Cluster logic"
              , "data":
              [218, 502, 2, 0]
              , "details":
              [
                "Logic required to efficiently support sets of operations that do not stall. This area cannot be affected directly."
              ]
            }
          ]
          , "computation":
          [
            {
              "name":"conv_pipe.cl:168"
              , "data":
              [7, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":168
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Integer Compare"
                    , "data":
                    [7, 0, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:178"
              , "data":
              [4, 32, 0, 1]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":178
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Mul"
                    , "data":
                    [0, 32, 0, 1]
                  }
                  , "count":1
                }
                , {
                  "info":
                  {
                    "name":"Sub"
                    , "data":
                    [4, 0, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:187"
              , "data":
              [0, 32, 0, 1]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":187
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Mul"
                    , "data":
                    [0, 32, 0, 1]
                  }
                  , "count":1
                }
              ]
            }
          ]
        }
        , {
          "name":"Block3"
          , "resources":
          [
            {
              "name":"State"
              , "data":
              [194, 403.5, 0, 0]
              , "details":
              [
                "Resources for live values and control logic. To reduce this area:\n- reduce size of local variables\n- reduce scope of local variables, localizing them as much as possible\n- reduce number of nested loops"
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Control flow logic"
                    , "data":
                    [66, 131.5, 0, 0]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"No Source Line"
                    , "data":
                    [80, 173.5, 0, 0]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:168"
                    , "data":
                    [0, 1.66667, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":168
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:169"
                    , "data":
                    [0, 0.833333, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":169
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:176"
                    , "data":
                    [16, 32, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":176
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:178"
                    , "data":
                    [24, 48, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":178
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:187"
                    , "data":
                    [8, 16, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":187
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
              ]
            }
            , {
              "name":"Feedback"
              , "data":
              [21.6667, 255.167, 0, 0]
              , "details":
              [
                "Resources for loop-carried dependencies. To reduce this area:\n- reduce number and size of loop-carried variables"
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"conv_pipe.cl:168"
                    , "data":
                    [6.66667, 84.1667, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":168
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:169"
                    , "data":
                    [7, 6, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":169
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:178"
                    , "data":
                    [8, 165, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":178
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
              ]
            }
            , {
              "name":"Cluster logic"
              , "data":
              [218, 502, 2, 0]
              , "details":
              [
                "Logic required to efficiently support sets of operations that do not stall. This area cannot be affected directly."
              ]
            }
          ]
          , "computation":
          [
            {
              "name":"conv_pipe.cl:176"
              , "data":
              [8, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":176
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Integer Compare"
                    , "data":
                    [8, 0, 0, 0]
                  }
                  , "count":2
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:178"
              , "data":
              [20, 32, 0, 1]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":178
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Add"
                    , "data":
                    [16, 0, 0, 0]
                  }
                  , "count":1
                }
                , {
                  "info":
                  {
                    "name":"Mul"
                    , "data":
                    [0, 32, 0, 1]
                  }
                  , "count":1
                }
                , {
                  "info":
                  {
                    "name":"Sub"
                    , "data":
                    [4, 0, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:187"
              , "data":
              [16, 32, 0, 1]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":187
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Add"
                    , "data":
                    [16, 0, 0, 0]
                  }
                  , "count":1
                }
                , {
                  "info":
                  {
                    "name":"Mul"
                    , "data":
                    [0, 32, 0, 1]
                  }
                  , "count":1
                }
              ]
            }
          ]
        }
        , {
          "name":"Block4"
          , "resources":
          [
            {
              "name":"State"
              , "data":
              [465, 3166.5, 8, 0]
              , "details":
              [
                "Resources for live values and control logic. To reduce this area:\n- reduce size of local variables\n- reduce scope of local variables, localizing them as much as possible\n- reduce number of nested loops"
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Control flow logic"
                    , "data":
                    [133, 393.5, 0, 0]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"No Source Line"
                    , "data":
                    [120.933, 1818.42, 5.6, 0]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:170"
                    , "data":
                    [17.0667, 669.583, 1.4, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":170
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:176"
                    , "data":
                    [5.33333, 12.6667, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":176
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:178"
                    , "data":
                    [101.333, 170.667, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":178
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:187"
                    , "data":
                    [87.3333, 101.667, 1, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":187
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
              ]
            }
            , {
              "name":"Feedback"
              , "data":
              [14.5789, 298.737, 0, 0]
              , "details":
              [
                "Resources for loop-carried dependencies. To reduce this area:\n- reduce number and size of loop-carried variables"
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"conv_pipe.cl:169"
                    , "data":
                    [7.57895, 292.737, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":169
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:170"
                    , "data":
                    [7, 6, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":170
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
              ]
            }
            , {
              "name":"Cluster logic"
              , "data":
              [218, 502, 2, 0]
              , "details":
              [
                "Logic required to efficiently support sets of operations that do not stall. This area cannot be affected directly."
              ]
            }
          ]
          , "computation":
          [
            {
              "name":"conv_pipe.cl:176"
              , "data":
              [8, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":176
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Integer Compare"
                    , "data":
                    [8, 0, 0, 0]
                  }
                  , "count":2
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:178"
              , "data":
              [501, 1985, 13, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":178
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Add"
                    , "data":
                    [16, 0, 0, 0]
                  }
                  , "count":1
                }
                , {
                  "info":
                  {
                    "name":"Load"
                    , "data":
                    [453, 1985, 13, 0]
                  }
                  , "count":1
                }
                , {
                  "info":
                  {
                    "name":"Pointer Computation"
                    , "data":
                    [32, 0, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:187"
              , "data":
              [50, 24, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":187
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Add"
                    , "data":
                    [16, 0, 0, 0]
                  }
                  , "count":1
                }
                , {
                  "info":
                  {
                    "name":"Store"
                    , "data":
                    [34, 24, 0, 0]
                    , "details":
                    [
                      "Stall-free write to memory declared on conv_pipe.cl:163."
                    ]
                  }
                  , "count":1
                }
              ]
            }
          ]
        }
        , {
          "name":"Block5"
          , "resources":
          [
            {
              "name":"State"
              , "data":
              [257, 385.5, 0, 0]
              , "details":
              [
                "Resources for live values and control logic. To reduce this area:\n- reduce size of local variables\n- reduce scope of local variables, localizing them as much as possible\n- reduce number of nested loops"
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Control flow logic"
                    , "data":
                    [257, 385.5, 0, 0]
                  }
                  , "count":0
                }
              ]
            }
          ]
          , "computation":
          [
          ]
        }
        , {
          "name":"Block6"
          , "resources":
          [
            {
              "name":"State"
              , "data":
              [1, 1.5, 0, 0]
              , "details":
              [
                "Resources for live values and control logic. To reduce this area:\n- reduce size of local variables\n- reduce scope of local variables, localizing them as much as possible\n- reduce number of nested loops"
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Control flow logic"
                    , "data":
                    [1, 1.5, 0, 0]
                  }
                  , "count":0
                }
              ]
            }
          ]
          , "computation":
          [
          ]
        }
        , {
          "name":"Block7"
          , "resources":
          [
            {
              "name":"State"
              , "data":
              [8, 38, 0, 0]
              , "details":
              [
                "Resources for live values and control logic. To reduce this area:\n- reduce size of local variables\n- reduce scope of local variables, localizing them as much as possible\n- reduce number of nested loops"
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"conv_pipe.cl:205"
                    , "data":
                    [8, 38, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":205
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
              ]
            }
            , {
              "name":"Cluster logic"
              , "data":
              [218, 502, 2, 0]
              , "details":
              [
                "Logic required to efficiently support sets of operations that do not stall. This area cannot be affected directly."
              ]
            }
          ]
          , "computation":
          [
            {
              "name":"conv_pipe.cl:205"
              , "data":
              [0, 48, 0, 2]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":205
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Mul"
                    , "data":
                    [0, 48, 0, 2]
                  }
                  , "count":2
                }
              ]
            }
          ]
        }
        , {
          "name":"Block8"
          , "resources":
          [
            {
              "name":"State"
              , "data":
              [99, 175, 1, 0]
              , "details":
              [
                "Resources for live values and control logic. To reduce this area:\n- reduce size of local variables\n- reduce scope of local variables, localizing them as much as possible\n- reduce number of nested loops"
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Control flow logic"
                    , "data":
                    [1, 1, 0, 0]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"No Source Line"
                    , "data":
                    [10.6667, 26.8667, 0, 0]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:197"
                    , "data":
                    [0, 0.1, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":197
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:198"
                    , "data":
                    [0, 0.345833, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":198
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:202"
                    , "data":
                    [0, 0.0333333, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":202
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:203"
                    , "data":
                    [0.666667, 1.7625, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":203
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:205"
                    , "data":
                    [0, 2.65, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":205
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:230"
                    , "data":
                    [12, 24, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":230
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:231"
                    , "data":
                    [8, 17.3333, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":231
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:233"
                    , "data":
                    [8, 16, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":233
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:238"
                    , "data":
                    [34, 27, 1, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":238
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:243"
                    , "data":
                    [8, 16, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":243
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:287"
                    , "data":
                    [12, 24, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":287
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:340"
                    , "data":
                    [0, 2.41667, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":340
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:341"
                    , "data":
                    [0, 0.345833, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":341
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:342"
                    , "data":
                    [0, 0.8625, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":342
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:343"
                    , "data":
                    [0, 0.345833, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":343
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:345"
                    , "data":
                    [0, 0.25, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":345
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:346"
                    , "data":
                    [0, 0.1, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":346
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:348"
                    , "data":
                    [0, 0.1, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":348
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:356"
                    , "data":
                    [0, 0.333333, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":356
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:357"
                    , "data":
                    [0.666667, 1.7625, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":357
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:358"
                    , "data":
                    [3.33333, 9.3625, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":358
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:359"
                    , "data":
                    [0.666667, 1.7625, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":359
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:362"
                    , "data":
                    [0, 0.0333333, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":362
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:363"
                    , "data":
                    [0, 0.2, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":363
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:364"
                    , "data":
                    [0, 0.0333333, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":364
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
              ]
            }
            , {
              "name":"Feedback"
              , "data":
              [12.3333, 137.333, 0, 0]
              , "details":
              [
                "Resources for loop-carried dependencies. To reduce this area:\n- reduce number and size of loop-carried variables"
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"conv_pipe.cl:205"
                    , "data":
                    [7, 6, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":205
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:350"
                    , "data":
                    [5.33333, 131.333, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":350
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
              ]
            }
            , {
              "name":"Cluster logic"
              , "data":
              [218, 502, 2, 0]
              , "details":
              [
                "Logic required to efficiently support sets of operations that do not stall. This area cannot be affected directly."
              ]
            }
          ]
          , "computation":
          [
            {
              "name":"No Source Line"
              , "data":
              [7, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":""
                    , "line":0
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Integer Compare"
                    , "data":
                    [7, 0, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:230"
              , "data":
              [16, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":230
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Add"
                    , "data":
                    [8, 0, 0, 0]
                  }
                  , "count":2
                }
                , {
                  "info":
                  {
                    "name":"Integer Compare"
                    , "data":
                    [8, 0, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:243"
              , "data":
              [0, 27, 0, 1]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":243
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Mul"
                    , "data":
                    [0, 27, 0, 1]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:244"
              , "data":
              [0, 24, 0, 1]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":244
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Mul"
                    , "data":
                    [0, 24, 0, 1]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:281"
              , "data":
              [0, 32, 0, 1]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":281
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Mul"
                    , "data":
                    [0, 32, 0, 1]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:287"
              , "data":
              [8, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":287
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Add"
                    , "data":
                    [8, 0, 0, 0]
                  }
                  , "count":2
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:340"
              , "data":
              [16, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":340
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Integer Compare"
                    , "data":
                    [16, 0, 0, 0]
                  }
                  , "count":2
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:342"
              , "data":
              [8, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":342
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Integer Compare"
                    , "data":
                    [8, 0, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:345"
              , "data":
              [8, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":345
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Integer Compare"
                    , "data":
                    [8, 0, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:356"
              , "data":
              [8, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":356
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Integer Compare"
                    , "data":
                    [8, 0, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:358"
              , "data":
              [16, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":358
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Integer Compare"
                    , "data":
                    [16, 0, 0, 0]
                  }
                  , "count":2
                }
              ]
            }
          ]
        }
        , {
          "name":"Block9"
          , "resources":
          [
            {
              "name":"State"
              , "data":
              [1760, 2635, 0, 0]
              , "details":
              [
                "Resources for live values and control logic. To reduce this area:\n- reduce size of local variables\n- reduce scope of local variables, localizing them as much as possible\n- reduce number of nested loops"
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Control flow logic"
                    , "data":
                    [1752, 2628, 0, 0]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:205"
                    , "data":
                    [8, 7, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":205
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
              ]
            }
            , {
              "name":"Feedback"
              , "data":
              [0, 3, 0, 0]
              , "details":
              [
                "Resources for loop-carried dependencies. To reduce this area:\n- reduce number and size of loop-carried variables"
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"conv_pipe.cl:307"
                    , "data":
                    [0, 3, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":307
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
              ]
            }
          ]
          , "computation":
          [
          ]
        }
      ]
    }
    , {
      "name":"memWrite"
      , "compute_units":1
      , "details":
      [
        "Number of compute units: 1"
      ]
      , "resources":
      [
        {
          "name":"Function overhead"
          , "data":
          [1598, 1736, 0, 0]
          , "details":
          [
            "Kernel dispatch logic."
          ]
        }
        , {
          "name":"conv_pipe.cl:636 (buffer)"
          , "data":
          [0, 0, 4, 0]
          , "debug":
          [
            [
              {
                "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                , "line":636
              }
            ]
          ]
          , "details":
          [
            "Local memory: Optimal.\nRequested size 16 bytes (rounded up to nearest power of 2), implemented size 48 bytes, replicated 3 times total, stall-free, 1 read and 1 write. Additional information:\n- Replicated 3 times to efficiently support multiple simultaneous workgroups. This replication resulted in no increase in actual block RAM usage."
          ]
        }
      ]
      , "basicblocks":
      [
        {
          "name":"Block22.wii_blk"
          , "resources":
          [
            {
              "name":"State"
              , "data":
              [128, 128, 0, 0]
              , "details":
              [
                "Resources for live values and control logic. To reduce this area:\n- reduce size of local variables\n- reduce scope of local variables, localizing them as much as possible\n- reduce number of nested loops"
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Control flow logic"
                    , "data":
                    [128, 128, 0, 0]
                  }
                  , "count":0
                }
              ]
            }
          ]
          , "computation":
          [
            {
              "name":"conv_pipe.cl:659"
              , "data":
              [0, 32, 0, 2]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":659
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Mul"
                    , "data":
                    [0, 32, 0, 2]
                    , "details":
                    [
                      "This instruction does not depend on thread ID. Consider moving it, and all related instructions to the host to save area."
                      , "Work-Item Invariant instruction."
                    ]
                  }
                  , "count":2
                }
              ]
            }
          ]
        }
        , {
          "name":"Block23"
          , "resources":
          [
            {
              "name":"State"
              , "data":
              [439, 775, 0, 0]
              , "details":
              [
                "Resources for live values and control logic. To reduce this area:\n- reduce size of local variables\n- reduce scope of local variables, localizing them as much as possible\n- reduce number of nested loops"
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Control flow logic"
                    , "data":
                    [128, 330, 0, 0]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"No Source Line"
                    , "data":
                    [53.5, 111.5, 0, 0]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:638"
                    , "data":
                    [10, 9, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":638
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:642"
                    , "data":
                    [128, 128, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":642
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:646"
                    , "data":
                    [4, 4, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":646
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:654"
                    , "data":
                    [13.5, 30, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":654
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:657"
                    , "data":
                    [4, 8, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":657
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:659"
                    , "data":
                    [65, 130.5, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":659
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
                , {
                  "info":
                  {
                    "name":"conv_pipe.cl:673"
                    , "data":
                    [33, 24, 0, 0]
                    , "debug":
                    [
                      [
                        {
                          "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                          , "line":673
                        }
                      ]
                    ]
                  }
                  , "count":0
                }
              ]
            }
            , {
              "name":"Cluster logic"
              , "data":
              [242, 902, 2, 0]
              , "details":
              [
                "Logic required to efficiently support sets of operations that do not stall. This area cannot be affected directly."
              ]
            }
          ]
          , "computation":
          [
            {
              "name":"conv_pipe.cl:646"
              , "data":
              [34, 24, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":646
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Store"
                    , "data":
                    [34, 24, 0, 0]
                    , "details":
                    [
                      "Stall-free write to memory declared on conv_pipe.cl:636."
                    ]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:650"
              , "data":
              [110, 73, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":650
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"'__acl_barrier' Function Call"
                    , "data":
                    [110, 73, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:654"
              , "data":
              [9, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":654
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Add"
                    , "data":
                    [1, 0, 0, 0]
                  }
                  , "count":1
                }
                , {
                  "info":
                  {
                    "name":"Sub"
                    , "data":
                    [8, 0, 0, 0]
                  }
                  , "count":2
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:657"
              , "data":
              [12, 0, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":657
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Integer Compare"
                    , "data":
                    [12, 0, 0, 0]
                  }
                  , "count":2
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:659"
              , "data":
              [499, 2167, 16, 2]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":659
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"Add"
                    , "data":
                    [36, 0, 0, 0]
                  }
                  , "count":5
                }
                , {
                  "info":
                  {
                    "name":"Load"
                    , "data":
                    [9, 8, 0, 0]
                    , "details":
                    [
                      "Stall-free read from memory declared on conv_pipe.cl:636."
                    ]
                  }
                  , "count":1
                }
                , {
                  "info":
                  {
                    "name":"Mul"
                    , "data":
                    [0, 64, 0, 2]
                  }
                  , "count":2
                }
                , {
                  "info":
                  {
                    "name":"Pointer Computation"
                    , "data":
                    [16, 0, 0, 0]
                  }
                  , "count":2
                }
                , {
                  "info":
                  {
                    "name":"Store"
                    , "data":
                    [438, 2095, 16, 0]
                  }
                  , "count":1
                }
              ]
            }
            , {
              "name":"conv_pipe.cl:671"
              , "data":
              [110, 73, 0, 0]
              , "debug":
              [
                [
                  {
                    "filename":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl"
                    , "line":671
                  }
                ]
              ]
              , "subinfos":
              [
                {
                  "info":
                  {
                    "name":"'__acl_barrier' Function Call"
                    , "data":
                    [110, 73, 0, 0]
                  }
                  , "count":1
                }
              ]
            }
          ]
        }
      ]
    }
  ]
}
;var fileJSON=[{"index":0, "path":"/home/dtyu/Git/pipecnn/project/device/conv_pipe.cl", "name":"conv_pipe.cl", "content":"/*\012 * ------------------------------------------------------\012 *\012 *   PipeCNN: An OpenCL-Based FPGA Accelerator for CNNs\012 *\012 * ------------------------------------------------------\012 * Filename:\012 *   - conv_pipe.cl\012 *\012 * Author(s):\012 *   - Dong Wang, wangdong@m.bjtu.edu.cn\012 *\012 * History:\012 *   - v1.3 Win-Buffer-Based Implementation\012 * ------------------------------------\012 *\012 *   Copyright (C) 2016, Institute of Information Science,\012 *   Beijing Jiaotong University. All rights reserved.\012 *\012 *   Licensed under the Apache License, Version 2.0 (the \"License\");\012 *   you may not use this file except in compliance with the License.\012 *   You may obtain a copy of the License at\012 *\012 *     http://www.apache.org/licenses/LICENSE-2.0\012 *\012 *   Unless required by applicable law or agreed to in writing, software\012 *   distributed under the License is distributed on an \"AS IS\" BASIS,\012 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\012 *   See the License for the specific language governing permissions and\012 *   limitations under the License.\012 *\012 */\012\012#define USE_ROM \012\012// The following macros are used for debug\012//#define DEBUG_MEMRD\012//#define DEBUG_CONV\012//#define DEBUG_POOL\012//#define DEBUG_MEMWR\012//#define DEBUG_LRN\012//#define DEBUG_LRN_OUT\012\012#include \"hw_param.cl\"\012#include \"rtl_lib.h\"\012\012#pragma OPENCL_EXTENSION cl_altera_channels : enable\012\012// Define the precision of the data-path\012typedef char DPTYPE;\012typedef int  MACTYPE;\012\012// Vectorized data type\012typedef struct {\012   DPTYPE data[VEC_SIZE];\012} lane_data;\012\012// Combined vec-data type from multiple lane\012typedef struct {\012   lane_data lane[LANE_NUM];\012} channel_vec;\012\012// Combined scalar data type from multiple lane\012typedef struct {\012   DPTYPE lane[LANE_NUM];\012} channel_scal;\012\012\012channel channel_vec    data_ch    __attribute__((depth(0)));\012channel channel_vec    weight_ch  __attribute__((depth(0)));\012channel channel_scal   bias_ch    __attribute__((depth(8)));\012channel channel_scal   conv_ch    __attribute__((depth(CHN_DEPTH)));\012channel channel_scal   pool_ch    __attribute__((depth(CHN_DEPTH)));\012channel channel_scal   bypass_ch  __attribute__((depth(CHN_DEPTH)));\012\012\012// parallel MAC units including (VEC_SIZE-1) multipliers\012MACTYPE mac(lane_data input, lane_data weights)\012{\012	MACTYPE output = MASK_MULT & CZERO;\012	\012	#pragma unroll\012	for(int i=0; i<VEC_SIZE/4; i++){\012		output += MASK_MULT & mult_add_fix8bx4(input.data[i*4], weights.data[i*4], input.data[i*4+1], weights.data[i*4+1], input.data[i*4+2], weights.data[i*4+2], input.data[i*4+3], weights.data[i*4+3]);\012	}\012	return output;\012}\012\012DPTYPE pool_max(DPTYPE a_in, DPTYPE b_in)\012{\012	DPTYPE max_value;\012	\012	if(a_in >= b_in)\012		max_value = a_in;\012	else\012		max_value = b_in;\012	\012	return max_value;\012\012}\012\012// Fetch Data from Global Memory\012__kernel\012__attribute__((task))\012__attribute__((max_global_work_dim(0)))\012void memRead(\012			// Params Ports\012			uchar  data_dim1,\012			uchar  data_dim2,\012			ushort data_dim1xdim2,\012			uchar  weight_dim1,\012			uchar  weight_dim2,\012			ushort weight_dim3,\012			ushort weight_dim4_div_lane, // avoid generating divider\012			uchar  weight_dim1x2,\012			uint   weight_dim1x2x3,\012			uchar  conv_x,\012			//uchar  conv_y,           // not used in this version\012			uchar  stride,\012			uchar  padding,\012			uchar  split,\012			uchar  group_num_x,\012			uchar  group_num_y,\012			uchar  group_rem_size_x,\012			//uchar  group_rem_size_y, // not used in this version\012			uint   group_rem_size_xyz,\012			uchar  win_size_x,\012			uchar  win_size_y,\012			uint   win_size_xyz,  \012			// Data Ports\012			__global lane_data    *restrict bottom,\012			__global channel_vec  *restrict weights,\012			__global channel_scal *restrict bias        )\012\012{\012\012\012	// Input Data, Weights and Bias\012	lane_data     data_vec;\012	channel_vec   data_ch_vec;\012	channel_vec   weight_ch_vec;\012	channel_scal  bias_ch_in;\012	ushort        data_offset = 0; // assuming the 1st layer is not in split\012	\012	// virtual loop counters\012	ushort gp_num_x, gp_num_y, out_idx_z;\012	ushort gp_num_x_winbuf, gp_num_y_winbuf, out_idx_z_winbuf;\012	uchar  output_idx_dim1, output_idx_dim2;\012	ushort output_idx_dim3;\012	uchar  win_itm_x, win_itm_y;\012	ushort win_itm_z;\012	\012	uchar  gp_item_idx_x;\012\012	ushort feature_idx_dim1, feature_idx_dim2;\012	ushort feature_idx_dim3;\012\012	uint   item_loop_bound;\012	\012	uchar  flag; // ping-pong flag\012	\012	// Ping-pong buffer\012	__local lane_data    win_buffer[2][WIN_BUF_SIZE]; // working sequence 0->1->0->1 ...\012	// Weight buffer\012	__local channel_vec  weight_buffer[WEIGHT_BUF_SIZE];\012\012	// Initialize the winbuf with the data in the first iteration of the group looping (as gp_num_x_winbuf=0, gp_num_y_winbuf=0)\012	for(unsigned short win_itm_z=0; win_itm_z<weight_dim3/VEC_SIZE; win_itm_z++){\012		for(unsigned char  win_itm_y=0; win_itm_y<win_size_y; win_itm_y++){\012			for(unsigned char  win_itm_x=0; win_itm_x<win_size_x; win_itm_x++){\012	\012			feature_idx_dim1 = win_itm_x;\012			feature_idx_dim2 = win_itm_y;\012			feature_idx_dim3 = win_itm_z;\012	\012			if((feature_idx_dim1>=padding && feature_idx_dim1<data_dim1+padding) && (feature_idx_dim2>=padding && feature_idx_dim2<data_dim2+padding)){\012			\012				data_vec = bottom[data_offset*data_dim1xdim2 + feature_idx_dim3*data_dim1xdim2 + (feature_idx_dim2-padding)*data_dim1 + (feature_idx_dim1-padding)];\012			}\012			else{\012				#pragma unroll\012				for(unsigned char vv=0; vv<VEC_SIZE; vv++){\012					data_vec.data[vv] = CZERO;\012				}\012			}\012			\012			win_buffer[0][win_itm_z*win_size_y*win_size_x + win_itm_y*win_size_x + win_itm_x] = data_vec;\012\012			}\012		}\012	}\012\012	if(group_num_x==1)\012		gp_num_x_winbuf = 0; // there is only one group for FC mode when batch=1\012	else\012		gp_num_x_winbuf = 1; // loop start from the second group\012	gp_num_y_winbuf = 0;\012	out_idx_z_winbuf = 0;\012	\012	// reset global group virtual loop counters\012	gp_num_x = 0;\012	gp_num_y = 0;\012	out_idx_z = 0;\012	\012	Group:for(unsigned int out_idx_xyz=0; out_idx_xyz<(weight_dim4_div_lane*group_num_y*group_num_x); out_idx_xyz++){\012\012		// special case when split==1, the output feature maps depend on only half the input feature maps\012		if(split==0)\012			data_offset = 0;\012		else if(out_idx_z_winbuf<(weight_dim4_div_lane>>1)) // the lower half of the output feature maps depend on the lower half of the input\012			data_offset = 0;\012		else\012			data_offset = weight_dim3/VEC_SIZE;	// the upper half of the output feature maps depend on the upper half of the input\012	\012				flag = out_idx_xyz & 0x01; //ping-pong flag\012				\012				// reset output loop counters\012				output_idx_dim1 = 0;\012				output_idx_dim2 = 0;\012				output_idx_dim3 = 0;\012				// reset in-group item counters \012				gp_item_idx_x = 0;\012				\012				// reset input winbuffer loop counters\012				win_itm_x = 0;\012				win_itm_y = 0;\012				win_itm_z = 0;\012				\012				\012				if(gp_num_x==group_num_x-1)\012					item_loop_bound = win_size_x>=group_rem_size_x?(win_size_xyz/VEC_SIZE):(group_rem_size_xyz/VEC_SIZE);\012				else{\012					item_loop_bound = (weight_dim1x2x3*CONV_GP_SIZE_Y*CONV_GP_SIZE_X/VEC_SIZE);\012				}\012\012				#pragma ivdep array(win_buffer)\012				#pragma ivdep array(weight_buffer)\012				Item:for(unsigned int  win_itm_xyz=0; win_itm_xyz<item_loop_bound; win_itm_xyz++){\012\012							// Winbuffer loading operations\012							if(win_itm_z<weight_dim3/VEC_SIZE){\012\012								feature_idx_dim1 = win_itm_x+gp_num_x_winbuf*CONV_GP_SIZE_X*stride;\012								feature_idx_dim2 = win_itm_y+gp_num_y_winbuf*CONV_GP_SIZE_Y*stride;\012								feature_idx_dim3 = win_itm_z;\012\012								if((feature_idx_dim1>=padding && feature_idx_dim1<data_dim1+padding) && (feature_idx_dim2>=padding && feature_idx_dim2<data_dim2+padding)){\012								\012									data_vec = bottom[data_offset*data_dim1xdim2 + feature_idx_dim3*data_dim1xdim2 + (feature_idx_dim2-padding)*data_dim1 + (feature_idx_dim1-padding)];\012																	}\012								else{ // for padding (feature_idx<padding or data_dim+padding<=feature_idx<data_dim+2*padding)\012									#pragma unroll\012									for(unsigned char vv=0; vv<VEC_SIZE; vv++){\012										data_vec.data[vv] = CZERO;\012									}\012								}\012								\012								win_buffer[(~flag)&0x01][win_itm_z*win_size_y*win_size_x + win_itm_y*win_size_x + win_itm_x] = data_vec;\012\012								// used as loop counters\012								if((win_itm_z==weight_dim3/VEC_SIZE-1) && (win_itm_y==win_size_y-1) && (win_itm_x==win_size_x-1))\012									win_itm_z = 0;\012								else if((win_itm_y==win_size_y-1) && (win_itm_x==win_size_x-1))\012									win_itm_z++;\012\012								if((win_itm_y==win_size_y-1) && (win_itm_x==win_size_x-1))\012									win_itm_y = 0;\012								else if(win_itm_x==win_size_x-1)\012									win_itm_y++;\012								\012								if(win_itm_x==win_size_x-1)\012									win_itm_x = 0;\012								else\012									win_itm_x++;\012									\012							}\012							\012							// Load weight into weight buffer\012							if(gp_item_idx_x==0){\012								\012								weight_ch_vec = weights[out_idx_z*weight_dim1x2x3/VEC_SIZE + output_idx_dim3*weight_dim1x2 + output_idx_dim2*weight_dim1 + output_idx_dim1];\012								weight_buffer[output_idx_dim3*weight_dim2*weight_dim1 + output_idx_dim2*weight_dim1 + output_idx_dim1] = weight_ch_vec;\012\012							}\012							\012							// In this version, grouping is only performed in row (x) direction\012							if(gp_num_x*CONV_GP_SIZE_X+gp_item_idx_x<conv_x){\012                    \012								if(output_idx_dim1==0 && output_idx_dim2==0 && output_idx_dim3==0){\012									bias_ch_in = bias[out_idx_z];\012									write_channel_altera(bias_ch, bias_ch_in);\012									//#ifdef DEBUG_MEMRD\012									//printf(\"work-item x=%d, y=%d, z=%d, channel =0, write bias=%f\\n\", output_idx_dim1, output_idx_dim2, output_idx_dim3, bias_ch_in.lane[0]);\012									//#endif\012								}\012\012								// data\012								data_vec = win_buffer[flag][output_idx_dim3*win_size_y*win_size_x + output_idx_dim2*win_size_x + (output_idx_dim1+gp_item_idx_x*stride)];\012								#pragma unroll\012								for(unsigned char ll=0; ll<LANE_NUM; ll++){\012									data_ch_vec.lane[ll] = data_vec;\012								}\012								write_channel_altera(data_ch, data_ch_vec);	\012								\012								\012								// weight and bias fetcher\012								weight_ch_vec = weight_buffer[output_idx_dim3*weight_dim2*weight_dim1 + output_idx_dim2*weight_dim1 + output_idx_dim1];\012								write_channel_altera(weight_ch, weight_ch_vec);	\012								\012								#ifdef DEBUG_MEMRD\012								//if(gp_num_x==group_num_x-1 && gp_num_y==0 && out_idx_z==0){\012									printf(\"work-item x=%d, y=%d, z=%d, offset=%d, write data in channel 0=%f\\n\", output_idx_dim1, output_idx_dim2, output_idx_dim3, data_offset, (float)data_ch_vec.lane[0].data[0]);\012									//printf(\"work-item x=%d, y=%d, z=%d, write weight in channel 0=%f\\n\", output_idx_dim1, output_idx_dim2, output_idx_dim3, (float)weight_ch_vec.lane[0].data[0]);\012								//}\012								#endif\012	\012								// used as output loop counters\012								if((output_idx_dim3==weight_dim3/VEC_SIZE-1) && (output_idx_dim2==weight_dim2-1) && (output_idx_dim1==weight_dim1-1)){\012									output_idx_dim3 = 0;\012									gp_item_idx_x++;\012								}\012								else if((output_idx_dim2==weight_dim2-1)&& (output_idx_dim1==weight_dim1-1))\012									output_idx_dim3++;\012								\012								if((output_idx_dim2==weight_dim2-1) && (output_idx_dim1==weight_dim1-1))\012									output_idx_dim2 = 0;\012								else if(output_idx_dim1==weight_dim1-1)\012									output_idx_dim2++;\012	                \012								if(output_idx_dim1==weight_dim1-1)\012									output_idx_dim1 = 0;\012								else\012									output_idx_dim1++;\012\012								}\012\012				}\012\012		// used as virtual group loop counters for winbuf loading operations\012		if((out_idx_z_winbuf==weight_dim4_div_lane-1) && (gp_num_y_winbuf==group_num_y-1) && (gp_num_x_winbuf==group_num_x-1))\012			out_idx_z_winbuf = 0;\012		else if((gp_num_y_winbuf==group_num_y-1) && (gp_num_x_winbuf==group_num_x-1))\012			out_idx_z_winbuf++;	\012\012		if((gp_num_y_winbuf==group_num_y-1) && (gp_num_x_winbuf==group_num_x-1))\012			gp_num_y_winbuf = 0;\012		else if(gp_num_x_winbuf==group_num_x-1)\012			gp_num_y_winbuf++;	\012\012		if(gp_num_x_winbuf==group_num_x-1)\012			gp_num_x_winbuf = 0;\012		else\012			gp_num_x_winbuf++;\012		\012		// used as virtual group loop counters\012		if((out_idx_z==weight_dim4_div_lane-1) && (gp_num_y==group_num_y-1) && (gp_num_x==group_num_x-1))\012			out_idx_z = 0;\012		else if((gp_num_y==group_num_y-1) && (gp_num_x==group_num_x-1))\012			out_idx_z++;	\012        \012		if((gp_num_y==group_num_y-1) && (gp_num_x==group_num_x-1))\012			gp_num_y = 0;\012		else if(gp_num_x==group_num_x-1)\012			gp_num_y++;\012        \012		if(gp_num_x==group_num_x-1)\012			gp_num_x = 0;\012		else\012			gp_num_x++;\012\012	}\012	\012	//printf(\"Kernel 0 lanched !!!\\n\");\012}\012\012\012__kernel\012__attribute__((task))\012__attribute__((max_global_work_dim(0)))\012void coreConv(\012			// Params Ports\012			uint  output_num,\012			uint  conv_loop_cnt,\012			uint  contol, //[0]-> relu  [1]->bypass pooling\012			char  frac_w,\012			char  frac_din,\012			char  frac_dout\012			)\012{\012	channel_vec mac_data;\012 	channel_vec mac_weight;\012	channel_scal bias_ch_out;\012	channel_scal conv_ch_in;\012	DPTYPE  bias[LANE_NUM];\012	MACTYPE conv_out[LANE_NUM];\012	MACTYPE lane_accum[LANE_NUM];\012	MACTYPE accum_piped[LANE_NUM][PIPE_DEPTH];\012	MACTYPE conv_sign_exten[LANE_NUM];\012	MACTYPE conv_with_rnd_bit[LANE_NUM];\012	MACTYPE conv_sum_bias[LANE_NUM];\012	DPTYPE  conv_final[LANE_NUM];\012\012	// each iteration generates one output\012	for(unsigned int k=0; k<output_num; k++){\012		\012		bias_ch_out = read_channel_altera(bias_ch);\012\012		#pragma unroll\012		for(unsigned char ll=0; ll<LANE_NUM; ll++){\012\012			conv_out[ll] = CZERO;\012			bias[ll] = bias_ch_out.lane[ll];\012\012			#pragma unroll\012			for(unsigned int p=0; p<PIPE_DEPTH; p++){\012				accum_piped[ll][p] = MASK_ACCUM & CZERO;\012			}\012		}\012\012		for(int j=0; j<conv_loop_cnt; j++){\012\012			mac_data = read_channel_altera(data_ch);\012			mac_weight = read_channel_altera(weight_ch);\012\012			#pragma unroll\012			for(unsigned char ll=0; ll<LANE_NUM; ll++){\012				\012				lane_accum[ll] = (MASK_ACCUM & accum_piped[ll][PIPE_DEPTH-1]) + (MASK_MULT & mac(mac_data.lane[ll], mac_weight.lane[ll]));\012			\012				#pragma unroll\012				for(unsigned int p=PIPE_DEPTH-1; p>0; p-- ){\012					accum_piped[ll][p] = MASK_ACCUM & accum_piped[ll][p-1];\012				}\012				\012				accum_piped[ll][0] = MASK_ACCUM & lane_accum[ll];\012\012				#ifdef DEBUG_CONV\012				//if(ll==0 && k==0){\012				//	printf(\"dot_cnt=%d data=%f weight=%f (loop=%d, lane= %d, vec=0)\\n\", k, (float)mac_data.lane[ll].data[0], (float)mac_weight.lane[ll].data[0], j, ll);\012				//}\012				#endif\012			}\012		}// end of conv loop\012\012		#pragma unroll\012		for(unsigned char ll=0; ll<LANE_NUM; ll++){\012\012			#pragma unroll\012			for(unsigned i=0; i<PIPE_DEPTH; i++){\012				conv_out[ll] += accum_piped[ll][i];\012			}\012			\012			if(conv_out[ll]>=0)\012				conv_sign_exten[ll] = 0x00;\012			else\012				conv_sign_exten[ll] = ~(0xFFFFFFFF>>(frac_w+frac_din-frac_dout-1));\012			\012			conv_with_rnd_bit[ll] = (conv_sign_exten[ll] | (conv_out[ll]>>(frac_w+frac_din-frac_dout-1))) + 0x01;\012\012			if(conv_with_rnd_bit[ll]>=256)\012				conv_sum_bias[ll] = MASK9B & 0xFF;\012			else if(conv_with_rnd_bit[ll]<-256)\012				conv_sum_bias[ll] = MASK9B & 0x100;\012			else\012				conv_sum_bias[ll] = (MASK9B & conv_with_rnd_bit[ll])+(bias[ll]>>(frac_w-frac_dout-1))+0x01;\012\012			conv_final[ll] = MASK8B & (conv_sum_bias[ll]>>0x01);\012			\012			// Relu operation\012			if((contol&0x01)==0x01){\012				if((conv_final[ll]&MASKSIGN)==MASKSIGN)\012					conv_ch_in.lane[ll] = 0;\012				else\012					conv_ch_in.lane[ll] = conv_final[ll];\012			}\012			else\012				conv_ch_in.lane[ll] = conv_final[ll];\012			\012			#ifdef DEBUG_CONV\012			if(ll==0 && k==0)\012				printf(\"dot_cnt=%d sum=%f rnd=%f sum_bias=%f final=%f (bias=%f)\\n\\n\", k, (float)conv_out[ll], (float)conv_with_rnd_bit[ll], (float)conv_sum_bias[ll], (float)conv_final[ll], (float)bias[ll]);\012			#endif\012\012		}\012\012		// write convoluation results\012		if((contol&0x02)==0x02)\012			//by-pass pooling\012			write_channel_altera(bypass_ch, conv_ch_in);\012		else // to pooling kernel\012			write_channel_altera(conv_ch, conv_ch_in);\012			//printf(\"Write channel item-%d is written in channel %d...\\n\", k, ll);\012\012	}// end of output loop\012 \012}\012\012\012__kernel\012__attribute__((task))\012void maxPool(\012			// Params Ports\012			uint  input_num,\012			uchar line_size,  // line_size should be no larger than POOL_LBUF_DEPTH\012			uchar pool_size,  // by now, only pooling size no larger than 3\012			uchar pool_stride\012			\012			)\012{\012	channel_scal conv_ch_out;\012	channel_scal pool_final;\012\012	DPTYPE line_buf_0[LANE_NUM][POOL_LBUF_DEPTH];\012	DPTYPE line_buf_1[LANE_NUM][POOL_LBUF_DEPTH];\012	uchar  line_buf_ptr;\012	uchar  col_pool_cnt;\012	uchar  row_pool_cnt;\012	uchar  row_cnt;\012	DPTYPE row_pool_reg[LANE_NUM];\012	DPTYPE col_pool_reg[LANE_NUM];\012	DPTYPE pool_reg[LANE_NUM][POOL_MAX_SIZE];\012	\012	// Each iteration consumes one output from convolution kernel\012	// and then Pooling is performed in column and row directions\012	line_buf_ptr = 0;\012	row_pool_cnt = 0;\012	col_pool_cnt = 0;\012	for(unsigned int k=0; k<input_num; k++){\012\012		conv_ch_out = read_channel_altera(conv_ch);\012	\012		// Two line buffer to form the 3x3 pooling window\012		#pragma unroll\012		for(unsigned char ll=0; ll<LANE_NUM; ll++){\012\012			if(pool_size==3)\012				row_pool_reg[ll] = pool_max(line_buf_1[ll][line_buf_ptr], line_buf_0[ll][line_buf_ptr]);\012			else // pool_size==2\012				row_pool_reg[ll] = line_buf_0[ll][line_buf_ptr];\012			\012			pool_reg[ll][0] = pool_max(row_pool_reg[ll], conv_ch_out.lane[ll]);\012			\012			if(pool_size==3)\012				col_pool_reg[ll] = pool_max(pool_reg[ll][1], pool_reg[ll][2]);\012			else //pool_size==2\012				col_pool_reg[ll] = pool_reg[ll][1];\012\012			pool_final.lane[ll] = pool_max(col_pool_reg[ll], pool_reg[ll][0]);\012\012			line_buf_1[ll][line_buf_ptr] = line_buf_0[ll][line_buf_ptr];\012			line_buf_0[ll][line_buf_ptr] = conv_ch_out.lane[ll];\012\012			#pragma unroll\012			for(unsigned char p=POOL_MAX_SIZE-1; p>0; p--){\012				pool_reg[ll][p]=pool_reg[ll][p-1];\012			}\012		}\012		\012		#ifdef DEBUG_POOL\012		printf(\"Maxpool input_num=%d, line_buf_ptr=%d, row_pool_cnt=%d, col_pool_cnt=%d\\n\", k, line_buf_ptr, row_pool_cnt, col_pool_cnt);\012		printf(\"        row_cnt=%d\\n\", row_cnt);\012		#endif\012		\012		// Generates pooling pipeline register wr/rd pointer\012		if(row_pool_cnt==(pool_size-1)){\012\012			if(col_pool_cnt==(pool_size-1)){\012				write_channel_altera(pool_ch, pool_final);\012				#ifdef DEBUG_POOL\012				printf(\"        reg0=%f, reg1=%f, reg2=%f, max=%f\\n\", (float)pool_reg[0][0], (float)pool_reg[0][1], (float)pool_reg[0][2], (float)pool_final.lane[0]);\012				#endif\012\012				col_pool_cnt = (pool_size-pool_stride);\012			}\012			else\012				col_pool_cnt = col_pool_cnt + 1;\012		}\012		else\012			col_pool_cnt = 0;\012\012		// Generates line buffer wr/rd pointer\012		if(line_buf_ptr==(line_size-1)){\012			line_buf_ptr = 0;\012\012			// Row counters for recognize frames\012			if(row_cnt == (line_size-1)) // assuming row_num = line_size, i.e. rectangular frame\012				row_cnt = 0;\012			else\012				row_cnt = row_cnt + 1;\012\012			// Pooling window slide counter for rows\012			if(row_cnt == 0)\012				row_pool_cnt = 0;\012			else if(row_pool_cnt==(pool_size-1))\012				row_pool_cnt = (pool_size-pool_stride);\012			else\012				row_pool_cnt = row_pool_cnt + 1;\012		}\012		else{\012			line_buf_ptr = line_buf_ptr + 1;\012		}\012\012	}\012}\012\012\012// Store Data to Global Memory\012__kernel\012__attribute__((reqd_work_group_size(1,1,LANE_NUM)))\012void memWrite(\012				// Params Ports\012				uchar  out_dim1,\012				uchar  out_dim2,\012				ushort out_dim3,\012				ushort out_dim1xbatch, // out_dim1 x sqrt(batch_size)\012				uint   out_dim1x2xbatch, // out_dim1 x out_dim2 x batch_size\012				uchar  batch_indx_dim1,\012				uchar  batch_indx_dim2,\012				uchar  bypass,\012				uchar  padd_offset,\012				// Data Ports\012                __global DPTYPE *restrict top\012				)\012{\012	uchar  global_x = get_global_id(0); // max value 256\012	uchar  global_y = get_global_id(1); // max value 256\012	ushort global_z = get_global_id(2); // max value 4096\012	uchar  local_x 	= get_local_id(0); // max value 256\012	uchar  local_y 	= get_local_id(1); // max value 256\012	uchar  local_z 	= get_local_id(2); // max value 256\012\012	uchar  index_z_item; // max value 256\012	ushort index_z_group;// max value 4096\012\012	channel_scal   output;\012	__local DPTYPE buffer[LANE_NUM];\012\012	if(local_z==0){\012		if((bypass&0x01)==0x01)\012			output = read_channel_altera(bypass_ch);\012		else\012			output = read_channel_altera(pool_ch);\012\012		#pragma unroll\012		for(uchar ll=0; ll<LANE_NUM; ll++){\012			buffer[ll]=output.lane[ll];\012		}\012	}\012\012	barrier(CLK_LOCAL_MEM_FENCE);\012\012\012	// fetch data from local buffer and write back to DDR\012	index_z_group = (global_z-padd_offset)/VEC_SIZE;\012	index_z_item  = (global_z-padd_offset)%VEC_SIZE;\012\012	if((global_z-padd_offset)<out_dim3 && (global_z>=padd_offset)){\012\012		top[index_z_group*out_dim1x2xbatch*VEC_SIZE + (global_y+batch_indx_dim2*out_dim2)*out_dim1xbatch*VEC_SIZE + (global_x+batch_indx_dim1*out_dim1)*VEC_SIZE + index_z_item] = buffer[local_z];\012\012		#ifdef DEBUG_MEMWR\012		//if((global_z-padd_offset) == 0){\012			//for(unsigned char ll=0; ll<LANE_NUM; ll++){\012			printf(\"MemWr results= %f (x=%d, y=%d, z=%d, ll=%d)\\n\", (float)output.lane[0], global_x, global_y, global_z, 0);\012			//}\012		//	}\012		#endif\012\012	}\012	\012	barrier(CLK_LOCAL_MEM_FENCE);\012\012}\012\012\012__kernel\012__attribute__((max_work_group_size(LRN_MAX_LOCAL_SIZE)))\012void lrn(\012			// Params Ports\012			uchar data_dim1,\012			uchar data_dim2,\012			char  frac_dout,\012			// Data Ports\012			__global lane_data *restrict bottom,\012			__global lane_data *restrict top\012		)\012{\012	uchar  global_x = get_global_id(0); // max value 256\012	uchar  global_y = get_global_id(1); // max value 256\012	ushort global_z = get_global_id(2); // max value 4096\012\012	#ifdef DEBUG_LRN\012	int local_x = get_local_id(0);\012	int local_y = get_local_id(1);\012	int local_z = get_local_id(2);\012	int block_x = get_group_id(0);\012	int block_y = get_group_id(1);\012	int block_z = get_group_id(2);\012	#endif\012	\012	__local DPTYPE z_buffer[VEC_SIZE*LRN_MAX_LOCAL_SIZE+LRN_WIN_SIZE]; // allocate two more points for padding\012	__local DPTYPE lrn_buffer[VEC_SIZE*LRN_MAX_LOCAL_SIZE];\012	channel_scal data_in;\012	channel_scal data_pad_left;\012	channel_scal data_pad_right;\012	channel_scal data_out;\012	lane_data    data_in_partial;\012	lane_data    data_left_partial;\012	lane_data    data_right_partial;\012	lane_data    data_out_partial;\012	int          *convert_ptr;\012	int          expo;\012	uint         manti;\012	uint         addr_1, addr_2, addr;\012	float        lrn_reg1, lrn_reg2, lrn_tmp, lrn_out;\012	short        lrn_cnvt, lrn_cnvt2;\012	\012	// Load the all data in one line along dim3 into local line buffer\012	#pragma unroll\012	for(unsigned char ll=0; ll<VEC_SIZE; ll++){\012		z_buffer[global_z*VEC_SIZE+ll+LRN_WIN_SIZE/2] = bottom[global_z*data_dim2*data_dim1 + global_y*data_dim1+ global_x].data[ll];\012	}\012	\012	//Padding left\012	if(global_z==0){\012		#pragma unroll\012		for(unsigned char ll=0; ll<LRN_WIN_SIZE/2; ll++){\012			z_buffer[ll] = CZERO;\012		}\012	}\012\012	// Padding right\012	if(global_z==(get_global_size(2)-1)){\012		#pragma unroll\012		for(unsigned char ll=0; ll<LRN_WIN_SIZE/2; ll++){\012			z_buffer[VEC_SIZE*get_local_size(2)+ll+LRN_WIN_SIZE/2] = CZERO;\012		}\012	}\012\012	#ifdef DEBUG_LRN\012	if(global_z==0&&global_x==0&&global_y==0)\012	printf(\"Kernel LRN: work-item x=%d, y=%d, z=%d(z_local=%d)\\n\", global_x, global_y, global_z, local_z);\012	#endif\012	barrier(CLK_LOCAL_MEM_FENCE);\012\012	// Piecewise interpolation pipeline for lrn operation (i.e., y=pwlf(x'))\012	for(unsigned char ll=0; ll<VEC_SIZE; ll++){\012		// First Step: Coefficients table looking-up\012		// Calculate x'=sum(x(k)^2) for the pwlf function, x(k)s are from adjacent featuremaps\012		lrn_reg2 = CZERO;\012		#pragma unroll\012		for(char k=-LRN_WIN_SIZE/2; k<=LRN_WIN_SIZE/2; k++){\012			lrn_cnvt = z_buffer[global_z*VEC_SIZE+ll+k+LRN_WIN_SIZE/2]<<(-frac_dout);\012			lrn_reg1 = convert_float(lrn_cnvt);\012			lrn_reg2 += lrn_reg1 * lrn_reg1;\012			#ifdef DEBUG_LRN\012			if(global_z==0&&global_x==0&&global_y==0)\012			printf(\"x=%f(k=%d), \", lrn_reg1, k);\012			#endif\012		}\012		convert_ptr = (int*) (&lrn_reg2);\012		expo = (EXP_MASK & (*convert_ptr >> MAN_BITS)) - 127;\012		manti = ((*convert_ptr) & MAN_MASK);\012		\012		addr_1 = ((expo-EXP_STEP_MIN)>>EXP_STEP_LOG)<<MAN_INDEX_BITS;\012		addr_2 = (manti>>(MAN_BITS-MAN_INDEX_BITS) & MAN_INDEX_MASK)+1;\012		if(expo<EXP_STEP_MIN)\012			addr = 0;\012		else\012			addr = addr_1+addr_2;\012\012		lrn_tmp = ((lrn_reg2-x_sample[addr])*h_inv[addr])*coef1[addr] + coef0[addr];	\012		\012		lrn_cnvt2 = z_buffer[global_z*VEC_SIZE+ll+LRN_WIN_SIZE/2]<<(-frac_dout);\012		lrn_out = lrn_tmp*convert_float(lrn_cnvt2);\012\012		// Convert float to DPTYPE fixed-point\012		// Note: current version only support frac_din=0 for next layer\012		lrn_buffer[global_z*VEC_SIZE+ll] = convert_char_rte(lrn_out);\012\012		#ifdef DEBUG_LRN\012		if(global_z==0&&global_x==0&&global_y==0)\012		printf(\"\\nKernel LRN (ll=%d): pwlf_x=%f, expo=%d, addr=%d, pwlf_y=%f, lrn=%f\\n\", ll, lrn_reg2, expo, addr, lrn_tmp, lrn_out);\012		#endif\012		barrier(CLK_LOCAL_MEM_FENCE);\012	}\012\012	// Store the results back to global mem\012	#pragma unroll\012	for(unsigned char vv=0; vv<VEC_SIZE; vv++){\012		data_out_partial.data[vv]=lrn_buffer[global_z*VEC_SIZE+vv];\012	}\012	top[global_z*data_dim2*data_dim1 + global_y*data_dim1 + global_x] = data_out_partial;\012	\012	#ifdef DEBUG_LRN_OUT\012	if(global_z==0&&global_x==0&&global_y==0)\012	printf(\"\\nKernel LRN OUT: x=%d, y=%d, z=%d, result=%f\\n\", global_x, global_y, global_z, (float)data_out_partial.data[0]);\012	#endif\012\012}\012\012"}, {"index":1, "path":"/home/dtyu/Git/pipecnn/project/device/hw_param.cl", "name":"hw_param.cl", "content":"/*\012 * ------------------------------------------------------\012 *\012 *   PipeCNN: An OpenCL-Based FPGA Accelerator for CNNs\012 *\012 * ------------------------------------------------------\012 * Filename:\012 *   - hw_param.cl\012 *\012 * Author(s):\012 *   - Dong Wang, wangdong@m.bjtu.edu.cn\012 *\012 * History:\012 *   - v1.3 Win-Buffer-Based Implementation\012 * ------------------------------------\012 *\012 *   Copyright (C) 2016, Institute of Information Science,\012 *   Beijing Jiaotong University. All rights reserved.\012 *\012 *   Licensed under the Apache License, Version 2.0 (the \"License\");\012 *   you may not use this file except in compliance with the License.\012 *   You may obtain a copy of the License at\012 *\012 *     http://www.apache.org/licenses/LICENSE-2.0\012 *\012 *   Unless required by applicable law or agreed to in writing, software\012 *   distributed under the License is distributed on an \"AS IS\" BASIS,\012 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\012 *   See the License for the specific language governing permissions and\012 *   limitations under the License.\012 *\012 */\012\012#ifndef _HW_PARAM_H\012#define _HW_PARAM_H\012\012\012// Macro architecture parameters\012// General\012#define VEC_SIZE            4              // larger than 4, i.e., 4, 8, 16, ...\012#define LANE_NUM            16             // larger than 1, for alexnet: 2, 3, 4, 8, 12, 15, 16, 22, 28, 32, 34, 48, 50, 51, 52, 64, ...\012#define CHN_DEPTH           0\012//MemRD Kernel\012#define CONV_GP_SIZE_X      7\012#define CONV_GP_SIZE_Y      1              // In this version, CONV_GP_SIZE_Y must be 1\012#define WIN_BUF_SIZE        9216/VEC_SIZE  // for AlexNet  batch=1\012#define WEIGHT_BUF_SIZE     9216/VEC_SIZE  // for AlexNet  batch=1\012//#define WIN_BUF_SIZE        25088/VEC_SIZE // for VGG-16  batch=1\012//#define WEIGHT_BUF_SIZE     25088/VEC_SIZE // for VGG-16  batch=1\012//#define WIN_BUF_SIZE        CONV_GP_SIZE_X*9216/VEC_SIZE  // for AlexNet  batch>=4\012//#define WEIGHT_BUF_SIZE     9216/VEC_SIZE                 // for AlexNet  batch>=4\012// Conv Kernel\012#define PIPE_DEPTH          6\012// Pooling Kernel\012#define POOL_LBUF_DEPTH     224            // Must be large enough to hold one line (dim1/dim2)\012#define POOL_MAX_SIZE       3\012// Lrn Kernel\012#define LRN_WIN_SIZE        5\012#define LRN_MAX_LOCAL_SIZE  (256/VEC_SIZE) // For alexnet the max dim3 size is 256\012#define MAN_BITS            23             // Floating point format setting\012#define EXP_MASK            0xFF           // Floating point format setting\012#define MAN_MASK            0x7FFFFF       // Floating point format setting\012#define EXP_STEP_MIN        13             // PWLF table setting\012#define EXP_STEP_LOG        0              // PWLF table setting\012#define MAN_INDEX_BITS      2              // PWLF table setting\012#define MAN_INDEX_MASK      0x03           // PWLF table setting\012// Parameters for fixed-point design\012#define CZERO       0x00     // constant zero\012#define MASK8B      0xFF     // used for final rounding\012#define MASK9B      0x1FE    // used for final rounding\012#define MASKSIGN    0x80     // used for final rounding\012// #define MASK_ACCUM  0x1FFFFFFF // not used for reducing mac pipeline logic cost (when PIPE_DEPTH=6, MASK_ACCUM has 16+13=29 bits)\012//#define MASK_MULT   0x1FFFFF   // not used for reducing mac pipeline logic cost (four parallel mac, max VEC_SIZE is 32, MASK_MULT has 16+5=21 bits)\012#define MASK_ACCUM  0xFFFFFFFF // use this value\012#define MASK_MULT   0xFFFFFFFF // use this value\012\012\012#ifdef USE_ROM\012//Coefficients lookup table for lrn computation\012constant float coef0[46] = {9.98312401e-01,8.92383765e-01,8.69534866e-01,8.48001507e-01,8.27672857e-01,8.08269896e-01,7.72814246e-01,7.40785193e-01,7.11686616e-01,6.84743320e-01,6.38046300e-01,5.98139529e-01,5.63585746e-01,5.32842946e-01,4.82570938e-01,4.42066574e-01,4.08721176e-01,3.80120836e-01,3.35733988e-01,3.01782553e-01,2.74896454e-01,2.52503409e-01,2.19044754e-01,1.94367577e-01,1.75328514e-01,1.59766323e-01,1.37073713e-01,1.20695464e-01,1.08253750e-01,9.81965345e-02,8.37272488e-02,7.34111523e-02,6.56398695e-02,5.93964327e-02,5.04776032e-02,4.41593533e-02,3.94211944e-02,3.56262849e-02,3.02252062e-02,2.64117530e-02,2.35583854e-02,2.12767794e-02,1.80355644e-02,1.57509127e-02,1.40434261e-02};\012constant float coef1[46] = {-1.07542919e-01,-2.28535953e-02,-2.15331066e-02,-2.03286855e-02,-1.92268508e-02,-3.55023570e-02,-3.20657642e-02,-2.91245494e-02,-2.65861837e-02,-4.68257134e-02,-3.99817597e-02,-3.45887189e-02,-3.02571264e-02,-5.05149626e-02,-4.06040782e-02,-3.34413514e-02,-2.80826706e-02,-4.46757687e-02,-3.40991637e-02,-2.69894342e-02,-2.19616650e-02,-3.37238519e-02,-2.48195600e-02,-1.91265576e-02,-1.52482883e-02,-2.29016249e-02,-1.64847560e-02,-1.25042597e-02,-9.85141038e-03,-1.46114169e-02,-1.03881575e-02,-7.81187564e-03,-6.11526810e-03,-9.00946183e-03,-6.36361270e-03,-4.76376961e-03,-3.71675305e-03,-5.45684726e-03,-3.84135330e-03,-2.86894660e-03,-2.23458481e-03,-3.27498492e-03,-2.30149338e-03,-1.71686994e-03,-1.33609904e-03};\012constant float h_inv[46] = {1.22085215e-04,4.88281250e-04,4.88281250e-04,4.88281250e-04,4.88281250e-04,2.44140625e-04,2.44140625e-04,2.44140625e-04,2.44140625e-04,1.22070313e-04,1.22070313e-04,1.22070313e-04,1.22070313e-04,6.10351563e-05,6.10351563e-05,6.10351563e-05,6.10351563e-05,3.05175781e-05,3.05175781e-05,3.05175781e-05,3.05175781e-05,1.52587891e-05,1.52587891e-05,1.52587891e-05,1.52587891e-05,7.62939453e-06,7.62939453e-06,7.62939453e-06,7.62939453e-06,3.81469727e-06,3.81469727e-06,3.81469727e-06,3.81469727e-06,1.90734863e-06,1.90734863e-06,1.90734863e-06,1.90734863e-06,9.53674316e-07,9.53674316e-07,9.53674316e-07,9.53674316e-07,4.76837158e-07,4.76837158e-07,4.76837158e-07,4.76837158e-07};\012constant float x_sample[46] = {1.00000000e+00,8.19200000e+03,1.02400000e+04,1.22880000e+04,1.43360000e+04,1.63840000e+04,2.04800000e+04,2.45760000e+04,2.86720000e+04,3.27680000e+04,4.09600000e+04,4.91520000e+04,5.73440000e+04,6.55360000e+04,8.19200000e+04,9.83040000e+04,1.14688000e+05,1.31072000e+05,1.63840000e+05,1.96608000e+05,2.29376000e+05,2.62144000e+05,3.27680000e+05,3.93216000e+05,4.58752000e+05,5.24288000e+05,6.55360000e+05,7.86432000e+05,9.17504000e+05,1.04857600e+06,1.31072000e+06,1.57286400e+06,1.83500800e+06,2.09715200e+06,2.62144000e+06,3.14572800e+06,3.67001600e+06,4.19430400e+06,5.24288000e+06,6.29145600e+06,7.34003200e+06,8.38860800e+06,1.04857600e+07,1.25829120e+07,1.46800640e+07,1.67772160e+07};\012#endif\012\012#endif"}];